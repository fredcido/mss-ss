<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompulsoryAccessionAbroadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('person')
            ->add('company')
            ->add('country')
            ->add('securityCode')
            ->add('hasSubsidie')
            ->add('subsidieType')
            ->add('socialSecurityEntity')
            ->add('foreignWorker')
            ->add('countryWork')
            ->add('subsidieCost')
            ->add('socialSecurityEntityWork')
            ->add('version')
            ->add('monthsWorkingTL');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\CompulsoryAccessionAbroad',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_compulsoryaccessionabroad';
    }
}
