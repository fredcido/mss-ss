<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SchoolYear;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class SchoolYearController extends BaseController
{
    protected $repository = 'AppBundle:SchoolYear';

    protected $form = 'AppBundle\Form\SchoolYearType';

    protected $routeGet = 'app.school_year.get';

    protected $entity = SchoolYear::class;
}
