<?php

namespace AppBundle\Service;

use AppBundle\Service\Settings;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Dashboard
{
    use ContainerAwareTrait;

    protected $settings;

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function setSettings(Settings $settings)
    {
        $this->settings = $settings;
        return $this;
    }

    public function getCountEmployers(array $filters = array())
    {
        $employerRepository = $this->em->getRepository('AppBundle:Employer');
        $employers = $employerRepository->findByFilters($filters);

        return count($employers);
    }

    public function getCountEmployees(array $filters = array())
    {
        $filters['subscription_active'] = 1;

        $personRepository = $this->em->getRepository('AppBundle:Person');
        $people = $personRepository->findByFilters($filters);

        return count($people);
    }

    public function getCountStatements(array $filters = array())
    {
        $statementRepository = $this->em->getRepository('AppBundle:RemunerationStatement');
        $statements = $statementRepository->findByFilters($filters);

        return count($statements);
    }

    public function getCountOptional(array $filters = array())
    {
        $filters['optional_subscription'] = 1;

        $personRepository = $this->em->getRepository('AppBundle:Person');
        $people = $personRepository->findByFilters($filters);

        return count($people);
    }

    public function getChartDistrictEmployer(array $filters = array())
    {
        $employerRepository = $this->em->getRepository('AppBundle:Employer');
        $employers = $employerRepository->findByFilters($filters);

        $districts = [];
        foreach ($employers as $employer) {
            $addresses = $employer->getAddresses();
            foreach ($addresses as $address) {
                if (!$address->getSuku()) {
                    continue;
                }

                $municipio = $address->getSuku()->getPostoAdministrativo()->getMunicipio();
                if (!isset($districts[$municipio->getName()])) {
                    $districts[$municipio->getName()] = 0;
                }

                $districts[$municipio->getName()]++;
            }
        }

        ksort($districts);

        return $districts;
    }

    public function getChartDistrictEmployee(array $filters = array())
    {
        $filters['subscription_active'] = 1;

        $personRepository = $this->em->getRepository('AppBundle:Person');
        $people = $personRepository->findByFilters($filters);

        $districts = [];
        foreach ($people as $person) {
            $address = $person->getAddress();
            if (!$address->getSuku()) {
                continue;
            }

            $municipio = $address->getSuku()->getPostoAdministrativo()->getMunicipio();
            if (!isset($districts[$municipio->getName()])) {
                $districts[$municipio->getName()] = 0;
            }

            $districts[$municipio->getName()]++;
        }

        ksort($districts);

        return $districts;
    }

    public function getChartRemunerationMonth(array $filters = array())
    {
        $statementRepository = $this->em->getRepository('AppBundle:RemunerationStatement');
        $statements = $statementRepository->findByFilters($filters);

        $data = [];
        foreach ($statements as $statement) {
            $label = sprintf('%d/%02s', $statement->getYear(), $statement->getMonth());

            if (!isset($data[$label])) {
                $data[$label] = 0;
            }

            $data[$label]++;
        }

        ksort($data);

        return $data;
    }

    public function getChartOptionalGender(array $filters = array())
    {
        $filters['optional_subscription'] = 1;

        $personRepository = $this->em->getRepository('AppBundle:Person');
        $people = $personRepository->findByFilters($filters);

        $data = [];
        $gender = ['male' => 0, 'female' => 0];
        $unknown = 'Unkown';

        foreach ($people as $person) {
            $address = $person->getAddress();

            $district = $unknown;
            if ($address->getSuku()) {
                $municipio = $address->getSuku()->getPostoAdministrativo()->getMunicipio();
                $district = $municipio->getName();
            }

            if (!isset($data[$district])) {
                $data[$district] = $gender;
            }

            if ('M' == $person->getGender()) {
                $data[$district]['male']++;
            } else {
                $data[$district]['female']++;
            }
        }

        ksort($data);

        return $data;
    }
}
