app.directive('status', function($translate) {
    return {
        restrict: 'E',
        scope: {
            value: "="
        },
        template: "<b class='text-capitalize'><i class='fa fa-circle' style='color:{{color}}'></i>&nbsp;{{description}}</b>",
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                setStatus(e.value);
            });

            var setStatus = function(value) {
                if (value) {
                    scope.color = '#3FB618';
                    scope.description = $translate.instant('active');
                } else {
                    scope.color = "#FF0039";
                    scope.description = $translate.instant('inactive');
                }
            };
        }
    };
});
