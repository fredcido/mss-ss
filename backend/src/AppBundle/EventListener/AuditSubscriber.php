<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\EntityAuditTrait;
use AppBundle\Entity\Staff;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AuditSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use GetUserTrait;
    use InjectUserTrait;
    use ComputeChangeTrait;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em = $args->getEntityManager();

        $this->insertions($uow, $em);
        $this->updates($uow, $em);
    }

    /**
     * @param UnitOfWork $args
     */
    private function insertions(UnitOfWork $uow, EntityManager $em)
    {
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!in_array(EntityAuditTrait::class, class_uses($entity))) {
                continue;
            }

            $this->injectCreate($entity);
            $this->compute($em, $entity);
        }
    }

    /**
     * @param OnFlushEventArgs $args
     */
    private function updates(UnitOfWork $uow, EntityManager $em)
    {
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!in_array(EntityAuditTrait::class, class_uses($entity))) {
                continue;
            }

            $this->injectUpdate($entity);
            $this->compute($em, $entity);
        }
    }
}
