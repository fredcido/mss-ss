<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Psr\Log\LoggerInterface;

/**
 * Stores the locale of the user in the session after the login.
 */
class UserLocaleListener
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Session $session, LoggerInterface $logger)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user->getDefaultLocale()) {
            $this->session->set('_locale', $user->getDefaultLocale());

            $this->logger->info(sprintf("After login: setting user preferred language to: [ %s ]", $user->getDefaultLocale()));            
        }
    }
}
