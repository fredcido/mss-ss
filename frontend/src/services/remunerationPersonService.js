app.service(
    "RemunerationPersonService", 
    function(
        $http, 
        AppConstant, 
        EnumService,
        RemunerationDayService
    ) {

    var _preparateData = function(model) {
        var data = angular.copy(model);

        var convertObjects = [
            'remunerationStatement',
            'compulsoryAccessionSubscription',
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        if (model.remunerationDays) {
            data.remunerationDays = model.remunerationDays.map(function(item){
                return RemunerationDayService.preparateData(item);
            });
        }

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        return model;
    };

    var _initModel = function() {
        var model = {
            period: 1,
            remunerationDays: []
        };

        return model;
    };


    return {
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel
    };
});
