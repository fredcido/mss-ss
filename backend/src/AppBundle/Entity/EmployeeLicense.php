<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * EmployeeLicense
 *
 * @ORM\Table(name="employee_license")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployeeLicenseRepository")
 */
class EmployeeLicense
{
    use EntityAuditTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="days", type="integer")
     * @JMS\Groups({"details"})
     */
    private $days;

    /**
     * @var License
     *
     * @ORM\ManyToOne(targetEntity="License")
     * @JMS\Groups({"details"})
     */
    private $license;

    /**
     * @var Contribution
     *
     * @ORM\ManyToOne(targetEntity="Contribution", inversedBy="licenses")
     * @JMS\Exclude()
     */
    private $contribution;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set days
     *
     * @param integer $days
     *
     * @return EmployeeLicense
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return int
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Gets the value of license.
     *
     * @return License
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Sets the value of license.
     *
     * @param License $license the license
     *
     * @return self
     */
    public function setLicense(License $license)
    {
        $this->license = $license;
        return $this;
    }

    /**
     * Gets the value of contribution.
     *
     * @return Contribution
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Sets the value of contribution.
     *
     * @param Contribution $contribution the contribution
     *
     * @return self
     */
    public function setContribution(Contribution $contribution)
    {
        $this->contribution = $contribution;
        return $this;
    }
}
