app.service("AclService", function($cookies, $http, AppConstant) {

    var permissions = null;

    var getPermissions = function() {
        permissions = localStorage.getItem('permissions');
        if (permissions) {
            permissions = JSON.parse(permissions);
        } else {
            permissios = null;
        }

        return permissions;
    };

    var _load = function() {
        return $http({
            method: 'GET',
            url: AppConstant.apiUrl + "acl",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(function(response) {
            var permissions = response.data;

            var filteredPermissions = {};
            _.each(
                permissions,
                function(item, key) {
                    item.all = _.every(item, function(flag){ return flag});
                    if (item.all) {
                        filteredPermissions[key] = {all: true};
                    } else {
                        var keys = Object.keys(item);
                        filteredPermissions[key] = keys.filter(function(key) {
                            return item[key]
                        });
                    }
                }
            );

            permissions = filteredPermissions;
            localStorage.setItem("permissions", JSON.stringify(permissions));
        });
    };

    var _canI = function(resource, action) {
        var stateAllowed = [
            'login',
            'logout',
            '404',
            '500',
            'home',
            'forbidden'
        ].some(function(s) {
            return resource == s;
        });

        if (stateAllowed) {
            return true;
        }

        var permissions = getPermissions();

        if (!permissions) {
            return false;
        }

        if (!permissions.hasOwnProperty(resource)) {
            return false;
        }

        if (permissions[resource].all) {
            return true;
        }

        if (!permissions[resource].hasOwnProperty(action)) {
            return false;
        }

        return permissions[resource][action];
    };

    return {
        load: _load,
        canI: _canI,
        VIEW: 'view',
        CREATE: 'create',
        EDIT: 'edit',
        DELETE: 'delete'
    };
});
