app.service("UserCompanyService", function($rootScope, $cookies, $http, $state, AppConstant, AuthService) {

    var statesToRedirect = ['home', ''];

    var intercept = function() {
        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {

            var user = AuthService.getUserLogged();
            if (!user || !user.employer) {
                return;
            }

            if ('home' == toState.resource) {
                event.preventDefault();
                $rootScope.$evalAsync(function() {
                    $state.go('app.home-company');
                });
                return;
            }
        });
    };

    return {
        intercept: intercept,
    };
});
