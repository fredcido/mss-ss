<?php

namespace AppBundle\Mailer;

use AppBundle\Entity\Employer;
use AppBundle\Entity\Feedback;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserCompany
{
    use ContainerAwareTrait;
    use MailerTrait;

    protected $newUserTemplatePath = 'emails/company_registered.html.twig';
    protected $activateCompanyTemplatePath = 'emails/company_activate.html.twig';
    protected $feedbackCompanyTemplatePath = 'emails/company_feedback.html.twig';
    protected $submissionCompanyTemplatePath = 'emails/company_submission.html.twig';

    public function getNewUserTemplatePath()
    {
        return $this->newUserTemplatePath;
    }

    public function getActivateCompanyTemplatePath()
    {
        return $this->activateCompanyTemplatePath;
    }

    public function getFeedbackCompanyTemplatePath()
    {
        return $this->feedbackCompanyTemplatePath;
    }

    public function getSubmissionCompanyTemplatePath()
    {
        return $this->submissionCompanyTemplatePath;
    }

    /**
     * Send a mail to the admin when a new company user is created
     * @param  User   $user
     * @return EmailQueue
     */
    public function newUserMail(User $user)
    {
        $context = ['user' => $user];
        $subject = $this->buildTitle('user_company_registration');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getNewUserTemplatePath();
        $body = $this->renderBody($template, $context);

        $mailAddresses = [$user->getEmail()];

        $mailAdmin = $this->container->getParameter('mail_admin');
        $emailQueue->setBody($body)->setToMail($mailAddresses);

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send a mail to the admin when a new company user is activated
     * @param  User   $user
     * @return EmailQueue
     */
    public function activateEmail(User $user)
    {
        $context = ['user' => $user];
        $subject = $this->buildTitle('user_company_activate');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getActivateCompanyTemplatePath();
        $body = $this->renderBody($template, $context);

        $mailAdmin = $this->container->getParameter('mail_admin');
        $emailQueue->setBody($body)->setToMail($mailAdmin);

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send a mail to the user company when a new feedback is created
     * @param  User   $user
     * @return EmailQueue
     */
    public function newFeedbackMail(User $user, Employer $employer, Feedback $feedback)
    {
        $context = ['user' => $user, 'feedback' => $feedback];
        $subject = $this->buildTitle('user_company_feedback');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getFeedbackCompanyTemplatePath();
        $body = $this->renderBody($template, $context);

        $mailAddresses = [$user->getEmail()];

        // Get the email contact from the employer
        if ($employer->getContactDetail()) {
            $emails = $employer->getContactDetail()->getEmail();
            if (!empty($emails)) {
                $mailAddresses = array_merge($mailAddresses, $emails);
            }
        }

        // Grab the email address from the staffs
        if ($employer->getStaff()) {
            foreach ($employer->getStaff() as $staff) {
                $person = $staff->getPerson();
                if ($person->getContactDetail()) {
                    $emails = $person->getContactDetail()->getEmail();
                    if (!empty($emails)) {
                        $mailAddresses = array_merge($mailAddresses, $emails);
                    }
                }
            }
        }

        $mailAddresses = array_unique($mailAddresses);

        $emailQueue->setBody($body)->setToMail($mailAddresses);

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send a mail to the admin when a new submission is done
     * @param  User   $user
     * @return EmailQueue
     */
    public function submissionMail(User $user)
    {
        $context = ['user' => $user];
        $subject = $this->buildTitle('user_company_submission');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getSubmissionCompanyTemplatePath();
        $body = $this->renderBody($template, $context);

        $mailAdmin = $this->container->getParameter('mail_admin');
        $emailQueue->setBody($body)->setToMail($mailAdmin);

        return $this->saveEmail($emailQueue);
    }
}
