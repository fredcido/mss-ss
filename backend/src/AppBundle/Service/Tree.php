<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Tree
{
    use ContainerAwareTrait;

    public function nestify($collection)
    {
        $nested = [];
        $items = [];
        foreach ($collection as $node) {
            $items[$node->getId()] = $node;
            // Remove all nodes, so we can control which node should be presented or not (Inactive nodes must be removed from the tree)
            $node->cleanChildren();
        }

        foreach ($collection as $node) {
            if (!$node->getParent()) {
                $nested[] = $node;
                continue;
            }

            // If the parent is not present, do not show children nodes
            if (!isset($items[$node->getParent()->getId()])) {
                continue;
            }

            $items[$node->getParent()->getId()]->addChild($node);
        }

        return $nested;
    }
}
