<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class Document implements Enum
{
    use EnumTrait;
    
    const LEGISLATION    = 0;
    const PRATICAL_GUIDE = 1;
    const OTHERS         = 2;

    /**
     * @var array
     */
    protected static $data = [
        self::LEGISLATION    => 'Legislação',
        self::PRATICAL_GUIDE => 'Guias Praticos',
        self::OTHERS         => 'Outra Documentação',
    ];
}
