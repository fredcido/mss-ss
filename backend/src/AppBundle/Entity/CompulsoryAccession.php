<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CompulsoryAccession
 *
 * @ORM\Table(name="compulsory_accession")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompulsoryAccessionRepository")
 */
class CompulsoryAccession
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use VersionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="compulsoryAccessions")
     * @JMS\Groups({"list", "details"})
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity="CompulsoryAccessionSubscription", mappedBy="compulsoryAccession", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     *
     * @var CompulsoryAccessionSubscription
     */
    private $compulsorySubscriptions;

    /**
     * @var CompulsoryAccession
     *
     * @ORM\ManyToOne(targetEntity="CompulsoryAccession", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="CompulsoryAccession", mappedBy="source")
     */
    private $versions;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set monthWorkingTL
     *
     * @param integer $monthWorkingTL
     *
     * @return CompulsoryAccession
     */
    public function setMonthWorkingTL($monthWorkingTL)
    {
        $this->monthWorkingTL = $monthWorkingTL;

        return $this;
    }

    /**
     * Get monthWorkingTL
     *
     * @return int
     */
    public function getMonthWorkingTL()
    {
        return $this->monthWorkingTL;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->compulsorySubscriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add compulsorySubscription
     *
     * @param CompulsoryAccessionSubscription $compulsorySubscription
     *
     * @return CompulsoryAccession
     */
    public function addCompulsorySubscription(CompulsoryAccessionSubscription $compulsorySubscription)
    {
        $this->compulsorySubscriptions[] = $compulsorySubscription;
        $compulsorySubscription->setCompulsoryAccession($this);
        return $this;
    }

    public function cleanCompulsorySubscriptions()
    {
        $this->compulsorySubscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Remove compulsorySubscription
     *
     * @param CompulsoryAccessionSubscription $compulsorySubscription
     */
    public function removeCompulsorySubscription(CompulsoryAccessionSubscription $compulsorySubscription)
    {
        $this->compulsorySubscriptions->removeElement($compulsorySubscription);
    }

    /**
     * Get compulsorySubscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompulsorySubscriptions()
    {
        return $this->compulsorySubscriptions;
    }

    /**
     * Add compulsoryAccessionsAbroad
     *
     * @param CompulsoryAccessionAbroad $compulsoryAccessionsAbroad
     *
     * @return CompulsoryAccession
     */
    public function addCompulsoryAccessionsAbroad(CompulsoryAccessionAbroad $compulsoryAccessionsAbroad)
    {
        $this->compulsoryAccessionsAbroad[] = $compulsoryAccessionsAbroad;
        $compulsoryAccessionsAbroad->setCompulsoryAccession($this);

        return $this;
    }

    /**
     * Remove compulsoryAccessionsAbroad
     *
     * @param CompulsoryAccessionAbroad $compulsoryAccessionsAbroad
     */
    public function removeCompulsoryAccessionsAbroad(CompulsoryAccessionAbroad $compulsoryAccessionsAbroad)
    {
        $this->compulsoryAccessionsAbroad->removeElement($compulsoryAccessionsAbroad);
    }

    /**
     * Get compulsoryAccessionsAbroad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompulsoryAccessionsAbroad()
    {
        return $this->compulsoryAccessionsAbroad;
    }

    /**
     * Gets the value of person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets the value of person.
     *
     * @param Person $person the person
     *
     * @return self
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return CompulsoryAccession
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param CompulsoryAccession $source
     *
     * @return self
     */
    public function setSource(CompulsoryAccession $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\CompulsoryAccession $version
     *
     * @return CompulsoryAccession
     */
    public function addVersion(\AppBundle\Entity\CompulsoryAccession $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\CompulsoryAccession $version
     */
    public function removeVersion(\AppBundle\Entity\CompulsoryAccession $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
