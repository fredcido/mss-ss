<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Occupation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class OccupationController extends BaseController
{
    protected $repository = 'AppBundle:Occupation';

    protected $form = 'AppBundle\Form\OccupationType';

    protected $routeGet = 'app.occupation.get';

    protected $entity = Occupation::class;
}
