<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Calendar
{
    use ContainerAwareTrait;

    public function workDays($dateStart, $dateEnd)
    {
        if (!($dateStart instanceof \DateTime)) {
            $dateStart = new \DateTime($dateStart);
        }

        if (!($dateEnd instanceof \DateTime)) {
            $dateEnd = new \DateTime($dateEnd);
        }

        $daysInPeriod = (int)$dateEnd->diff($dateStart)->format("%a");
        $holidays = $this->findHolidaysBetween($dateStart, $dateEnd);
        $weekends = $this->getWeekends($dateStart, $dateEnd);

        $workDays = $daysInPeriod - (int)count($holidays) - (int)count($weekends) + 1;

        return $workDays;
    }

    public function getWeekends($dateStart, $dateEnd)
    {
        $weekendDays = array(6, 7);

        $dateEndPeriod = clone $dateEnd;
        $dateEndPeriod->modify('+1 day');

        $period = new \DatePeriod(
            $dateStart,
            new \DateInterval('P1D'),
            $dateEndPeriod
        );

        $weekends = [];
        foreach ($period as $day) {
            if (in_array($day->format('N'), $weekendDays)) {
                $weekends[] = $day;
            }
        }

        return $weekends;
    }

    public function findHolidaysBetween($dateStart, $dateEnd)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em->getRepository('AppBundle:Calendar')->findHolidaysBetween($dateStart, $dateEnd);
    }

    public function addWorkDays($dateStart, $days)
    {
        $dateStart = new \DateTime($dateStart);
        $dateEnd = clone $dateStart;
        $dateEnd->modify(sprintf('+%d day', $days));

        do {
            $holidays = $this->findHolidaysBetween($dateStart, $dateEnd);
            $weekends = $this->getWeekends($dateStart, $dateEnd);

            $daysToAdd = (int)count($holidays) + (int)count($weekends);
            if ($daysToAdd < 1) {
                break;
            }

            $dateStart = clone $dateEnd;
            $dateStart->modify('+1 day');
            $dateEnd->modify(sprintf('+%d day', $daysToAdd));
        } while ($daysToAdd > 0);

        return $dateEnd;
    }
}
