app.component('subscriptionStatusList', {
    templateUrl: 'views/components/subscriptionStatusList.html',
    bindings: {
        rows: '<',
        actions: '<',
    },
    controller: function($translate, $timeout, $uibModal, GenericService, ViewHelper, EnumService) {
        var self = this;

        self.EnumService = EnumService;

        self.$onChanges = function (changesObj) {
            if (changesObj.rows) {
                ViewHelper.blockContainer('.subscription-status-list', $translate.instant('loading'));
                self.rows = changesObj.rows.currentValue;

                self.rowCollection = self.rows;

                self.displayCollection = self.rows;
                ViewHelper.unblockContainer('.subscription-status-list');
            }
        };

        self.statusDesc = function(status) {
            return EnumService.geSubscriptionStatusesDescription(status);
        };
    }
});
