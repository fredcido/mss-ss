app.controller("KinshipFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'kinship';
    var _stateList = 'app.kinship-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.inverse = [];

    var _getKinships = function() {
        GenericService.find('kinship').then(function(response) {
            $scope.inverse = response.data;
        });
    };

    _getKinships();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            var data = response.data;
            $scope.model = data;

            if (data.inverse) {
                $scope.model.inverse = data.inverse.id;
            }
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
