<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * CompulsoryAccessionAbroad
 *
 * @ORM\Table(name="compulsory_accession_abroad")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompulsoryAccessionAbroadRepository")
 */
class CompulsoryAccessionAbroad
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use VersionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\OneToOne(targetEntity="Person", inversedBy="accessionAbroad")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $person;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $company;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="securityCode", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $securityCode;

    /**
     * @var int
     *
     * @ORM\Column(name="hasSubsidie", type="smallint", options={"default" : 0})
     * @JMS\Groups({"list", "details", "form"})
     */
    private $hasSubsidie = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="subsidieType", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $subsidieType;

    /**
     * @var string
     *
     * @ORM\Column(name="socialSecurityEntity", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $socialSecurityEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="subsidieCost", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $subsidieCost;

    /**
     * @var int
     *
     * @ORM\Column(name="foreignWorker", type="smallint", options={"default" : 0})
     * @JMS\Groups({"list", "details", "form"})
     */
    private $foreignWorker = 0;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $countryWork;

    /**
     * @var string
     *
     * @ORM\Column(name="socialSecurityEntityWork", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $socialSecurityEntityWork;

    /**
     * @var int
     *
     * @ORM\Column(name="monthsWorkingTL", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $monthsWorkingTL;

     /**
     * @var CompulsoryAccessionAbroad
     *
     * @ORM\ManyToOne(targetEntity="CompulsoryAccessionAbroad", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="CompulsoryAccessionAbroad", mappedBy="source")
     */
    private $versions;
    

    public function __construct()
    {
        $this->hasSubsidie = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return CompulsoryAccessionAbrod
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set securityCode
     *
     * @param string $securityCode
     *
     * @return CompulsoryAccessionAbrod
     */
    public function setSecurityCode($securityCode)
    {
        $this->securityCode = $securityCode;

        return $this;
    }

    /**
     * Get securityCode
     *
     * @return string
     */
    public function getSecurityCode()
    {
        return $this->securityCode;
    }

    /**
     * Set socialSecurityEntity
     *
     * @param string $socialSecurityEntity
     *
     * @return CompulsoryAccessionAbrod
     */
    public function setSocialSecurityEntity($socialSecurityEntity)
    {
        $this->socialSecurityEntity = $socialSecurityEntity;

        return $this;
    }

    /**
     * Get socialSecurityEntity
     *
     * @return string
     */
    public function getSocialSecurityEntity()
    {
        return $this->socialSecurityEntity;
    }

    /**
     * Set subsidieType
     *
     * @param string $subsidieType
     *
     * @return CompulsoryAccessionAbrod
     */
    public function setSubsidieType($subsidieType)
    {
        $this->subsidieType = $subsidieType;

        return $this;
    }

    /**
     * Get subsidieType
     *
     * @return string
     */
    public function getSubsidieType()
    {
        return $this->subsidieType;
    }

    /**
     * Set subsidieCost
     *
     * @param string $subsidieCost
     *
     * @return CompulsoryAccessionAbrod
     */
    public function setSubsidieCost($subsidieCost)
    {
        $this->subsidieCost = $subsidieCost;

        return $this;
    }

    /**
     * Get subsidieCost
     *
     * @return string
     */
    public function getSubsidieCost()
    {
        return $this->subsidieCost;
    }

    /**
     * Gets the value of country.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the value of country.
     *
     * @param Country $country the country
     *
     * @return self
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Set compulsoryAccession
     *
     * @param CompulsoryAccession $compulsoryAccession
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setCompulsoryAccession(CompulsoryAccession $compulsoryAccession = null)
    {
        $this->compulsoryAccession = $compulsoryAccession;

        return $this;
    }

    /**
     * Get compulsoryAccession
     *
     * @return CompulsoryAccession
     */
    public function getCompulsoryAccession()
    {
        return $this->compulsoryAccession;
    }

    /**
     * Set hasSubsidie
     *
     * @param integer $hasSubsidie
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setHasSubsidie($hasSubsidie)
    {
        $this->hasSubsidie = $hasSubsidie;

        return $this;
    }

    /**
     * Get hasSubsidie
     *
     * @return integer
     */
    public function getHasSubsidie()
    {
        return $this->hasSubsidie;
    }

    /**
     * Set monthsWorkingTL
     *
     * @param integer $monthsWorkingTL
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setMonthsWorkingTL($monthsWorkingTL)
    {
        $this->monthsWorkingTL = $monthsWorkingTL;

        return $this;
    }

    /**
     * Get monthsWorkingTL
     *
     * @return integer
     */
    public function getMonthsWorkingTL()
    {
        return $this->monthsWorkingTL;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set foreignWorker
     *
     * @param integer $foreignWorker
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setForeignWorker($foreignWorker)
    {
        $this->foreignWorker = (int)$foreignWorker;
        return $this;
    }

    /**
     * Get foreignWorker
     *
     * @return integer
     */
    public function getForeignWorker()
    {
        return $this->foreignWorker;
    }

    /**
     * Set socialSecurityEntityWork
     *
     * @param string $socialSecurityEntityWork
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setSocialSecurityEntityWork($socialSecurityEntityWork)
    {
        $this->socialSecurityEntityWork = $socialSecurityEntityWork;

        return $this;
    }

    /**
     * Get socialSecurityEntityWork
     *
     * @return string
     */
    public function getSocialSecurityEntityWork()
    {
        return $this->socialSecurityEntityWork;
    }

    /**
     * Set countryWork
     *
     * @param \AppBundle\Entity\Country $countryWork
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setCountryWork(\AppBundle\Entity\Country $countryWork = null)
    {
        $this->countryWork = $countryWork;

        return $this;
    }

    /**
     * Get countryWork
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountryWork()
    {
        return $this->countryWork;
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\CompulsoryAccessionAbroad $source
     *
     * @return CompulsoryAccessionAbroad
     */
    public function setSource(\AppBundle\Entity\CompulsoryAccessionAbroad $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\CompulsoryAccessionAbroad
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\CompulsoryAccessionAbroad $version
     *
     * @return CompulsoryAccessionAbroad
     */
    public function addVersion(\AppBundle\Entity\CompulsoryAccessionAbroad $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\CompulsoryAccessionAbroad $version
     */
    public function removeVersion(\AppBundle\Entity\CompulsoryAccessionAbroad $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
