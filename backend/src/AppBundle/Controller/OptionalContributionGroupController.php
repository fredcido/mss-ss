<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OptionalContributionGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class OptionalContributionGroupController extends BaseController
{
    protected $repository = 'AppBundle:OptionalContributionGroup';

    protected $form = 'AppBundle\Form\OptionalContributionGroupType';

    protected $routeGet = 'app.optional_contribution_group.get';

    protected $entity = OptionalContributionGroup::class;
}
