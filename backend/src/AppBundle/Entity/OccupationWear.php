<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OccupationWear
 *
 * @ORM\Table(name="occupation_wear")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OccupationWearRepository")
 * @UniqueEntity(fields={"occupation", "wear"}, message="This wear is already inserted for this occupation.")
 */
class OccupationWear
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="years", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $years;

    /**
     * @ORM\ManyToOne(targetEntity="Occupation", inversedBy="occupationWears")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="occupation_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $occupation;

    /**
     * @ORM\ManyToOne(targetEntity="Wear", inversedBy="occupationWears")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="wear_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $wear;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set years
     *
     * @param integer $years
     *
     * @return OccupationWear
     */
    public function setYears($years)
    {
        $this->years = $years;

        return $this;
    }

    /**
     * Get years
     *
     * @return int
     */
    public function getYears()
    {
        return $this->years;
    }

    /**
     * Set occupation
     *
     * @param Occupation $occupation
     *
     * @return OccupationWear
     */
    public function setOccupation(Occupation $occupation = null)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return Occupation
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set wear
     *
     * @param Wear $wear
     *
     * @return OccupationWear
     */
    public function setWear(Wear $wear)
    {
        $this->wear = $wear;
        return $this;
    }

    /**
     * Get wear
     *
     * @return Wear
     */
    public function getWear()
    {
        return $this->wear;
    }
}
