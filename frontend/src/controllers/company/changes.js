app.controller("CompanyChangesController", 
    function(
        $scope, 
        $stateParams, 
        $uibModal, 
        $translate, 
        $state, 
        $timeout,
        GenericService,
        EmployerService,
        AuthService, 
        EnumService, 
        ViewHelper
    ) {

    $scope.user = AuthService.getUserLogged();
    $scope.id = $stateParams.id || null;

    if ($scope.user.employer) {
        $scope.id = $scope.user.employer.internalCode;
    }

    $scope.actionsGrid = [{
        title: $translate.instant('view'),
        icon: 'fa-eye',
        callback: function(employer) {
            $state.go('app.company-version', {
                id: employer.internalCode,
                updateSubscription: employer.version == EnumService.getVersion('subscription') ? 0 : 1,
                version: employer.id
            });
        }
    }];

    $scope.canEdit = function()
    {
        return $scope.user.isEmployer() 
                && ($scope.user.employer.isApproved() || $scope.user.employer.isErrored());
    }

    var versions = [
        EnumService.getVersion('subscription'),
        EnumService.getVersion('update')
    ];

    var filters = {
        version: versions,
        internalCode: $scope.id
    };

    var init = function() {
        $scope.params = filters;

        if ($scope.id) {
            var version = EnumService.getVersion('standard');
            ViewHelper.blockContainer('.panel', $translate.instant('loading'));
            GenericService.getVersion('employer', $scope.id, version).then(function (response) {
                $scope.employer = EmployerService.populateModel(response.data);
                ViewHelper.unblockContainer('.panel');
            });
        }
    };

    init();
});
