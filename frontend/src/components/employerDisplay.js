app.component('employerDisplay', {
    templateUrl: 'views/components/employerDisplay.html',
    bindings: {
        employer: '<',
    },
    controller: function($translate, $timeout) {
        var self = this;

        self.$onChanges = function(changesObj) {
            if (changesObj.employer) {
                self.employer = changesObj.employer.currentValue;
            }
        };
    }
});
