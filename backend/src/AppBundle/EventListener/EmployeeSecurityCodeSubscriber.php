<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\OptionalAccession;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class EmployeeSecurityCodeSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $securityCodeGenerator;
    protected $needsFlush;

    public function setSecurityCodeGenerator($service)
    {
        $this->securityCodeGenerator = $service;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        $gatherEntities = function ($entity) use (&$injectEntities) {
            switch (true) {
                case $entity instanceof OptionalAccession && $entity->isSubmitted():
                    $person = $entity->getPerson();
                    break;
                case $entity instanceof CompulsoryAccessionSubscription && $entity->isSubmitted():
                    $person = $entity->getCompulsoryAccession()->getPerson();
                    break;
                default:
                    return;
            }

            if (!$this->securityCodeGenerator->hasSecurityCode($person)) {
                $injectEntities[] = $person;
            }
        };

        $entities = array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates());
        array_walk($entities, $gatherEntities);

        foreach ($injectEntities as $entity) {
            $this->securityCodeGenerator->generate($entity);
            $this->compute($em, $entity);
        }
    }
}
