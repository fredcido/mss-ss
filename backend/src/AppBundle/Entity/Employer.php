<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Employer
 *
 * @ORM\Table(name="employer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployerRepository")
 * @UniqueEntity(fields={"internalCode", "name"}, message="name_already_use", repositoryMethod="validateUnique", errorPath="name")
 * @UniqueEntity(fields={"internalCode", "securityCode"}, message="security_code_already_use", repositoryMethod="validateUnique", errorPath="securityCode")
 * @UniqueEntity(fields={"internalCode", "taxNumber"}, message="tax_number_already_use", repositoryMethod="validateUnique", errorPath="taxNumber")
 */
class Employer implements SocialSecurityInterface
{
    use EntityAuditTrait;
    use InternalCodeTrait;
    use EmployerDataTrait;
    use FileContainerTrait;
    use EntityPermissionTrait;
    use SubscriptionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="commercialName", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $commercialName;

    /**
     * @var string
     *
     * @ORM\Column(name="shortName", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column(name="securityCode", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $securityCode;

    /**
     * @ORM\OneToMany(targetEntity="EmployerContact", mappedBy="employer", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details", "form"})
     * @Assert\Valid
     *
     * @var EmployerContact
     */
    private $contacts;

    /**
     * @var ContactDetail
     *
     * @ORM\ManyToOne(targetEntity="ContactDetail", cascade={"persist"})
     * @JMS\Groups({"details", "form"})
     */
    private $contactDetail;

    /**
     * @ORM\OneToMany(targetEntity="BankInformation", mappedBy="employer", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details", "form"})
     * @Assert\Valid
     *
     * @var BankInformation
     */
    private $bankInformation;

    /**
     * @ORM\OneToMany(targetEntity="Staff", mappedBy="employer", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details", "form"})
     * @Assert\Valid
     *
     * @var Staff
     */
    private $staff;

    /**
     * @ORM\OneToMany(targetEntity="Subsidiary", mappedBy="employer", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details", "form"})
     * @Assert\Valid
     *
     * @var Subsidiary
     *
     */
    private $subsidiaries;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="Sector")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $sector;

    /**
     * @var string
     *
     * @ORM\Column(name="taxNumber", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $taxNumber;

    /**
     * @ORM\OneToMany(targetEntity="WorkSituation", mappedBy="employer")
     *
     * @var WorkSituation
     */
    private $workSituations;

    /**
     * @var int
     *
     * @ORM\Column(name="numWorkers", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Expression("this.getNumWorkers() >= this.getNumWorkersNational()", message="min_num_workers")
     */
    private $numWorkers;

    /**
     * @var int
     *
     * @ORM\Column(name="numWorkersNational", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Expression("this.getNumWorkersNational() <= this.getNumWorkers()", message="max_num_workers")
     */
    private $numWorkersNational;

    /**
     * @ORM\ManyToMany(targetEntity="StatusHistory")
     * @ORM\OrderBy({"id" = "DESC"})
     * @JMS\Groups({"list", "details"})
     */
    private $statusHistory;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="Employer", mappedBy="source")
     */
    private $versions;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contacts        = new \Doctrine\Common\Collections\ArrayCollection();
        $this->staff           = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subsidiaries    = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addresses       = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bankInformation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activitySectors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->statusHistory   = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getDisplayName()
    {
        $name = $this->getName();
        if ($this->getShortName()) {
            $name = $this->getShortName() . ' - ' . $name;
        }
        
        return sprintf("%s - %s - [%s]", $this->getInternalCode(), $name, $this->getSecurityCode());
    }

    /**
     * Set securityCode
     *
     * @param string $securityCode
     *
     * @return Employer
     */
    public function setSecurityCode($securityCode)
    {
        $this->securityCode = $securityCode;

        return $this;
    }

    /**
     * Get securityCode
     *
     * @return string
     */
    public function getSecurityCode()
    {
        return $this->securityCode;
    }

    /**
     * Add contact
     *
     * @param EmployerContact $contact
     *
     * @return Employer
     */
    public function addContact(EmployerContact $contact)
    {
        $this->contacts[] = $contact;
        $contact->setEmployer($this);
        return $this;
    }

    /**
     * Remove contact
     *
     * @param EmployerContact $contact
     */
    public function removeContact(EmployerContact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add bankInformation
     *
     * @param BankInformation $bankInformation
     *
     * @return Employer
     */
    public function addBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation[] = $bankInformation;
        $bankInformation->setEmployer($this);
        return $this;
    }

    /**
     * Remove bankInformation
     *
     * @param BankInformation $bankInformation
     */
    public function removeBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation->removeElement($bankInformation);
    }

    /**
     * Get bankInformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBankInformation()
    {
        return $this->bankInformation;
    }

    /**
     * Gets the value of shortName.
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Sets the value of shortName.
     *
     * @param string $shortName the short name
     *
     * @return self
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Gets the value of contactDetail.
     *
     * @return ContactDetail
     */
    public function getContactDetail()
    {
        return $this->contactDetail;
    }

    /**
     * Sets the value of contactDetail.
     *
     * @param ContactDetail $contactDetail the contact detail
     *
     * @return self
     */
    public function setContactDetail(ContactDetail $contactDetail)
    {
        $this->contactDetail = $contactDetail;
        return $this;
    }

    /**
     * Gets the value of sector.
     *
     * @return Sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Sets the value of marital status.
     *
     * @param Sector $sector the Sectors
     *
     * @return self
     */
    public function setSector(Sector $sector)
    {
        $this->sector = $sector;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecurityCodePrefix()
    {
        return '9';
    }

    /**
     * Gets the value of taxNumber.
     *
     * @return string
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * Sets the value of taxNumber.
     *
     * @param string $taxNumber the tax number
     *
     * @return self
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;
        return $this;
    }

    /**
     * Set commercialName
     *
     * @param string $commercialName
     *
     * @return Employer
     */
    public function setCommercialName($commercialName)
    {
        $this->commercialName = $commercialName;
        return $this;
    }

    /**
     * Get commercialName
     *
     * @return string
     */
    public function getCommercialName()
    {
        return $this->commercialName;
    }

    /**
     * Add staff
     *
     * @param Staff $staff
     *
     * @return Employer
     */
    public function addStaff(Staff $staff)
    {
        $this->staff[] = $staff;
        $staff->setEmployer($this);
        return $this;
    }

    /**
     * Remove staff
     *
     * @param Staff $staff
     */
    public function removeStaff(Staff $staff)
    {
        $this->staff->removeElement($staff);
    }

    /**
     * Get staff
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStaff()
    {
        return $this->staff;
    }

    /**
     * @return self
     */
    public function cleanStaff()
    {
        $this->staff = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    public function cleanSubsidiaries()
    {
        $this->subsidiaries = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Add subsidiary
     *
     * @param Subsidiary $subsidiary
     *
     * @return Employer
     */
    public function addSubsidiary(Subsidiary $subsidiary)
    {
        $this->subsidiaries[] = $subsidiary;
        $subsidiary->setEmployer($this);
        return $this;
    }

    /**
     * Remove subsidiary
     *
     * @param Subsidiary $subsidiary
     */
    public function removeSubsidiary(Subsidiary $subsidiary)
    {
        $this->subsidiaries->removeElement($subsidiary);
    }

    /**
     * Get subsidiaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubsidiaries()
    {
        return $this->subsidiaries;
    }

    /**
     * Add workSituation
     *
     * @param WorkSituation $workSituation
     *
     * @return Employer
     */
    public function addWorkSituation(WorkSituation $workSituation)
    {
        $this->workSituations[] = $workSituation;

        return $this;
    }

    /**
     * Remove workSituation
     *
     * @param WorkSituation $workSituation
     */
    public function removeWorkSituation(WorkSituation $workSituation)
    {
        $this->workSituations->removeElement($workSituation);
    }

    /**
     * Get workSituations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorkSituations()
    {
        return $this->workSituations;
    }

    /**
     * Set numWorkers
     *
     * @param integer $numWorkers
     *
     * @return Employer
     */
    public function setNumWorkers($numWorkers)
    {
        $this->numWorkers = (int) $numWorkers;
        return $this;
    }

    /**
     * Get numWorkers
     *
     * @return integer
     */
    public function getNumWorkers()
    {
        return $this->numWorkers;
    }

    /**
     * Gets the value of numWorkersNational.
     *
     * @return int
     */
    public function getNumWorkersNational()
    {
        return $this->numWorkersNational;
    }

    /**
     * Sets the value of numWorkersNational.
     *
     * @param int $numWorkersNational the num workers national
     *
     * @return self
     */
    public function setNumWorkersNational($numWorkersNational)
    {
        $this->numWorkersNational = $numWorkersNational;
        return $this;
    }

    /**
     * Add statusHistory
     *
     * @param StatusHistory $statusHistory
     *
     * @return Setting
     */
    public function addStatusHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory[] = $statusHistory;
        return $this;
    }

    /**
     * Remove statusHistory
     *
     * @param StatusHistory $statusHistory
     */
    public function removeHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Get statusHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatusHistory()
    {
        return $this->statusHistory;
    }

    /**
     * @return Employer
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param Employer $source
     *
     * @return self
     */
    public function setSource(Employer $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Remove statusHistory
     *
     * @param \AppBundle\Entity\StatusHistory $statusHistory
     */
    public function removeStatusHistory(\AppBundle\Entity\StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\Employer $version
     *
     * @return Employer
     */
    public function addVersion(\AppBundle\Entity\Employer $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\Employer $version
     */
    public function removeVersion(\AppBundle\Entity\Employer $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
