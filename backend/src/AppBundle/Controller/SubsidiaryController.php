<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subsidiary;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class SubsidiaryController extends BaseController
{
    protected $repository = 'AppBundle:Subsidiary';

    protected $form = 'AppBundle\Form\SubsidiaryType';

    protected $entity = Subsidiary::class;
}
