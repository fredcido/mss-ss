app.config(function ($httpProvider) {
	$httpProvider.interceptors.push("requestInterceptor");
    $httpProvider.interceptors.push("languageInterceptor");
});
