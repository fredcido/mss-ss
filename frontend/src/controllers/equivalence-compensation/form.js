app.controller("EquivalenceCompensationFormController", function($scope, $stateParams, $translate, GenericService) {
    $scope.supplements = [];
    $scope.compensation = {'C':[], 'O':[]};

    var _endpoint = 'equivalence-compensation';

    var searchSupplements = function() {
        GenericService.find('supplement').then(function(response) {
            response.data.map(function(o){
                if ('S' == o.type)
                    o.typeName = $translate.instant('supplement');
                else
                    o.typeName = $translate.instant('subsidie');
            });

            $scope.supplements = response.data;
        });
    };

    var searchBasis = function(accession) {
        GenericService.httpGet('equivalence-compensation/find/' + accession).then(function(response) {
            response.data.map(function(compensation){
                $scope.compensation[accession][compensation.supplement.id] = 1;
            });
        });
    };

    $scope.changedCompensation = function(supplement, accession) {
        if ($scope.compensation[accession][supplement]) {
            addCompensation(supplement, accession);
        } else {
            removeCompensation(supplement, accession);
        }
    };

    var addCompensation = function(supplement, accession) {
        var data = {
            'accession': accession,
            'supplement': supplement
        };

        GenericService.httpPost(_endpoint + '/add', data).then(function(){

        }).catch(function(){
            $scope.compensation[accession][supplement] = 0;
        });
    };

    var removeCompensation = function(supplement, accession) {
        var data = {
            'accession': accession,
            'supplement': supplement
        };

        GenericService.deleteMany(_endpoint, data).then(function(){

        }).catch(function(){
            $scope.compensation[accession][supplement] = 1;
        });
    };

    var init = function() {
        searchSupplements();
        searchBasis('C');
        searchBasis('O');
    };

    init();
});
