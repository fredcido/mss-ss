app.directive("localStorage", function($timeout, $state, $translate) {
    return {
        restrict: 'A',
        require: 'form',
        scope: {
            storage: '=',
            dates: '='
        },
        link: function(scope, elem, attrs) {
            var isWarned = false;
            var form = angular.element(elem[0]);
            var controller = form.controller('form');

            scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                if (isWarned === false && !toParams.ignoreLocalStorage && controller.$dirty && !controller.$submitted) {
                    event.preventDefault();
                    swal({
                        title: $translate.instant('local_storage_save_title'),
                        text: $translate.instant('local_storage_save_message'),
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            localStorage.setItem(window.location.href, JSON.stringify(scope.storage));
                        }

                        isWarned = true;
                        $state.go(toState.name, toParams);
                    });
                }
            });

            var _recoverLocalStorage = function() {
                var dataLocalStorage = _getLocalStorage();

                if (dataLocalStorage) {
                    swal({
                        title: $translate.instant('local_storage_recover_title'),
                        text: $translate.instant('local_storage_recover_message'),
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            scope.$apply(function() {
                                scope.storage = dataLocalStorage;
                            });
                        }
                        localStorage.removeItem(window.location.href);
                    });
                }
            };

            var _getLocalStorage = function() {
                var data = localStorage.getItem(window.location.href);
                if (data) {
                    data = JSON.parse(data);
                    if (scope.dates) {
                        scope.dates.map(function(name, index) {
                            if (data[name]) {
                                data[name] = moment(data[name], "YYYY/MM/DD").toDate();
                            }
                        });
                    }
                }

                return data;
            };

            $timeout(function() {
                _recoverLocalStorage();
            }, 1000);
        }
    };
});
