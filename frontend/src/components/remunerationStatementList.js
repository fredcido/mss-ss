app.component('remunerationStatementList', {
    templateUrl: 'views/components/remunerationStatementList.html',
    bindings: {
        rows: '<',
        actions: '<'
    },
    controller: function(GenericService, ViewHelper, $translate, $timeout, $uibModal) {
        var self = this;

        this.$onChanges = function (changesObj) {
            if (changesObj.rows) {
                self.rows = changesObj.rows.currentValue;
                self.rowCollection = self.rows;
                self.displayCollection = self.rows;
            }
        };
    }
});
