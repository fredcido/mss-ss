app.directive('acl', function(AclService) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs) {
            var resource = $attrs.acl;
            var action = angular.isUndefined($attrs.aclAction) ? AclService.CREATE : $attrs.aclAction;

            if (AclService.canI(resource, action)) {
                $element.removeClass('forbidden').show();
            } else {
                $element.addClass('forbidden').hide();
            }
        }
    };
});
