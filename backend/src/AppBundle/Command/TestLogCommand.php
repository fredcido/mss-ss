<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DBLS\Bundle\JerseyColoursBundle\Adapters\FileAdapter;

class TestLogCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mss:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $em = $container->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('AppBundle:Employer');
        $repoVersions = $em->getRepository('Gedmo\Loggable\Entity\LogEntry');

        $company = $repository->find(26);
        $logs = $repoVersions->getLogEntries($company);

        /*$repoVersions->revert($company, 1);
        $em->persist($company);
        $em->flush();*/

        dump($logs);exit;

    }
}
