-- DILI
INSERT INTO municipio (name) VALUES ('DILI');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ATAURO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BELOI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIQUELI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MACADADE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAQUILI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VILA MAUMETA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'CRISTO REI - DILI ORIENTAL');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AREIA BRANCA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BALIBAR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BECORA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HERA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMEA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CULU HUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIDAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAKEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIDAU SANTANA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAMEA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATU - AHI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LECIDERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILIOMAR2');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUKOKO MATE');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'DOM ALEIXO - DILI OCIDENTAL');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAIRO PITE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COMORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUHADA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPUNG ALOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SURIKMAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAMPON ALOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAROL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TASI TOLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANLEUANA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEBONUK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DELTA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITARAK LARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPUN TUTI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AVANCA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AIMUTIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MARCONI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PANTAI KELAPA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'WEDALAK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BETO TIMUR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LURU MATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PERUNAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAI KOTU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPO TUTI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BETO BARAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ZERO QUATRO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CINTAL BOOT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAKAULIDUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPUNG MERDEKA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUMETA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LISBUTAK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FOMENTO 3');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'USINDO III');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AILOK LARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MORIS FOUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOROMATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ZERO TRES');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FOMENTO 1');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATU META');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAI NAKDOKO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GOLGOTA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAROMAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'POSTU FINAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSKA BUBU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALINAMO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEIRA MAR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SOMETIN 2');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SULEUR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOSCABUBU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAZARE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TERA SANTA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'METINARO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DUYUNG');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SABULI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'METINARO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'NAIN FETO - DILI OCIDENTAL');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEMORI');INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAIRO CENTRAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAIRO DOS GRILHOS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAIRO FORMOSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIDAU LECIDERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'METI AUT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SANTA CRUZ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHANE ORIENTAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RIENTAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHANE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TAIBESI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AUDIAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GRENCIFOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAIN FETO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'QUINTAL BOOT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ACADIRU HUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'QUINTAL KIIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CRISINFOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LISCENFOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HALI LARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LICIDERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AILELE - HUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPUNG DEMOCRACIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'METIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITURILARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ASUKAI LOROS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAI MEAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEHEDA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, '20 DE MAIO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'INUR FUIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIOBI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AIDENI HUAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BECAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BENAMAUK');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'VERA CRUZ');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAICOLI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DARE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHANE OCIDENTAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MASCARINHAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COLMERA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MOTAEL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VILA VERDE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BALIDE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MATA DOURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KAMPUNG BARU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEBORA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TUANALARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PALAPASO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHANE ALTO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RUMBIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VERA CRUZ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANDARIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TANALARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HUDI LARAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANU FUIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATSABE');

-- BAUCAU
INSERT INTO municipio (name) VALUES ('BAUCAU');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BAUCAU');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAHU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BUCOLI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BURUMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BUIBAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GARIUAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TIRILOLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TRILOKA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAILILI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAIBADA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SEISAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMALARI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BAGUIA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AFALOICAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ALAUA KRAIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ALAUA LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DEFAUASI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HAECONI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LARISULA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAVATERI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSSO HUNA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMALARI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UACALA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LAGA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATELARI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIBAGUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NUNIRA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAGADATE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SOBA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TEQUINAUMATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAELARI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMALARI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'QUELICAI');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ABAFALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ABO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AFAÇÁ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAGUIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BUALALE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GURUSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LACULIO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAISOROLAI DE BAIXO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAISOROLAI DE CIMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LELALAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LETEMUMO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MACALACO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAMANEI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAITAME');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'VEMASSE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAICUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOILUBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSSO-UALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAIGAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSTICO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VEMASSE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UATULARI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'VENILALE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BADO-HO\'O');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAHA-MORI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATULIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UATO-HACO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAI - OLI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAI-LAHA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA ANA ICU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA ANA ULU');

-- AILEU
INSERT INTO municipio (name) VALUES ('AILEU');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'AILEU-VILA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AISSIRIMOU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BANDUDATU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAHIRIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUBOSSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOHOLAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUSI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SUCO LIURAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SABORIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SELOI KRAIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SELOI MALERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AILEU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LAULARA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATISI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COTOLAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MADABENO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TALITU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TOHUMETA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LIQUIDOE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ACUBILITOHO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BERELEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BETULAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAHISOI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANUCASSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAMOLESSO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATURILAU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'REMEXIO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ACUMAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FADABLOCO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATURASA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HAUTUHO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUMETA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TULATAQUEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAHISOI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SUCO LIURAI');

-- AINARO
INSERT INTO municipio (name) VALUES ('AINARO');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'AINARO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AINARO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CASSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANUTACI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAU-NUNO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SORO    ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SURO-CRAIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAU-ULO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'HATU-BUILICO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUCHIGA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NUNO-MOGUE');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'HATO-UDO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FOHO-AI-LICO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOLIMA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'MAUBISSE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITUTO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'EDI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUBESSI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HORAI-QUIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANELOBAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANETU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUBISSE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAULAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIURAI');

-- COVALIMA
INSERT INTO municipio (name) VALUES ('COVALIMA');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'FATULULIC');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATULULIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TAROMAN');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'FATUMEAN');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BELULIC LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUMEA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NANU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'FOHOREM');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DATA RUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DATA TOLU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FOHOREN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LACTOS');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ZUMALAI');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATULETO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEPO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAPE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAIMEA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UCECAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ZULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TASHILIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOUR');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'MAUCATAR');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BELECASAC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOLPILAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MATAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OGUES');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'SUAI');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BECO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DEBOS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LABARAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SUAI LORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAMENASA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'TILOMAR');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CASABAUC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LALAWA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUDEMU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEISEUC');

-- OECUSSE
INSERT INTO municipio (name) VALUES ('OECUSSE');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'NITIBE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BANAFI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEBE UFE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LELA UFE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SUNI UFE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'USI TACO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'OESILO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BOBOMETO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'USITAKENO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'USITASAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OESILO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'PANTE MAKASSAR');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BOBOCASAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COSTA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CUNHA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LALISUK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAIMECO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NIPANE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TAIBOCO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SANONE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KABUN');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'PASSABE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ABANI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALELAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'USI TAQUER');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAKI');

-- ERMERA
INSERT INTO municipio (name) VALUES ('ERMERA');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ATSABE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAKLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BABOE CRAIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BATUMANU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LASAUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUBONU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEIMEA LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALABE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OBULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PARAMI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TIARLELO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BABOE-LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATARA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ERMERA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ESTADO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HUMBOE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MERTUTU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'POETETE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PONILALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAIMERHEI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RIHEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TALIMORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ERMERA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEGUIMEA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'HATULIA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AILELO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ACULAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUBOLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HATOLIA VILA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEIMEA-CRAIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEIMEA-SORINBALO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LISAPAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANUSAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAU-UBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUBESSI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COILATE-LETELO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'URAHOU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LETEFOHO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DUCURAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ERAULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GOULOLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HATUGAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'KATRAI KARAIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CATRAILETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUANA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HAUPU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'RAILACO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUQUERO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIHU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MATATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAILACO KRAIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAILACO LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMALETE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TARACO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TOKOLULI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DELESO');

-- LAUTEM
INSERT INTO municipio (name) VALUES ('LAUTEM');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ILIOMAR');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AELEBERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAENLIO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FUAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILIOMAR I');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILIOMAR II');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TIRILOLO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LAUTEM');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COM');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DAUDERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'EUQUISI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILILAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAINA I');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAINA II');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PAIRARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SERELAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PARLAMENTO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BADURO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LOSPALOS');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CACAVEI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FUILORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOME');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LORE I');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LORE II');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MUAPITINE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAÇA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SOURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOSPALOS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUTEM');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LURO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AFABUBU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BARICAFA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COTAMUTU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LACAWA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'WAIROQUE');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'TUTUALA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MEHARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TUTUALA');

-- LIQUICA
INSERT INTO municipio (name) VALUES ('LIQUICA');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BAZARTETE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAHILEBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUMASI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAUHATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOREMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'METAGOU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MOTAULUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TIBAR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ULMERA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUMETA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LIQUICA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ASUMANO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DARULETE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DATO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOTALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOIDAHAR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LUCULAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HATUQUESSI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'MAUBARA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GUGLEUR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GUICO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LISSADILA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUBARALISA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VATUBORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VATUVOU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VAVIQUINIA');

-- MANATUTO
INSERT INTO municipio (name) VALUES ('MANATUTO');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BARIQUE - NATARBORA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AUBEON');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BARIQUE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATU WAQUEC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANEHAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA BOCU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LACLO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOHORAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA CADUAK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA NARUK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LACU MESAC');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LACLUBAR');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BATARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUK MAKEREK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANE LIMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ORLALAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SANAN NAIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FUNAR');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LALEIA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAIRUI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HATU RALAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIFAU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'MANATUTO VILA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AILILI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITEAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CRIBAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILIHEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MA\'ABAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'SOIBADA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAUNFAHE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SAMORO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANLALA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOHAT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUK MAKEREK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BOARAHUN');

-- MANUFAHI
INSERT INTO municipio (name) VALUES ('MANUFAHI');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ALAS');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITUHA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DOTIK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MAHAQUIDAN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TAITUDAK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA BERLOIK');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'FATUBERLIO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BUBUSSUSO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUKAHI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAICASSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CLACUC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUBERLIU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FAHINEHAN');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'SAME');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BABULU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BETANO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DAI-SUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GROTO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOLARUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TUTULURO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LETEFOHO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ROTUTO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'TURISCAI');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AITEMUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BEREMANA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUCALO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FOHOLAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CAIMAUC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LESSUATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANUMERA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MATOREK');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MINDELO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ORANA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIURAI');

-- VIQUEQUE
INSERT INTO municipio (name) VALUES ('VIQUEQUE');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LACLUTA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DILOR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LALINE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA TOLU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AHIC');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'OSSU');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BUILALE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LIARUCA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOI-HUNO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NEHARECA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSSORUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OSSU DE CIMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UABUBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAGUIA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAIBOBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'NAHARECA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'VIQUEQUE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAHALARAUAIN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BIBILEO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CARAUBALO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'FATUDERE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LUCA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALURU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA QUIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA UAIN CRAIC');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UMA UAIN LETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAIMORI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'UATOCARBAU');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BAHATATA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'IRABIN DE BAIXO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'IRABIN DE CIMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOI ULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UANI UMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AFALOICAI');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'UATO-LARI');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BABULO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MACADIQUE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'UAITAME');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'VESSORU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AFALOICAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MATAHOI');

-- BOBONARO
INSERT INTO municipio (name) VALUES ('BOBONARO');

SELECT LAST_INSERT_ID() INTO @municipio_id FROM dual;

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'ATABAE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AIDABALETEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATABAE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HATAZ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAEROBO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MIGIR');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BALIBO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BALIBO VILLA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BATUGADE');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COVÁ');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOHITU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEOLIMA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SANIRIN');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'BOBONARO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COLIMAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEBER');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOUR');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LOURBA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALILAIT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MOLOP');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OELE\'U');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SOILESO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TAPO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATUABEN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'BOBONARO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'CARABAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'COTA BO\'OT');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SIBUNI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'TEBABUI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ILATLAUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'AI\'ASSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MALEUBU');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'CAILACO');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ATUDARA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DA\'UDO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MANAPA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GENULAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'PURUGUA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAIHEU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GOULOLO');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MELIGO');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'LOLOTOE');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'DEUDET');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GILDAPIL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'GUDA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LEBOS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LONTAS');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LUPAL');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'OPA');

INSERT INTO posto_administrativo (municipio_id, name) VALUES (@municipio_id, 'MALIANA');

SELECT LAST_INSERT_ID() INTO @posto_administrativo_id FROM dual;

INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'HOLSA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'LAHOMEA');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'ODOMAU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RAIFUN');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'RITABOU');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'SABURAI');
INSERT INTO suku (posto_administrativo_id, name) VALUES (@posto_administrativo_id, 'MEMO');

-- Settings
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('contribution_employer', 6, 'Taxa de contribuição do empregador', '%');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('contribution_employee', 66, 'Taxa de contribuição do empregado', '%');
INSERT INTO `setting` (name, value, explanation) VALUES ('minimal_age_women', 60, 'Idade mínima de reforma - Mulheres');
INSERT INTO `setting` (name, value, explanation) VALUES ('minimal_age_men', 65, 'Idade mínima de reforma - Homens');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('interest', 5, 'Taxa de juro (para efeitos de processamento de dívidas à Segurança Social)', '%');
INSERT INTO `setting` (name, value, explanation) VALUES ('minimal_contribution_age_men', 16, 'Idade mínima de contribuição - Homens');
INSERT INTO `setting` (name, value, explanation) VALUES ('minimal_contribution_age_women', 16, 'Idade mínima de contribuição - Mulheres');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('time_to_update_contribution', 3, 'Tempo permitido para atualizar contribuições', 'anos');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('worked_days_reference', 30, 'Referência de dias de trabalho', 'dias');
INSERT INTO `setting` (name, value, explanation) VALUES ('lower_limit_mandatory_contribution', 0, 'Adesão Obrigatória - Data limite inferior para entrega das declarações de remuneração');
INSERT INTO `setting` (name, value, explanation) VALUES ('upper_limit_mandatory_contribution', 10, 'Adesão Obrigatória - Data limite superior para entrega das declarações de remuneração');
INSERT INTO `setting` (name, value, explanation) VALUES ('lower_limit_optional_contribution', 0, 'Adesão Facultativa - Data limite inferior para entrega das declarações de remuneração');
INSERT INTO `setting` (name, value, explanation) VALUES ('upper_limit_optional_contribution', 0, 'Adesão Facultativa - Data limite superior para entrega das declarações de remuneração');
INSERT INTO `setting` (name, value, explanation) VALUES ('lower_limit_mandatory_payment', 10, 'Adesão Obrigatória - Data limite inferior para pagamento das contribuições');
INSERT INTO `setting` (name, value, explanation) VALUES ('upper_limit_mandatory_payment', 20, 'Adesão Obrigatória - Data limite superior para pagamento das contribuições');
INSERT INTO `setting` (name, value, explanation) VALUES ('lower_limit_optional_payment', 0, 'Adesão Facultativa - Data limite inferior para pagamento das contribuições');
INSERT INTO `setting` (name, value, explanation) VALUES ('upper_limit_optional_payment', 10, 'Adesão Facultativa - Data limite superior para pagamento das contribuições');
INSERT INTO `setting` (name, value, explanation) VALUES ('saii', 30, 'SAII');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('days_open_subscription', 5, 'Dias para alteração da ficha de inscrição', 'dias');
INSERT INTO `setting` (name, value, explanation, measure) VALUES ('day_update_subscription', 10, 'Dia limite do mês para alteração da ficha de inscrição', 'dia');

-- Contract Type
INSERT INTO contract_type(`name`, `status`) VALUES('A termo certo', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('A termo incerto', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Sem termo', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Curta duração', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Trabalhador estrangeiro', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Tempo parcial', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Pluralidade de empregadores', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Intermitente', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Comissão de serviço', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Promessa de trabalho', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Prestação subordinada de tele-trabalho', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Pré-forma', 1);
INSERT INTO contract_type(`name`, `status`) VALUES('Cedência ocasional de trabalhadores', 1);

-- Licenses
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LM', 'Licença de Maternidade', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LP', 'Licença de Paternidade', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('BM', 'Baixa Médica', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LA', 'Licença Anual', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LL', 'Licença de Luto', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LC', 'Licença Casamento', 1, 1, 1, 1);
INSERT INTO license(`acronym`, `name`, `status`, `daysInMonth`, `daysInYear`, `type`) VALUES('LE', 'Licença para Estudos', 1, 1, 1, 1);

-- Kinship
INSERT INTO kinship(`name`, `status`) VALUES('Trisavô(ó)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisavô(ó)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Tia-avó', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Avô(ó)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Tio-avô', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Filha da Tia-avó', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Tio(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Pai-mãe', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Sogro(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Filho do Tio-avô', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto da Tia-avó', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Prima', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Irmã', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Cunhado', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Cônjuge', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Irmão', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Cunhada', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Primo', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto do', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto da Tia-avó', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Filho da Prima', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Sobrinha', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Filho(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Sobrinho', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Filho do Primo', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto do Tio-avô', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto da Tia-avó', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto da Prima', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto da Irmã', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto do Irmão', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Neto do Primo', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto do Tio-avô', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto da Prima', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto da Irmã', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto do Irmão', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Bisneto do Primo', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto da Prima', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto da Irmã', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto(a)', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto do Irmão', 1);
INSERT INTO kinship(`name`, `status`) VALUES('Trineto do Primo', 1);

-- Oauth Client
INSERT INTO `oauth2_clients` VALUES (NULL, '3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4', 'a:0:{}', '4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k', 'a:1:{i:0;s:8:"password";}');

INSERT INTO contract_status(id, `name`, `status`) VALUES(0, 'Continuo', 1);