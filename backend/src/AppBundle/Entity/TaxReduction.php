<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * TaxReduction
 *
 * @ORM\Table(name="tax_reduction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaxReductionRepository")
 * @UniqueEntity(fields="year", message="There is already a reduction for this year.")
 */
class TaxReduction
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="reduction", type="decimal", precision=5, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $reduction;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalPercent", type="decimal", precision=5, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $nationalPercent = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="employers", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $employers;

    /**
     * @var int
     *
     * @ORM\Column(name="acceptDebt", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $acceptDebt = 0;


    public function __construct()
    {
        $this->acceptDebt = 0;
        $this->nationalPercent = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return TaxReduction
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set reduction
     *
     * @param string $reduction
     *
     * @return TaxReduction
     */
    public function setReduction($reduction)
    {
        $this->reduction = $reduction;

        return $this;
    }

    /**
     * Get reduction
     *
     * @return string
     */
    public function getReduction()
    {
        return $this->reduction;
    }

    /**
     * Set employers
     *
     * @param integer $employers
     *
     * @return TaxReduction
     */
    public function setEmployers($employers)
    {
        $this->employers = $employers;

        return $this;
    }

    /**
     * Get employers
     *
     * @return int
     */
    public function getEmployers()
    {
        return $this->employers;
    }

    /**
     * Set acceptDebt
     *
     * @param integer $acceptDebt
     *
     * @return TaxReduction
     */
    public function setAcceptDebt($acceptDebt)
    {
        $this->acceptDebt = (int)$acceptDebt;

        return $this;
    }

    /**
     * Get acceptDebt
     *
     * @return int
     */
    public function getAcceptDebt()
    {
        return $this->acceptDebt;
    }

    /**
     * @return string
     */
    public function getNationalPercent()
    {
        return $this->nationalPercent;
    }

    /**
     * @param string $nationalPercent
     *
     * @return self
     */
    public function setNationalPercent($nationalPercent)
    {
        $this->nationalPercent = $nationalPercent;

        return $this;
    }
}
