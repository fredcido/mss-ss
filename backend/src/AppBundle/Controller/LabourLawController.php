<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LabourLaw;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class LabourLawController extends BaseController
{
    protected $repository = 'AppBundle:LabourLaw';

    protected $form = 'AppBundle\Form\LabourLawType';

    protected $routeGet = 'app.labour_law.get';

    protected $entity = LabourLaw::class;
}
