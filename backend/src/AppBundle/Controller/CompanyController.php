<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employer;
use AppBundle\Form\EmployerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_COMPANY')")
 */
class CompanyController extends BaseController
{
    protected $repository = 'AppBundle:Employer';

    protected $form = EmployerType::class;

    protected $entity = Employer::class;
}
