app.service("SettingService", function($http, $cookies, AppConstant, $translate, $state, GenericService) {

    var _save = function(endpoint, data) {
        return $http({
            method: 'POST',
            url: AppConstant.apiUrl + endpoint + '/' + data.id + "/edit",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: $.param(data),
        });
    };

    var _load = function() {
        return GenericService.find('setting', true).then(function(response){
            settings = {};
            response.data.forEach(function(item){
                settings[item.name] = item.value;
            });
            localStorage.setItem('mss_settings', JSON.stringify(settings));
            //$cookies.put("mss_settings", JSON.stringify(settings));
        });
    };

    var _get = function(key, fallback) {
        //settings = JSON.parse($cookies.get('mss_settings')) || null;
        settings = JSON.parse(localStorage.getItem('mss_settings')) || null;
        if (!settings || !settings.hasOwnProperty(key)) {
            return fallback;
        } else {
            return settings[key];
        }
    };

    return {
        save: _save,
        get: _get,
        load: _load,
    };
});
