app.component('fileUpload', {
    templateUrl: 'views/components/fileUpload.html',
    bindings: {
        onNewFile: '&',
        model: '<',
        reference: '@',
        disabled: '<'
    },
    controller: function($scope, $q, GenericService, ViewHelper, $translate, $timeout, Upload, AppConstant) {
        var self = this;
        var _endpoint = 'upload';
        self.filesConfiguration = [];
        self.filesUploaded = [];

        self.$onInit = function() {
            getFilesConfiguration();
        };

        self.$onChanges = function(changesObj) {
            if (self.model && self.model.id && !self.model.fileLoaded) {
                GenericService.httpGet('file-configuration/files/' + self.reference + '/' + self.model.internalCode + '?all').then(function(response) {
                    self.filesUploaded = response.data;
                    setFilesFromServer(self.filesUploaded);
                    self.model.fileLoaded = true;
                });
            }
        };

        var setFilesFromServer = function(filesUploaded) {
            self.filesConfiguration.some(function(config){
                if (!config.hasFiles && !config.required) return;

                var hasFile = filesUploaded.some(function(file){
                    return config.id == file.configuration.id;
                });

                config.required = hasFile ? 0 : 1;
                config.hasFiles = hasFile;
                config.file = hasFile ? true : undefined;
            });
        };

        var getFilesConfiguration = function() {
            return GenericService.httpGet('file-configuration/find/' + self.reference).then(function(response) {
                self.filesConfiguration = response.data;
                setFilesFromServer(self.filesUploaded);
            });
        };

        var setFileUploaded = function(configuration, file) {
            if (!self.filesUploaded) {
                self.filesUploaded = [];
            }

            self.filesUploaded.push(file);
        };

        var setErrorFile = function(configuration, file, error) {
            if (!configuration.errors) {
                configuration.errors = {};
            }

            if (!configuration.errors[file.name]) {
                configuration.errors[file.name] = [];
            }

            if (!angular.isObject(error) && !angular.isArray(error)) {
                error = [error];
            }

            angular.forEach(
                error,
                function(e) {
                    if (!angular.isArray(e)) {
                        e = [e];
                    }
                    angular.forEach(
                        e,
                        function(m){
                            configuration.errors[file.name].push(m);
                        }
                    );
                }
            );

            configuration.file = undefined;
        };

        var setUploadPercentage = function(configuration, file, percentage) {
            if (!configuration.percentages) {
                configuration.percentages = {};
            }

            configuration.percentages[file.name] = percentage;
        };

        self.removeUploadedFile = function(savedFile) {
            swal({
                    title: $translate.instant('confirm'),
                    text: $translate.instant('confirm'),
                    type: 'warning',
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (!isConfirm) {
                        return;
                    } else {
                        GenericService.delete('file-configuration', savedFile.id).then(function(response) {
                            self.filesUploaded.some(function(f, i) {
                                if (f == savedFile) {
                                    self.filesUploaded.splice(i, 1);
                                }
                            });

                            self.model.files.some(function(f, i) {
                                if (f == savedFile.id) {
                                    self.model.files.splice(i, 1);
                                }
                            });

                            setFilesFromServer(self.filesUploaded);
                        });
                    }
                }
            );
        };

        self.pathDownload = function(savedFile) {
            return AppConstant.apiUrl + savedFile.path;
        };

        self.uploadFiles = function(files, configuration) {
            configuration.errors = null;
            configuration.percentages = null;
            
            angular.forEach(
                files,
                function(file) {
                    Upload.upload({
                        url: AppConstant.apiUrl + _endpoint,
                        data: {
                            file: file,
                            'configuration': configuration.id,
                            'entity': self.model.internalCode
                        }
                    }).then(function(response) {
                        setFileUploaded(configuration, response.data);
                        setFilesFromServer(self.filesUploaded);
                        
                        if (typeof self.onNewFile == 'function') {
                            self.onNewFile({file: response.data});
                        }

                        /*$timeout(function() {
                            ViewHelper.scrollTo('.list-files');
                        }, 500);*/

                    }, function(resp) {
                        var response = resp.data;
                        if (response.errors) {
                            if (!angular.isArray(response.errors)) {
                                response.errors = [response.errors];
                            }

                            response.errors.map(function(error) {
                                if (error) {
                                    setErrorFile(configuration, file, error);
                                }
                            });
                        } else {
                            setErrorFile(configuration, file, $translate.instant('error_text'));
                        }

                    }, function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        setUploadPercentage(configuration, file, progressPercentage);

                        var finishedAll = true;
                        angular.forEach(configuration.percentages,
                            function(p) {
                                if (p < 100) finishedAll = false;
                            }
                        );

                        if (finishedAll) {
                            configuration.percentages = null;
                        }
                    });
                }
            );
        };

        self.isMultiple = function(file) {
            return file.mutiple == 1;
        };

        self.isRequired = function(file) {
            return file.required == 1;
        };

        self.hasFilesMissing = function() {
            flag = self.filesConfiguration.some(function(config){
                return config.required;
            });
            
            return flag;
        }
    }
});
