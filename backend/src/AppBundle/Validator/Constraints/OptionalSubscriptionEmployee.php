<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OptionalSubscriptionEmployee extends Constraint
{
    public $message = 'optional_subscription_employee';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'app.validator.optional_subscription_employee';
    }
}
