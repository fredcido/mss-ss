<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Convention;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class ConventionController extends BaseController
{
    protected $repository = 'AppBundle:Convention';

    protected $form = 'AppBundle\Form\ConventionType';

    protected $routeGet = 'app.convention.get';

    protected $entity = Convention::class;

    protected function getOrderBy()
    {
        return array('country' => 'asc');
    }
}
