app.controller("CompanyListController", 
    function(
        $scope, 
        $translate, 
        $state, 
        $stateParams, 
        EnumService,
        AuthService
    ) {

    $scope.suspend = parseInt($stateParams.suspend);
    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.user = AuthService.getUserLogged();

    $scope.actionsGrid = [{
        title: $translate.instant('view'),
        icon: 'fa-eye',
        callback: function(employer) {
            if ($scope.suspend) {
                $state.go('app.company-suspension', {
                    id: employer.internalCode
                });
            } else {
                $state.go('app.company-form', {
                    id: employer.internalCode,
                    updateSubscription: $scope.updateSubscription
                });
            }
        }
    }];

    $scope.params = {
        version: EnumService.getVersion('standard'),
        all_status: 1
    };

    if ($scope.user.isEmployer()) {
        $scope.params.intenalCode = $scope.user.employer.internalCode;
    }

    // If viewing the change 
    if ($scope.updateSubscription) {
        $scope.params.notSubscriptionStatus = [
            EnumService.getSubscriptionStatus('pending'),
            EnumService.getSubscriptionStatus('error')
        ];

        var viewChangesButton = {
            title: $translate.instant('history'),
            icon: 'fa-history',
            callback: function(employer) {
                $state.go(
                    'app.company-list-changes', 
                    {
                        id: employer.internalCode
                    }
                );
            }
        };
        $scope.actionsGrid.push(viewChangesButton);
    }

    if ($scope.suspend) {
        $scope.params.all_status = 1;
        
        $scope.params.hadSubscriptionStatus = [
            EnumService.getSubscriptionStatus('suspended'),
            EnumService.getSubscriptionStatus('ceased')
        ];

        /*var viewChangesButton = {
            title: $translate.instant('history'),
            icon: 'fa-history',
            callback: function(employer) {
                $state.go(
                    'app.company-list-suspensions', 
                    {
                        id: employer.internalCode
                    }
                );
            }
        };
        $scope.actionsGrid.push(viewChangesButton);*/
    }
});
