app.controller(
    "RemunerationStatementListController", 
    function(
        $scope, 
        $translate, 
        $state, 
        EnumService,
        AuthService,
        GenericService,
        ViewHelper
    ) {

    $scope.user = AuthService.getUserLogged();

    $scope.actionsGrid = [{
        title: $scope.user.isStaff() ? $translate.instant('view') : $translate.instant('edit'),
        icon: $scope.user.isStaff() ? 'fa-eye' : 'fa-pencil',
        callback: function(statement) {
            $state.go('app.remuneration-statement', {
                id: statement.id
            });
        }
    }];

    $scope.params = {
        all_status: 1
    };

    if ($scope.user.isEmployer()) {
        search();
    }

    function search() {
        var url = 'remuneration-statement/search';
        var filters = {
            employer: $scope.user.employer.id
        };

        ViewHelper.blockContainer('.panel-body', $translate.instant('loading'));
        GenericService.httpGet(url, filters).then(function(response){
            $scope.rows = response.data;
            ViewHelper.unblockContainer('.panel-body');
        });
    }
});
