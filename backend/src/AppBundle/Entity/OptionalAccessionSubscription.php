<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints as InssAssert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * OptionalAccessionSubscription
 *
 * @ORM\Table(name="optional_accession_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OptionalAccessionSubscriptionRepository")
 */
class OptionalAccessionSubscription implements PersonAwareInterface
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $dateEnd;


    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $cost;

    /**
     * @var OptionalAccession
     *
     * @ORM\ManyToOne(targetEntity="OptionalAccession", inversedBy="optionalAccessionSubscriptions")
     * @JMS\Groups({"list", "details"})
     */
    private $optionalAccession;

    /**
     * @var OptionalStep
     *
     * @ORM\ManyToOne(targetEntity="OptionalStep")
     * @JMS\Groups({"list", "details"})
     */
    private $optionalStep;

    /**
     * @var OptionalAccessionSubscription
     *
     * @ORM\ManyToOne(targetEntity="OptionalAccessionSubscription", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="OptionalAccessionSubscription", mappedBy="source")
     */
    private $versions;
    

    public function __construct()
    {
        $this->setDateStart(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return OptionalAccessionSubscription
     */
    public function setDateStart($dateStart)
    {
        if (!$dateStart) {
            $this->setDateStart(new \DateTime('now'));
        } else {
            $this->dateStart = $dateStart;
        }

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return OptionalAccessionSubscription
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }


    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return OptionalAccessionSubscription
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set optionalAccession
     *
     * @param OptionalAccession $optionalAccession
     *
     * @return OptionalAccessionSubscription
     */
    public function setOptionalAccession(OptionalAccession $optionalAccession = null)
    {
        $this->optionalAccession = $optionalAccession;
        return $this;
    }

    /**
     * Get optionalAccession
     *
     * @return OptionalAccession
     */
    public function getOptionalAccession()
    {
        return $this->optionalAccession;
    }

    /**
     * Set optionalStep
     *
     * @param OptionalStep $optionalStep
     *
     * @return OptionalAccessionSubscription
     */
    public function setOptionalStep(OptionalStep $optionalStep = null)
    {
        $this->optionalStep = $optionalStep;
        if ($this->optionalStep) {
            $this->setCost($this->optionalStep->getCost());
        }

        return $this;
    }

    /**
     * Get optionalStep
     *
     * @return OptionalStep
     */
    public function getOptionalStep()
    {
        return $this->optionalStep;
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->getOptionalAccession()->getPerson();
    }


    /**
     * @Assert\Callback
     */
    public function isDateValid(ExecutionContextInterface $context)
    {
        if ($this->getDateStart() && $this->getDateEnd() && $this->getDateStart() > $this->getDateEnd()) {
            $context->buildViolation('error_min_date')
                                ->atPath('dateEnd')
                                ->addViolation();
        }
    }

    /**
     * @return OptionalAccessionSubscription
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param OptionalAccessionSubscription $source
     *
     * @return self
     */
    public function setSource(OptionalAccessionSubscription $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\OptionalAccessionSubscription $version
     *
     * @return OptionalAccessionSubscription
     */
    public function addVersion(\AppBundle\Entity\OptionalAccessionSubscription $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\OptionalAccessionSubscription $version
     */
    public function removeVersion(\AppBundle\Entity\OptionalAccessionSubscription $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
