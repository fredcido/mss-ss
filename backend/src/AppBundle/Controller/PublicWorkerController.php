<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PublicWorker;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class PublicWorkerController extends BaseController
{
    protected $repository = 'AppBundle:PublicWorker';

    protected $form = 'AppBundle\Form\PublicWorkerType';

    protected $entity = PublicWorker::class;

    public function getByPersonAction($id)
    {
        $entity = $this->getRepository()->findBy(['person' => $id]);

        if (empty($entity)) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->createApiResponse($entity[0], Response::HTTP_OK, array('details'));
    }
}
