<?php

namespace AppBundle\Mailer;

use AppBundle\Entity\RemunerationRevision;
use AppBundle\Entity\RemunerationStatement as RemunerationStatementEntity;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class RemunerationStatement
{
    use ContainerAwareTrait;
    use MailerTrait;

    protected $newRemunerationStatementTemplatePath = 'emails/remuneration_statement.html.twig';
    protected $revisionRemunerationStatementTemplatePath = 'emails/remuneration_revision.html.twig';

    public function getNewRemunerationStatementTemplatePath()
    {
        return $this->newRemunerationStatementTemplatePath;
    }

    public function getRevisionRemunerationStatementTemplatePath()
    {
        return $this->revisionRemunerationStatementTemplatePath;
    }

    /**
     * Send email to the admins when a new Remuneration Statement is persisted
     * @param  RemunerationStatementEntity $remunerationStatement
     * @return EmailQueue
     */
    public function newStatement(RemunerationStatementEntity $remunerationStatement)
    {
        $context = ['remunerationStatement' => $remunerationStatement];
        $subject = $this->buildTitle('remuneration_statement');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getNewRemunerationStatementTemplatePath();
        $body = $this->renderBody($template, $context);

        $mailAdmin = $this->container->getParameter('mail_admin');
        $emailQueue->setBody($body)->setToMail($mailAdmin);

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send email to the company when a revision is created on the Remuneration Statement
     * @param  RemunerationRevision $revision
     * @return EmailQueue
     */
    public function newRevision(RemunerationRevision $revision)
    {
        $remunerationStatement = $revision->getRemunerationStatement();
        $employer = $remunerationStatement->getEmployer();

        $employerService = $this->container->get('app.employer');
        $address = $employerService->getValidMailAddress($employer);

        // If employer does not have a valid mail address, ignore the message
        if (empty($address)) {
            return;
        }

        $context = ['revision' => $revision];
        $subject = $this->buildTitle('remuneration_statement_revision');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template =  $this->getRevisionRemunerationStatementTemplatePath();
        $body = $this->renderBody($template, $context);

        $emailQueue->setBody($body)->setToMail($address);

        return $this->saveEmail($emailQueue);
    }
}
