<?php

namespace AppBundle\EventListener;

use AppBundle\Enum\Status;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UserCompanyActivateListener
{
    use ContainerAwareTrait;
    use GetUserTrait;

    public function activateCompany(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $employer = $user->getEmployer();
        if ($employer && $employer->getStatus() == Status::INACTIVE) {
            $employer->setStatus(Status::ACTIVE);

            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->persist($employer);

            $this->container->get('app.mail.user_company')->activateEmail($user);

            $em->flush();
        }
    }
}
