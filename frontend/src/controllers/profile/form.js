app.controller("ProfileController", 
    function(
        $scope, 
        $cookies, 
        $translate, 
        $window, 
        $timeout,
        GenericService, 
        AuthService
    ) {
    var _endpoint = 'profile';
    var _stateList = 'app.profile';

    var handleRoleName = function(name) {
        name = name.toLowerCase();
        return $translate.instant(name);
    };

    var user = AuthService.getUserLogged();
    user.roles = user.roles.filter(function(item){
        return item;
    }).map(function(item){
        return handleRoleName(item);
    });

    $scope.model = user;

    $scope.editPassword = false;
    $scope.locales = [
        {id: 'en', 'name': $translate.instant('en')},
        {id: 'pt', 'name': $translate.instant('pt')},
        {id: 'tl', 'name': $translate.instant('tl')},
    ];

    $scope.clickEditPassword = function() {
        if ($scope.editPassword) {
            $scope.editPassword = false;
        } else {
            $scope.editPassword = true;
        }
    };

    // save
    $scope.save = function(data) {
        var dataPrepared = {
            'email': data.email,
            'username': data.username,
            'locale': data.locale,
            'plainPassword': {}
        };

        if ($scope.editPassword && data.plainPassword) {
            dataPrepared.plainPassword = {
                'first': data.plainPassword.first,
                'second': data.plainPassword.second
            };
        }

        GenericService.save(_endpoint, dataPrepared).then(
            function(response) {
                $timeout(function(){
                    $scope.editPassword = false;
                    
                    if (response.data.locale) {
                        localStorage.setItem('lang', response.data.locale);
                        $window.location.reload();
                    }
                }, 1000);
            }
        );
    };
});
