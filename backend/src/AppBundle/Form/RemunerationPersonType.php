<?php

namespace AppBundle\Form;

use AppBundle\Form\RemunerationDayType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RemunerationPersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('workedDays')
            ->add('remuneration')
            ->add('period')
            ->add('remunerationStatement')
            ->add('compulsoryAccessionSubscription');

        $builder->add(
            'remunerationDays',
            CollectionType::class,
            array(
                'entry_type'   => RemunerationDayType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => false
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\RemunerationPerson',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_remunerationperson';
    }
}
