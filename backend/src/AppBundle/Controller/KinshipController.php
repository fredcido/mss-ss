<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Kinship;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class KinshipController extends BaseController
{
    protected $repository = 'AppBundle:Kinship';

    protected $form = 'AppBundle\Form\KinshipType';

    protected $routeGet = 'app.kinship.get';

    protected $entity = Kinship::class;
}
