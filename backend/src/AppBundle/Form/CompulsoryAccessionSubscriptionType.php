<?php

namespace AppBundle\Form;

use AppBundle\Form\WorkSituationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompulsoryAccessionSubscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateStart', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('dateEnd', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('incomeBase')
            ->add('compulsoryAccession')
            ->add('weeklyHours')
            ->add('weeklyDays')
            ->add('timePercentage')
            ->add('version')
            ->add('subscriptionStatus');

        $builder->add(
            'workSituation',
            WorkSituationType::class,
            array(
                'required'       => true,
                'error_bubbling' => false
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\CompulsoryAccessionSubscription',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_compulsoryaccessionsubscription';
    }
}
