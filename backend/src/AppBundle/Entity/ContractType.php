<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContractType
 *
 * @ORM\Table(name="contract_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContractTypeRepository")
 * @UniqueEntity(fields={"name", "labourLaw"}, message="name_already_use")
 */
class ContractType
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="LabourLaw", inversedBy="contractTypes")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="labour_law_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $labourLaw;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ContractType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of labourLaw.
     *
     * @return mixed
     */
    public function getLabourLaw()
    {
        return $this->labourLaw;
    }

    /**
     * Sets the value of labourLaw.
     *
     * @param mixed $labourLaw the labour law
     *
     * @return self
     */
    public function setLabourLaw($labourLaw)
    {
        $this->labourLaw = $labourLaw;
        return $this;
    }
}
