app.factory("requestInterceptor", function($rootScope, $q, $location, $window, $translate, $cookies, ViewHelper) {
    return {
        request: function(config) {
            if (config.beforeSend)
                config.beforeSend();

            config.headers = config.headers || {};

            //var token = $cookies.get('token');
            var token = localStorage.getItem('token');
            if (token) {
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        },
        requestError: function(request) {
            return $q.reject(request);
        },
        response: function(response) {
            if (response.config.complete)
                response.config.complete(response);

            return response || $q.when(response);
        },
        responseError: function(rejection) {
            switch (rejection.status) {
                case 404:
                    console.warn('404');
                    //swal($translate.instant('not_found_title'), $translate.instant('not_found_text'), 'error');
                    break;
                case 403:
                    console.warn('403');
                    swal($translate.instant('forbidden_title'), $translate.instant('forbidden_text'), 'error');
                    break;
                case 401:
                    console.warn('401');
                    //$cookies.remove('profile');
                    localStorage.removeItem('profile');
                    $window.location.href = "#!/login";
                    break;
                case 400:
                    console.warn('400');
                    if (rejection.data && 'errors' in rejection.data) {
                        $rootScope.serverErrors = rejection.data.errors;
                    }
                    break;
                case 503:
                    console.log('ERROR 503');
                    $window.location.href = '/maintenance.html';
                    break;
                default:
                    if (rejection.data && 'errors' in rejection.data) {
                        $rootScope.serverErrors = rejection.data.errors;
                    } else {
                        //swal($translate.instant('error_title'), $translate.instant('error_text'), 'error');
                    }
                    break;

            }

            ViewHelper.unblockPage();
            return $q.reject(rejection);
        }
    };
});
