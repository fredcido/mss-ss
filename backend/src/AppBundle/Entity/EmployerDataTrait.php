<?php

namespace AppBundle\Entity;

use AppBundle\Enum\Status;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
trait EmployerDataTrait
{
     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank", groups={"Default", "User"})
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="ActivitySector")
     * @JMS\Groups({"list", "form", "details"})
     * )
     */
    private $activitySectors;

    /**
     * @ORM\ManyToMany(targetEntity="Address", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details", "form"})
     *
     * @var Address
     */
    private $addresses;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStartActivity", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     *
     */
    private $dateStartActivity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEndActivity", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     *
     */
    private $dateEndActivity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEmployees", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     *
     */
    private $dateEmployees;


    /**
     * Add address
     *
     * @param Address $address
     *
     * @return Employer
     */
    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;
        return $this;
    }

    /**
     * @return self
     */
    public function cleanAddresses()
    {
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add activitySector
     *
     * @param ActivitySector $activitySector
     *
     * @return Employer
     */
    public function addActivitySector(ActivitySector $activitySector)
    {
        if ($this->activitySectors && $this->activitySectors->contains($activitySector)) {
            return $this;
        }

        $this->activitySectors[] = $activitySector;

        return $this;
    }

    /**
     * Remove activitySector
     *
     * @param ActivitySector $activitySector
     */
    public function removeActivitySector(ActivitySector $activitySector)
    {
        $this->activitySectors->removeElement($activitySector);
    }

    /**
     * Get activitySectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivitySectors()
    {
        return $this->activitySectors;
    }

    /**
     * Set dateStartActivity
     *
     * @param \DateTime $dateStartActivity
     *
     * @return Employer
     */
    public function setDateStartActivity($dateStartActivity)
    {
        $this->dateStartActivity = $dateStartActivity;

        return $this;
    }

    /**
     * Get dateStartActivity
     *
     * @return \DateTime
     */
    public function getDateStartActivity()
    {
        return $this->dateStartActivity;
    }

    /**
     * Set dateEmployees
     *
     * @param \DateTime $dateEmployees
     *
     * @return Employer
     */
    public function setDateEmployees($dateEmployees)
    {
        $this->dateEmployees = $dateEmployees;

        return $this;
    }

    /**
     * Get dateEmployees
     *
     * @return \DateTime
     */
    public function getDateEmployees()
    {
        return $this->dateEmployees;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of dateEndActivity.
     *
     * @return \DateTime
     */
    public function getDateEndActivity()
    {
        return $this->dateEndActivity;
    }

    /**
     * Sets the value of dateEndActivity.
     *
     * @param \DateTime $dateEndActivity the date end activity
     *
     * @return self
     */
    public function setDateEndActivity(\DateTime $dateEndActivity = null)
    {
        $this->dateEndActivity = $dateEndActivity;
        /*if ($this->dateEndActivity) {
            $this->setStatus(Status::INACTIVE);
        }*/
        return $this;
    }
}
