app.directive('hideNoChildren', function(AclService, $timeout) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs) {
            var selector = '>';
            if ($attrs.hideNoChildren) {
                selector = $attrs.hideNoChildren;
            }

            $scope.$watch(function() {
              if ($(selector, $element).not('.forbidden').length) {
                    $element.removeClass('forbidden').show();
                } else {
                    $element.addClass('forbidden').hide();
                }
            });
        }
    };
});
