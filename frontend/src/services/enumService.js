app.service("EnumService", function($translate) {
    var _getFileReferences = function() {
        return [{
            id: 'E',
            label: $translate.instant('employer')
        },
        {
            id: 'P',
            label: $translate.instant('employee')
        }];
    };

    var _getFileReferenceDescription = function(value) {
        var array = _getFileReferences();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var _getMonths = function() {
        var months = [{
            id: 1,
            label: $translate.instant('january')
        }, {
            id: 2,
            label: $translate.instant('february')
        }, {
            id: 3,
            label: $translate.instant('march')
        }, {
            id: 4,
            label: $translate.instant('april')
        }, {
            id: 5,
            label: $translate.instant('may')
        }, {
            id: 6,
            label: $translate.instant('june')
        }, {
            id: 7,
            label: $translate.instant('july')
        }, {
            id: 8,
            label: $translate.instant('august')
        }, {
            id: 9,
            label: $translate.instant('september')
        }, {
            id: 10,
            label: $translate.instant('october')
        }, {
            id: 11,
            label: $translate.instant('november')
        }, {
            id: 12,
            label: $translate.instant('december')
        }];

        return months;
    };

    var _getMonthDescription = function(value) {
        var months = _getMonths();
        var description = '';
        months.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var _getCapitais = function() {
        var capitais = [{
            id: 0,
            label: $translate.instant('public')
        }, {
            id: 1,
            label: $translate.instant('private')
        }];

        return capitais;
    };

    var _getCapitalDescription = function(value) {
        var capitais = _getCapitais();
        var description = '';
        capitais.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var _getAccountables = function() {
        var array = [{
            id: 0,
            label: $translate.instant('employer')
        }, {
            id: 1,
            label: $translate.instant('employee')
        }];

        return array;
    };

    var _getAccountablesDescription = function(value) {
        var array = _getAccountables();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var _getTypesValues = function() {
        var array = [{
            id: 0,
            label: $translate.instant('theoretical_value')
        }, {
            id: 1,
            label: $translate.instant('amount_paid')
        }, {
            id: 2,
            label: $translate.instant('contributed')
        }];

        return array;
    };

    var _getTypeValueDescription = function(value) {
        var array = _getTypesValues();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var _getReferencesValues = function() {
        var array = [{
            id: 0,
            label: $translate.instant('default')
        }, {
            id: 1,
            label: $translate.instant('activity')
        }, {
            id: 2,
            label: $translate.instant('equivalence')
        }, {
            id: 3,
            label: $translate.instant('voluntary')
        }];

        return array;
    };

    var _getReferenceValueDescription = function(value) {
        var array = _getReferencesValues();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.label;
            }
        });

        return description;
    };

    var subscriptionStatus = {
        'subscription_pending': '0',
        'subscription_submitted': '1',
        'subscription_error': '2',
        'subscription_approved': '3',
        'subscription_ceased': '4',
        'subscription_suspended': '5',
    };

    var _geSubscriptionStatuses = function() {
        var array = [];

        for (var label in subscriptionStatus) {
            array.push({id: subscriptionStatus[label], name: $translate.instant(label)});
        }

        return array;
    };

    var _geSubscriptionStatusesDescription = function(value) {
        var array = _geSubscriptionStatuses();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.name;
            }
        });

        return description;
    };

    var _getSubscriptionStatus = function(status) {
        var status = status.replace(/subscription_/, '');
        status = 'subscription_' + status;
        return parseInt(subscriptionStatus[status]);
    };

    var versions = {
        'standard': 0,
        'subscription': 1,
        'update': 2
    };

    var _getVersions = function() {
        var array = [];

        for (var label in versions) {
            array.push({id: versions[label], name: $translate.instant(label)});
        }

        return array;
    };

    var _getVersionDescription = function(value) {
        var array = _getVersions();
        var description = '';
        array.some(function(i) {
            if (i.id == value) {
                description = i.name;
            }
        });

        return description;
    };

    var _getVersion = function(status) {
        return parseInt(versions[status]);
    };

    return {
        getMonths: _getMonths,
        getMonthDescription: _getMonthDescription,
        getCapitais: _getCapitais,
        getCapitalDescription: _getCapitalDescription,
        getAccountables: _getAccountables,
        getAccountablesDescription: _getAccountablesDescription,
        getTypesValues: _getTypesValues,
        getTypeValueDescription: _getTypeValueDescription,
        getReferencesValues: _getReferencesValues,
        getReferenceValueDescription: _getReferenceValueDescription,
        getFileReferences: _getFileReferences,
        getFileReferenceDescription: _getFileReferenceDescription,
        geSubscriptionStatuses: _geSubscriptionStatuses,
        geSubscriptionStatusesDescription: _geSubscriptionStatusesDescription,
        getSubscriptionStatus: _getSubscriptionStatus,
        getVersions: _getVersions,
        getVersionDescription: _getVersionDescription,
        getVersion: _getVersion,
    };
});
