<?php

namespace AppBundle\Service;

use AppBundle\Entity\File;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Uploader
{
    use ContainerAwareTrait;

    protected $uploadFolder;
    protected $uploadPublic;

    public function setUploadFolder($uploadFolder)
    {
        $this->uploadFolder = $uploadFolder;
        return $this;
    }

    public function setUploadPublic($uploadPublic)
    {
        $this->uploadPublic = $uploadPublic;
        return $this;
    }

    /**
     * @param  File   $file
     * @return File
     */
    public function upload(File $file)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        $uploadFolder = $this->getUploadFolder();

        if (!$file->getEntity()) {
            $file->setTemporary(1);
            $uploadFolder = $this->getTmpFolder();
        }

        $uploadedFile = $file->getFile();

        $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();
        $file->setPath($fileName)
            ->setName($uploadedFile->getClientOriginalName())
            ->setReference($file->getConfiguration()->getReference());

        $uploadedFile->move($uploadFolder, $fileName);

        $em->persist($file);
        $em->flush();

        $this->injectPublicPath($file);

        return $file;
    }

    public function makePermanent(File $file)
    {
        $file->setTemporary(0);

        $path = $this->getTmpFolder() . DIRECTORY_SEPARATOR . $file->getPath();
        $newPath = $this->getUploadFolder() . DIRECTORY_SEPARATOR . $file->getPath();
        
        if (file_exists($path)) {
            rename($path, $newPath);
        }
    }

    public function injectPublicPath(File $file)
    {
        $uploadFolder = $this->uploadPublic;
        if ($file->getTemporary()) {
            $uploadFolder .= 'tmp';
        }

        $fullPath = $uploadFolder . $file->getPath();
        $file->setPath($fullPath);
    }

    public function injectFullPath(File $file)
    {
        $uploadFolder = $this->getUploadFolder();
        if ($file->getTemporary()) {
            $uploadFolder = $this->getTmpFolder();
        }

        $fullPath = $uploadFolder . DIRECTORY_SEPARATOR . $file->getPath();
        $file->setPath($fullPath);
    }

    public function delete(File $file)
    {
        $uploadFolder = $this->getUploadFolder();
        if ($file->getTemporary()) {
            $uploadFolder = $this->getTmpFolder();
        }

        $fullPath = $uploadFolder . DIRECTORY_SEPARATOR . $file->getPath();
        unlink($fullPath);
    }

    protected function getUploadFolder()
    {
        if (!is_dir($this->uploadFolder)) {
            mkdir($this->uploadFolder);
        }

        return $this->uploadFolder;
    }

    protected function getTmpFolder()
    {
        $tmpFolder = $this->getUploadFolder() . DIRECTORY_SEPARATOR . 'tmp';
        if (!is_dir($tmpFolder)) {
            mkdir($tmpFolder);
        }

        return $tmpFolder;
    }
}
