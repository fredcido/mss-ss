app.component('accessionAbroad', {
    templateUrl: 'views/components/accessionAbroad.html',
    bindings: {
        model: '=',
        disabled: '<'
    },
    controller: function($translate, $timeout, GenericService) {
        var self = this;

        var getCountries = function() {
            GenericService.find('country').then(function(response) {
                self.countries = response.data;
            });
        };

        self.$onInit = function() {
            self.countries = [];
            getCountries();
        };

        self.$onChanges = function(changesObj) {
            
        };
    }
});
