<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class DocumentController extends BaseController
{
    protected $repository = 'AppBundle:Document';

    protected $form = 'AppBundle\Form\DocumentType';

    protected $entity = Document::class;

    protected function getOrderBy()
    {
        return array('title' => 'asc');
    }

    protected function getUploadFolder()
    {
        $uploadFolder = $this->getParameter('files_directory');
        if (!is_dir($uploadFolder)) {
            mkdir($uploadFolder);
        }

        return $uploadFolder;        
    }

    protected function handleForm($entity, Request $request, $statusCode = Response::HTTP_OK)
    {
        $uploadFolder = $this->getUploadFolder();

        if ($entity->getPath()) {
            $oldFile = $entity->getPath();
        }

        $form = $this->createForm($this->form, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($entity->getFile()) {
                $file = $entity->getFile();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($uploadFolder, $fileName);

                if (!empty($oldFile)) {
                    $fullOldFile = $uploadFolder . '/' . $oldFile;
                    if (file_exists($fullOldFile)) {
                        unlink($fullOldFile);
                    }
                }

                $entity->setPath($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Remove the cached data
            if ($this->getParameter('use_cache') && $this->has('cache')) {
                $cache = $this->get('cache');
                $cache->delete($this->repository);
            }
        } else {
            $this->throwApiProblemValidationException($form);
        }

        return $this->createApiResponse($entity, Response::HTTP_OK, array('list', 'audit'));
    }

    protected function handleFindEntities($entities)
    {
        $uploadPublic = $this->getParameter('uploads_public');

        foreach ($entities as $entity) {
            $entity->setPath($uploadPublic . $entity->getPath());
        }

        return $entities;
    }
}
