app.controller("OptionalStepFormController", function($scope, $stateParams, GenericService, SettingService) {
    var _endpoint = 'optional-step';
    var _stateList = 'app.optional-step-list';
    $scope.saii = 0;

    var getSaii = function() {
        $scope.saii = SettingService.get('saii');
        calcCost();
    };

    var calcCost = function() {
        $scope.model.cost = $scope.model.indexer * $scope.saii;
    };

    $scope.$watch('model.indexer', function(){
        calcCost();
    });

    $scope.id = $stateParams.id || null;
    $scope.model = {
        status: 1, 
        indexer: 1, 
        cost: 0, 
        maxAgeAccession: 1, 
        decrease: 1, 
        increase: 1, 
        increasePaidMonths: 12 
    };

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };

    getSaii();
});
