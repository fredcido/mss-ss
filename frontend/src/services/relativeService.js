app.service("RelativeService", function($http, AppConstant, EnumService, AddressService) {

    var _populateModel = function(data) {
        var model = data;

        if (model.inSchool) {
            model.inSchool = 1;
        }

        if (model.sinceWhen) {
            model.sinceWhen = moment(model.sinceWhen, "YYYY/MM/DD").toDate();
        }

        return model;
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);
        if (data.kinship) {
            data.kinship = data.kinship.id;
        }

        if (data.schoolYear) {
            data.schoolYear = data.schoolYear.id;
        }

        if(data.person && data.person.id){
            data.person = data.person.id;
        }

        if(data.relative){
            data.relative = data.relative.id;
        }

        if (data.sinceWhen) {
            data.sinceWhen = moment(data.sinceWhen).format('YYYY-MM-DD');
        }

        return data;
    };

    var _initModel = function() {
        var model = {
            relative: {}
        };

        return model;
    };

    return {
        initModel: _initModel,
        populateModel: _populateModel,
        preparateData: _preparateData
    };
});
