app.component('personDisplay', {
    templateUrl: 'views/components/personDisplay.html',
    bindings: {
        person: '<',
        displayAddress: '<'
    },
    controller: function($translate, $timeout) {
        var self = this;

        if (!angular.isDefined(self.displayAddress)) {
            self.displayAddress = true;
        }
    }
});
