app.directive('maxDate', [function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            scope.$watch(function() {
                return [scope.$eval(attrs.maxDate), ctrl.$modelValue];
            }, function(values) {
                if (values[0] && values[1]) {
                    var precision = attrs.maxDatePrecision || "day";

                    var dateStart = moment(values[1]);
                    var dateEnd = moment(values[0]);

                    ctrl.$setValidity('maxDate', dateEnd.isSameOrAfter(dateStart, precision));
                    ctrl.$dirty = true;
                } else {
                    ctrl.$setValidity('maxDate', true);
                }
            }, true);
        }
    };
}]);
