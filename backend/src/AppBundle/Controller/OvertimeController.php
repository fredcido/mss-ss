<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Overtime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class OvertimeController extends BaseController
{
    protected $repository = 'AppBundle:Overtime';

    protected $form = 'AppBundle\Form\OvertimeType';

    protected $routeGet = 'app.overtime.get';

    protected $entity = Overtime::class;

    protected function getOrderBy()
    {
        return array('ratio' => 'asc');
    }
}
