<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Naturality
 *
 * @ORM\Table(name="naturality")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NaturalityRepository")
 */
class Naturality
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cityAbroad", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $cityAbroad;

    /**
     * @var Village
     *
     * @ORM\ManyToOne(targetEntity="Village")
     * @JMS\Groups({"list", "details"})
     */
    private $village;

    /**
     * @ORM\ManyToOne(targetEntity="PostoAdministrativo")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $postoAdministrativo;

    /**
     * @var Suku
     *
     * @ORM\ManyToOne(targetEntity="Suku")
     * @JMS\Groups({"list", "details"})
     */
    private $suku;

    /**
     * @ORM\ManyToOne(targetEntity="Municipio")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $municipio;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @JMS\Groups({"list", "details"})
     */
    private $country;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cityAbroad
     *
     * @param string $cityAbroad
     *
     * @return Naturality
     */
    public function setCityAbroad($cityAbroad)
    {
        $this->cityAbroad = $cityAbroad;
        return $this;
    }

    /**
     * Get cityAbroad
     *
     * @return string
     */
    public function getCityAbroad()
    {
        return $this->cityAbroad;
    }

    /**
     * Set village
     *
     * @param Village $village
     *
     * @return Naturality
     */
    public function setVillage(Village $village = null)
    {
        $this->village = $village;
        return $this;
    }

    /**
     * Get village
     *
     * @return Village
     */
    public function getVillage()
    {
        return $this->village;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Naturality
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set postoAdministrativo
     *
     * @param \AppBundle\Entity\PostoAdministrativo $postoAdministrativo
     *
     * @return Naturality
     */
    public function setPostoAdministrativo(\AppBundle\Entity\PostoAdministrativo $postoAdministrativo = null)
    {
        $this->postoAdministrativo = $postoAdministrativo;

        return $this;
    }

    /**
     * Get postoAdministrativo
     *
     * @return \AppBundle\Entity\PostoAdministrativo
     */
    public function getPostoAdministrativo()
    {
        return $this->postoAdministrativo;
    }

    /**
     * Set suku
     *
     * @param \AppBundle\Entity\Suku $suku
     *
     * @return Naturality
     */
    public function setSuku(\AppBundle\Entity\Suku $suku = null)
    {
        $this->suku = $suku;

        return $this;
    }

    /**
     * Get suku
     *
     * @return \AppBundle\Entity\Suku
     */
    public function getSuku()
    {
        return $this->suku;
    }

    /**
     * Set municipio
     *
     * @param \AppBundle\Entity\Municipio $municipio
     *
     * @return Naturality
     */
    public function setMunicipio(\AppBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return \AppBundle\Entity\Municipio
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }
}
