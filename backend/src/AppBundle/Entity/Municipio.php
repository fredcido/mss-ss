<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Municipio
 *
 * @ORM\Table(name="municipio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MunicipioRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 */
class Municipio
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use ReferenceCodeTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="PostoAdministrativo", mappedBy="municipio")
     */
    private $postosAdministrativos;

    public function __construct()
    {
        $this->postosAdministrativos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Municipio
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add postoAdministrativos
     *
     * @param PostoAdministrativo $postoAdministrativo
     * @return Race
     */
    public function addPostoAdministrativo(PostoAdministrativo $postoAdministrativo)
    {
        $this->postosAdministrativos[] = $postoAdministrativo;
        return $this;
    }

    /**
     * Remove postoAdministrativo
     *
     * @param PostoAdministrativo $postoAdministrativo
     */
    public function removePostoAdministrativo(PostoAdministrativo $postoAdministrativo)
    {
        $this->postosAdministrativos->removeElement($postoAdministrativo);
    }

    /**
     * Get postoAdministrativos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostoAdministrativos()
    {
        return $this->postosAdministrativos;
    }

    /**
     * Add postosAdministrativo
     *
     * @param \AppBundle\Entity\PostoAdministrativo $postosAdministrativo
     *
     * @return Municipio
     */
    public function addPostosAdministrativo(\AppBundle\Entity\PostoAdministrativo $postosAdministrativo)
    {
        $this->postosAdministrativos[] = $postosAdministrativo;

        return $this;
    }

    /**
     * Remove postosAdministrativo
     *
     * @param \AppBundle\Entity\PostoAdministrativo $postosAdministrativo
     */
    public function removePostosAdministrativo(\AppBundle\Entity\PostoAdministrativo $postosAdministrativo)
    {
        $this->postosAdministrativos->removeElement($postosAdministrativo);
    }

    /**
     * Get postosAdministrativos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostosAdministrativos()
    {
        return $this->postosAdministrativos;
    }
}
