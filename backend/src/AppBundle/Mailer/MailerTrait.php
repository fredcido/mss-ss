<?php

namespace AppBundle\Mailer;

use AppBundle\Entity\EmailQueue;

trait MailerTrait
{
    protected $prefix = '[MSS - SISS]';

    /**
     * @param  string $title
     * @return string
     */
    public function buildTitle($title)
    {
        $translator = $this->container->get('translator');
        return sprintf("%s - %s", $this->getPrefix(), $translator->trans($title));
    }

    /**
     * Gets the value of prefix.
     *
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @return EmailQueue
     */
    public function newEmailQueue()
    {
        $mailFrom = $this->container->getParameter('mail_from');
        $mailName = $this->container->getParameter('mail_name');

        $fromMail = [$mailFrom => $mailName];

        $emailQueue = new EmailQueue();
        $emailQueue->setSent(0)->setFromMail($fromMail);

        return $emailQueue;
    }

    /**
     * @param  EmailQueue $emailQueue
     */
    public function saveEmail(EmailQueue $emailQueue)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($emailQueue);

        return $emailQueue;
    }

    /**
     * @param  string $template
     * @param  array $context
     * @return string
     */
    public function renderBody($template, $context)
    {
        $templating = $this->container->get('templating');
        return $templating->render(
            $template,
            $context
        );
    }
}
