app.controller(
    "MainController", 
    function(
        $scope, 
        AuthService
    ) 
{
    $scope.user = AuthService.getUserLogged();
});
