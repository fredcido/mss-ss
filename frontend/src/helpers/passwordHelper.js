app.service("PasswordHelper", function($translate, $rootScope) {
    
    var _generatePassword = function(length)
    {
        var length = length || 8;
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    };

    return {
        generatePassword: _generatePassword
    };
});
