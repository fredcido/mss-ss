app.component('employerHeader', {
    templateUrl: 'views/components/employerHeader.html',
    bindings: {
        employer: '=',
        disabled: '<',
        onUpdate: '&'
    },
    controller: function($scope, $translate, $timeout, GenericService, AuthService, ViewHelper, AuthService) {
        var self = this;
        self.user = AuthService.getUserLogged();

        var codeLabels = {
            'taxNumber': $translate.instant('tax_number')
        };

        var user = AuthService.getUserLogged();

        var getCodeLabel = function(code) {
            return codeLabels[code];
        };

        self.displayAllData = function() {
            if (self.employer) {
                self.employer.displayAllData = self.employer.id || (self.employer && self.employer.taxNumber && self.employer.name && angular.isDefined(self.employer.notDuplicated));
                return self.employer.displayAllData;
            } else {
                return false;
            }
        };

        self.setEmployer = function(entity) {
            $scope.$apply(function() {
                self.employer.notDuplicated = true;
                self.onUpdate({
                    employer: entity
                });
            });
        };

        self.cleanEmployer = function(value) {
            $scope.$apply(function() {
                self.employer[value] = null;
                self.employer.notDuplicated = false;
            });
        };

        self.onNotFound = function() {
            self.employer.notDuplicated = true;
        };

        self.foundEmployer = function(entity, value) {
            if (self.employer && entity.internalCode == self.employer.internalCode) {
                return false;
            }

            if (self.user.isEmployer()) {
                self.employer[value] = null;
                
                var attributeDash = value.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
                var replacements = {'code': $translate.instant(attributeDash), 'value': entity[value]};
                var message = $translate.instant('employer_inserted_message', replacements);

                swal({
                    title: $translate.instant('employer_inserted'),
                    text: message,
                    type: 'warning'
                });

                return false;
            } else {
                return true;
            }
        };
    }
});
