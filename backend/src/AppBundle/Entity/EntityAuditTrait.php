<?php

namespace AppBundle\Entity;

use AppBundle\Enum\Status;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 */
trait EntityAuditTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form", "audit"})
     */
    protected $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"audit"})
     */
    protected $created_by;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @JMS\Groups({"audit"})
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=true)
     * @JMS\Groups({"audit"})
     */
    protected $updated_by;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint", options={"default" : 1})
     * @JMS\Groups({"list", "details", "audit"})
     */
    private $status = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="justification", type="text", nullable=true)
     * @JMS\Groups({"list", "details", "form", "audit"})
     */
    protected $justification;

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return \AppBundle\Entity\EntityAuditTrait
     */
    public function setCreatedAt(\DateTime $created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set created_by
     *
     * @param int $created_by
     * @return \AppBundle\Entity\EntityAuditTrait
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;

        return $this;
    }

    /**
     * Get created_by
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return \AppBundle\Entity\EntityAuditTrait
     */
    public function setUpdatedAt(\DateTime $updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set updated_by
     *
     * @param int $updated_by
     * @return \AppBundle\Entity\EntityAuditTrait
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * Get updated_by
     *
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * Gets the value of status.
     *
     * @return smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the value of status.
     *
     * @param smallint $status the status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Gets the value of justification.
     *
     * @return string
     */
    public function getJustification()
    {
        return $this->justification;
    }

    /**
     * Sets the value of justification.
     *
     * @param string $justification the justification
     *
     * @return self
     */
    public function setJustification($justification)
    {
        $this->justification = $justification;
        return $this;
    }

    /**
     * @param  string  $status
     * @return boolean
     */
    public function isOfStatus($status)
    {
        return $this->getStatus() == $status;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->isOfStatus(Status::ACTIVE);
    }

    /**
     * @return boolean
     */
    public function isInactive()
    {
        return $this->isOfStatus(Status::INACTIVE);
    }
}
