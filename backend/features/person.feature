Feature: Manage People data via the RESTful API

Scenario: Finding a User
    Given that I want to find a "User"
    And that its "name" is "Chris"
    When I request "/user"
    Then the "name" property equals "Chris"