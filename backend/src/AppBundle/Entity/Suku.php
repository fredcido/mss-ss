<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Suku
 *
 * @ORM\Table(name="suku")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SukuRepository")
 */
class Suku
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use ReferenceCodeTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PostoAdministrativo", inversedBy="sukus")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="posto_administrativo_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $postoAdministrativo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Village", mappedBy="suku")
     */
    private $villages;

    public function __construct()
    {
        $this->villages = new ArrayCollection();
    }

    /**
     * Add village
     *
     * @param Village $village
     * @return Suku
     */
    public function addVillage(Village $village)
    {
        $this->villages[] = $village;
        return $this;
    }

    /**
     * Remove village
     *
     * @param Village $village
     */
    public function removeVillage(Village $village)
    {
        $this->villages->removeElement($village);
    }

    /**
     * Get villages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVillages()
    {
        return $this->villages;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Suku
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getPostoAdministrativo()
    {
        return $this->postoAdministrativo;
    }

    /**
     * Set postoAdministrativo
     *
     * @param \AppBundle\Entity\PostoAdministrativo $postoAdministrativo
     *
     * @return Suku
     */
    public function setPostoAdministrativo(\AppBundle\Entity\PostoAdministrativo $postoAdministrativo)
    {
        $this->postoAdministrativo = $postoAdministrativo;

        return $this;
    }
}
