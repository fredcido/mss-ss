<?php

namespace AppBundle\Service;

use AppBundle\Entity\Employer as EmployerEntity;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Employer
{
    use ContainerAwareTrait;

    public function getValidMailAddress(EmployerEntity $employer)
    {
        // Check whether the employer is related to an user
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['employer' => $employer]);

        $mailAddress = null;
        if (empty($user)) {
            $contact = $employer->getContactDetail();
            $addresses = $contact->getEmail();
            if (!empty($addresses)) {
                $mailAddress = array_shift($addresses);
            }
        } else {
            $mailAddress = $user->getEmail();
        }

        return $mailAddress;
    }
}
