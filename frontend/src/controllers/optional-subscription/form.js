app.controller("OptionalSubscriptionFormController", 
    function(
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $q,
        $rootScope,
        ViewHelper, 
        $translate, 
        AuthService,
        GenericService, 
        PersonService, 
        EmployerService,
        RelativeService, 
        EnumService,
        OptionalAccessionService
    ) {

    $scope.id = $stateParams.id || null;

    $scope.accession = null;
    $scope.subscription = null;
    $scope.employer = null;
    $scope.standardVersion = null;
    $scope.user = AuthService.getUserLogged();

    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.version = $stateParams.version;
    
    $scope.today = new Date();
    $scope.maxDate = $scope.today;

    $scope.disabilities = [];
    $scope.nationalities = [];
    $scope.maritalStatuses = [];
    $scope.activitiesSectorsTree = [];
    $scope.optionalSteps = [];
    $scope.optionalContributionGroups = [];
    $scope.occupations = [];
    $scope.posts = [];
    
    _personEndpoint = 'person';
    _acessionEndpoint = 'optional-accession';
    _subscriptionEndpoint = 'optional-subscription';

    $scope.currentSteps = ['info'];

    var releaseSteps = function() {
        $timeout(function(){
            $scope.currentSteps = ['info'];

            if ($scope.person.id) {
                $scope.currentSteps.push('relative');
                $scope.currentSteps.push('employment');
                $scope.currentSteps.push('step');
                $scope.currentSteps.push('files');
            }
        });
    };

    var mixPersonModel = function(source, target) {
        var model = PersonService.populateModel(source);
        return angular.extend(model, target);
    };

    var getDisabilities = function() {
        GenericService.find('disability').then(function(response) {
            $scope.disabilities = response.data;
        });
    };

    var getMaritalStatuses = function() {
        GenericService.find('marital-status').then(function(response) {
            $scope.maritalStatuses = response.data;
        });
    };

    var getNationalities = function() {
        GenericService.find('country').then(function(response) {
            $scope.nationalities = response.data;
        });
    };

    var getActivitiesSectors = function () {
        GenericService.httpGet('activity-sector/tree').then(function (response) {
            $scope.activitiesSectorsTree = GenericService.addTreeLevel(response.data);
        });
    };

    var getOptionalContributionGroups = function() {
        GenericService.find('optional-contribution-group').then(function(response) {
            $scope.optionalContributionGroups = response.data;
        });
    };

    var getOptionalSteps = function() {
        if (!$scope.person.id) {
            return;
        }

        var age = angular.isUndefined($scope.person.age) ? 0 : parseInt($scope.person.age);
        ViewHelper.blockContainer('.step-container', $translate.instant('loading'));
        GenericService.httpGet('optional-step/person/' + $scope.person.id + '/allowed/' + age).then(function(response) {
            $scope.optionalSteps = response.data;
            ViewHelper.unblockContainer('.step-container');
        });
    };

    var getCurrentOptionalStep = function() {
        GenericService.httpGet('optional-step/person/' + $scope.person.id + '/current').then(function(response) {
            $scope.currentOptionalStep = response.data;
        });
    };

    var getOccupations = function() {
        GenericService.find('occupation').then(function(response) {
            $scope.occupations = response.data;
        });
    };

    var getPosts = function() {
        GenericService.find('post').then(function(response) {
            $scope.posts = response.data;
        });
    };

    var getEmployersByStaff = function() {
        var url = 'employer/search';
        var filters = {
            staffs: $scope.person.id
        };

        GenericService.httpGet(url, filters).then(function(response){
            var data = response.data;
            data.map(function(item){
                return EmployerService.populateModel(item);
            })
            $scope.employers = data;
        });
    };

    var callBackErrorSubmit = function(form) {
        return function(response) {
            ViewHelper.errorServer(form, response);
        }
    };

    var afterSaveInformation = function(data) {
        initPerson(data.internalCode).then(function(){
            ViewHelper.scrollTo('.relative-container');
        });
    };

    var getVersion = function(version) {
        var version = version || EnumService.getVersion('subscription');
        return 'OPTIONAL_' + version;
    };

    var checkCompulsorySubscription = function() {
        var uri = 'compulsory-subscription/' + $scope.person.id + '/active';
        return GenericService.httpGet(uri).then(function(response) {
            if (response.data && response.data.length) {
                swal({ 
                    title: $translate.instant('optional_subscription_employee'),
                    text: $translate.instant('optional_subscription_employee'),
                    type: "error" 
                  },
                  function(){
                    $timeout(function(){
                        $state.go('app.home');
                    });
                });

                return $q.reject(null);
            } else {
                return $q.when($scope.person);
            }
        });
    };

    var setPersonCallback = function() {
        getAccession();
        getOptionalSteps();
        getEmployersByStaff();

        releaseSteps();

        $timeout(function(){
            ViewHelper.scrollTo('.identification-container');
        }, 500);

        //ViewHelper.unblockContainer('.container');

        if (!$scope.updateSubscription) {
            return checkCompulsorySubscription();
        } else {
            return $q.when();
        }
    };

    var initPerson = function(id) {
        $scope.id = id;
        ViewHelper.blockContainer('.container', $translate.instant('loading'));

        if ($scope.version) {
            return GenericService.get(_personEndpoint, $scope.version)
                    .then(function(response){
                        $scope.person = PersonService.populateModel(response.data);
                        return setPersonCallback();
                    });
        }

        return GenericService.getVersion(_personEndpoint, id, EnumService.getVersion('standard'))
            .then(function(response){
                $scope.standardVersion = PersonService.populateModel(response.data);
                return $q.when(response);
            })
            .then(function(response){
                if ($scope.updateSubscription) {
                    return $q.when(response);
                }

                return GenericService.getVersion(_personEndpoint, id, getVersion())
            })
            .then(function(response){
                $scope.person = PersonService.populateModel(response.data);
                return setPersonCallback();
            })
            .catch(function(error){
                if (404 != error.status) return;
                $scope.person = $scope.standardVersion;
                return setPersonCallback();
            })/*.finally(function(){
                ViewHelper.unblockContainer('.container');
            })*/;
    };

    var initAccession = function() {
        $scope.accession = OptionalAccessionService.initModel();
        $scope.accession.person = $scope.person;
        
        $scope.subscription = OptionalAccessionService.initSubscriptionModel();
    };

    var getAccession = function() {
        $scope.accession = null;
        $scope.subscription = null;

        //ViewHelper.blockContainer('.container', $translate.instant('loading'));
        var url = 'optional-accession/person/' + $scope.person.id + '/latest';

        var defaultAccessionPromise = null;
        // If we are not already checking the standard version or just seeing a version
        if (!$scope.version && $scope.person.id != $scope.standardVersion.id) {

            // Search for the standard accession
            var urlStandard = 'optional-accession/person/' + $scope.standardVersion.id + '/latest';            
            defaultAccessionPromise = GenericService.httpGet(urlStandard).then(function(response) {
                $scope.standardAccession = OptionalAccessionService.populateModel(response.data);
                return $q.when();
            });
        } else {
            defaultAccessionPromise = $q.when();
        }

        defaultAccessionPromise.then(function(){
            // If we have a standard accession and it is ceased, re-start the proccess
            if ($scope.standardAccession && $scope.standardAccession.isCeased()) {
                $scope.person = $scope.standardVersion;
                initAccession();
            } else {
                GenericService.httpGet(url).then(function(response) {
                    $scope.accession = OptionalAccessionService.populateModel(response.data);
                    getSubscription();
                }, function(){
                    initAccession();
                    ViewHelper.unblockContainer('.container');
                });
            }
        });

    };

    var getSubscription = function() {
        //ViewHelper.blockContainer('.container', $translate.instant('loading'));
        GenericService.httpGet('optional-subscription/person/' + $scope.person.id + '/current').then(function(response) {
            $scope.subscription = OptionalAccessionService.populateSubscriptionModel(response.data);
        }, function(){
            $scope.subscription = OptionalAccessionService.initSubscriptionModel();
            $scope.subscription.optionalAccession = $scope.accession;
        }).finally(function(){
            ViewHelper.unblockContainer('.container');
        });
    };

    var init = function() {
        getDisabilities();
        getMaritalStatuses();
        getNationalities();
        getActivitiesSectors();
        getOptionalContributionGroups();
        getOccupations();
        getPosts();

        if ($scope.user.isPerson()) {
            $scope.id = $scope.user.person.internalCode;
        }

        if ($scope.id) {
            initPerson($scope.id);
        } else {
            $scope.person = PersonService.initModel();
            releaseSteps();
        }
    };

    var saveAccession = function() {
        $scope.accession.person = $scope.person;
        var accession = OptionalAccessionService.preparateData($scope.accession);
        var urlAccession = _acessionEndpoint;

        if (accession.id) {
            urlAccession += '/' + accession.id + '/edit';
        } else {
            urlAccession += '/add';
        }

        return GenericService.httpPost(urlAccession, accession);
    };

    var saveSubscription = function() {
        $scope.subscription.optionalAccession = $scope.accession;
        
        var subscription = OptionalAccessionService.preparateSubscriptionData($scope.subscription);
        var urlSubscription = _subscriptionEndpoint;

        if (subscription.id) {
            urlSubscription += '/' + subscription.id + '/edit';
        } else {
            urlSubscription += '/add';
        }

        // If it's updating the standard version, save a update version of it
        if ($scope.updateSubscription && subscription.version == EnumService.getVersion('standard')) {
            subscription.version = getVersion(EnumService.getVersion('update'));
        }

        return GenericService.httpPost(urlSubscription, subscription);
    };

    var saveAll = function(form) {
        var callbackError = function(response) {
            ViewHelper.errorServer(form, response);
            ViewHelper.unblockContainer('.container');

            return $q.reject(response);
        };

        ViewHelper.blockContainer('.container', $translate.instant('data_saving'));
        $timeout(function() {
            ViewHelper.scrollTo('.container .blockMsg');
        }, 500);

        var person = PersonService.preparateData($scope.person);
        var personPromise = GenericService.httpPost(_personEndpoint + '/' + person.id + '/edit', person);

        // Save the person data
        return personPromise.then(function(){
            // Save the accession
            return saveAccession();
        }).then(function(accession){
            $scope.accession = OptionalAccessionService.populateModel(accession.data);

            // Save subscription data
            return saveSubscription();
        }).then(function(subscription){
            $scope.subscription = OptionalAccessionService.populateSubscriptionModel(subscription.data);

            var title, message, callback;

            // When the subscription is submitted
            if ($scope.isSubmitted()) {

                title = $translate.instant('suscription_submitted');
                message = $translate.instant('person_suscription_submitted_text');

                callback = function() {
                    $timeout(function() {
                        ViewHelper.blockContainer('.container', $translate.instant('loading'));
                        // Update the user to release couple of things
                        AuthService.setUser($scope.user).then(
                            function(){
                                $rootScope.$broadcast('userChanged');
                                ViewHelper.unblockContainer('.container');

                                if ($scope.updateSubscription) {
                                    $state.go('app.optional-subscription-list-changes', {
                                        id: $scope.id
                                    });
                                } else {
                                    $state.go('app.optional-subscription-thankyou');
                                }
                            }
                        );
                    }, 500);
                };
            } else {
                title = $translate.instant('worker_save_title');
                message = $translate.instant('worker_save_text');

                callback = function() {
                    $scope.submit(form);
                };
            }

            swal({
                    title: title,
                    text: message,
                    type: 'success',
                    timer: 2000,
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (!isConfirm) {
                        $timeout(function() {
                            ViewHelper.scrollTo('.inss-logo');
                        }, 500);
                    } else {
                        callback();
                    }
                }
            );

            $timeout(function(){
                $scope.person = mixPersonModel($scope.accession.person, $scope.person);
                $scope.accession.person = $scope.person;
                ViewHelper.scrollTo('.inss-logo');
            }, 300);

            ViewHelper.unblockContainer('.container');

            return $q.when();
        }).catch(callbackError);
    };
    
    $scope.isCurrentStep = function(step) {
        return $scope.currentSteps.indexOf(step) > -1;
    };

    $scope.isSubmitted = function() {
        return $scope.accession && 
            (1 == $scope.accession.subscriptionStatus || 3 == $scope.accession.subscriptionStatus);
    };

    $scope.saveInformation = function(form, model) {
        if (form.$valid) {
            var data = PersonService.preparateData(model);

            if ($scope.id) {
                GenericService.edit(_personEndpoint, $scope.id, data, afterSaveInformation, callBackErrorSubmit(form));
            } else {
                GenericService.add(_personEndpoint, data, afterSaveInformation, callBackErrorSubmit(form));
            }
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.setWorker = function(person) {
        $timeout(function() {
            $state.go('app.optional-subscription-form', {
                id: person.internalCode,
                ignoreLocalStorage: true,
                updateSubscription: 0
            });
        }, 500);
    };

    $scope.formatOptionalStep = function(item) {
        return OptionalAccessionService.formatOptionalStep(item);
    };

    $scope.setCompany = function(employer) {
        $scope.optionalAccession.employer = employer;
    };

    $scope.save = function(form) {
        if (form.$valid) {
            if (form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_worker'),
                    type: 'warning'
                });
                return false;
            }

            saveAll(form);
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.submit = function(form) {
        if (form.$valid) {

            if ($scope.updateSubscription && form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_worker'),
                    type: 'warning'
                });
                return false;
            }

            // Submit the accession
            var oldSubscriptionStatus = $scope.accession.subscriptionStatus;
            var oldVersion = $scope.subscription.version;

            if ($scope.updateSubscription && $scope.subscription.version == EnumService.getVersion('standard')) {
                $scope.subscription.version = getVersion(EnumService.getVersion('update'));
            } else {
                $scope.subscription.version = getVersion();
            }

            $scope.accession.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');

            form.$submitted = true;
            saveAll(form).catch(function(){
                $scope.accession.subscriptionStatus = oldSubscriptionStatus;
                $scope.subscription.version = oldVersion;
            });
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.canEdit = function() {
        var invalidStatuses = [
            EnumService.getSubscriptionStatus('suspended'),
            EnumService.getSubscriptionStatus('ceased')
        ];

        return $scope.user.isPerson() 
                && !$scope.version
                && (!$scope.accession || !$scope.accession.isOfAnyStatus(invalidStatuses))
                && ($scope.updateSubscription || !$scope.isSubmitted());
    };

    $scope.$watch('person.age', function(value) {
        if (value) {
            getOptionalSteps();
        }
    });

    init();
});
