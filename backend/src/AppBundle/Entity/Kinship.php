<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Kinship
 *
 * @ORM\Table(name="kinship")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KinshipRepository")
 * @UniqueEntity(fields="name", message="name_already_user")
 */
class Kinship
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     *
     */
    private $name;

    /**
     * @var Kinship
     *
     * @ORM\ManyToOne(targetEntity="Kinship")
     * @JMS\Groups({"list", "details"})
     * @JMS\MaxDepth(depth=1)
     *
     */
    private $inverse;

    /**
     * @var int
     *
     * @ORM\Column(name="requireDate", type="smallint", options={"default" : 0})
     * @JMS\Groups({"list", "details", "form"})
     */
    private $requireDate = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Kinship
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of inverse.
     *
     * @return Kinship
     */
    public function getInverse()
    {
        return $this->inverse;
    }

    /**
     * Sets the value of inverse.
     *
     * @param Kinship $inverse the inverse
     *
     * @return self
     */
    public function setInverse(Kinship $inverse = null)
    {
        $this->inverse = $inverse;

        return $this;
    }

    /**
     * Gets the value of requireDate.
     *
     * @return int
     */
    public function getRequireDate()
    {
        return $this->requireDate;
    }

    /**
     * Sets the value of requireDate.
     *
     * @param int $requireDate the require date
     *
     * @return self
     */
    public function setRequireDate($requireDate)
    {
        $this->requireDate = (int)$requireDate;
        return $this;
    }
}
