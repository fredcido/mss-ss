app.service("PersonService", function($http, AppConstant, EnumService, AddressService, NaturalityService) {

    var _populateModel = function(model) {
        var model = angular.copy(model);
        model.dob = moment(model.dob, "YYYY/MM/DD").toDate();

        if (model.dod) {
            model.dod = moment(model.dod, "YYYY/MM/DD").toDate();
        }

        if (!model.contactDetail) {
            model.contactDetail = {
                email: [],
                telephones: []
            };
        }

        if (model.address) {
            model.address = AddressService.populateModel(model.address);
        }

        if (model.naturality) {
            model.naturality = NaturalityService.populateModel(model.naturality);
        }

        /*if (model.bankInformation) {
            model.bankInformation.map(function(i) {
                if (i.bank) {
                    i.bank = i.bank.id;
                }
            });
        }*/

        _attachMethods(model);

        return model;
    };

    var _attachMethods = function(model) {
        var securityCode = '';
        if (model.securityCode) {
            securitCode = ' [' + model.securityCode + ']';
        }

        model.isNational = function() {
            return model.nationalities && model.nationalities.some(function(item){return (/^timor/gi).exec(item.name);});
        };

        model.displayName = model.internalCode + ' - ' + model.name + securityCode;
    }; 

    var _fetchCurrentContribution = function(person)
    {
        return $http({
            method: 'GET',
            url: AppConstant.apiUrl + 'contribution/' + person + "/current"
        });
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.dob) {
            data.dob = moment(data.dob).format('YYYY-MM-DD');
        }

        if (data.dod) {
            data.dod = moment(data.dod).format('YYYY-MM-DD');
        }

        if (data.nationalities) {
            data.nationalities = data.nationalities.map(function(i) {
                return i.id;
            });
        }
        
        if (data.address) {
            data.address = AddressService.preparateData(data.address);
        }

        if (data.naturality) {
            data.naturality = NaturalityService.preparateData(data.naturality);
        } else {
            delete data.naturality;
        }

        if (data.bankInformation) {
            data.bankInformation.map(function(i) {
                if (i.status === undefined) {
                    i.status = 1;
                }

                if (i.bank) {
                    i.bank = i.bank.id;
                }
            });
        }

        if (angular.isDefined(data.contactDetail) && !data.contactDetail.email) {
            data.contactDetail.email = [];
        }

        if (data.disability) {
            data.disability = data.disability.id;
        }

        if (data.maritalStatus) {
            data.maritalStatus = data.maritalStatus.id;
        }

        if (data.statusHistory) {
            delete data.statusHistory;
        }

        if (data.relatives) {
            delete data.relatives;
        }

        if (data.compulsorySubscriptions) {
            delete data.compulsorySubscriptions;
        }

        if (data.compulsoryAccessions) {
            delete data.compulsoryAccessions;
        }

        if (data.optionalAccessions) {
            delete data.optionalAccessions;   
        }

        return data;
    };

    var _initModel = function() {
        var model = {
            bankInformation: [],
            relatives: [],
            version: EnumService.getVersion('standard'),
            //address: {main: 1},
            //publicWorker: {hasSubsidie: 0}
        };

        return model;
    };

    return {
        initModel: _initModel,
        populateModel: _populateModel,
        preparateData: _preparateData,
        fetchCurrentContribution: _fetchCurrentContribution
    };
});
