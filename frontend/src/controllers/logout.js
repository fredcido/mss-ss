app.controller("LogoutController", function ($cookies, $location, AuthService) {
    //$cookies.remove('token');
    //$cookies.remove('profile');
    AuthService.clearAuth();
    $location.url('/login');
});
