app.controller("DocumentViewController", function($scope, GenericService, ViewHelper, $translate, AppConstant) {
    $scope.documents = [];
    $scope.hasDocuments = false;

    var _uri = 'document';
    var _init = function() {
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));

        GenericService.find(_uri).then(function(response) {
            var documents = response.data;
            $scope.documents = _.groupBy(documents, 'typeName');

            $scope.hasDocuments = response.data.length > 0;

            ViewHelper.unblockContainer('.panel');
        });
    };

    _init();

    $scope.pathDownload = function(file) {
        return AppConstant.apiUrl + file.path;
    };
});
