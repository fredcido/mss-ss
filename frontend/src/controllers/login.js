app.controller(
    "LoginController", 
    function(
        $scope, 
        $cookies, 
        $state, 
        $translate, 
        $timeout, 
        AuthService, 
        AclService, 
        SettingService,
        EmployerService,
        UserService
    ) 
{
    $scope.usuario = '';
    $scope.btnLoginText = $translate.instant('login');
    $scope.btnLoginEnable = true;
    $scope.invalidLogin = false;

    var redirectAfterLogin = function(user) {
        if (user.isStaff()) {
            $state.go('app.home');
        } else {
            $state.go('app.home-company');
        }
    };

    $scope.login = function(username, password) {
        $scope.invalidLogin = false;
        $scope.btnLoginText = $translate.instant('loading');
        $scope.btnLoginEnable = false;

        localStorage.removeItem('token');
        localStorage.removeItem('permissions');
        localStorage.removeItem('profile');

        AuthService.authenticate(username, password)
            .then(function(response) {
                var data = response.data;
                localStorage.setItem('token', data.access_token);
               
                AuthService.getProfile().then(function(response) {
                    var profile = response.data;
                    if (profile) {
                        var authPromise = AuthService.setUser(profile);

                        authPromise.then(function(){
                            AclService.load().then(function(){
                                SettingService.load();
                                redirectAfterLogin(AuthService.getUserLogged());
                            });
                        });

                        if (profile.locale) {
                            localStorage.setItem('lang', profile.locale);
                            $translate.use(profile.locale);
                        }
                    }
                });
            })
            .catch(function onError(response) {
                $scope.btnLoginEnable = true;
                $scope.btnLoginText = $translate.instant('login');
                swal({
                    title: $translate.instant('login_fail_title'),
                    text: $translate.instant('login_fail_text'),
                    type: 'warning'
                }, function() {
                    $timeout(function() {
                        angular.element('#username').focus();
                    }, 50);
                });
            });
    };

    user = AuthService.getUserLogged();
    if (user) {
        redirectAfterLogin(user);
    }
});
