<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contribution;
use AppBundle\Form\ContributionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class ContributionController extends BaseController
{
    protected $repository = 'AppBundle:Contribution';

    protected $form = ContributionType::class;

    protected $routeGet = 'app.contribution.get';

    protected $entity = Contribution::class;

    public function byPersonAction($id, Request $request)
    {
        $entities = $this->getRepository()->findByPerson($id);
        return $this->createApiResponse($entities);
    }

    public function summaryByPersonAction($id, Request $request)
    {
        $report = $this->get('app.contribution');
        $summary = $report->summaryByPerson($id);
        return $this->createApiResponse($summary);
    }

    public function summaryTotalByPersonAction($id, Request $request)
    {
        $report = $this->get('app.contribution');
        $summary = $report->summaryTotalByPerson($id);
        return $this->createApiResponse($summary);
    }

    public function currentAction($id, Request $request)
    {
        $report = $this->get('app.contribution');
        $em = $this->getDoctrine()->getManager();
        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        $state = $report->personCurrentState($person);
        
        $return = array(
            'state'             => $state,
            'currentContract'   => $person->getCurrentContract()
        );

        return $this->createApiResponse($return, Response::HTTP_OK, 'details');
    }

    public function summaryByEmployerAction($id, Request $request)
    {
        $report = $this->get('app.contribution');
        $summary = $report->summaryByEmployer($id);
        return $this->createApiResponse($summary);
    }
}
