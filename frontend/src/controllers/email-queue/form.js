app.controller("EmailQueueFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'email-queue';
    var _stateList = 'app.email-queue-list';

    $scope.id = $stateParams.id || null;
    $scope.model = {};
    $scope.minDate = new Date();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            var model = response.data;
            model.whenSend = moment(model.whenSend, "YYYY/MM/DD").toDate();
            $scope.model = model;
        });
    }

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };

    $scope.canEdit = function() {
        return false; //$scope.model && !$scope.model.sent;
    };
});
