<?php

namespace AppBundle\Security;

use AppBundle\Entity\CompulsoryAccession;
use AppBundle\Entity\CompulsoryAccessionAbroad;
use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\Employer;
use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Entity\Person;
use AppBundle\Entity\PublicWorker;
use AppBundle\Entity\User;
use AppBundle\Security\Actions;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter as AbstractVoter;

class StaffVoter extends AbstractVoter
{
    use RoleVoterTrait;

    protected $allowedSubjects = array(
        Person::class,
        Employer::class,
        CompulsoryAccession::class,
        CompulsoryAccessionSubscription::class,
        PublicWorker::class,
        CompulsoryAccessionAbroad::class
    );

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$this->decisionManager->decide($token, array('ROLE_STAFF'))) {
            return false;
        }

        if (Actions::VIEW != $attribute) {
            if (!is_string($subject)) {
                $subject = get_class($subject);
            }
            
            return in_array($subject, $this->allowedSubjects);
        } else {
            return true;
        }
    }
}
