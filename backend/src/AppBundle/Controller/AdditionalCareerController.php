<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AdditionalCareer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdditionalCareerController extends BaseController
{
    protected $repository = 'AppBundle:AdditionalCareer';

    protected $form = 'AppBundle\Form\AdditionalCareerType';

    protected $routeGet = 'app.additional_career.get';

    protected $entity = AdditionalCareer::class;
}
