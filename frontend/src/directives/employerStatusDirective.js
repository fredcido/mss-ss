app.directive('employerStatus', function($translate, EnumService) {
    return {
        restrict: 'E',
        scope: {
            employer: "="
        },
        template: "<b class='text-capitalize label {{color}}'>{{description}}</b>",
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                if (e.employer) {
                    setStatus(e.employer);
                }
            });

            var setStatus = function(model) {
                switch (true) {
                    case 0 == model.status:
                        scope.color = 'label-danger';
                        scope.description = $translate.instant('inactive');
                        break;
                    case EnumService.getSubscriptionStatus('approved') != model.subscriptionStatus:
                        switch (model.subscriptionStatus) {
                            case EnumService.getSubscriptionStatus('pending'):
                                scope.color = 'label-warning';
                                scope.description = $translate.instant('pending');
                                break;
                            case EnumService.getSubscriptionStatus('submitted'):
                                scope.color = 'label-success';
                                scope.description = $translate.instant('submitted');
                                break;
                            case EnumService.getSubscriptionStatus('error'):
                                scope.color = 'label-danger';
                                scope.description = $translate.instant('error');
                                break;
                            case EnumService.getSubscriptionStatus('suspended'):
                                scope.color = 'label-warning';
                                scope.description = $translate.instant('suspended');
                                break;
                            case EnumService.getSubscriptionStatus('ceased'):
                                scope.color = 'label-danger';
                                scope.description = $translate.instant('ceased');
                                break;
                        }
                        break;
                    case 1 == model.status:
                        scope.color = 'label-success';
                        scope.description = $translate.instant('active');
                        break;
                }
            };
        }
    };
});
