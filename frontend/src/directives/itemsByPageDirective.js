app.directive("itemsByPage", function($translate) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        template: [
            '<div class="row" style="margin-bottom: 10px">',
            '<div class="col-sm-12 col-md-12">',
            '<form class="form-inline">',
            '   <label class="control-label">{{ labelRowPagination }}</label>&nbsp;',
            '   <div class="form-group">',
            '       <select class="form-control" ng-options="option.value as option.label for option in itemsRowPagination" ng-model="itemsByPage"></select>',
            '   </div>',
            '</form>',
            '</div>',
            '</div>'
        ].join(''),
        link: function(scope, elem, attr, ctrl) {
            scope.labelRowPagination = $translate.instant('select_number_registers');
            scope.itemsRowPagination = [
                {value: 10, label: '10'},
                {value: 50, label: '50'},
                {value: 100, label: '100'},
                {value: undefined, label: $translate.instant('all') },
            ];
        }
    };
});
