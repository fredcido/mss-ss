app.component('personList', {
    templateUrl: 'views/components/personList.html',
    bindings: {
        rows: '<',
        actions: '<'
    },
    controller: function(GenericService, ViewHelper, $translate, $timeout, $uibModal) {
        var self = this;

        this.$onChanges = function (changesObj) {
            if (changesObj.rows) {
                ViewHelper.blockContainer('.tablePersonSearch', $translate.instant('loading'));
                self.rows = changesObj.rows.currentValue;
                self.rowCollection = self.rows;
                self.displayCollection = self.rows;
                ViewHelper.unblockContainer('.tablePersonSearch');
            }
        };
    }
});
