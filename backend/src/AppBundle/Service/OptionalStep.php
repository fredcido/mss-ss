<?php

namespace AppBundle\Service;

use AppBundle\Entity\Person;
use AppBundle\Enum\Status;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class OptionalStep
{
    use ContainerAwareTrait;

    public function listAllowedByPerson(Person $person, $age = null)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        // Get the worker's current step
        $optionalSubscriptionRepository = $em->getRepository('AppBundle:OptionalAccessionSubscription');
        $currentOptionalSubscription = $optionalSubscriptionRepository->getLatestByPerson($person);

        $optionalStepRepository = $em->getRepository('AppBundle:OptionalStep');

        $qb = $optionalStepRepository->createQueryBuilder('os');

        // If it is the first accession, check the maxAgeAccession criteria
        if (empty($currentOptionalSubscription)) {
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('os.maxAgeAccession', 0),
                    $qb->expr()->gte('os.maxAgeAccession', ':age')
                )
            );

            if (empty($age)) {
                $age = $person->getAge();
            }

            $qb->setParameter('age', $age);
        }

        $qb->orderBy('os.indexer', 'ASC');
        $steps = $qb->getQuery()->getResult();

        // If the worker has a current optional step, we check the steps it can choose
        if (!empty($currentOptionalSubscription)) {
            
            $currentOptionalStep = $currentOptionalSubscription->getOptionalStep();
            $currentIndex = array_search($currentOptionalStep, $steps);
            $compensate = 0;

            $allowedSteps = array();
            foreach ($steps as $index => $step) {
                // If the step is inactive we should not considarate it, however, we 
                // need to compensate its gap when getting the next steps
                if ($step->isInactive()) {
                    $compensate++;
                    continue;
                }

                // If the step requires someone younger to increase the step, continue
                if ($step->getMaxAgeIncrease() < $age) {
                    continue;
                }

                // If the current step does not allow decreasing lower than till the current step, continue
                if (($currentIndex - $currentOptionalStep->getDecrease() - $compensate) > $index) {
                    continue;
                }

                // If the current step does not allow increasing higher than till the current step, continue
                if (($currentIndex + $currentOptionalStep->getIncrease() + $compensate) < $index) {
                    continue;
                }

                $allowedSteps[] = $step;
            }

            $steps = $allowedSteps;
        }
        

        return $steps;
    }
}
