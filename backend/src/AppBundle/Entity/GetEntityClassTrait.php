<?php

namespace AppBundle\Entity;

use Doctrine\Common\Persistence\Proxy;

trait GetEntityClassTrait
{
    protected function getEntityClass($subject)
    {
        if (is_object($subject)) {
            $subject = ($subject instanceof Proxy)
                ? get_parent_class($subject)
                : get_class($subject);
        }

        return $subject;
    }
}
