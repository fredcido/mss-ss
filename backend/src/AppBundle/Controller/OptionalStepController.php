<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OptionalStep;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class OptionalStepController extends BaseController
{
    protected $repository = 'AppBundle:OptionalStep';

    protected $form = 'AppBundle\Form\OptionalStepType';

    protected $routeGet = 'app.optional_step.get';

    protected $entity = OptionalStep::class;

    protected function getOrderBy()
    {
        return array('id' => 'asc');
    }

    public function findAllowedByPersonAction($id, $age, Request $request)
    {
        $optionalStepService = $this->get('app.optional_step');
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $entities = $optionalStepService->listAllowedByPerson($person, $age);
        return $this->createApiResponse($entities, Response::HTTP_OK, array('list'));
    }
}
