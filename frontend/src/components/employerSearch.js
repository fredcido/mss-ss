app.component('employerSearch', {
    templateUrl: 'views/components/employerSearch.html',
    bindings: {
        actions: "=",
        params: "<"
    },
    controller: function($translate, $timeout, ViewHelper, $cookies, AppConstant, $compile, GenericService) {
        var self = this;
        var _uri = 'employer/search';
        self.searchForm = false;
        self.calendar = {
            opened: false
        };

        self.openCalendar = function() {
            self.calendar.opened = true;
        };

        var _search = function(params) {
            ViewHelper.blockContainer('.employerContainer', $translate.instant('loading'));

            if (self.params) {
                params = angular.extend({}, params, self.params);
            }

            GenericService.httpGet(_uri, params).then(function(response) {
                self.rows = response.data;

                ViewHelper.unblockContainer('.employerContainer');
            });
        };

        self.reload = function(params) {
            var _params = angular.copy(params) || null;
            _search(_params);
        };

        self.$onInit = function() {
            _search();
        };
    }
});
