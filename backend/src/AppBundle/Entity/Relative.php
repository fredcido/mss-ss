<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Relative
 *
 * @ORM\Table(name="relative")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RelativeRepository")
 */
class Relative
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="inSchool", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $inSchool;

    /**
     * @var SchoolYear
     *
     * @ORM\ManyToOne(targetEntity="SchoolYear")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $schoolYear;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="relatives")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $person;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $relative;

    /**
     * @var Kinship
     *
     * @ORM\ManyToOne(targetEntity="Kinship")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $kinship;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $school;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sinceWhen", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     *
     */
    private $sinceWhen;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inSchool
     *
     * @param boolean $inSchool
     *
     * @return Relative
     */
    public function setInSchool($inSchool)
    {
        $this->inSchool = $inSchool;

        return $this;
    }

    /**
     * Get inSchool
     *
     * @return bool
     */
    public function getInSchool()
    {
        return $this->inSchool;
    }

    /**
     * Gets the value of person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets the value of person.
     *
     * @param Person $person the person
     *
     * @return self
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Gets the value of kinship.
     *
     * @return Kinship
     */
    public function getKinship()
    {
        return $this->kinship;
    }

    /**
     * Sets the value of kinship.
     *
     * @param Kinship $kinship the kinship
     *
     * @return self
     */
    public function setKinship(Kinship $kinship)
    {
        $this->kinship = $kinship;
        return $this;
    }

    /**
     * Gets the value of relative.
     *
     * @return Person
     */
    public function getRelative()
    {
        return $this->relative;
    }

    /**
     * Sets the value of relative.
     *
     * @param Person $relative the relative
     *
     * @return self
     */
    public function setRelative(Person $relative)
    {
        $this->relative = $relative;
        return $this;
    }

    /**
     * Set school
     *
     * @param \AppBundle\Entity\Employer $school
     *
     * @return Relative
     */
    public function setSchool(\AppBundle\Entity\Employer $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\Employer
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Gets the value of schoolYear.
     *
     * @return SchoolYear
     */
    public function getSchoolYear()
    {
        return $this->schoolYear;
    }

    /**
     * Sets the value of schoolYear.
     *
     * @param SchoolYear $schoolYear the school year
     *
     * @return self
     */
    public function setSchoolYear(SchoolYear $schoolYear = null)
    {
        $this->schoolYear = $schoolYear;
        return $this;
    }

    /**
     * Set sinceWhen
     *
     * @param \DateTime $sinceWhen
     *
     * @return Relative
     */
    public function setSinceWhen($sinceWhen)
    {
        $this->sinceWhen = $sinceWhen;

        return $this;
    }

    /**
     * Get sinceWhen
     *
     * @return \DateTime
     */
    public function getSinceWhen()
    {
        return $this->sinceWhen;
    }
}
