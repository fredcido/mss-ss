app.controller("SupplementFormController", function($scope, $stateParams, $translate, GenericService) {
    var _endpoint = 'supplement';
    var _stateList = 'app.supplement-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.supplementTypes = [
        {id: 'S', name: $translate.instant('supplement')},
        {id: 'D', name: $translate.instant('subsidie')},
    ];

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
