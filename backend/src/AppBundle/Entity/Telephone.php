<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Telephone
 *
 * @ORM\Table(name="telephone")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TelephoneRepository")
 */
class Telephone
{
    use EntityAuditTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     * @JMS\Groups({"details"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=200)
     * @JMS\Groups({"details"})
     */
    private $number;

    /**
     * @var ContactDetail
     *
     * @ORM\ManyToOne(targetEntity="ContactDetail", inversedBy="telephones")
     * @Assert\NotBlank(message = "not_blank")
     * @JMS\Exclude()
     */
    private $contactDetail;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Telephone
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Telephone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Gets the value of contactDetail.
     *
     * @return ContactDetail
     */
    public function getContactDetail()
    {
        return $this->contactDetail;
    }

    /**
     * Sets the value of contactDetail.
     *
     * @param ContactDetail $contactDetail the contact detail
     *
     * @return self
     */
    public function setContactDetail(ContactDetail $contactDetail)
    {
        $this->contactDetail = $contactDetail;
        return $this;
    }
}
