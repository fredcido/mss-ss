<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class PostController extends BaseController
{
    protected $repository = 'AppBundle:Post';

    protected $form = 'AppBundle\Form\PostType';

    protected $entity = Post::class;

    protected function getOrderBy()
    {
        return array('acronym' => 'asc', 'name' => 'asc');
    }
}
