app.component('staff', {
    templateUrl: 'views/components/staff.html',
    bindings: {
        employer: '=',
        disabled: '<'
    },
    controller: function($uibModal, $translate, $timeout, GenericService, ViewHelper) {
        var self = this;
        self.posts = [];

        var hasPersonAlready = function(person) {
            return self.employer.staff.some(function(staff) {
                if (staff.person.id == person.id) {
                    return true;
                }
            });
        };

        var getPosts = function() {
            GenericService.find('post').then(function(response) {
                self.posts = response.data;
            });
        };

        self.$onInit = function() {
            getPosts();
        };

        self.addNewStaff = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'personFormDialog'
            });

            modalInstance.result.then(function (person) {
                if (person && person.id) {
                    self.addStaff(person);
                }
            }, function () {

            });
        };

        self.searchPerson = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'personSearchDialog'
            });

            modalInstance.result.then(function (person) {
                if (person) {
                    self.addStaff(person);
                }
            }, function () {

            });
        };

        self.addStaff = function(person) {
            if (hasPersonAlready(person)) {
                swal($translate.instant('staff_already_added'), $translate.instant('staff_already_added_message'), 'error');
            } else {
                var staff = {person: person};
                self.employer.staff.push(staff);
                self.addStaffRole(staff);

                /*$timeout(function() {
                    ViewHelper.scrollTo('.staff-item:last');
                }, 500);*/
            }
        };

        self.removeStaff = function(staff) {
            self.employer.staff.some(function(i, index) {
                if (i == staff) {
                    self.employer.staff.splice(index, 1);
                }
            });
        };

        self.addStaffRole = function(staff) {
            if (!staff.roles) {
                staff.roles = [];
            }

            staff.roles.push({});
        };

        self.removeStaffRole = function(staff, role) {
            staff.roles.some(function(i, index) {
                if (i == role) {
                    staff.roles.splice(index, 1);
                }
            });
        };

    }
});
