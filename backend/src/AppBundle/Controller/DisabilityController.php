<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Disability;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class DisabilityController extends BaseController
{
    protected $repository = 'AppBundle:Disability';

    protected $form = 'AppBundle\Form\DisabilityType';

    protected $routeGet = 'app.disability.get';

    protected $entity = Disability::class;
}
