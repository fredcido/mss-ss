app.component('personFormDialog', {
    templateUrl: 'views/components/personFormDialog.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function ($translate, GenericService, ViewHelper, PersonService) {
        var self = this;
        var _personEndpoint = 'person';

        self.person = {};

        self.setPerson = function(person) {
            self.close({
                $value: person
            });
        };

        self.closeModal = function () {
            self.close({
                $value: self.person
            });
        };

        self.save = function(form, model) {
            if (form.$valid) {
                ViewHelper.blockContainer('.person-form', $translate.instant('loading'));
                ViewHelper.scrollTo('.person-form .blockMsg');
                var data = PersonService.preparateData(model);
                GenericService.add(
                    _personEndpoint, 
                    data, 
                    function(data) {
                        self.setPerson(PersonService.populateModel(data));
                    }, 
                    function(response) {
                        ViewHelper.errorServer(form, response.data);
                    },
                    true
                ).finally(function(){
                    ViewHelper.unblockContainer('.person-form');
                });
            } else {
                ViewHelper.errorForm(form);
            }
        }
    }
});