<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Employer;
use AppBundle\Entity\Feedback;
use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FeedbackSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::postPersist, Events::postFlush];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em     = $args->getEntityManager();

        if (!($entity instanceof Feedback)) {
            return;
        }

        $reference = null;
        if ($entity->getEmployer()) {
            $reference = $entity->getEmployer();
        }

        if ($reference->getSubscriptionStatus() == $entity->getSubscriptionStatus()) {
            return;
        }

        $reference->setSubscriptionStatus($entity->getSubscriptionStatus())
                  ->setJustification($entity->getComment())
                  ->setSubscriptionChangedAt(new \DateTime('now'));

        // If the reference is an employer
        if ($reference instanceof Employer) {
            // Check whether the employer is related to an user
            $userManager = $this->container->get('fos_user.user_manager');
            $user = $userManager->findUserBy(['employer' => $reference]);

            // If there is an user for that employer, send a email
            if (!empty($user)) {
                $this->container->get('app.mail.user_company')->newFeedbackMail($user, $reference, $entity);
            }

            // If there is an error on the subscription
            if ($reference->isErrored()) {
                $employerRepository = $em->getRepository('AppBundle:Employer');
                // Get the last subscription
                $lastSubscription = $employerRepository->findByVersion($reference->getInternalCode(), Version::SUBSCRIPTION);

                // Set the error status for it
                $lastSubscription->setSubscriptionStatus($entity->getSubscriptionStatus())
                                ->setJustification($entity->getComment())
                                ->setSubscriptionChangedAt(new \DateTime('now'));

                $em->persist($lastSubscription);
            }
        }

        $em->persist($reference);
        $this->needsFlush = true;
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }
}
