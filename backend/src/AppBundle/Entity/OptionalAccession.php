<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use AppBundle\Validator\Constraints as InssAssert;

/**
 * OptionalAccession
 *
 * @ORM\Table(name="optional_accession")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OptionalAccessionRepository")
 * @InssAssert\SubscriptionAge
 * @InssAssert\OptionalSubscriptionEmployee
 */
class OptionalAccession
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use SubscriptionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="optionalAccessions")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $person;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $dateEnd;

    /**
     * @var OptionalContributionGroup
     *
     * @ORM\ManyToOne(targetEntity="OptionalContributionGroup")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $optionalContributionGroup;

    /**
     * @ORM\OneToMany(targetEntity="OptionalAccessionSubscription", mappedBy="optionalAccession", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     *
     * @var OptionalAccessionSubscription
     */
    private $optionalAccessionSubscriptions;

    /**
     * @var WorkSituation
     *
     * @ORM\ManyToOne(targetEntity="WorkSituation", cascade={"persist"})
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $workSituation;

    /**
     * @ORM\ManyToMany(targetEntity="StatusHistory")
     * @JMS\Groups({"list", "details"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $statusHistory;

    /**
     * @var OptionalAccession
     *
     * @ORM\ManyToOne(targetEntity="OptionalAccession", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="OptionalAccession", mappedBy="source")
     */
    private $versions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activitySectors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->statusHistory   = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return OptionalAccession
     */
    public function setDateStart($dateStart)
    {
        if (!$dateStart) {
            $this->setDateStart(new \DateTime('now'));
        } else {
            $this->dateStart = $dateStart;
        }
        
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return OptionalAccession
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set optionalContributionGroup
     *
     * @param OptionalContributionGroup $optionalContributionGroup
     *
     * @return OptionalAccession
     */
    public function setOptionalContributionGroup(OptionalContributionGroup $optionalContributionGroup = null)
    {
        $this->optionalContributionGroup = $optionalContributionGroup;

        return $this;
    }

    /**
     * Get optionalContributionGroup
     *
     * @return OptionalContributionGroup
     */
    public function getOptionalContributionGroup()
    {
        return $this->optionalContributionGroup;
    }

    public function cleanOptionalAccessionSubscriptions()
    {
        $this->optionalAccessionSubscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Add optionalAccessionSubscription
     *
     * @param OptionalAccessionSubscription $optionalAccessionSubscription
     *
     * @return OptionalAccession
     */
    public function addOptionalAccessionSubscription(OptionalAccessionSubscription $optionalAccessionSubscription)
    {
        $this->optionalAccessionSubscriptions[] = $optionalAccessionSubscription;
        $optionalAccessionSubscription->setOptionalAccession($this);
        return $this;
    }

    /**
     * Remove optionalAccessionSubscription
     *
     * @param OptionalAccessionSubscription $optionalAccessionSubscription
     */
    public function removeOptionalAccessionSubscription(OptionalAccessionSubscription $optionalAccessionSubscription)
    {
        $this->optionalAccessionSubscriptions->removeElement($optionalAccessionSubscription);
    }

    /**
     * Get optionalAccessionSubscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionalAccessionSubscriptions()
    {
        return $this->optionalAccessionSubscriptions;
    }

    /**
     * Set person
     *
     * @param Person $person
     *
     * @return OptionalAccession
     */
    public function setPerson(Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set workSituation
     *
     * @param WorkSituation $workSituation
     *
     * @return OptionalAccession
     */
    public function setWorkSituation(WorkSituation $workSituation = null)
    {
        $this->workSituation = $workSituation;

        return $this;
    }

    /**
     * Get workSituation
     *
     * @return WorkSituation
     */
    public function getWorkSituation()
    {
        return $this->workSituation;
    }

    /**
     * Add statusHistory
     *
     * @param StatusHistory $statusHistory
     *
     * @return Setting
     */
    public function addStatusHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory[] = $statusHistory;
        return $this;
    }

    /**
     * Remove statusHistory
     *
     * @param StatusHistory $statusHistory
     */
    public function removeHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Get statusHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatusHistory()
    {
        return $this->statusHistory;
    }

    /**
     * @Assert\Callback
     */
    public function isStartDateBeforeEEStartDate(ExecutionContextInterface $context)
    {
        $workSituation = $this->getWorkSituation();
        $placeWork = $workSituation->getPlaceOfWork();

        if ($placeWork && $this->getDateStart() < $placeWork->getDateStartActivity()) {
            $context->buildViolation('subscription_date_employer')
                                ->atPath('dateStart')
                                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function isStartDateBeforePersonDod(ExecutionContextInterface $context)
    {
        $person = $this->getPerson();
        if ($person && $person->getDod() && $this->getDateStart() > $person->getDod()) {
            $context->buildViolation('subscription_date_start_before_dod')
                                ->atPath('dateStart')
                                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function isDateValid(ExecutionContextInterface $context)
    {
        if ($this->getDateStart() && $this->getDateEnd() && $this->getDateStart() > $this->getDateEnd()) {
            $context->buildViolation('error_min_date')
                                ->atPath('dateEnd')
                                ->addViolation();
        }
    }

    /**
     * @return OptionalAccession
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param OptionalAccession $source
     *
     * @return self
     */
    public function setSource(OptionalAccession $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Remove statusHistory
     *
     * @param \AppBundle\Entity\StatusHistory $statusHistory
     */
    public function removeStatusHistory(\AppBundle\Entity\StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\OptionalAccession $version
     *
     * @return OptionalAccession
     */
    public function addVersion(\AppBundle\Entity\OptionalAccession $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\OptionalAccession $version
     */
    public function removeVersion(\AppBundle\Entity\OptionalAccession $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
