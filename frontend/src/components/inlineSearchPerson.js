app.component(
    'inlineSearchPerson', {
    templateUrl: 'views/components/inlineSearchPerson.html',
    bindings: {
        callback: '&',
        person: '=',
        hideSearchButton: '<',
        clearAfterFound: '<',
        personRequired: '<'
    },
    controller: function($translate, $timeout, $uibModal, $element, GenericService, ViewHelper) {
        var self = this;
        var _uri = 'person/search';
        var modal = '#modal-inline-search-person';

        self.search = {filter: 'securityCode'};
        self.peopleList = [];
        self.actionsPeopleList = [{
            title: $translate.instant('select'),
            callback: function(person) {
                $(modal).modal('toggle');
                callbackPerson(person);
            }
        }];
        self.personNotFound = false;

        self.filters = [
            {id: 'securityCode', name: $translate.instant('security_code')},
            {id: 'internalCode', name: $translate.instant('internal_code')},
            {id: 'electoralCard', name: $translate.instant('electoral_card')},
            {id: 'identityCode', name: $translate.instant('identity_code')},
            {id: 'taxNumber', name: $translate.instant('tax_number')},
        ];

        self.personModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'personSearchDialog'
            });

            modalInstance.result.then(function (person) {
                if (person) {
                    callbackPerson(person);
                }
            }, function () {

            });
        };

        self.searchPerson = function() {
            if (!self.search.term) return;

            var termTyped = angular.copy(self.search.term);
            $timeout(function(){
                if (termTyped === self.search.term) {
                    self.validatePerson();
                }
            },1000);
        };

        self.validatePerson = function() {
            self.person = {};
            self.personNotFound = false;

            $timeout(
                function() {
                    ViewHelper.blockContainer($element.find('.inline-search-person'), $translate.instant('loading'));
                    GenericService.httpGet(_uri, self.search).then(function(response) {
                        var data = response.data;

                        if (data.length > 1) {
                            self.peopleList = data;
                            $(modal).modal('toggle');
                        } else if (data.length == 1) {
                            callbackPerson(data[0]);
                        } else {
                            self.personNotFound = true;
                            $timeout(
                                function() {
                                    self.personNotFound = false;
                                },
                                5000
                            );
                        }

                        ViewHelper.unblockContainer($element.find('.inline-search-person'));
                    });
                },
                500
            );
        };

        var listPeople = function(rows)
        {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                template: 'personSearchDialog'
            });
        };

        var callbackPerson = function(person) {
            self.person = person;
            self.person.personDisplay = person.identityCode + ' - ' + person.name;
            
            self.filters.some(function(filter){
                if (person[filter.id]) {
                    self.search.filter = filter.id;
                    self.search.term = person[filter.id];
                    return true;
                }
            });

            if (typeof self.callback == 'function') {
                self.callback({person: person});
            }

            if (self.clearAfterFound) {
                self.person = {};
                self.search.term = null;
            }

            self.personNotFound = false;
        };
    }
});
