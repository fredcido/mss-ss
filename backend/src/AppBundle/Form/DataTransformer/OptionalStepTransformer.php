<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\OptionalStep;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class OptionalStepTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (optionalStep) to a string (number).
     *
     * @param  Issue|null $optionalStep
     * @return string
     */
    public function transform($optionalStep)
    {
        if (null === $optionalStep) {
            return '';
        }

        return $optionalStep->getId();
    }

    /**
     * Transforms a string (number) to an object (optionalStep).
     *
     * @param  string $id
     * @return Issue|null
     * @throws TransformationFailedException if object (optionalStep) is not found.
     */
    public function reverseTransform($id)
    {
        // no optionalStep number? It's optional, so that's ok
        if (!$id) {
            return;
        }

        $optionalStep = $this->manager
            ->getRepository('AppBundle:OptionalStep')
            ->find($id);

        if (null === $optionalStep) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An optional step with ID "%s" does not exist!',
                $id
            ));
        }

        return $optionalStep;
    }
}
