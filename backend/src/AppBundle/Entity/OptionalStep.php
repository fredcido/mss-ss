<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * OptionalStep
 *
 * @ORM\Table(name="optional_step")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OptionalStepRepository")
 * @UniqueEntity(fields="indexer", message="indexer_already_use")
 */
class OptionalStep
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="indexer", type="decimal", precision=5, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $indexer;

    /**
     * @var string
     *
     * @ORM\Column(name="cost", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $cost;

    /**
     * @var int
     *
     * @ORM\Column(name="decrease", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $decrease;

    /**
     * @var int
     *
     * @ORM\Column(name="increase", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $increase;

    /**
     * @var int
     *
     * @ORM\Column(name="maxAgeAccession", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $maxAgeAccession;

    /**
     * @var int
     *
     * @ORM\Column(name="increasePaidMonths", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $increasePaidMonths;

    /**
     * @var int
     *
     * @ORM\Column(name="maxAgeIncrease", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $maxAgeIncrease;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indexer
     *
     * @param string $indexer
     *
     * @return OptionalStep
     */
    public function setIndexer($indexer)
    {
        $this->indexer = $indexer;

        return $this;
    }

    /**
     * Get indexer
     *
     * @return string
     */
    public function getIndexer()
    {
        return $this->indexer;
    }

    /**
     * Set cost
     *
     * @param string $cost
     *
     * @return OptionalStep
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set decrease
     *
     * @param integer $decrease
     *
     * @return OptionalStep
     */
    public function setDecrease($decrease)
    {
        $this->decrease = $decrease;

        return $this;
    }

    /**
     * Get decrease
     *
     * @return int
     */
    public function getDecrease()
    {
        return $this->decrease;
    }

    /**
     * Set increase
     *
     * @param integer $increase
     *
     * @return OptionalStep
     */
    public function setIncrease($increase)
    {
        $this->increase = $increase;

        return $this;
    }

    /**
     * Get increase
     *
     * @return int
     */
    public function getIncrease()
    {
        return $this->increase;
    }

    /**
     * Set maxAgeAccession
     *
     * @param integer $maxAgeAccession
     *
     * @return OptionalStep
     */
    public function setMaxAgeAccession($maxAgeAccession)
    {
        $this->maxAgeAccession = $maxAgeAccession;

        return $this;
    }

    /**
     * Get maxAgeAccession
     *
     * @return int
     */
    public function getMaxAgeAccession()
    {
        return $this->maxAgeAccession;
    }

    /**
     * Set increasePaidMonths
     *
     * @param integer $increasePaidMonths
     *
     * @return OptionalStep
     */
    public function setIncreasePaidMonths($increasePaidMonths)
    {
        $this->increasePaidMonths = $increasePaidMonths;

        return $this;
    }

    /**
     * Get increasePaidMonths
     *
     * @return int
     */
    public function getIncreasePaidMonths()
    {
        return $this->increasePaidMonths;
    }

    /**
     * Set maxAgeIncrease
     *
     * @param integer $maxAgeIncrease
     *
     * @return OptionalStep
     */
    public function setMaxAgeIncrease($maxAgeIncrease)
    {
        $this->maxAgeIncrease = $maxAgeIncrease;

        return $this;
    }

    /**
     * Get maxAgeIncrease
     *
     * @return int
     */
    public function getMaxAgeIncrease()
    {
        return $this->maxAgeIncrease;
    }
}
