app.controller("ActivitySectorListController", function($scope, $stateParams, GenericService, ViewHelper, $translate) {
    var _endpoint = 'activity-sector';
    var _uri = 'activity-sector/tree?all=1';

    var _createTreeView = function() {
        ViewHelper.blockContainer('#treeview', $translate.instant('loading'));
        GenericService.httpGet(_uri).then(function(response) {
            $scope.data = response.data;
            $scope.dataInit = response.data;
            ViewHelper.unblockContainer('#treeview');
        });
    };

    var _formatData = function(data) {
        var dataCopy = {
            code: data.code,
            name: data.name,
            status: data.status,
        };

        if (data.parent) {
            dataCopy.parent = data.parent.id;
        }

        return dataCopy;
    };

    var initModel = function() {
        return {
            status: 1
        };
    };

    var _init = function() {
        $scope.id = $stateParams.id || null;
        $scope.model = initModel();
        $scope.query = '';

        _createTreeView();
    };

    $scope.findNodes = function() {
        var query = $scope.query.toUpperCase();
        if (query.length > 0) {
            $scope.data = $scope.data.filter(function(e, i) {
                var name = e.name.toUpperCase();
                if (name.search(query) != -1) {
                    return e;
                }
            });
        } else {
            $scope.data = $scope.dataInit;
        }
    };

    $scope.add = function(scopeData) {
        var data = _formatData(scopeData);
        GenericService.add(_endpoint, data, function(response) {
            $scope.$apply(function(){
                $scope.model = initModel();
                if (scopeData.parent) {
                    scopeData.parent.children.push(response);
                }
            });
        });
    };

    $scope.edit = function(scopeData) {
        var data = _formatData(scopeData);
        return GenericService.edit(_endpoint, scopeData.id, data, function(response) {
            $scope.$apply(function(){
                $scope.model = initModel();
            }); 
        });
    };

    $scope.cancel = function() {
        $scope.model = initModel();
    };

    $scope.toggle = function(scope) {
        scope.toggle();
    };

    $scope.populateForm = function(node, $parentNodeScope) {
        $scope.model = node;
        if ($parentNodeScope) {
            $scope.model.parent = $parentNodeScope.$modelValue;
        }

        ViewHelper.scrollTo('#form');
        $('#code').focus();
    };

    $scope.addNewChild = function(node) {
        $scope.model = initModel();
        $scope.model.parent = node;

        ViewHelper.scrollTo('#form');
        $('#code').focus();
    };

    $scope.collapseAll = function() {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function() {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.treeOptions = {
        beforeDrop: function(e) {
            ViewHelper.blockContainer('#treeview', $translate.instant('loading'));

            var sourceValue = e.source.nodeScope.$modelValue;
            var destValue = e.dest.nodesScope.node ? e.dest.nodesScope.node : null;
            var data = angular.copy(sourceValue);

            if (destValue && destValue.id) {
                data.parent = {
                    id: destValue.id
                };
            }

            return $scope.edit(data).then(function(){
                ViewHelper.unblockContainer('#treeview');
            });
        }
    };

    _init();
});
