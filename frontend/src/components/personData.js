app.component('personData', {
    transclude: true,
    templateUrl: 'views/components/personData.html',
    bindings: {
        person: '<',
        onUpdate: '&',
        onCalcAge: '&',
        disabled: '<',
        allowAnotherPerson: '<'
    },
    controller: function($scope, $state, $translate, $timeout, $element, ViewHelper, GenericService, PersonService, AuthService) {
        var self = this;
        self.person = {};
        self.user = AuthService.getUserLogged();

        self.$onInit = function() {
            self.nationalities = [];
            self.calendarOptions = {
                maxDate: new Date(),
            };

            self.calendar = {
                dob: false
            };

            self.genders = [{
                id: 'M',
                name: $translate.instant('male')
            }, {
                id: 'F',
                name: $translate.instant('female')
            }];

            var getNationalities = function() {
                GenericService.find('country').then(function(response) {
                    self.nationalities = response.data;
                });
            };

            getNationalities();
            self.displayAllData();
        };

        self.$onChanges = function(changesObj) {
            if (changesObj.person) {
                self.person = changesObj.person.currentValue;
                self.calcAge();
                
                if (self.person)
                    self.person.notDuplicated = !!self.person.id;
            }
        };

        self.openCalendar = function(param) {
            self.calendar[param] = true;
        };

        self.displayAllData = function() {
             if (!self.person) {
                return false;
            }

            var items = [
                self.person.identityCode,
                self.person.electoralCard,
                self.person.birthCertificate,
                self.person.taxNumber,
                self.person.numRef
            ];

            var anyItems = _.filter(items);

            self.person.displayAllData = self.person.id || (anyItems.length && angular.isDefined(self.person.notDuplicated));
            return self.person.displayAllData;
        };

        self.calcAge = function() {
            if (!self.person) return;

            if (self.person.dod) {
                self.person.age = moment(self.person.dod).diff(self.person.dob, 'years', false);
            } else {
                self.person.age = moment().diff(self.person.dob, 'years', false);
            }

            self.onCalcAge({
                person: self.person
            });
        };

        self.isNational = function() {
            return self.person && self.person.nationalities && self.person.nationalities.some(function(item){
                return (/^timor/gi).exec(item.name);
            });
        };

        self.hasAtLeastOne = function() {
            var fields = ['identityCode', 'electoralCard', 'birthCertificate', 'taxNumber', 'numRef'];
            return fields.some(function(item){ return self.person && self.person[item]; });
        }

        self.isElectoralRequired = function() {
            return !self.hasAtLeastOne() && self.person && self.person.notDuplicated && self.person.age >= 18 && self.isNational();
        };

        self.isBirthCertificateRequired = function() {
            return !self.hasAtLeastOne() && self.person && self.person.notDuplicated && self.isNational();
        };

        self.isIdentityCodeRequired = function() {
            return !self.hasAtLeastOne() && self.person && self.person.notDuplicated && !self.isNational() || self.isElectoralRequired();
        };

        self.isTaxNumberRequired = function() {
            return !self.hasAtLeastOne() && self.person && self.person.notDuplicated;
        };

        self.isNumRefRequired = function() {
            return !self.hasAtLeastOne() && self.person && self.person.notDuplicated;
        };

        self.foundPerson = function(entity, value) {
            if (self.person && entity.internalCode == self.person.internalCode) {
                return false;
            }

            if (self.user.isPerson() && !self.allowAnotherPerson) {
                self.person[value] = null;

                var attributeDash = value.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
                var replacements = {'code': $translate.instant(attributeDash), 'value': entity[value]};
                var message = $translate.instant('person_inserted_message', replacements);

                swal({
                    title: $translate.instant('person_inserted'),
                    text: message,
                    type: 'warning'
                });


                return false;
            } else {
                return true;
            }
        };

        self.setPerson = function(entity) {
            $scope.$apply(function() {
                self.person.notDuplicated = true;
                self.onUpdate({
                    person: entity
                });
            });
        };

        self.cleanPerson = function(value) {
            $scope.$apply(function() {
                self.person[value] = null;
                self.person.notDuplicated = false;
            });
        };

        self.onNotFound = function() {
            self.person.notDuplicated = true;
        };
    }
});
