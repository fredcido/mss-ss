app.controller("SupplementListController", function($scope, $translate, $translate, ViewHelper, GenericService) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    var _uri = 'supplement/find?all=1';
    var _init = function() {
        ViewHelper.blockContainer('.table', $translate.instant('loading'));

        GenericService.httpGet(_uri).then(function(response) {
            response.data.map(function(o){
                if ('S' == o.type)
                    o.typeName = $translate.instant('supplement');
                else
                    o.typeName = $translate.instant('subsidie');
            });

            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
