app.controller("OptionalSubscriptionChangesController", 
    function(
        $scope, 
        $stateParams, 
        $uibModal, 
        $translate, 
        $state, 
        $timeout,
        $q,
        GenericService,
        EmployerService,
        PersonService,
        AuthService, 
        EnumService, 
        ViewHelper,
        OptionalAccessionService
    ) {

    var _personEndpoint = "person";
    var _accessionEndpoint = 'optional-accession';
    var _subscriptionEndpoint = 'optional-subscription';

    var checkVersion = new RegExp('_' + EnumService.getVersion('subscription') + '$');

    $scope.user = AuthService.getUserLogged();
    $scope.id = $stateParams.id || null;

    if ($scope.user.isPerson()) {
        $scope.id = $scope.user.person.internalCode;

        ViewHelper.blockContainer('.container', $translate.instant('loading'));

        // Get the person
        var version = EnumService.getVersion('standard');
        GenericService.getVersion(_personEndpoint, $scope.id, version).then(function (response) {
            $scope.person = PersonService.populateModel(response.data);
            return $q.when(response);
        }).then(function() {

            // Get the current accession
            var url = _accessionEndpoint + '/person/' + $scope.person.id + '/latest';
            return GenericService.httpGet(url).then(function(response) {
                $scope.accession = OptionalAccessionService.populateModel(response.data);
                return $q.when(response);
            });
        }).then(function(){

            // Get the current subscription
            var url = _subscriptionEndpoint + '/person/' + $scope.person.id + '/latest';
            return GenericService.httpGet(url).then(function(response) {
                $scope.subscription = OptionalAccessionService.populateSubscriptionModel(response.data);
                return $q.when(response);
            });
        }).finally(function(){
            ViewHelper.unblockContainer('.container');
        });
    }

    $scope.actionsGrid = [{
        title: $translate.instant('view'),
        icon: 'fa-eye',
        callback: function(subscription) {
            $state.go('app.optional-subscription-version', {
                id: subscription.person.internalCode,
                updateSubscription: checkVersion.exec(subscription.version) ? 0 : 1,
                version: subscription.person.id
            });
        }
    }];

    var buildVersion = function(version) {
        return 'OPTIONAL_' + version;
    }

    var versions = [
        buildVersion(EnumService.getVersion('subscription')),
        buildVersion(EnumService.getVersion('update'))
    ];

    var filters = {
        version: versions,
        person: $scope.id,
        all_status: 1
    };

    var init = function() {
        var version = EnumService.getVersion('standard');
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));
        GenericService.getVersion(_personEndpoint, $scope.id, version).then(function (response) {
            $scope.person = PersonService.populateModel(response.data);

            search();
        });
    };

    var search = function() {
        var url = 'optional-subscription/search';
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));
        GenericService.httpGet(url, filters).then(function(response){
            $scope.rows = response.data;
            ViewHelper.unblockContainer('.panel');
        });
    };

    init();
});
