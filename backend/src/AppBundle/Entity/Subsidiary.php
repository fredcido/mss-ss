<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Subsidiary
 *
 * @ORM\Table(name="subsidiary")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubsidiaryRepository")
 * @UniqueEntity(fields={"employer", "name"}, message="name_already_use", errorPath="name")
 */
class Subsidiary
{
    use EntityAuditTrait;
    use EmployerDataTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Employer", inversedBy="subsidiaries")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $employer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Gets the value of employer.
     *
     * @return mixed
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Sets the value of employer.
     *
     * @param mixed $employer the employer
     *
     * @return self
     */
    public function setEmployer(Employer $employer)
    {
        $this->employer = $employer;
        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            $addresses = $this->getAddresses();
            $this->cleanAddresses();

            foreach ($addresses as $address) {
                $newAddress = clone $address;
                $this->addAddress($newAddress);
            }
        }
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getDisplayName()
    {
        $employer = $this->getEmployer();
        $label = sprintf('[%s] - %s', $employer->getSecurityCode(), $this->getName());
        
        $addressLabel = [];
        foreach ($this->getAddresses() as $address) {
            if ($address->getSuku()) {
                $suku = $address->getSuku();
                $postoAdministrativo = $suku->getPostoAdministrativo();

                $addressLabel[] = $postoAdministrativo->getMunicipio()->getName();
                $addressLabel[] = $postoAdministrativo->getName();
            }

            if (count($addressLabel) < 1 && $address->getCountry()) {
                $addressLabel[] = $address->getCityAbroad();
                $addressLabel[] = $address->getCountry()->getName();
            }

            if (count($addressLabel)) {
                break;
            }
        }

        if (count($addressLabel)) {
            $label .= ' - ' . implode(' - ', $addressLabel);
        }

        return $label;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activitySectors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
