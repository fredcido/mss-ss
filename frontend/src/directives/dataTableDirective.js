app.directive('datatable', function($http, $compile, $state, AppConstant, $translate) {
    return {
        restrict: 'A',
        scope: {
            options: "=",
            callback: "="
        },
        transclude: true,
        link: function(scope, elem, attr) {
            $.fn.dataTable.ext.errMode = 'throw';

            $(elem).on('xhr.dt', function(e, settings, json, xhr) {
                switch (xhr.status) {
                    case 401:
                        $state.go('login', {});
                        break;
                    case 500:
                        swal('Ops', $translate.instant('error_datatable'), 'warning');
                        break;
                }
            });

            var options = {
                processing: true,
                responsive: true,
                ajaxDataProp: null,
                language: {
                    url: AppConstant.pathLang + $translate.use() + '.json'
                },
                createdRow: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $compile(angular.element(nRow).contents())(scope);
                },
                order: [
                    [0, 'asc']
                ]
            };

            angular.extend(options, scope.options);
            return $(elem).dataTable(options);
        }
    };
});
