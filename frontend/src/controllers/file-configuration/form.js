app.controller("FileConfigurationFormController", function($scope, $stateParams, GenericService, EnumService) {
    var _endpoint = 'file-configuration';
    var _stateList = 'app.file-configuration-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.references = EnumService.getFileReferences();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
