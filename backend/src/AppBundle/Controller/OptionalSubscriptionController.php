<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OptionalAccessionSubscription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class OptionalSubscriptionController extends BaseController
{
    protected $repository = 'AppBundle:OptionalAccessionSubscription';

    protected $form = 'AppBundle\Form\OptionalAccessionSubscriptionType';

    protected $entity = OptionalAccessionSubscription::class;

    public function getCurrentByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $subscription = $this->getRepository()->getCurrentByPerson($person);
        if (!$subscription) {
            throw $this->createNotFoundException('There is no active optional subscription for the provided person');
        }

        return $this->createApiResponse($subscription, Response::HTTP_OK, array('details'));
    }

    /**
     * @param  int  $id
     * @param  Request $request
     * @return array
     */
    public function getLatestByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $accession = $this->getRepository()->getLatestByPerson($person);
        if (!$accession) {
            throw $this->createNotFoundException('There is no optional subscription for the provided person');
        }

        return $this->createApiResponse($accession, Response::HTTP_OK, array('details'));
    }
}
