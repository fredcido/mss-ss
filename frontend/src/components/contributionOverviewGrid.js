app.component('contributionOverviewGrid', {
    templateUrl: 'views/components/contributionOverviewGrid.html',
    bindings: {
        personId: '='
    },
    controller: function ($translate, $timeout, ViewHelper, $cookies, EnumService, AppConstant, $compile, GenericService, $uibModal) {
        var self = this;
        self.overtimes = [];
        self.displayCollection = [];
        self.rowCollection = [];

        // var _getOvertime = function () {
        //     GenericService.find('overtime').then(function(response) {
        //         self.overtimes = response.data;
        //         // $scope.overtimes.map(function (i) {
        //         //     $scope.model.overtime.push({
        //         //         total: undefined === _personOvertime[i.id] ? 0 : _personOvertime[i.id].total,
        //         //         value: undefined === _personOvertime[i.id] ? 0 : _personOvertime[i.id].value,
        //         //         overtime: i.id,
        //         //         data: i,
        //         //         id: undefined === _personOvertime[i.id] ? null : _personOvertime[i.id].id
        //         //     });
        //         // });
        //     });
        // };

        // _getOvertime();

        var _getSumary = function () {
            GenericService.httpGet('contribution/' + self.personId + '/summary').then(function(response) {
                self.rowCollection = response.data;
            });
        }

        _getSumary();

        self.enumService = EnumService;

        self.showDetailContribution = function (row) {
            var param = {
                employer_id: row.employer,
                person_id: row.person
            };
            row.show = !row.show;

            if (row.show) {
                GenericService.httpGet('contribution/search', param).then(function(response) {
                    row.detail = response.data;
                });
            }
        };


        self.showDeclaredIncome = function (row) {
            $uibModal.open({
                animation: true,
                templateUrl: 'declaredIncomeDialog.html', // NOTE: The view is in contributionOverviewGrid.html
                size: 'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.row = row;

                    $scope.close = function(){
                        $uibModalInstance.close();
                    }
                }
            });
        };

        self.showTotalPaid = function (row) {
            $uibModal.open({
                animation: true,
                templateUrl: 'totalPaidDialog.html', // NOTE: The view is in contributionOverviewGrid.html
                size: 'lg',
                controller: function ($scope, $uibModalInstance) {
                    $scope.row = row;

                    $scope.close = function(){
                        $uibModalInstance.close();
                    }
                }
            });
        };
    }
});