<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * SettingValue
 *
 * @ORM\Table(name="setting_value")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingValueRepository")
 */
class SettingValue
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="newValue", type="string", length=255)
     * @JMS\Groups({"details"})
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="oldValue", type="string", length=255)
     * @JMS\Groups({"details"})
     */
    private $oldValue;

    /**
     * @ORM\ManyToOne(targetEntity="Setting", inversedBy="history")
     */
    private $setting;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newValue
     *
     * @param string $newValue
     *
     * @return SettingValue
     */
    public function setNewValue($newValue)
    {
        $this->newValue = $newValue;

        return $this;
    }

    /**
     * Get newValue
     *
     * @return string
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * Set oldValue
     *
     * @param string $oldValue
     *
     * @return SettingValue
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;

        return $this;
    }

    /**
     * Get oldValue
     *
     * @return string
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * Set setting
     *
     * @param Setting $setting
     *
     * @return SettingValue
     */
    public function setSetting(Setting $setting = null)
    {
        $this->setting = $setting;

        return $this;
    }

    /**
     * Get setting
     *
     * @return Setting
     */
    public function getSetting()
    {
        return $this->setting;
    }
}
