app.controller("EmployerFormController", 
    function (
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $translate,
        ViewHelper, 
        GenericService, 
        EmployerService, 
        PersonService, 
        EnumService, 
        CompulsoryAccessionService,
        AddressService
    ) 
    {

    var _endpoint = "employer";
    var _stateForm = "app.employer-form";
    $scope.id = $stateParams.id || null;
    
    $scope.sectors = [];
    $scope.sectorsTree = [];
    $scope.posts = [];
    $scope.rowCollectionEmployees = [];
    $scope.displayCollectionEmployees = [];
    $scope.enumService = EnumService;
    $scope.totalEmployeesActive = 0;
    $scope.totalEmployeesInactive = 0;

    $scope.stats = {};
    $scope.employeesSubscribed = null;

    $scope.focus = function (element) {
        $timeout(function () {
            angular.element('input[name="' + element + '"]').focus();
        });
    };

    $scope.employerAlreadyThere = function(employer) {
        $timeout(function() {
            $state.go('app.employer-form', {
                id: employer.id,
                ignoreLocalStorage: true
            });
        }, 500);
    };

    $scope.addContact = function () {
        $scope.model.contacts.push({
            name: null,
            post: [],
            contactDetail: null
        });
    };

    $scope.removeContact = function (contact) {
        $scope.model.contacts.some(function (i) {
            if (i == contact) {
                $scope.model.contacts.splice($scope.model.contacts.indexOf(i), 1);
            }
        });
    };

    $scope.addAddress = function () {
        $scope.model.addresses.push({main: 0});
    };

    $scope.removeAddress = function (address) {
        $scope.model.addresses.some(function (i) {
            if (i == address) {
                $scope.model.addresses.splice($scope.model.addresses.indexOf(i), 1);
            }
        });
    };

    $scope.addSubsidiary = function() {
        $scope.model.subsidiaries.push({
            addresses: [{main: 1}]
        });
        $timeout(function() {
            ViewHelper.scrollTo('.subsidiary-item:last')
        }, 500);
    };

    $scope.removeSubsidiary = function(subsidiary) {
        $scope.model.subsidiaries.some(function(i) {
            if (i == subsidiary) {
                $scope.model.subsidiaries.splice($scope.model.subsidiaries.indexOf(i), 1);
            }
        });
    };

    $scope.submit = function (form, model) {
        if (form.$valid) {
            var data = EmployerService.preparateData(model);

            if ($scope.id) {
                GenericService.edit(_endpoint, $scope.id, data, callBackSubmit)
                    .catch(function (response) {
                        callBackErrorSubmit(response.data);
                    });
            } else {
                data.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');

                GenericService.add(_endpoint, data, callBackSubmit)
                    .catch(function (response) {
                        callBackErrorSubmit(response.data);
                    });
            }
        } else {
            ViewHelper.errorForm(form);
        }
    };
    
    $scope.openCalendar = function(param) {
        $scope.calendar[param] = true;
    };

    $scope.setNewFeedback = function(feedback) {
        $scope.model.subscriptionStatus = feedback.subscriptionStatus;
    };

    $scope.canEdit = function() {
        return false;// angular.isUndefined($scope.model.id);
    };

    var initStats = function() {
        $scope.stats = {
            totalEmployeesActive: 0,
            totalNationalActive: 0,
            totalManagers: 0,
            totalEmployeesInactive: 0,
            totalEmployees: 0,
            nationalPercent: 0,
            totalDebt: 0,
            totalFine: 0,
            totalInterest: 0,
            totalFinalDebt: 0
        };

        ViewHelper.blockContainer('.stats-container', $translate.instant('loading'));

        var uri = 'compulsory-subscription/search';
        var filters = {
            employer: $scope.id,
            version: EnumService.getVersion('standard'),
            all_status: 1
        };

        GenericService.httpGet(uri, filters).then(function(response){
            $scope.employeesSubscribed = response.data.map(function(o){ return CompulsoryAccessionService.populateSubscriptionModel(o);});
            
            $scope.employeesSubscribed.forEach(function(subscription){
                // If it's active
                if (subscription.isActive()) {
                    $scope.stats.totalEmployeesActive++;

                    // If it's national
                    if (subscription.compulsoryAccession.person.isNational()) {
                        $scope.stats.totalNationalActive++;
                    }

                } else {
                    $scope.stats.totalEmployeesInactive++;
                }
            });

            $scope.stats.totalEmployees = $scope.employeesSubscribed.length;
            $scope.stats.nationalPercent = $scope.stats.totalNationalActive / $scope.stats.totalEmployeesActive * 100;
            $scope.stats.totalManagers = $scope.model.staff.length;

            ViewHelper.unblockContainer('.stats-container');
        });
    };
    
    var getSectors = function () {
        GenericService.httpGet('sector/tree').then(function (response) {
            $scope.sectorsTree = GenericService.addTreeLevel(response.data);
        });
    };

    var callBackSubmit = function (data) {
        $state.go(_stateForm, {
            id: data.id
        });
    };

    var callBackErrorSubmit = function (responseServer) {
        $scope.form.$invalid = true;
        $scope.errorServer = responseServer.errors;
        angular.forEach($scope.errorServer, function(msg, field) {
            $scope.errorServer[field] = msg.join('; ');
            $scope.form[field].$invalid = true;
        });

        ViewHelper.showErrorApi(responseServer);
    };

    var getPosts = function() {
        GenericService.find('post').then(function(response) {
            $scope.posts = response.data;
        });
    };

    var init = function () {
        getSectors();
        getPosts();
        $scope.model = {};

        // Populate
        if ($scope.id) {
            ViewHelper.blockContainer('.panel', $translate.instant('loading'));
            GenericService.get(_endpoint, $scope.id).then(function (response) {
                $scope.model = EmployerService.populateModel(response.data);
                
                initStats();
                ViewHelper.unblockContainer('.panel');
            });
        } else {
            $scope.model = EmployerService.initModel();
        }

        $scope.model.hasHeadquarter = true;
    };

    var hasPersonAlready = function(person) {
        return $scope.model.staff.some(function(staff) {
            if (staff.person.id == person.id) {
                return true;
            }
        });
    };

    init();
});
