<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompulsoryAccession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class CompulsoryAccessionController extends BaseController
{
    protected $repository = 'AppBundle:CompulsoryAccession';

    protected $form = 'AppBundle\Form\CompulsoryAccessionType';

    protected $entity = CompulsoryAccession::class;

    public function getCurrentByPersonAction($person, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->getCurrentByPerson($person);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find current compulsory accession');
        }

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }
}
