<?php

namespace AppBundle\Security;

abstract class Actions
{
    const VIEW = 'view';
    const CREATE = 'create';
    const EDIT = 'edit';
    const DELETE = 'delete';

    public static function getAll()
    {
        return array(
            self::VIEW,
            self::CREATE,
            self::EDIT,
            self::DELETE
        );
    }
}
