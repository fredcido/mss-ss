<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class EmployerVersionSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em  = $args->getEntityManager();

        $injectEntities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!($entity instanceof Employer)) {
                continue;
            }

            if ($entity->isStandard()) {
                continue;
            }

            $injectEntities[] = $entity;
        }
        
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!($entity instanceof Employer)) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['version']) 
                || $changedValues['version'][0] == $changedValues['version'][1]
                || $changedValues['version'][0] != Version::STANDARD) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        $duplicator = $this->container->get('app.duplicate_entity');
        foreach ($injectEntities as $entity) {

            $employer = $duplicator->duplicateEmployer($entity);
            $employer->setSource($entity);
            
            $entity->setVersion(Version::STANDARD);

            $this->compute($em, $entity);

            // Persist the new version
            $uow->persist($employer);
            //$metadata = $em->getClassMetadata(get_class($entity));
            //$uow->computeChangeSet($metadata, $employer);
            $uow->computeChangeSets();
        }
    }
}
