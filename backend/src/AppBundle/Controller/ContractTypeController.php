<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContractType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class ContractTypeController extends BaseController
{
    protected $repository = 'AppBundle:ContractType';

    protected $form = 'AppBundle\Form\ContractTypeType';

    protected $entity = ContractType::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function findByLabourLawAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(['labourLaw' => $id]);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('list');
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
