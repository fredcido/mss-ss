<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Entity\StatusHistory;
use AppBundle\Entity\SubscriptionTrait;
use AppBundle\Enum\Status;
use AppBundle\Enum\SubscriptionStatus;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SubscriptionStatusSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;
    use GetUserTrait;
    use InjectUserTrait;
    use GetEntityClassTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        //return [Events::onFlush];
        return [Events::postUpdate, Events::postFlush];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em     = $args->getEntityManager();
        $uow = $args->getEntityManager()->getUnitOfWork();

        if (!in_array(SubscriptionTrait::class, class_uses($this->getEntityClass($entity)))) {
            return;
        }

        $changedValues = $uow->getEntityChangeSet($entity);
        if (!isset($changedValues['subscriptionStatus'])) {
            return;
        }

        $today = new \DateTime('now');
        $entity->setSubscriptionChangedAt($today);

        $statusHistory = new StatusHistory();

        $changedValues = $uow->getEntityChangeSet($entity);
        if (!empty($changedValues['subscriptionStatus'])) {
             $statusHistory->setPreviousStatus($changedValues['subscriptionStatus'][0])
                            ->setStatusChanged($changedValues['subscriptionStatus'][1]);
        } else {
            $statusHistory->setStatusChanged($entity->getSubscriptionStatus());
        }

        $entity->addStatusHistory($statusHistory);

        // When the subscription is ceased, ceases all of its versions
        if ($entity->isCeased()) {
            $entity->setStatus(Status::INACTIVE);
            foreach ($entity->getVersions() as $version) {
                $version->setSubscriptionStatus($entity->getSubscriptionStatus());
                $version->setStatus(Status::INACTIVE);
                $em->persist($version);
            }
        }

        $em->persist($statusHistory);
        $em->persist($entity);
        $this->needsFlush = true;
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }

    /**
     * @return mixed
     */
    public function getNeedsFlush()
    {
        return $this->needsFlush;
    }

    /**
     * @param mixed $needsFlush
     *
     * @return self
     */
    public function setNeedsFlush($needsFlush)
    {
        $this->needsFlush = $needsFlush;

        return $this;
    }
}
