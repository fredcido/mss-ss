app.controller("UserListController", function($scope, GenericService, ViewHelper, $translate) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    var _uri = 'user/find?all=1';
    var _init = function() {
        ViewHelper.blockContainer('.table', $translate.instant('loading'));

        GenericService.httpGet(_uri).then(function(response) {
            var users = [].concat(response.data);

            users.map(function(item){
                if (item.employer) {
                    item.roleName = $translate.instant('company');
                } else if (item.person) {
                    item.roleName = $translate.instant('person');
                } else {
                    item.roleName = $translate.instant('user');
                }

                return item;
            });

            $scope.rowCollection = users;
            $scope.displayCollection = [].concat($scope.rowCollection);

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
