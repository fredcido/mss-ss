<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use AppBundle\Entity\FileConfiguration;
use AppBundle\Form\FileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class FileConfigurationController extends BaseController
{
    protected $repository = 'AppBundle:FileConfiguration';

    protected $form = 'AppBundle\Form\FileConfigurationType';

    protected $routeGet = 'app.file_configuration.get';

    protected $entity = FileConfiguration::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function findByReferenceAction($reference, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(array('reference' => $reference));
        $groups = array('list');

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function findByEntityReferenceAction($reference, $entity, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:File');

        $entities = $repository->findBy(array('entity' => $entity, 'reference' => $reference));
        $groups = array('list', 'audit');

        $uploader = $this->get('app.uploader');

        foreach ($entities as $entity) {
            $uploader->injectPublicPath($entity);
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteFileAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('AppBundle:File')->find($id);

        if (!$file) {
            throw $this->createNotFoundException('Unable to find file');
        }

        $uploader = $this->get('app.uploader');
        $uploader->delete($file);

        $em->remove($file);
        $em->flush();

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function downloadFileAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('AppBundle:File')->find($id);

        $this->get('app.uploader')->injectFullPath($file);

        $response = new BinaryFileResponse($file->getPath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file->getName(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $file->getName())
        );

        return $response;
    }


    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function uploadAction(Request $request)
    {
        $file = new File();
        $form = $this->createForm(FileType::class, $file);
        $form->handleRequest($request);

        $uploader = $this->get('app.uploader');

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $uploader->upload($file);
        } else {
            $this->throwApiProblemValidationException($form);
        }

        return $this->createApiResponse($file, Response::HTTP_OK, array('list', 'audit'));
    }
}
