app.filter('myStrictFilter', function($filter) {
    return function(input, predicate) {
        return $filter('filter')(input, predicate, true);
    };
});
