app.component('remunerationStatementSearch', {
    templateUrl: 'views/components/remunerationStatementSearch.html',
    bindings: {
        actions: "=",
        params: "<"
    },
    controller: function($translate, $timeout, ViewHelper, $cookies, AppConstant, $compile, GenericService) {
        var self = this;
        var _uri = 'remuneration-statement/search';

        self.searchForm = false;

        var _search = function(params) {
            ViewHelper.blockContainer('.remuneration-statement-search-container', $translate.instant('loading'));

            if (self.params) {
                params = angular.extend({}, params, self.params);
            }

            GenericService.httpGet(_uri, params).then(function(response) {
                self.rows = response.data;

                ViewHelper.unblockContainer('.remuneration-statement-search-container');
            });
        };

        self.reload = function(params) {
            var _params = angular.copy(params) || null;
            _search(_params);
        };

        self.$onInit = function() {
            self.months = [];
            for (var m = 1; m <= 12; m++) {
                self.months.push(m);
            }

            _search();
        };

        self.setEmployer = function(employer) {
            self.params.employer = employer.id;
        };
    }
});
