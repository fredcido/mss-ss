(function (){

    var _swal = window.swal;

    window.swal = function(){

        var previousWindowKeyDown = window.onkeydown;
        _swal.apply(this, Array.prototype.slice.call(arguments, 0));

        window.onkeydown = previousWindowKeyDown;

    };

    var close = _swal.close;
    window.swal.close = function() {
        close();
        window.onkeydown = null;
    };

})();