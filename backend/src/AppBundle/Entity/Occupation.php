<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Occupation
 *
 * @ORM\Table(name="occupation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OccupationRepository")
 * @UniqueEntity(fields="name", message="The name is already in use, choose another.")
 */
class Occupation
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "max_chars"
     * )
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="OccupationWear", mappedBy="occupation", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $occupationWears;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**.
     *
     * @param string $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->occupationWears = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add occupationWear
     *
     * @param OccupationWear $occupationWear
     *
     * @return Occupation
     */
    public function addOccupationWear(OccupationWear $occupationWear)
    {
        $this->occupationWears[] = $occupationWear;
        $occupationWear->setOccupation($this);

        return $this;
    }

    /**
     * Remove occupationWear
     *
     * @param OccupationWear $occupationWear
     */
    public function removeOccupationWear(OccupationWear $occupationWear)
    {
        $this->occupationWears->removeElement($occupationWear);
    }

    /**
     * Get occupationWears
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOccupationWears()
    {
        return $this->occupationWears;
    }
}
