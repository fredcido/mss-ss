<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Entity\OptionalAccession;
use AppBundle\Enum\SubscriptionStatus;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SubscriptionSuspensionSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;
    use InjectUserTrait;
    use GetEntityClassTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $employerSubscription = [];
        $optionalSubscription = [];
        $today = new \DateTime('now');

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['subscriptionStatus']) || (!$entity->isSuspended() && !$entity->isCeased())) {
                continue;
            }

            switch (true) {
                case $entity instanceof Employer:
                    $employerSubscription[] = $entity;
                    break;
                case $entity instanceof OptionalAccession:
                    $optionalSubscription[] = $entity;
                    break;
            }
        }

        foreach ($optionalSubscription as $entity) {

            $subscription = $this->getActiveSubscription($entity);

            $subscription->setDateEnd($entity->getDateEnd());
            $uow->persist($subscription);

            $metadata = $em->getClassMetadata(get_class($subscription));
            $uow->computeChangeSet($metadata, $subscription);
        }

        foreach ($employerSubscription as $entity) {
            $employees = $this->getActiveEmployees($entity);

            foreach ($employees as $employee) {
                $employee->setSubscriptionStatus($entity->getSubscriptionStatus());
                $employee->setDateEnd($entity->getDateEndActivity());
                $uow->persist($employee);

                $metadata = $em->getClassMetadata(get_class($employee));
                $uow->computeChangeSet($metadata, $employee);
            }
        }
    }

    protected function getActiveEmployees(Employer $employer)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $compulsorySubscription = $em->getRepository('AppBundle:CompulsoryAccessionSubscription');

        $filters = [
            'employer'              => $employer,
            'notSubscriptionStatus' => [
                SubscriptionStatus::SUSPENDED,
                SubscriptionStatus::CEASED
            ]
        ];

        return $compulsorySubscription->findByFilters($filters);
    }


    protected function getActiveSubscription(OptionalAccession $accession)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $optionalSubscription = $em->getRepository('AppBundle:OptionalAccessionSubscription');

        $person = $accession->getPerson();
        return $optionalSubscription->getCurrentByPerson($person);
    }
}
