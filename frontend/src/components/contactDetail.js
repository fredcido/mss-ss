app.component('contactDetail', {
    templateUrl: 'views/components/contactDetail.html',
    bindings: {
        model: '=',
        disabled: '<',
    },
    controller: function($translate) {
        var self = this;

        this.$onUpdate = function() {
            self.initializationValues();
        };

        self.initializationValues = function() {
            self.model = self.model || {
                email: [],
                telephones: []
            };
        };

        self.phonesTypes = [{
            value: '0',
            label: $translate.instant('fix')
        }, {
            value: '1',
            label: $translate.instant('cellphone')
        }];

        self.addPhone = function() {
            self.initializationValues();
            self.model.telephones.push({});
        };

        self.removePhone = function(phone) {
            self.model.telephones.some(function(i) {
                if (i == phone) {
                    self.model.telephones.splice(self.model.telephones.indexOf(i), 1);
                }
            });
        };

        self.addEmail = function() {
            self.initializationValues();
            self.model.email.push('');
        };

        self.removeEmail = function(email) {
            self.model.email.some(function(i) {
                if (i == email) {
                    self.model.email.splice(self.model.email.indexOf(i), 1);
                }
            });
        };
    }
});
