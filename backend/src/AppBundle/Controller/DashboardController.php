<?php

namespace AppBundle\Controller;

use AppBundle\Enum\Status;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends BaseController
{
    protected function responseData($data)
    {
        return $this->createApiResponse($data, Response::HTTP_OK, array('form'));
    }

    protected function responseStat($stat)
    {
        return $this->responseData(['stat' => $stat]);
    }

    public function getStatsEmployersAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();
        $count = $dashboard->getCountEmployers($filters);

        return $this->responseStat($count);
    }

    public function getStatsEmployeesAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();
        $count = $dashboard->getCountEmployees($filters);

        return $this->responseStat($count);
    }

    public function getStatsStatementsAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();
        $count = $dashboard->getCountStatements($filters);

        return $this->responseStat($count);
    }

    public function getStatsOptionalAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();
        $count = $dashboard->getCountOptional($filters);

        return $this->responseStat($count);
    }

    public function getChartDistrictEmployerAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();

        $data = $dashboard->getChartDistrictEmployer($filters);

        return $this->responseData($data);
    }

    public function getChartDistrictEmployeeAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();

        $data = $dashboard->getChartDistrictEmployee($filters);

        return $this->responseData($data);
    }

    public function getChartRemunerationMonthAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();

        $data = $dashboard->getChartRemunerationMonth($filters);

        return $this->responseData($data);
    }

    public function getChartOptionalGenderAction(Request $request)
    {
        $dashboard = $this->get('app.dashboard');
        $filters = $request->request->all();

        $data = $dashboard->getChartOptionalGender($filters);
        return $this->responseData($data);
    }

    public function chartContributionByGenderByLocalizationAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = array(
            'drilldown' => array(
                'series' => array(
                    array(
                         'id'   => 'dili',
                         'name' => "Dili",
                         'data' => array(
                             array(
                                'name' => 'Atauro',
                                'y'    => 24
                             ),
                             array(
                                'name' => 'Metinaro',
                                'y'    => 19
                             ),
                         )
                    )
                )
            ),
            'series' => array(
                array(
                    'name'         => 'Masculino',
                    'colorByPoint' => true,
                    'data'         => array(
                        array(
                            'name'      => 'Dili',
                            'drilldown' => "dili",
                            'y'         => 156
                        ),
                        array(
                            'name' => 'Kubanakan',
                            'y'    => 285
                        ),
                    )
                ),
                array(
                    'name'         => 'Feminino',
                    'colorByPoint' => true,
                    'data'         => array(
                        array(
                            'name' => 'Dili',
                            'y'    => 134,
                        ),
                        array(
                            'name' => 'Kubanakan',
                            'y'    => 148,
                        ),
                    ),
                ),
            ),
        );

        return $this->responseData($entities);
    }
}
