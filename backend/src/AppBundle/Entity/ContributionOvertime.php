<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ContributionOvertime
 *
 * @ORM\Table(name="contribution_overtime")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContributionValueRepository")
 */
class ContributionOvertime
{
    use EntityAuditTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"details"})
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"details"})
     */
    private $total;

    /**
     * @var Contribution
     *
     * @ORM\ManyToOne(targetEntity="Contribution", inversedBy="overtime")
     * @JMS\Exclude()
     */
    private $contribution;

    /**
     * @var Overtime
     *
     * @ORM\ManyToOne(targetEntity="Overtime")
     * @JMS\Groups({"details"})
     */
    private $overtime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set value
     *
     * @param string $value
     *
     * @return ContributionOvertime
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set contribution
     *
     * @param \AppBundle\Entity\Contribution $contribution
     *
     * @return ContributionOvertime
     */
    public function setContribution(\AppBundle\Entity\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution
     *
     * @return \AppBundle\Entity\Contribution
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Set overtime
     *
     * @param \AppBundle\Entity\Overtime $overtime
     *
     * @return ContributionOvertime
     */
    public function setOvertime(\AppBundle\Entity\Overtime $overtime = null)
    {
        $this->overtime = $overtime;

        return $this;
    }

    /**
     * Get overtime
     *
     * @return \AppBundle\Entity\Overtime
     */
    public function getOvertime()
    {
        return $this->overtime;
    }

    /**
     * Gets the value of total.
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Sets the value of total.
     *
     * @param string $total the total
     *
     * @return self
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }
}
