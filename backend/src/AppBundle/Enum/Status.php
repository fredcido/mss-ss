<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class Status implements Enum
{
    use EnumTrait;

    const INACTIVE = 0;
    const ACTIVE = 1;
    
    /**
     * @var array
     */
    protected static $data = [
        self::INACTIVE  => 'Inactive',
        self::ACTIVE    => 'Active',
    ];
}
