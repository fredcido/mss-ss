app.component('navigation', {
    templateUrl: 'views/components/navigation.html',
    controller: function($rootScope, $scope, $state, $window, GenericService, AuthService, ViewHelper) {
        var self = this;
        
        self.profile = AuthService.getUserLogged();
        self.langSelected = localStorage.getItem('lang');
        self.currentState = $state.current;

        self.changeLanguage = function(lang) {
            self.langSelected = lang;
            localStorage.setItem('lang', lang);
            $window.location.reload();
        };

        $rootScope.$on('userChanged', function () {
            self.profile = AuthService.getUserLogged();
        });
    }
});
