app.component('relative', {
    templateUrl: 'views/components/relative.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function($uibModal, $q, $translate, GenericService, ViewHelper, PersonService, RelativeService) {
        var self = this;
        var _personEndpoint = 'person';
        var _endpoint = 'relative';

        self.calendarOptions = {
            minMode: 'month'
        };

        var getKinships = function() {
            GenericService.find('kinship').then(function(response) {
                self.kinships = response.data;
            });
        };

        var getSchoolYears = function() {
            GenericService.find('school-year').then(function(response) {
                self.schoolYears = response.data;
            });
        };

        var savePerson = function() {
            var data = PersonService.preparateData(self.relative.relative);

            if (data.id) {
                return GenericService.edit(_personEndpoint, data.id, data);
            } else {
                return GenericService.add(_personEndpoint, data);
            }
        };

        self.closeModal = function () {
            self.close({
                $value: null
            });
        };

        self.changeInSchool = function() {
            if (!self.relative.inSchool) {
                self.relative.school = null;
                self.relative.schoolYear = null;
                self.schoolDisplay = '';
            }
        };

        self.setPerson = function(person) {
            self.relative.relative = person;
        };

        self.errorSave = function(form) {
            return function(response) {
                ViewHelper.unblockContainer('.form-relative');
                ViewHelper.errorServer(form, response);
            };
        };

        self.save = function(form) {
            if (form.$valid) {

                ViewHelper.blockContainer('.form-relative', $translate.instant('loading'));

                var deferred = $q.defer();
                var personPromise;

                if (!self.relative.relative.id) {
                    personPromise = savePerson();
                } else {
                    deferred.resolve(self.relative.relative);
                    personPromise = deferred.promise;
                }

                personPromise.then(function(person){
                    self.relative.relative = PersonService.populateModel(person);
                    var data = RelativeService.preparateData(self.relative);

                    var relativePromise;
                    if (data.id) {
                        relativePromise = GenericService.edit(_endpoint, data.id, data);
                    } else {
                        relativePromise = GenericService.add(_endpoint, data);
                    }

                    relativePromise.then(function(response){
                        ViewHelper.unblockContainer('.form-relative');
                        self.close({
                            $value: response
                        });
                    }, self.errorSave(form));
                }, self.errorSave(form));

            } else {
                ViewHelper.errorForm(form);
            }
        };

        self.$onInit = function() {
            if (self.resolve.model) {
               self.relative = self.resolve.model;
            } else {
                self.relative = {
                    person: self.resolve.person
                };

                if (self.resolve.relative) {
                    self.relative.relative = self.resolve.relative;
                } else {
                    self.relative.relative = PersonService.initModel();
                }
            }

            self.kinships = [];
            self.schoolYears = [];

            getKinships();
            getSchoolYears();
        };
    }
});
