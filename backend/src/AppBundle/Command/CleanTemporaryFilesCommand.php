<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DBLS\Bundle\JerseyColoursBundle\Adapters\FileAdapter;

class CleanTemporaryFilesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mss:clean:files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $cleanFiles = $container->get('app.clean_files');
        $temporaryFiles = $cleanFiles->clean();

        $output->writeln(sprintf('%s files removed', count($temporaryFiles)));
    }
}
