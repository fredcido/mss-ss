<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class CountryController extends BaseController
{
    protected $repository = 'AppBundle:Country';

    protected $form = 'AppBundle\Form\CountryType';

    protected $routeGet = 'app.country.get';

    protected $entity = Country::class;
}
