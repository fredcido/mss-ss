<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompulsoryAccessionAbroad;
use AppBundle\Form\CompulsoryAccessionAbroadType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class AccessionAbroadController extends BaseController
{
    protected $repository = 'AppBundle:CompulsoryAccessionAbroad';

    protected $form = 'AppBundle\Form\CompulsoryAccessionAbroadType';

    protected $entity = CompulsoryAccessionAbroad::class;

    public function getByPersonAction($id, Request $request)
    {
        $accessionAbroadRepository = $this->getRepository();
        $accessionAbroad = $accessionAbroadRepository->findBy(['person' => $id]);

        if (empty($accessionAbroad)) {
            throw $this->createNotFoundException('Unable to find accession abroad');
        }

        return $this->createApiResponse($accessionAbroad[0], Response::HTTP_OK, array('list', 'audit'));
    }
}
