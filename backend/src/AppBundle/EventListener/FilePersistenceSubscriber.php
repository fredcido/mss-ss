<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\FileContainerTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FilePersistenceSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    protected $uploader;

    protected $needsFlush = false;

    public function setUploader($service)
    {
        $this->uploader = $service;
        return $this;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::postPersist, Events::postFlush];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em     = $args->getEntityManager();

        if (!in_array(FileContainerTrait::class, class_uses($entity))) {
            return;
        }

        $files = $entity->getFiles();
        if (!empty($files)) {
            $em = $args->getObjectManager();

            $repositoryFiles = $em->getRepository('AppBundle:File');
            $filesTemporary = $repositoryFiles->findTemporaryIn($files);

            foreach ($filesTemporary as $file) {
                $file->setEntity($entity->getInternalCode());

                $this->uploader->makePermanent($file);
                $em->persist($file);
            }

            if (!empty($filesTemporary)) {
                $this->needsFlush = true;
            }
        }
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }
}
