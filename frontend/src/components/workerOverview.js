app.component('workerOverview', {
    templateUrl: 'views/components/workerOverview.html',
    bindings: {
        person: '<'
    },
    controller: function($translate, $timeout, GenericService, CompulsoryAccessionService, OptionalAccessionService) {
        var self = this;

        function getCurrentEmployemnts() {
            var compulsoryUrl = 'compulsory-subscription/search';
            var filter = {
                person: self.person.internalCode,
                all_status: 1
            };

            GenericService.httpGet(compulsoryUrl, filter).then(function(response) {
                var rows = response.data.map(function(item){
                    return CompulsoryAccessionService.populateSubscriptionModel(item);
                });

                self.employments = rows;
            });
        }

        function getCurrentOptionalSubscription() {
            var optionalUrl = 'optional-subscription/person/' + self.person.id + '/current';
            GenericService.httpGet(optionalUrl).then(function(response){
                self.optionalSubscription = OptionalAccessionService.populateSubscriptionModel(response.data);
            });
        }

        self.$onChanges = function() {
            if (!self.person || !self.person.id) return;

            getCurrentEmployemnts();
            getCurrentOptionalSubscription();
        };
    }
});
