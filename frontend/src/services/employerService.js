app.service("EmployerService", function(AddressService, PersonService, EnumService, SubscriptionService) {

    var _preparateData = function (model) {
        var data = angular.copy(model);

        if (data.activitySectors) {
            var activitySectorsArray = [];
            data.activitySectors.some(function(i) {
                activitySectorsArray.push(i.id);
            });

            data.activitySectors = activitySectorsArray;
        }

        if (data.dateStartActivity) {
            data.dateStartActivity = moment(data.dateStartActivity).format('YYYY-MM-DD');
        }

        if (data.dateEndActivity) {
            data.dateEndActivity = moment(data.dateEndActivity).format('YYYY-MM-DD');
        }

        if (data.dateEmployees) {
            data.dateEmployees = moment(data.dateEmployees).format('YYYY-MM-DD');
        }

        if (data.subsidiaries) {
            var subsidiaries = [];
            data.subsidiaries.some(function(subsidiary) {
                

                if (subsidiary.employer) {
                    subsidiary.employer = subsidiary.employer.id;
                }

                if (subsidiary.activitySectors) {
                    var activitySectorsArray = [];
                    subsidiary.activitySectors.some(function(i) {
                        activitySectorsArray.push(i.id);
                    });

                    subsidiary.activitySectors = activitySectorsArray;
                }

                if (subsidiary.dateStartActivity) {
                    subsidiary.dateStartActivity = moment(subsidiary.dateStartActivity).format('YYYY-MM-DD');
                }

                if (subsidiary.dateEndActivity) {
                    subsidiary.dateEndActivity = moment(subsidiary.dateEndActivity).format('YYYY-MM-DD');
                }

                if (subsidiary.dateEmployees) {
                    subsidiary.dateEmployees = moment(subsidiary.dateEmployees).format('YYYY-MM-DD');
                }

                if (subsidiary.addresses) {
                    subsidiary.addresses = AddressService.preparateDatas(subsidiary.addresses);
                }

                subsidiaries.push(subsidiary);
            });

            data.subsidiaries = subsidiaries;
        }

        if (data.staff) {
            var staffs = [];
            data.staff.some(function(staff) {

                staff.person = staff.person.id;

                if (staff.roles) {
                    var roles = [];
                    staff.roles.some(function(i) {
                        i.role = i.role.id;
                        i.since = moment(i.since).format('YYYY-MM-DD');

                        roles.push(i);
                    });

                    staff.roles = roles;
                }

                staffs.push(staff);
            });
            data.staff = staffs;
        }

        if (data.addresses) {
            data.addresses = AddressService.preparateDatas(data.addresses);
        }

        if (data.sector) {
            data.sector = data.sector.id;
        }

        if (data.activitySectors) {
            data.activitySectors.some(function(i){
                i = i.id;
            });
        }

        if (data.bankInformation) {
            data.bankInformation.some(function(i){
                if(i.bank){
                    i.bank = i.bank.id;
                }
            });
        }

        if (!data.contactDetail) {
            delete data.contactDetail;
        }

        delete data.workSituations;
        delete data.history;

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);
        
        if (model.dateStartActivity) {
            model.dateStartActivity = moment(model.dateStartActivity, "YYYY/MM/DD").toDate();
        }

        if (model.dateEndActivity) {
            model.dateEndActivity = moment(model.dateEndActivity, "YYYY/MM/DD").toDate();
        }

        if (model.dateEmployees) {
            model.dateEmployees = moment(model.dateEmployees, "YYYY/MM/DD").toDate();
        }

        if (model.addresses && model.addresses.length) {
            model.addresses = AddressService.populateModels(model.addresses);
        } else {
            model.addresses = [{
                main: 1
            }];
        }

        if (model.contacts) {
            model.contacts.some(function(i){
                if (i.post) {
                    i.post = i.post.id;
                }
            });
        }

        if (model.subsidiaries) {
            model.subsidiaries.some(function(subsidiary) {
                if (subsidiary.activitySectors) {
                    subsidiary.activitySectors.some(function(i){
                        i = i.id;
                    });
                }

                if (subsidiary.dateStartActivity) {
                    subsidiary.dateStartActivity = moment(subsidiary.dateStartActivity, "YYYY/MM/DD").toDate();
                }

                if (subsidiary.dateEndActivity) {
                    subsidiary.dateEndActivity = moment(subsidiary.dateEndActivity, "YYYY/MM/DD").toDate();
                }

                if (subsidiary.dateEmployees) {
                    subsidiary.dateEmployees = moment(subsidiary.dateEmployees, "YYYY/MM/DD").toDate();
                }

                if (subsidiary.addresses) {
                    subsidiary.addresses = AddressService.populateModels(subsidiary.addresses);
                }
            });
        }

        if (model.staff) {
            model.staff.some(function(staff) {
                staff.person = PersonService.populateModel(staff.person);

                if (staff.roles) {;
                    staff.roles.some(function(i) {
                        i.since = moment(i.since, "YYYY/MM/DD").toDate();
                    });
                }
            });
        }

        _attachMethods(model);

        return model;
    };

    var _attachMethods = function(model) {
        model.displayName = model.internalCode + ' - ' + model.name + ' [' + model.securityCode + ']';

        SubscriptionService.attachMethods(model);
    };  

    var _initModel = function() {
        var model = {
            name: null,
            shortName: null,
            securityCode: null,
            employerSector: null,
            contacts: [],
            subsidiaries: [],
            staff: [],
            addresses: [{
                main: 1
            }],
            bankInformation: [],
            version: EnumService.getVersion('standard')
        };

        SubscriptionService.attachMethods(model);

        return model;
    };

    return {
        initModel: _initModel,
        populateModel: _populateModel,
        preparateData: _preparateData
    };
});
