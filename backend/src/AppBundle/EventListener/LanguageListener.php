<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LanguageListener
{
    use ContainerAwareTrait;
    use GetUserTrait;

    private $session;

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    public function setLocale(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $validLocales = array('en', 'pt', 'tl');

        $user = $this->getUser();
        if ($user && $user->getLocale()) {
            $userLocale = $user->getLocale();
        } else {
            $userLocale = $request->getPreferredLanguage($validLocales);
        }

        if ($locale = $request->attributes->get('_locale')) {
            $defaultLocale = $locale;
        } else {
            $defaultLocale = $request->getSession()->get('_locale', $userLocale);
        }

        if (in_array($defaultLocale, $validLocales)) {
            $userLocale = $defaultLocale;
        }

        $request->getSession()->set('_locale', $userLocale);
        $request->setLocale($userLocale);
    }
}
