app.service("GenericService", function($http, $q, AppConstant, ViewHelper, $translate, $state, $sce) {
    var _apiUrl = AppConstant.apiUrl;

    var _data = function(endpoint) {
        return $http({
            method: 'GET',
            url: _apiUrl + 'data/' + endpoint
        });
    };

    var _save = function(endpoint, data) {
        return $http({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: _apiUrl + endpoint + '/save',
            data: $.param(data),
            beforeSend: function() {
                ViewHelper.blockPage('');
            },
            complete: function() {
                ViewHelper.unblockPage();
            }
        }).then(function(response) {
            swal({
                title: $translate.instant('success_title'),
                text: $translate.instant('success_text'),
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            });

            return $q.resolve(response);
        });
    };

    var _find = function(endpoint, all) {
        var findAll = "";
        if (all === true) {
            findAll = "?all=1";
        }

        return $http({
            method: 'GET',
            url: _apiUrl + endpoint + "/find" + findAll
        });
    };

    var _get = function(endpoint, id) {
        return $http({
            method: 'GET',
            url: _apiUrl + endpoint + '/' + id + "/get"
        });
    };

    var _getVersion = function(endpoint, id, version) {
        return $http({
            method: 'GET',
            url: _apiUrl + endpoint + '/version/' + id + "/" + version
        });
    };

    var _add = function(endpoint, data, callback, errorCallback, noMessage) {
        var deferred = $q.defer();

        return $http({
            url: _apiUrl + endpoint + "/add",
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: $.param(data),
            beforeSend: function() {
                ViewHelper.blockPage('');
            },
            complete: function() {
                ViewHelper.unblockPage();
            }
        }).then(function(response) {
            if (noMessage) {
                if (callback !== undefined) {
                    if (typeof callback == 'function') {
                        eval(callback(response.data));
                    } else {
                        $state.go(callback);
                    }
                }
            } else {
                swal({
                    title: $translate.instant('success_title'),
                    text: $translate.instant('success_text'),
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false
                },
                function() {
                    if (callback !== undefined) {
                        if (typeof callback == 'function') {
                            eval(callback(response.data));
                        } else {
                            $state.go(callback);
                        }
                    }
                    swal.close();
                });
            }
            
            deferred.resolve(response.data);
            return deferred.promise;

        }, function(error) {
            if (errorCallback !== undefined && typeof errorCallback == 'function') {
                errorCallback.apply(this, arguments);
            }

            deferred.reject(error);
            return deferred.promise;
        });
    };

    var _edit = function(endpoint, id, data, callback, errorCallback, noMessage) {
        var deferred = $q.defer();
        return $http({
                method: 'POST',
                url: _apiUrl + endpoint + '/' + id + "/edit",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: $.param(data),
                beforeSend: function() {
                    ViewHelper.blockPage('');
                },
                complete: function() {
                    ViewHelper.unblockPage();
                }
            })
            .then(function(response) {
                if (noMessage) {
                    if (callback !== undefined) {
                        if (typeof callback == 'function') {
                            eval(callback(response.data));
                        } else {
                            $state.go(callback);
                        }
                    }
                } else {
                    swal({
                        title: $translate.instant('success_title'),
                        text: $translate.instant('success_text'),
                        type: 'success',
                        timer: 2000,
                        showConfirmButton: false
                    },
                    function() {
                        if (callback !== undefined) {
                            if (typeof callback == 'function') {
                                eval(callback(response.data));
                            } else {
                                $state.go(callback);
                            }
                        }
                        swal.close();
                    });
                }

                deferred.resolve(response.data);
                return deferred.promise;
            
            }, function(error) {
                if (errorCallback !== undefined && typeof errorCallback == 'function') {
                    errorCallback.apply(this, arguments);
                }

                deferred.reject(error);
                return deferred.promise;
            });
    };

    var _delete = function(endpoint, id) {
        return $http({
            method: 'DELETE',
            url: _apiUrl + endpoint + '/' + id + "/delete",
            beforeSend: function() {
                ViewHelper.blockPage('');
            },
            complete: function() {
                ViewHelper.unblockPage();
            }
        });
    };

    var _deleteMany = function(endpoint, data, justification) {
        var param = {
            items: data,
            justification: justification
        };

        return $http({
            method: 'POST',
            url: _apiUrl + endpoint + "/delete",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: $.param(param)
        });
    };

    var _activateMany = function(endpoint, data) {
        var param = {
            items: data
        };

        return $http({
            method: 'POST',
            url: _apiUrl + endpoint + "/active",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: $.param(param)
        });
    };

    var _httpGet = function(url, data) {
        var param = '';
        if (data) {
            param = '?' + $.param(data);
        }

        return $http({
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: _apiUrl + url + param
        });
    };

    var _httpPost = function(endpoint, data) {
        var param = null;
        if (data) {
            param = $.param(data);
        }

        return $http({
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            url: _apiUrl + endpoint,
            data: param
        });
    };

    var _addTreeLevel = function(items, level) {
        var finalItems = [];
        var currentLevel = angular.isUndefined(level) ? 0 : level + 1;

        _.each(items, function(item) {
            item.level = currentLevel;
            finalItems.push(item);

            if (item.children && item.children.length > 0) {
                var subItems = _addTreeLevel(item.children, currentLevel);
                _.each(subItems, function(subItem){
                    finalItems.push(subItem);
                });
            }
        });

        return finalItems;
    };

    return {
        data: _data,
        get: _get,
        getVersion: _getVersion,
        find: _find,
        add: _add,
        edit: _edit,
        save: _save,
        delete: _delete,
        deleteMany: _deleteMany,
        activateMany: _activateMany,
        httpGet: _httpGet,
        httpPost: _httpPost,
        addTreeLevel: _addTreeLevel
    };
});
