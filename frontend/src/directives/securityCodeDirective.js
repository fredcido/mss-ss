app.directive('securityCode', function($translate) {
    return {
        restrict: 'E',
        scope: {
            model: "<"
        },
        template: '<span class="label label-warning pull-right" ng-if="model.securityCode" style="margin-top: -5px; font-size: 20px"><label style="margin-bottom: 0" class="text-capitalize" translate="niss"></label>: {{model.securityCode}}</span>',
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                setModel(e.model);
            });

            var setModel = function(value) {
                scope.model = value;
            };
        }
    };
});
