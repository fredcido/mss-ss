<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * LicenseLabourLaw
 *
 * @ORM\Table(name="license_labour_law")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LicenseLabourLawRepository")
 * @UniqueEntity(fields={"license", "labourLaw"}, message="This labour law is already inserted for this license.")
 */
class LicenseLabourLaw
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="daysInMonth", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $daysInMonth;

    /**
     * @var int
     *
     * @ORM\Column(name="daysInYear", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $daysInYear;

    /**
     * @ORM\ManyToOne(targetEntity="License", inversedBy="licenseLabourLaws")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="license_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $license;

    /**
     * @ORM\ManyToOne(targetEntity="LabourLaw", inversedBy="licenseLabourLaws")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="labour_law_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $labourLaw;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set daysInMonth
     *
     * @param integer $daysInMonth
     *
     * @return LicenseLabourLaw
     */
    public function setDaysInMonth($daysInMonth)
    {
        $this->daysInMonth = $daysInMonth;

        return $this;
    }

    /**
     * Get daysInMonth
     *
     * @return int
     */
    public function getDaysInMonth()
    {
        return $this->daysInMonth;
    }

    /**
     * Set daysInYear
     *
     * @param integer $daysInYear
     *
     * @return LicenseLabourLaw
     */
    public function setDaysInYear($daysInYear)
    {
        $this->daysInYear = $daysInYear;

        return $this;
    }

    /**
     * Get daysInYear
     *
     * @return int
     */
    public function getDaysInYear()
    {
        return $this->daysInYear;
    }

    /**
     * Set license
     *
     * @param License $license
     *
     * @return LicenseLabourLaw
     */
    public function setLicense(License $license = null)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return License
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set labourLaw
     *
     * @param LabourLaw $labourLaw
     *
     * @return LicenseLabourLaw
     */
    public function setLabourLaw(LabourLaw $labourLaw)
    {
        $this->labourLaw = $labourLaw;

        return $this;
    }

    /**
     * Get labourLaw
     *
     * @return LabourLaw
     */
    public function getLabourLaw()
    {
        return $this->labourLaw;
    }
}
