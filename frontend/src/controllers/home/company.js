app.controller("HomeCompanyController", function ($scope, $translate, GenericService, AuthService, UserService) {

    var user = AuthService.getUserLogged();
    if (user.isPerson()) {
        UserService.checkOptionalSubsmission(user);
    }
});