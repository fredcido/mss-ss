<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use FOS\UserBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends BaseController
{
    protected $form = UserType::class;

    protected $repository = 'AppBundle:User';

    public function findAction(Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');

        $entities = $userManager->findUsers();

        return $this->createApiResponse($entities);
    }

    public function getAction($id, Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');

        $entity = $userManager->findUserBy(['id' => $id]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->createApiResponse($entity);
    }

    public function addAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        return $this->handleForm($user, $request, Response::HTTP_CREATED);
    }

    public function editAction($id, Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');

        $entity = $userManager->findUserBy(['id' => $id]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->handleForm($entity, $request);
    }

    public function persistEntity($entity)
    {
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($entity);

        return $entity;
    }
}
