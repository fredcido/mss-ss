<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * RemunerationStatement
 *
 * @ORM\Table(name="remuneration_statement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RemunerationStatementRepository")
 * @UniqueEntity(fields={"month", "year", "employer"}, message="remuneration_statement_already_inserted", errorPath="month")
 */
class RemunerationStatement
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $year;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $employer;

    /**
     * @ORM\OneToMany(targetEntity="RemunerationRevision", mappedBy="remunerationStatement", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     *
     * @var RemunerationRevision
     */
    private $revisions;

    /**
     * @ORM\OneToMany(targetEntity="RemunerationPerson", mappedBy="remunerationStatement", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     *
     * @var RemunerationPerson
     */
    private $remunerationPeople;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->remunerationPeople = new \Doctrine\Common\Collections\ArrayCollection();
        $this->revisions = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return RemunerationStatement
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return RemunerationStatement
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set employer
     *
     * @param Employer $employer
     *
     * @return RemunerationStatement
     */
    public function setEmployer(Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Add remunerationPerson
     *
     * @param RemunerationPerson $remunerationPerson
     *
     * @return RemunerationStatement
     */
    public function addRemunerationPerson(RemunerationPerson $remunerationPerson)
    {
        $this->remunerationPeople[] = $remunerationPerson;
        $remunerationPerson->setRemunerationStatement($this);
        return $this;
    }

    /**
     * Remove remunerationPerson
     *
     * @param RemunerationPerson $remunerationPerson
     */
    public function removeRemunerationPerson(RemunerationPerson $remunerationPerson)
    {
        $this->remunerationPeople->removeElement($remunerationPerson);
    }

    /**
     * Get remunerationPeople
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRemunerationPeople()
    {
        return $this->remunerationPeople;
    }

    /**
     * Add revision
     *
     * @param RemunerationRevision $revision
     *
     * @return RemunerationStatement
     */
    public function addRevision(RemunerationRevision $revision)
    {
        $this->revisions[] = $revision;
        $revision->setRemunerationStatement($this);
        return $this;
    }

    /**
     * Remove revision
     *
     * @param RemunerationRevision $revision
     */
    public function removeRevision(RemunerationRevision $revision)
    {
        $this->revisions->removeElement($revision);
    }

    /**
     * Get revisions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRevisions()
    {
        return $this->revisions;
    }
}
