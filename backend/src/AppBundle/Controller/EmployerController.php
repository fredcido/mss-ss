<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Employer;
use AppBundle\Enum\VersionStatus;
use AppBundle\Form\EmployerType;
use AppBundle\Security\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class EmployerController extends BaseController
{
    protected $repository = 'AppBundle:Employer';

    protected $form = EmployerType::class;

    protected $entity = Employer::class;

    public function getByVersionAction($id, $version, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->findByVersion($id, $version);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find employer');
        }

        $this->denyAccessUnlessGranted(Actions::VIEW, $entity);

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }

    public function duplicateAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find employer');
        }

        $duplicator = $this->get('app.duplicate_entity');
        $newEmployer = $duplicator->duplicateEmployer($entity);

        $data = $request->request->all();
        $newEmployer->setVersion($data['version']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newEmployer);
        $em->flush();

        return $this->createApiResponse($newEmployer, Response::HTTP_OK, array('details'));
    }
}
