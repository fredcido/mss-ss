var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var es = require('event-stream');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var debug = require('gulp-debug');
var jsValidate = require('gulp-jsvalidate');
var embedTemplates = require('gulp-angular-embed-templates');
var extractTranslate = require('gulp-angular-translate-extract');

gulp.task('extract-translate', function() {
    var i18nsrc = ['src/**/*.js', 'views/**/*.html']; // your source files
    var i18ndest = 'lang';
    return gulp.src(i18nsrc)
        .pipe(extractTranslate({
            lang: ['en', 'pt', 'tl'], // array of languages
            dest: i18ndest, // destination, default '.'
            prefix: '', // output filename prefix, default ''
            suffix: '', // output filename suffix, default '.json'
            safeMode: true, // do not delete old translations, true - contrariwise, default false
            stringifyOptions: true, // force json to be sorted, false - contrariwise, default false
        }))
        .pipe(gulp.dest(i18ndest));
});

gulp.task('clean', function() {
    return gulp.src('dist/')
        .pipe(clean());
});

gulp.task('scripts', function() {
    return es.merge([
        gulp.src([
            "node_modules/lodash/lodash.min.js",
            "node_modules/jquery/dist/jquery.min.js",
            "node_modules/angular/angular.min.js",
            "node_modules/moment/min/moment.min.js",
            "node_modules/bootstrap/dist/js/bootstrap.min.js",
            "node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js",
            "node_modules/angular-ui-router/release/angular-ui-router.min.js",
            "node_modules/angular-sanitize/angular-sanitize.min.js",
            "node_modules/angular-cookies/angular-cookies.min.js",
            "node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js",
            "node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js",
            "node_modules/angular-smart-table/dist/smart-table.min.js",
            "node_modules/angular-translate/dist/angular-translate.min.js",
            "node_modules/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js",
            "node_modules/sweetalert/dist/sweetalert.min.js",
            "node_modules/jquery-mask-plugin/dist/jquery.mask.min.js",
            "node_modules/ui-select/dist/select.min.js",
            "node_modules/block-ui/jquery.blockUI.js",
            "node_modules/angular-input-masks/releases/angular-input-masks-standalone.min.js",
            "node_modules/highcharts/highcharts.js",
            "node_modules/highcharts/modules/drilldown.js",
            "node_modules/highcharts-ng/dist/highcharts-ng.min.js",
            "node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js",
            "node_modules/ng-file-upload/dist/ng-file-upload.min.js",
            "node_modules/clipboard/dist/clipboard.min.js",
            "node_modules/ngclipboard/dist/ngclipboard.min.js",
            "node_modules/angular-ui-tree/dist/angular-ui-tree.min.js",
        ]),
        gulp.src(['src/**/*'])
        .pipe(concat('scripts.js'))
        .pipe(uglify({
            mangle: false
        }).on('error', function(e){
            console.log(e);
         }))

    ]).pipe(concat('all.min.js'))

    .pipe(gulp.dest('dist/js'));
});

gulp.task('htmlmin', function() {
    return es.merge([
            gulp.src(['views/**/*.html'])
        ])
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('dist/views'));
});

gulp.task('cssmin', function() {
    return es.merge([
            gulp.src([
                "node_modules/bootswatch/united/bootstrap.min.css",
                "node_modules/font-awesome/css/font-awesome.min.css",
                "node_modules/sweetalert/dist/sweetalert.css",
                "node_modules/ui-select/dist/select.min.css",
                "node_modules/ui-select/dist/select.min.css.map",
                "node_modules/angular-ui-tree/dist/angular-ui-tree.min.css",
                "css/style.css"
            ]),
            gulp.src(['css/*.css']).pipe(cleanCSS())
        ])
        .pipe(concat('styles.css'))

    .pipe(gulp.dest('dist/css'));
});

gulp.task('fonts', function() {
    return es.merge([
            gulp.src([
                'node_modules/font-awesome/fonts/**',
                'node_modules/bootstrap/fonts/**',
            ]),
        ])
        .pipe(gulp.dest('dist/fonts'));
});


gulp.task('lang', function() {
    return es.merge([
            gulp.src(['lang/**']),
        ])
        .pipe(gulp.dest('dist/lang'));
});

gulp.task('imgs', function() {
    return es.merge([
            gulp.src(['imgs/**']),
        ])
        .pipe(gulp.dest('dist/imgs'));
});

gulp.task('copy', function() {
    return gulp.src('index-prod.html')
        .pipe(replace(/all.min.js/g, 'all.min.js?v=' + (new Date()).getTime()))
        .pipe(replace(/styles.css/g, 'styles.css?v=' + (new Date()).getTime()))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('constant', function() {
    return gulp.src('constant-prod.js')
        .pipe(rename('constant.js'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('validate', function () {
  gulp
    .src(['src/**/*'])
    .pipe(debug())
    .pipe(jsValidate());
});

gulp.task('default', function(callback) {
    return runSequence('clean', 'scripts', [
        'htmlmin',
        'imgs',
        'cssmin',
        'lang',
        'fonts',
        'constant',
        'copy'
    ], callback);
});