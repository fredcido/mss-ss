<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonRepository")
 *
 * @UniqueEntity(fields={"internalCode", "securityCode"}, message="security_code_already_use", repositoryMethod="validateUnique", errorPath="securityCode")
 * @UniqueEntity(fields={"internalCode", "birthCertificate"}, message="birth_certificate_already_use", repositoryMethod="validateUnique", errorPath="birthCertificate")
 * @UniqueEntity(fields={"internalCode", "electoralCard"}, message="electoral_card_already_user", repositoryMethod="validateUnique", errorPath="electoralCard")
 * @UniqueEntity(fields={"internalCode", "taxNumber"}, message="tax_number_already_use", repositoryMethod="validateUnique", errorPath="taxNumber")
 * @UniqueEntity(fields={"internalCode", "numRef"}, message="tax_number_already_use", repositoryMethod="validateUnique", errorPath="taxNumber")
 */
class Person implements SocialSecurityInterface
{
    const NO_REGISTER = 'no_register';
    const STUDENT = 'student';
    const UNDERAGE = 'underage';
    const UNEMPLOYED = 'unemployed';
    const EMPLOYED = 'employed';
    const DEATH = 'death';

    use EntityAuditTrait;
    use InternalCodeTrait;
    use FileContainerTrait;
    use EntityPermissionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank", groups={"Default", "User"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dob", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     * @Assert\Expression("!this.getDod() || this.getDob() <= this.getDod()", message="date_birth_before_deceased")
     *
     */
    private $dob;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dod", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details"})
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     * @Assert\Expression("!this.getDod() || this.getDob() <= this.getDod()", message="date_deceased_before_birth")
     *
     */
    private $dod;

    /**
     * @var string
     *
     * @ORM\Column(name="securityCode", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $securityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="taxNumber", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $taxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="identityCode", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * Assert\NotBlank(message="not_blank")
     */
    private $identityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="electoralCard", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $electoralCard;

    /**
     * @var string
     *
     * @ORM\Column(name="birthCertificate", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $birthCertificate;

    /**
     * @var string
     *
     * @ORM\Column(name="numRef", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $numRef;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @JMS\Groups({"details"})
     */
    private $description;

    /**
     * @var ContactDetail
     *
     * @ORM\ManyToOne(targetEntity="ContactDetail", cascade={"persist"})
     * @JMS\Groups({"details"})
     * @Assert\Valid
     */
    private $contactDetail;


    /**
     * @var Address
     *
     * @ORM\ManyToOne(targetEntity="Address", cascade={"persist"})
     * @JMS\Groups({"details"})
     * @Assert\Valid
     */
    private $address;

    /**
     * @var Naturality
     *
     * @ORM\ManyToOne(targetEntity="Naturality", cascade={"persist"})
     * @JMS\Groups({"details"})
     * @Assert\Valid
     */
    private $naturality;

    /**
     * @var Disability
     *
     * @ORM\ManyToOne(targetEntity="Disability")
     * @JMS\Groups({"details"})
     */
    private $disability;

    /**
     * @var MaritalStatus
     *
     * @ORM\ManyToOne(targetEntity="MaritalStatus")
     * @JMS\Groups({"details"})
     */
    private $maritalStatus;

    /**
     * @ORM\ManyToMany(targetEntity="Country")
     * @ORM\JoinTable(name="person_nationality",
     *      joinColumns={@ORM\JoinColumn(name="person_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="nationality_id", referencedColumnName="id")}
     * )
     * @JMS\Groups({"list", "details"})
     */
    private $nationalities;

    /**
     * @ORM\OneToMany(targetEntity="BankInformation", mappedBy="person", cascade={"persist"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     * @Assert\Valid
     *
     * @var  ArrayCollection
     */
    private $bankInformation;

    /**
     * @ORM\OneToMany(targetEntity="Relative", mappedBy="person", cascade={"persist", "remove"}, orphanRemoval=true)
     * @Assert\Valid
     *
     * @var  ArrayCollection
     */
    private $relatives;

    /**
     * @ORM\OneToMany(targetEntity="Contribution", mappedBy="person", cascade={"persist"})
     *
     * @var  ArrayCollection
     */
    private $contributions;

    /**
     */
    private $currentState;

    /**
     *
     * @var Contribution
     */
    private $currentContract;

    /**
     * @ORM\OneToMany(targetEntity="CompulsoryAccession", mappedBy="person", cascade={"persist"})
     *
     * @var CompulsoryAccessionSubscription
     */
    private $compulsoryAccessions;

    /**
     * @ORM\OneToMany(targetEntity="OptionalAccession", mappedBy="person", cascade={"persist"})
     *
     * @var CompulsoryAccessionSubscription
     */
    private $optionalAccessions;

    /**
     * @var PublicWorker
     *
     * @ORM\OneToOne(targetEntity="PublicWorker", mappedBy="person", cascade={"persist"})
     */
    private $publicWorker;

    /**
     * @var CompulsoryAccessionAbroad
     *
     * @ORM\OneToOne(targetEntity="CompulsoryAccessionAbroad", mappedBy="person", cascade={"persist"})
     */
    private $accessionAbroad;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="Person", mappedBy="source")
     */
    private $versions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nationalities   = new ArrayCollection();
        $this->bankInformation = new ArrayCollection();
        $this->contributions = new ArrayCollection();
        $this->relatives = new ArrayCollection();
    }

    /**
     * @return string
     */
    public static function generateUniqueKey()
    {
        return sprintf("%s-%s", date('Y'), uniqid());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     *
     * @return Employee
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set securityCode
     *
     * @param string $securityCode
     *
     * @return Employee
     */
    public function setSecurityCode($securityCode)
    {
        $this->securityCode = $securityCode;

        return $this;
    }

    /**
     * Get securityCode
     *
     * @return string
     */
    public function getSecurityCode()
    {
        return $this->securityCode;
    }

    /**
     * Set identityCode
     *
     * @param string $identityCode
     *
     * @return Person
     */
    public function setIdentityCode($identityCode)
    {
        $this->identityCode = $identityCode;

        return $this;
    }

    /**
     * Get identityCode
     *
     * @return string
     */
    public function getIdentityCode()
    {
        return sprintf('P-%s', $this->identityCode);
    }

    /**
     * Get electoralCard
     *
     * @return string
     */
    public function getElectoralCard()
    {
        return $this->electoralCard;
    }

    /**
     * Set electoralCard
     *
     * @param string $electoralCard
     *
     * @return Person
     */
    public function setElectoralCard($electoralCard)
    {
        $this->electoralCard = $electoralCard;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Employee
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the value of contactDetail.
     *
     * @return ContactDetail
     */
    public function getContactDetail()
    {
        return $this->contactDetail;
    }

    /**
     * Sets the value of contactDetail.
     *
     * @param ContactDetail $contactDetail the contact detail
     *
     * @return self
     */
    public function setContactDetail(ContactDetail $contactDetail)
    {
        $this->contactDetail = $contactDetail;
        return $this;
    }

    /**
     * Gets the value of disability.
     *
     * @return Disability
     */
    public function getDisability()
    {
        return $this->disability;
    }

    /**
     * Sets the value of disability.
     *
     * @param Disability $disability the disability
     *
     * @return self
     */
    public function setDisability(Disability $disability = null)
    {
        $this->disability = $disability;
        return $this;
    }

    /**
     * Gets the value of marital status.
     *
     * @return MaritalStatus
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Sets the value of marital status.
     *
     * @param MaritalStatus $maritalStatus the marial status
     *
     * @return self
     */
    public function setMaritalStatus(MaritalStatus $maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
        return $this;
    }

    /**
     * Add nationalitys
     *
     * @param Country $nationalitys
     * @return Race
     */
    public function addNationality(Country $nationality)
    {
        $this->nationalities[] = $nationality;
        return $this;
    }

    public function cleanNationalities()
    {
        $this->nationalities = new ArrayCollection();
        return $this;
    }

    /**
     * Remove nationality
     *
     * @param Country $nationality
     */
    public function removeNationality(Country $nationality)
    {
        $this->nationalities->removeElement($nationality);
    }

    /**
     * Get nationalitys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNationalities()
    {
        return $this->nationalities;
    }

    /**
     * Gets the value of address.
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the value of address.
     *
     * @param Address $address the address
     *
     * @return self
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Add bankInformation
     *
     * @param BankInformation $bankInformation
     */
    public function addBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation[] = $bankInformation;
        $bankInformation->setPerson($this);
        return $this;
    }

    public function cleanBankInformation()
    {
        $this->bankInformation = new ArrayCollection();
        return $this;
    }

    /**
     * Remove bankInformation
     *
     * @param BankInformation $bankInformation
     */
    public function removeBankInformation(BankInformation $bankInformation)
    {
        $this->bankInformation->removeElement($bankInformation);
    }

    /**
     * Get bankInformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBankInformation()
    {
        return $this->bankInformation;
    }

    /**
     * Add relative
     *
     * @param BankInformation $relative
     */
    public function addRelative(Relative $relative)
    {
        $this->relatives[] = $relative;
        $relative->setPerson($this);
        return $this;
    }

    public function cleanRelatives()
    {
        $this->relatives = new ArrayCollection();
        return $this;
    }

    /**
     * Remove bankInformation
     *
     * @param BankInformation $bankInformation
     */
    public function removeRelative(Relative $relative)
    {
        $this->relatives->removeElement($relative);
    }

    /**
     * Get bankInformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelative()
    {
        return $this->relatives;
    }

    /**
     * Add contribution
     *
     * @param Contribution $contribution
     */
    public function addContribution(Contribution $contribution)
    {
        $this->contributions[] = $contribution;
        return $this;
    }

    public function cleanContributions()
    {
        $this->contributions = new ArrayCollection();
        return $this;
    }

    /**
     * Remove contribution
     *
     * @param Contribution $contribution
     */
    public function removeContribution(Contribution $contribution)
    {
        $this->contributions->removeElement($contribution);
    }

    /**
     * Get contribution
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributions()
    {
        return $this->contributions;
    }


    /**
     * Gets the value of dod.
     *
     * @return \DateTime
     */
    public function getDod()
    {
        return $this->dod;
    }

    /**
     * Sets the value of dod.
     *
     * @param \DateTime $dod the dod
     *
     * @return self
     */
    public function setDod(\DateTime $dod = null)
    {
        $this->dod = $dod;
        return $this;
    }

    /**
     * Gets the value of currentState.
     *
     * @return mixed
     */
    public function getCurrentState()
    {
        return $this->currentState;
    }

    /**
     * Sets the value of currentState.
     *
     * @param mixed $currentState the current state
     *
     * @return self
     */
    public function setCurrentState($currentState)
    {
        $this->currentState = $currentState;

        return $this;
    }

    /**
     * Gets the value of currentContract.
     *
     * @return Contribution
     */
    public function getCurrentContract()
    {
        return $this->currentContract;
    }

    /**
     * Sets the value of currentContract.
     *
     * @param Contribution $currentContract the current contract
     *
     * @return self
     */
    public function setCurrentContract(Contribution $currentContract)
    {
        $this->currentContract = $currentContract;
        return $this;
    }

    /**
     * Gets the value of gender.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the value of gender.
     *
     * @param string $gender the gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    public function getSecurityCodePrefix()
    {
        return '1';
    }

    /**
     * Gets the value of taxNumber.
     *
     * @return string
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * Sets the value of taxNumber.
     *
     * @param string $taxNumber the tax number
     *
     * @return self
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;
        return $this;
    }

    /**
     * Gets the value of naturality.
     *
     * @return Naturality
     */
    public function getNaturality()
    {
        return $this->naturality;
    }

    /**
     * Sets the value of naturality.
     *
     * @param Naturality $naturality the naturality
     *
     * @return self
     */
    public function setNaturality(Naturality $naturality = null)
    {
        $this->naturality = $naturality;
        return $this;
    }

    /**
     * Gets the value of naturalityCity.
     *
     * @return string
     */
    public function getNaturalityCity()
    {
        return $this->naturalityCity;
    }

    /**
     * Sets the value of naturalityCity.
     *
     * @param string $naturalityCity the naturality city
     *
     * @return self
     */
    public function setNaturalityCity($naturalityCity)
    {
        $this->naturalityCity = $naturalityCity;
        return $this;
    }

    /**
     * Get relatives
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelatives()
    {
        return $this->relatives;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getAge()
    {
        if (!$this->getDob()) {
            return 0;
        }
        
        $today = new \DateTime('today');
        return $this->getDob()->diff($today)->y;
    }

    /**
     * Add compulsoryAccession
     *
     * @param CompulsoryAccession $compulsoryAccession
     *
     * @return Person
     */
    public function addCompulsoryAccession(CompulsoryAccession $compulsoryAccession)
    {
        $this->compulsoryAccessions[] = $compulsoryAccession;
        $compulsoryAccession->setPerson($this);
        return $this;
    }

    public function cleanCompulsoryAccessions()
    {
        $this->compulsoryAccessions = new ArrayCollection();
        return $this;
    }

    /**
     * Remove compulsoryAccession
     *
     * @param CompulsoryAccession $compulsoryAccession
     */
    public function removeCompulsoryAccession(CompulsoryAccession $compulsoryAccession)
    {
        $this->compulsoryAccessions->removeElement($compulsoryAccession);
    }

    /**
     * Get compulsoryAccessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompulsoryAccessions()
    {
        return $this->compulsoryAccessions;
    }

    /**
     * Add optionalAccession
     *
     * @param OptionalAccession $optionalAccession
     *
     * @return Person
     */
    public function addOptionalAccession(OptionalAccession $optionalAccession)
    {
        $this->optionalAccessions[] = $optionalAccession;
        return $this;
    }

    public function cleanOptionalAccessions()
    {
        $this->optionalAccessions = new ArrayCollection();
        return $this;
    }

    /**
     * Remove optionalAccession
     *
     * @param OptionalAccession $optionalAccession
     */
    public function removeOptionalAccession(OptionalAccession $optionalAccession)
    {
        $this->optionalAccessions->removeElement($optionalAccession);
    }

    /**
     * Get optionalAccessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionalAccessions()
    {
        return $this->optionalAccessions;
    }

    /**
     * Gets the value of birthCertificate.
     *
     * @return string
     */
    public function getBirthCertificate()
    {
        return $this->birthCertificate;
    }

    /**
     * Sets the value of birthCertificate.
     *
     * @param string $birthCertificate the birth certificate
     *
     * @return self
     */
    public function setBirthCertificate($birthCertificate)
    {
        $this->birthCertificate = $birthCertificate;
        return $this;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getDisplayName()
    {
        $securityCode = false;
        if ($this->getSecurityCode()) {
            $securityCode = sprintf('- [%s]', $this->getSecurityCode());
        }

        $displayName = sprintf("%s - %s", $this->getInternalCode(), $this->getName());
        if ($securityCode) {
            $displayName .= $securityCode;
        }

        return $displayName;
    }

    /**
     * Gets the value of publicWorker.
     *
     * @return PublicWorker
     */
    public function getPublicWorker()
    {
        return $this->publicWorker;
    }

    /**
     * Sets the value of publicWorker.
     *
     * @param PublicWorker $publicWorker the public worker
     *
     * @return self
     */
    public function setPublicWorker(PublicWorker $publicWorker)
    {
        $this->publicWorker = $publicWorker;
        $this->publicWorker->setPerson($this);
        return $this;
    }

    /**
     * Gets the value of accessionAbroad.
     *
     * @return CompulsoryAccessionAbroad
     */
    public function getAccessionAbroad()
    {
        return $this->accessionAbroad;
    }

    /**
     * Sets the value of accessionAbroad.
     *
     * @param CompulsoryAccessionAbroad $accessionAbroad the accession abroad
     *
     * @return self
     */
    public function setAccessionAbroad(CompulsoryAccessionAbroad $accessionAbroad)
    {
        $this->accessionAbroad = $accessionAbroad;
        $this->accessionAbroad->setPerson($this);
        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            $relatives = $this->getRelatives();
            $this->cleanRelatives();
            foreach ($relatives as $relative) {
                $this->addRelative(clone $relative);
            }
        }
    }

    /**
     * Gets the value of numRef.
     *
     * @return string
     */
    public function getNumRef()
    {
        return $this->numRef;
    }

    /**
     * Sets the value of numRef.
     *
     * @param string $numRef the num ref
     *
     * @return self
     */
    public function setNumRef($numRef)
    {
        $this->numRef = $numRef;
        return $this;
    }

    /**
     * @return Person
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param Person $source
     *
     * @return self
     */
    public function setSource(Person $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\Person $version
     *
     * @return Person
     */
    public function addVersion(\AppBundle\Entity\Person $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\Person $version
     */
    public function removeVersion(\AppBundle\Entity\Person $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
