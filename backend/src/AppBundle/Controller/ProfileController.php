<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Enum\Status;
use AppBundle\Form\ProfileType;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class ProfileController extends BaseController
{
    protected $repository = 'AppBundle:User';
    
    protected $form = ProfileType::class;

    public function currentAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->createApiResponse($user);
    }

    public function saveAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $userManager = $this->get('fos_user.user_manager');

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        return $this->handleForm($user, $request);
    }

    public function getAclAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        $aclBuilder = $this->get('app.acl_builder');
        $acls = $aclBuilder->build();

        return $this->createApiResponse($acls);
    }

    protected function persistEntity($entity)
    {
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($entity);

        return $entity;
    }

    public function sendAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //$mailer = $this->get('app.mail.user_company');
        //$mailer->newUserMail($user);
        
        $templating = $this->get('templating');
        $content = $templating->render(
            'emails/company_submission.html.twig',
            ['user' => $user]
        );

        die($content);exit;

        return $this->createApiResponse($translator->trans('xxxxxx'));
    }
}
