<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Setting;
use AppBundle\Entity\SettingValue;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 *
 */
class SettingsSaiiSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $settingsValues = array();

    /**
     * (non-PHPdoc)
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    /**
     *
     * @param  LifecycleEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        $optionalStepRepository = $em->getRepository('AppBundle:OptionalStep');
        $metaData = $em->getClassMetadata('AppBundle:OptionalStep');

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Setting) {
                continue;
            }

            if ('saii' !== $entity->getName()) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['value'])) {
                return;
            }

            $newValue = $changedValues['value'][1];
            $steps = $optionalStepRepository->findAll();
            foreach ($steps as $step) {
                $oldCost = $step->getCost();
                $stepCost = $step->getIndexer() * $newValue;
                $step->setCost($stepCost);

                $this->compute($em, $step);
            }
        }
    }
}
