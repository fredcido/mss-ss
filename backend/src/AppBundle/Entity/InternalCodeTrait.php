<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 */
trait InternalCodeTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="internalCode", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $internalCode;

    /**
     * Set internalCode
     *
     * @param string $internalCode
     *
     * @return Employee
     */
    public function setInternalCode($internalCode)
    {
        $this->internalCode = $internalCode;

        return $this;
    }

    /**
     * Get internalCode
     *
     * @return string
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }
}
