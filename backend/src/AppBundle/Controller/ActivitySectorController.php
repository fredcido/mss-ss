<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ActivitySector;
use AppBundle\Enum;
use AppBundle\Security\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class ActivitySectorController extends BaseController
{
    protected $repository = 'AppBundle:ActivitySector';

    protected $form = 'AppBundle\Form\ActivitySectorType';

    protected $entity = ActivitySector::class;

    protected function getOrderBy()
    {
        return array('code' => 'asc', 'name' => 'asc');
    }
}
