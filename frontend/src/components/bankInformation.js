app.component('bankInformation', {
    templateUrl: 'views/components/bankInformation.html',
    bindings: {
        model: '=',
        disabled: '<'
    },
    controller: function(GenericService, ViewHelper, $translate) {
        var self = this;
        self.banks = [];

        self.addBank = function() {
            self.model.push({status: 1});
            ViewHelper.scrollTo('.scrollToBank:last');
        };

        self.removeBank = function(bank) {
            self.model.some(function(i) {
                if (i == bank) {
                    self.model.splice(self.model.indexOf(i), 1);
                }
            });
        };

        var _getBanks = function() {
            GenericService.find('bank').then(function(response) {
                self.banks = response.data;

                /*if (self.model) {
                    self.model.forEach(function(e) {
                        _selectAddressBank(e);
                    });
                }*/
            });
        };

        var init = function() {
            _getBanks();
        };

        var _selectAddressBank = function(bank) {
            ViewHelper.blockContainer(bank.id + '-address', $translate.instant('loading'));
            self.banks.some(function(i) {
                if (i.id == bank.bank) {
                    bank.addresses = i.addresses;
                }
            });
            ViewHelper.unblockContainer(bank.id + '-address');
        };

        self.changeBank = function(bank) {
            bank.addresses = null;
            _selectAddressBank(bank);
        };

        init();
    }
});
