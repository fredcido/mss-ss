/*!
 * APP init
 */
var app = {};

app = angular.module("app", [
    'ui.router',
    'ngCookies',
    'ui.bootstrap',
    'ngSanitize',
    'pascalprecht.translate',
    'ui.select',
    'ui.bootstrap.datetimepicker',
    'smart-table',
    'ui.utils.masks',
    'highcharts-ng',
    'ngFileUpload',
    'ngclipboard',
    'ui.tree'
]);

// lodash support
app.constant('_', window._);

app.config(function(AppConstant){
    if (AppConstant.prod) {
        var domain = location.protocol + "//" + location.hostname + "/backend/web/";
        AppConstant.apiUrl = domain;
    }
});

/*app.config(['$httpProvider', function ($httpProvider) {
    // enable http caching
   $httpProvider.defaults.cache = true;
}]);*/

app.run(function($rootScope, $stateParams, $state, $location, $translate, $cookies, AuthService, AclService, UserCompanyService, UserService) {

    // lodash support
    $rootScope._ = window._;

    if (!localStorage.getItem('lang')) {
        localStorage.setItem('lang', 'pt');
    }

    var langCurrent = localStorage.getItem('lang');
    console.log(langCurrent);
    $translate.use(langCurrent);

    $rootScope.location = $location;
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        
        if ('login' !== toState.name && !AuthService.getUserLogged()) {
            event.preventDefault();
            $rootScope.$evalAsync(function() {
                $state.go("login");
            });
        }

        if (!toState.resource || !toState.action) {
            return;
        }

        var hasPermission = AclService.canI(toState.resource, toState.action);
        if (!hasPermission) {
            event.preventDefault();
            $rootScope.$evalAsync(function() {
                $state.go("app.forbidden");
            });
        }
    });

    UserCompanyService.intercept();

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        if (error === "Not Authorized") {
            $state.go("login");
        }
    });
});
