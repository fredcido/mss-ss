<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * PublicWorker
 *
 * @ORM\Table(name="public_worker")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublicWorkerRepository")
 */
class PublicWorker
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\OneToOne(targetEntity="Person", inversedBy="publicWorker")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $person;

    /**
     * @var string
     *
     * @ORM\Column(name="identityNumber", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $identityNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="workTime", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $workTime;

    /**
     * @var int
     *
     * @ORM\Column(name="hasSubsidie", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $hasSubsidie;

    /**
     * @var string
     *
     * @ORM\Column(name="subsidieType", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $subsidieType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sinceWhen", type="date", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $sinceWhen;

    /**
     * @var string
     *
     * @ORM\Column(name="monthlyCost", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $monthlyCost;

    /**
     * @var PublicWorker
     *
     * @ORM\ManyToOne(targetEntity="PublicWorker", inversedBy="versions")
     * @JMS\Groups({"audit"})
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="PublicWorker", mappedBy="source")
     * @JMS\Groups({"audit"})
     */
    private $versions;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identityNumber
     *
     * @param string $identityNumber
     *
     * @return PublicWorker
     */
    public function setIdentityNumber($identityNumber)
    {
        $this->identityNumber = $identityNumber;

        return $this;
    }

    /**
     * Get identityNumber
     *
     * @return string
     */
    public function getIdentityNumber()
    {
        return $this->identityNumber;
    }

    /**
     * Set workTime
     *
     * @param integer $workTime
     *
     * @return PublicWorker
     */
    public function setWorkTime($workTime)
    {
        $this->workTime = $workTime;

        return $this;
    }

    /**
     * Get workTime
     *
     * @return int
     */
    public function getWorkTime()
    {
        return $this->workTime;
    }

    /**
     * Set hasSubsidie
     *
     * @param integer $hasSubsidie
     *
     * @return PublicWorker
     */
    public function setHasSubsidie($hasSubsidie)
    {
        $this->hasSubsidie = $hasSubsidie;

        return $this;
    }

    /**
     * Get hasSubsidie
     *
     * @return int
     */
    public function getHasSubsidie()
    {
        return $this->hasSubsidie;
    }

    /**
     * Set subsidieType
     *
     * @param string $subsidieType
     *
     * @return PublicWorker
     */
    public function setSubsidieType($subsidieType)
    {
        $this->subsidieType = $subsidieType;

        return $this;
    }

    /**
     * Get subsidieType
     *
     * @return string
     */
    public function getSubsidieType()
    {
        return $this->subsidieType;
    }

    /**
     * Set sinceWhen
     *
     * @param \DateTime $sinceWhen
     *
     * @return PublicWorker
     */
    public function setSinceWhen($sinceWhen)
    {
        $this->sinceWhen = $sinceWhen;

        return $this;
    }

    /**
     * Get sinceWhen
     *
     * @return \DateTime
     */
    public function getSinceWhen()
    {
        return $this->sinceWhen;
    }

    /**
     * Set monthlyCost
     *
     * @param string $monthlyCost
     *
     * @return PublicWorker
     */
    public function setMonthlyCost($monthlyCost)
    {
        $this->monthlyCost = $monthlyCost;

        return $this;
    }

    /**
     * Get monthlyCost
     *
     * @return string
     */
    public function getMonthlyCost()
    {
        return $this->monthlyCost;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Person $person
     *
     * @return PublicWorker
     */
    public function setPerson(\AppBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set source
     *
     * @param \AppBundle\Entity\PublicWorker $source
     *
     * @return PublicWorker
     */
    public function setSource(\AppBundle\Entity\PublicWorker $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \AppBundle\Entity\PublicWorker
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\PublicWorker $version
     *
     * @return PublicWorker
     */
    public function addVersion(\AppBundle\Entity\PublicWorker $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\PublicWorker $version
     */
    public function removeVersion(\AppBundle\Entity\PublicWorker $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
