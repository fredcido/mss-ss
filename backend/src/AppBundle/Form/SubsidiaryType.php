<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubsidiaryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('dateStartActivity', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('dateEmployees', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('activitySectors');

        $builder->add(
            'addresses',
            CollectionType::class,
            array(
                'entry_type'   => AddressType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Subsidiary',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_subsidiary';
    }
}
