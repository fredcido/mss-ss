app.service("WorkSituationService", function($http, AppConstant, EnumService) {

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.activitySectors) {
            data.activitySectors = data.activitySectors.map(function(i){
                return i.id;
            });
        }

        var convertObjects = [
            'person', 
            'employer', 
            'subdsidiary', 
            'contractType', 
            'regime', 
            'labourLaw', 
            'contractStatus', 
            'occupation', 
            'post', 
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        if (data.dateStart) {
            data.dateStart = moment(data.dateStart).format('YYYY-MM-DD');
        }

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        return model;
    };

    var _initModel = function() {
        var model = {
            //dateStart: moment().toDate()
        };

        return model;
    };


    return {
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel
    };
});
