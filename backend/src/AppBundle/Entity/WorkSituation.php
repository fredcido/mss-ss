<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * WorkSituation
 *
 * @ORM\Table(name="work_situation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkSituationRepository")
 */
class WorkSituation
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @JMS\Groups({"list", "details"})
     */
    private $person;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer", inversedBy="workSituations")
     * @JMS\Groups({"list", "details"})
     */
    private $employer;

    /**
     * @var Subsidiary
     *
     * @ORM\ManyToOne(targetEntity="Subsidiary")
     * @JMS\Groups({"list", "details"})
     */
    private $subdsidiary;

    /**
     * @var ContractType
     *
     * @ORM\ManyToOne(targetEntity="ContractType")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $contractType;

    /**
     * @var Regime
     *
     * @ORM\ManyToOne(targetEntity="Regime", fetch="EAGER")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $regime;

    /**
     * @var LabourLaw
     *
     * @ORM\ManyToOne(targetEntity="LabourLaw")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $labourLaw;

    /**
     * @var ContractStatus
     *
     * @ORM\ManyToOne(targetEntity="ContractStatus")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $contractStatus;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Post")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $post;

    /**
     * @var Occupation
     *
     * @ORM\ManyToOne(targetEntity="Occupation")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $occupation;

    /**
     * @ORM\ManyToMany(targetEntity="ActivitySector")
     * @JMS\Groups({"list", "details"})
     */
    private $activitySectors;

    /**
     * @var string
     *
     * @ORM\Column(name="payrollNumber", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $payrollNumber;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activitySectors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param Person $person
     *
     * @return WorkSituation
     */
    public function setPerson(Person $person = null)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Get person
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set employer
     *
     * @param Employer $employer
     *
     * @return WorkSituation
     */
    public function setEmployer(Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Set sector
     *
     * @param Sector $sector
     *
     * @return WorkSituation
     */
    public function setSector(Sector $sector = null)
    {
        $this->sector = $sector;
        return $this;
    }

    /**
     * Get sector
     *
     * @return Sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set contractType
     *
     * @param ContractType $contractType
     *
     * @return WorkSituation
     */
    public function setContractType(ContractType $contractType = null)
    {
        $this->contractType = $contractType;
        return $this;
    }

    /**
     * Get contractType
     *
     * @return ContractType
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Set regime
     *
     * @param Regime $regime
     *
     * @return WorkSituation
     */
    public function setRegime(Regime $regime = null)
    {
        $this->regime = $regime;
        return $this;
    }

    /**
     * Get regime
     *
     * @return Regime
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * Set labourLaw
     *
     * @param LabourLaw $labourLaw
     *
     * @return WorkSituation
     */
    public function setLabourLaw(LabourLaw $labourLaw = null)
    {
        $this->labourLaw = $labourLaw;
        return $this;
    }

    /**
     * Get labourLaw
     *
     * @return LabourLaw
     */
    public function getLabourLaw()
    {
        return $this->labourLaw;
    }

    /**
     * Set contractStatus
     *
     * @param ContractStatus $contractStatus
     *
     * @return WorkSituation
     */
    public function setContractStatus(ContractStatus $contractStatus = null)
    {
        $this->contractStatus = $contractStatus;

        return $this;
    }

    /**
     * Get contractStatus
     *
     * @return ContractStatus
     */
    public function getContractStatus()
    {
        return $this->contractStatus;
    }

    /**
     * Set post
     *
     * @param Post $post
     *
     * @return WorkSituation
     */
    public function setPost(Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set occupation
     *
     * @param Occupation $occupation
     *
     * @return WorkSituation
     */
    public function setOccupation(Occupation $occupation = null)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return Occupation
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Add activitySector
     *
     * @param ActivitySector $activitySector
     *
     * @return WorkSituation
     */
    public function addActivitySector(ActivitySector $activitySector)
    {
        if ($this->activitySectors && $this->activitySectors->contains($activitySector)) {
            return $this;
        }

        $this->activitySectors[] = $activitySector;

        return $this;
    }

    /**
     * Remove activitySector
     *
     * @param ActivitySector $activitySector
     */
    public function removeActivitySector(ActivitySector $activitySector)
    {
        $this->activitySectors->removeElement($activitySector);
    }

    /**
     * Get activitySectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivitySectors()
    {
        return $this->activitySectors;
    }

    /**
     * Gets the value of subdsidiary.
     *
     * @return Subsidiary
     */
    public function getSubdsidiary()
    {
        return $this->subdsidiary;
    }

    /**
     * Sets the value of subdsidiary.
     *
     * @param Subsidiary $subdsidiary the subdsidiary
     *
     * @return self
     */
    public function setSubdsidiary(Subsidiary $subdsidiary = null)
    {
        $this->subdsidiary = $subdsidiary;
        return $this;
    }

    public function getPlaceOfWork()
    {
        $place = $this->getEmployer();
        if ($this->getSubdsidiary()) {
            $place = $this->getSubdsidiary();
        }

        return $place;
    }

    /**
     * @return string
     */
    public function getPayrollNumber()
    {
        return $this->payrollNumber;
    }

    /**
     * @param string $payrollNumber
     *
     * @return self
     */
    public function setPayrollNumber($payrollNumber)
    {
        $this->payrollNumber = $payrollNumber;

        return $this;
    }
}
