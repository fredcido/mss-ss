<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityManager;

trait ComputeChangeTrait
{
    public function compute(EntityManager $em, $entity)
    {
        $uow = $em->getUnitOfWork();
        $metadata = $em->getClassMetadata(get_class($entity));

        if ($uow->getEntityChangeSet($entity)) {
            /** If the entity has pending changes, we need to recompute/merge. */
            $uow->recomputeSingleEntityChangeSet($metadata, $entity);
        } else {
            /** If there are no changes, we compute from scratch? */
            $uow->computeChangeSet($metadata, $entity);
        }
    }
}
