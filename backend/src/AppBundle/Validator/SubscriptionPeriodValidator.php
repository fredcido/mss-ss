<?php

namespace AppBundle\Validator;

use AppBundle\Entity\PersonAwareInterface;
use AppBundle\Service\Settings;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SubscriptionPeriodValidator extends ConstraintValidator
{
    use ContainerAwareTrait;

    public function validate($value, Constraint $constraint)
    {
        if ($value->isCeased() || $value->isSuspended()) {
            return;
        }

        $person = $value->getPerson();
        $regime = $value->getWorkSituation()->getRegime();

        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('AppBundle:CompulsoryAccessionSubscription');

        $activeEmployments = $repository->findActiveEmployments($person);

        if (!empty($activeEmployments)) {
            foreach ($activeEmployments as $activeEmployment) {

                if ($value->getId() && $activeEmployment->getId() == $value->getId())
                    continue;
                $activeRegime = $activeEmployment->getWorkSituation()->getRegime();
                $doubleFullTime = (int)$activeRegime->getFullTime() + (int)$regime->getFullTime();

                if ($doubleFullTime > 0) {
                    $this->context->buildViolation($constraint->message)
                                    ->atPath('workSituation.regime')
                                    ->addViolation();

                    break;
                }
            }
        }
    }
}
