app.controller("ContributionFormController", function ($scope, $state, $uibModal, $timeout, $stateParams, ViewHelper, $translate, GenericService, EnumService, PersonService) {
    var _endpoint = "contribution";
    var _stateList = "app.contribution-list";
    var _stateForm = "app.contribution-form";
    var _personOvertime = {};
    $scope.id = $stateParams.id || null;
    $scope.person = $stateParams.person || null;
    $scope.overtimes = [];
    $scope.contribution_employee = 0;
    $scope.contribution_employer = 0;
    $scope.calendar = {
        start: false,
        end: false
    };
    $scope.months = EnumService.getMonths();
    $scope.contractsTypes = [];
    $scope.contractsStatus = [];
    $scope.licensesCombo = [];
    $scope.contractsWork = [{
        id: 1,
        name: $translate.instant('continous')
    }, {
        id: 0,
        name: $translate.instant('suspension')
    }];

    $scope.model = {
        licenses: [],
        debt: 0,
        year: moment().toDate(),
        month: parseInt(moment().subtract(1, 'months').format('M')),
        dateStart: moment().subtract(1, 'months').startOf('month').toDate(),
        dateEnd: moment().subtract(1, 'months').endOf('month').toDate(),
        person: null,
        employer: null,
        workDays: 0,
        absences: 0,
        declaredIncome: 0,
        daysInPeriod: 0,
        overtime: [],
        hourCost: 0,
        hoursDay: 0,
        workedDays: 0,
        theoricalIncome: 0,
        allowance: 0,
        totalTheoricalIncome: 0,
        theoricalEmployeeContribution: 0,
        theoricalEmployerContribution: 0,
        theoricalTotalEmployeeContribution: 0,
        theoricalTotalEmployerContribution: 0,
        parEmployeeContribution: 0,
        parEmployerContribution: 0,
        theoricalContribution: 0,
        parTotalContribution: 0,
        theoricalTotalContribution: 0,
        paidEmployeeContribution: 0,
        paidEmployerContribution: 0,
        paidTotalContribution: 0,
        employeeTax: 0,
        employerTax: 0,
        voluntary: 0
    };

    $scope.enumService = EnumService;

    $scope.personModal = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            component: 'personSearchDialog'
        });

        modalInstance.result.then(function (person) {
            if (person) {
                _populatePersonSearch(person);
            }
        }, function () {

        });
    };

    $scope.dateStartOptions = {
        maxDate: $scope.model.dateEnd
    };
    $scope.dateEndOptions = {
        minDate: $scope.model.dateStart
    };

    $scope.cancelEmployer = function () {
        $scope.model.employer = null;
        $scope.employerDisplay = null;
        $scope.employerCapitalDisplay = null;
    };

    $scope.employerModal = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            component: 'employerSearchDialog'
        });

        modalInstance.result.then(function (employer) {
            if (employer) {
                _populateEmployerSearch(employer);
            }
        }, function () {
            console.log('close modal');
        });
    };

    $scope.openCalendar = function (type) {
        $scope.calendar[type] = true;
    };

    $scope.addLicense = function () {
        $scope.model.licenses.push({
            type: null,
            days: 0
        });
        ViewHelper.scrollTo('.trLicense:last');
    };

    $scope.removeLicense = function (license) {
        $scope.model.licenses.some(function (i) {
            if (i == license) {
                $scope.model.licenses.splice($scope.model.licenses.indexOf(i), 1);
            }
        });

        _calculateAll();
    };

    $scope.calculateWorkDay = function () {
        _getWorkDay();

        $scope.dateStartOptions.maxDate = $scope.model.dateEnd;
        $scope.dateEndOptions.minDate = $scope.model.dateStart;
    };

    $scope.calculateOvertime = function () {
        $scope.model.overtime.map(function (o) {
            o.total = o.value * o.data.ratio * $scope.model.hourCost;
        });

        $scope.model.totalOvertime = $scope.model.overtime.reduce(function (previous, current) {
            return previous + current.total;
        }, 0);
    };

    $scope.submit = function (form, model) {
        if (form.$valid) {
            ViewHelper.blockContainer('.panel', '');
            var data = _preparateData(model);

            if ($scope.id) {
                GenericService.edit(_endpoint, $scope.id, data, _callBackSubmit)
                    .catch(function (response) {
                        _callBackErrorSubmit(response.data);
                    });
            } else {
                GenericService.add(_endpoint, data, _callBackSubmit)
                    .catch(function (response) {
                        _callBackErrorSubmit(response.data);
                    });
            }

        } else {
            angular.forEach($scope.form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$dirty = true;
                });
            });
            swal($translate.instant('invalid_form_title'), $translate.instant('invalid_form_message'), 'warning');
        }
    };

    var _populatePersonSearch = function (person, searchLatest) {
        $scope.model.personDisplay = person.identityCode + ' - ' + person.name;
        $scope.model.person = person.id;
        $scope.person = person;

        if (person.dod) {
            $scope.dateEndOptions.maxDate = moment(person.dod);
        }

        if (false !== searchLatest) {
            _getLatestContribution(person);
        }
    };

    var _getLatestContribution = function (person) {
        PersonService.fetchCurrentContribution(person.id).then(
            function (response) {
                var current = response.data;
                if (current.currentContract) {
                    var contract = current.currentContract;
                    _populateEmployerSearch(contract.employer);
                    $scope.model.contractType = contract.contractType;
                    $scope.model.hourCost = contract.hourCost;
                    $scope.model.hoursDay = contract.hoursDay;
                    $scope.model.contractStatus = contract.contractStatus;
                }
            }
        );
    };

    var _populateEmployerSearch = function (employer) {
        $scope.model.employerDisplay = employer.shortName + ' - ' + employer.name;
        $scope.model.employer = employer.id;
        $scope.model.employerSector = employer.sector;
        $scope.model.voluntary = 0;
    };

    var _callBackSubmit = function (data) {
        $timeout(function () {
            ViewHelper.unblockContainer('.panel');
            $state.go(_stateList);
        }, 200);
    };

    var _callBackErrorSubmit = function (responseServer) {
        $scope.form.$invalid = true;
        $scope.errorServer = responseServer.errors;
        angular.forEach($scope.errorServer, function(msg, field) {
            $scope.errorServer[field] = msg.join('; ');
            $scope.form[field].$invalid = true;
        });

        ViewHelper.showErrorApi(responseServer);
        ViewHelper.unblockContainer('.panel');
    };

    var _preparateData = function (model) {
        var data = angular.copy(model);

        if (data.licenses) {
            data.licenses.some(function (i) {
                i.license = i.license.id;
            });
        }

        if (data.contractType) {
            data.contractType = data.contractType.id;
        }

        if (data.contractStatus) {
            data.contractStatus = data.contractStatus.id;
        }

        data.year = moment(data.year).format('YYYY');
        data.dateStart = moment(data.dateStart).format('YYYY-MM-DD');
        data.dateEnd = moment(data.dateEnd).format('YYYY-MM-DD');

        return data;
    };

    var _getContractsType = function () {
        GenericService.find('contract-type').then(function (response) {
            $scope.contractsTypes = response.data;
        });
    };

    var _getContractStatus = function () {
        GenericService.find('contract-status').then(function (response) {
            $scope.contractsStatus = response.data;
        });
    };

    var _getLicenses = function () {
        GenericService.find('license').then(function (response) {
            $scope.licensesCombo = response.data;
        });
    };

    var _getOvertime = function () {
        GenericService.find('overtime').then(function (response) {
            $scope.overtimes = response.data;
            $scope.overtimes.map(function (i) {
                $scope.model.overtime.push({
                    total: undefined === _personOvertime[i.id] ? 0 : _personOvertime[i.id].total,
                    value: undefined === _personOvertime[i.id] ? 0 : _personOvertime[i.id].value,
                    overtime: i.id,
                    data: i,
                    id: undefined === _personOvertime[i.id] ? null : _personOvertime[i.id].id
                });
            });

            _calculateAll();
        });
    };

    var _getSettingsContributionEmployee = function () {
        GenericService.get('setting', 2).then(function (response) {
            $scope.model.employeeTax = response.data.value;
        });
    };

    var _getSettingsContributionEmployer = function () {
        GenericService.get('setting', 1).then(function (response) {
            $scope.model.employerTax = response.data.value;
        });
    };

    var _getWorkDay = function () {
        var start = moment($scope.model.dateStart).format("YYYY-MM-DD");
        var end = moment($scope.model.dateEnd).format("YYYY-MM-DD");
        GenericService.httpGet('calendar/work-days/' + start + '/' + end).then(function (response) {
            $scope.model.workDays = response.data.days;
        });
    };

    var _calculateDaysInPeriod = function (workDays, absences) {
        var days = ($scope.model.workDays || 0) - ($scope.model.absences || 0);
        $scope.model.daysInPeriod = days;
    };

    $scope.getMaxLicense = function(license) {
        var max = null;
        if (license) {
            angular.forEach(
                license.licenseSectors,
                function(licenseSector) {
                    if (licenseSector.sector.id === $scope.model.employerSector.id) {
                        max = licenseSector.daysInMonth;
                    }
                }
            );
        }
        
        console.log(license, $scope.model.employerSector, max);
        return max;
    };

    $scope.calculateWorkedDays = function () {
        var workDays = $scope.model.workDays;
        var absences = $scope.model.absences;
        var licensesDays = 0;

        angular.forEach(
            $scope.model.licenses,
            function (license) {
                if (!license.license || license.license.type != 1) {
                    return true;
                }

                licensesDays += parseInt(license.days);
            }
        );

        $scope.model.workedDays = workDays - absences - licensesDays;
    };
    $scope.calculateTheoricalIncome = function () {
        $scope.model.theoricalIncome = ($scope.model.daysInPeriod * $scope.model.hourCost * $scope.model.hoursDay);
    };
    $scope.calculateHourCost = function () {
        $scope.model.hourCost = ($scope.model.theoricalIncome / $scope.model.daysInPeriod / $scope.model.hoursDay);
    };
    $scope.calculateDeclaredIncome = function () {
        $scope.model.declaredIncome = $scope.model.workedDays * $scope.model.hourCost * $scope.model.hoursDay + $scope.model.totalOvertime + $scope.model.allowance;
    };
    $scope.calculateTotalTheoricalIncome = function () {
        $scope.model.totalTheoricalIncome = $scope.model.theoricalIncome + $scope.model.totalOvertime + $scope.model.allowance;
    };
    $scope.calculateTheoricalEmployeeContribution = function () {
        $scope.model.theoricalEmployeeContribution = $scope.model.declaredIncome * ($scope.model.employeeTax / 100);
    };
    $scope.calculateTheoricalEmployerContribution = function () {
        $scope.model.theoricalEmployerContribution = $scope.model.declaredIncome * ($scope.model.employerTax / 100);
    };
    $scope.calculatePaidTotalContribution = function () {
        $scope.model.paidTotalContribution = $scope.model.paidEmployeeContribution + $scope.model.paidEmployerContribution;
        $scope.calculateDebt();
    };
    $scope.calculateParEmployeeContribution = function () {
        $scope.model.parEmployeeContribution = ($scope.model.totalTheoricalIncome - $scope.model.declaredIncome) * ($scope.model.employeeTax / 100);
    };
    $scope.calculateParEmployerContribution = function () {
        $scope.model.parEmployerContribution = ($scope.model.totalTheoricalIncome - $scope.model.declaredIncome) * ($scope.model.employerTax / 100);
    };
    $scope.calculateParTotalContribution = function () {
        $scope.model.parTotalContribution = $scope.model.parEmployeeContribution + $scope.model.parEmployerContribution;
    };
    $scope.calculateTheoricalContribution = function () {
        $scope.model.theoricalContribution = $scope.model.theoricalEmployeeContribution + $scope.model.theoricalEmployerContribution;
        $scope.model.paidTotalContribution = $scope.model.theoricalContribution;
        $scope.calculateDebt();
    };
    $scope.calculateTheoricalTotalEmployerContribution = function () {
        $scope.model.theoricalTotalEmployerContribution = $scope.model.theoricalEmployerContribution + $scope.model.parEmployerContribution;
    };
    $scope.calculateTheoricalTotalEmployeeContribution = function () {
        $scope.model.theoricalTotalEmployeeContribution = $scope.model.theoricalEmployeeContribution + $scope.model.parEmployeeContribution;
    };
    $scope.calculateTheoricalTotalContribution = function () {
        $scope.model.theoricalTotalContribution = $scope.model.theoricalTotalEmployeeContribution + $scope.model.theoricalTotalEmployerContribution;
    };
    $scope.calculateDebt = function () {
        $scope.model.debt = $scope.model.paidTotalContribution - $scope.model.theoricalContribution;
    };

     var _calculateAll = function () {
        _calculateDaysInPeriod();

        $scope.calculateWorkedDays();
        $scope.calculateOvertime();

        $scope.calculateDeclaredIncome();
        $scope.calculateTheoricalIncome();
        $scope.calculateTotalTheoricalIncome();

        $scope.calculateParEmployeeContribution();
        $scope.calculateParEmployerContribution();
        $scope.calculateParTotalContribution();

        $scope.calculateTheoricalEmployeeContribution();
        $scope.calculateTheoricalEmployerContribution();
        $scope.calculateTheoricalTotalEmployeeContribution();
        $scope.calculateTheoricalTotalEmployerContribution();
        $scope.calculateTheoricalContribution();

        $scope.calculateTheoricalTotalContribution();

        //$scope.calculateDebt();
    };

    $scope.$watch('model.workedDays', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.workDays', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.absences', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.daysInPeriod', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.hoursDay', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.hourCost', function (newValue, oldValue) {
        $scope.calculateTheoricalIncome();
        _calculateAll();
    });
    $scope.$watch('model.theoricalIncome', function (newValue, oldValue) {
        $scope.calculateHourCost();
        _calculateAll();
    });
    $scope.$watch('model.allowance', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.declaredIncome', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.employeeTax', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.employerTax', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.paidEmployeeContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.paidEmployerContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.parEmployeeContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.parEmployerContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.totalTheoricalIncome', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.theoricalEmployeeContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.theoricalEmployerContribution', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.totalOvertime', function (newValue, oldValue) {
        _calculateAll();
    });
    $scope.$watch('model.paidTotalContribution', function (newValue, oldValue) {
       $scope.calculateDebt();
    });


    var _init = function () {
        _getContractsType();
        _getContractStatus();
        _getLicenses();

        // Populate
        if ($scope.id !== null && $scope.id !== '') {
            ViewHelper.blockContainer('.panel', $translate.instant('loading'));
            GenericService.get(_endpoint, $scope.id).then(function (response) {
                var data = response.data;
                delete data.id;

                angular.forEach(
                    data.overtime,
                    function (o) {
                        _personOvertime[o.overtime.id] = o;
                    }
                );

                data.overtime = [];
                data.dateStart = moment(data.dateStart, "YYYY/MM/DD").toDate();
                data.dateEnd = moment(data.dateEnd, "YYYY/MM/DD").toDate();
                data.hasContract = data.hasContract;

                if (data.person) {
                    _populatePersonSearch(data.person, false);
                }

                if (data.employer) {
                    _populateEmployerSearch(data.employer);
                }

                _getOvertime();
                $scope.model = data;

                ViewHelper.unblockContainer('.panel');
            });
        } else {
            _getSettingsContributionEmployee();
            _getSettingsContributionEmployer();
            _getWorkDay();
            _getOvertime();
        }

        if ($scope.person !== null && $scope.person !== '') {
            GenericService.get('person', $scope.person).then(
                function(response) {
                    _populatePersonSearch(response.data, true);
                }
            );
        }
    };

    _init();
});
