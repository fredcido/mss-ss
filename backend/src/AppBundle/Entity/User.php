<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table("users")
 * @ORM\Entity
 * @UniqueEntity(fields="username", message="The username is already in use, choose another.")
 * @UniqueEntity(fields="email", message="The email is already in use, choose another.")
 */
class User extends BaseUser
{
    use EntityPermissionTrait;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    protected $id;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer", cascade={"persist", "remove"},)
     * @JMS\Groups({"list", "details"})
     */
    private $employer;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", cascade={"persist", "remove"},)
     * @JMS\Groups({"list", "details"})
     */
    private $person;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $locale;

    private $cleanPassword;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set employer
     *
     * @param Employer $employer
     *
     * @return User
     */
    public function setEmployer(Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Gets the value of companyName.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Sets the value of companyName.
     *
     * @param string $companyName the company name
     *
     * @return self
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * Gets the value of locale.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Sets the value of locale.
     *
     * @param string $locale the locale
     *
     * @return self
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Gets the value of person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets the value of person.
     *
     * @param Person $person the person
     *
     * @return self
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;
        return $this;
    }


    /**
     * Gets the value of employeeName.
     *
     * @return string
     */
    public function getEmployeeName()
    {
        return $this->employeeName;
    }

    /**
     * Sets the value of employeeName.
     *
     * @param string $employeeName the employee name
     *
     * @return self
     */
    public function setEmployeeName($employeeName)
    {
        $this->employeeName = $employeeName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCleanPassword()
    {
        return $this->cleanPassword;
    }

    /**
     * @param mixed $cleanPassword
     *
     * @return self
     */
    public function setCleanPassword($cleanPassword)
    {
        $this->cleanPassword = $cleanPassword;

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password)
    {
        $this->cleanPassword = $password;
        return parent::setPlainPassword($password);
    }
}
