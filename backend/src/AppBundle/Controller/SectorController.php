<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sector;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class SectorController extends BaseController
{
    protected $repository = 'AppBundle:Sector';

    protected $form = 'AppBundle\Form\SectorType';

    protected $entity = Sector::class;
}
