<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wear
 *
 * @ORM\Table(name="wear")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WearRepository")
 * @UniqueEntity(fields="name", message="The name is already in use, choose another.")
 */
class Wear
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="OccupationWear", mappedBy="wear", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $occupationWears;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Wear
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->occupationWears = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add occupationWear
     *
     * @param OccupationWear $occupationWear
     *
     * @return Wear
     */
    public function addOccupationWear(OccupationWear $occupationWear)
    {
        $this->occupationWears[] = $occupationWear;

        return $this;
    }

    /**
     * Remove occupationWear
     *
     * @param OccupationWear $occupationWear
     */
    public function removeOccupationWear(OccupationWear $occupationWear)
    {
        $this->occupationWears->removeElement($occupationWear);
    }

    /**
     * Get occupationWears
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOccupationWears()
    {
        return $this->occupationWears;
    }
}
