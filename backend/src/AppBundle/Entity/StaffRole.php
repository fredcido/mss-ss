<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * StaffRole
 *
 * @ORM\Table(name="staff_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaffRoleRepository")
 */
class StaffRole
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="since", type="datetime")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Expression(
     *     "this.getStaff().getEmployer().getDateStartActivity() <= value",
     *     message="staff_error_min_date"
     * )
     */
    private $since;

    /**
     * @var int
     *
     * @ORM\Column(name="paidRole", type="smallint", options={"default" : 0})
     * @JMS\Groups({"list", "details", "form"})
     */
    private $paidRole = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @Assert\NotBlank(message = "not_blank")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="Staff", inversedBy="roles")
     * @Assert\NotBlank(message = "not_blank")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $staff;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set since
     *
     * @param \DateTime $since
     *
     * @return StaffRole
     */
    public function setSince($since)
    {
        $this->since = $since;

        return $this;
    }

    /**
     * Get since
     *
     * @return \DateTime
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * Set paidRole
     *
     * @param integer $paidRole
     *
     * @return StaffRole
     */
    public function setPaidRole($paidRole)
    {
        $this->paidRole = (int)$paidRole;
        return $this;
    }

    /**
     * Get paidRole
     *
     * @return int
     */
    public function getPaidRole()
    {
        return $this->paidRole;
    }

    /**
     * Set role
     *
     * @param Post $role
     *
     * @return StaffRole
     */
    public function setRole(Post $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return Post
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set staff
     *
     * @param Staff $staff
     *
     * @return StaffRole
     */
    public function setStaff(Staff $staff = null)
    {
        $this->staff = $staff;

        return $this;
    }

    /**
     * Get staff
     *
     * @return Staff
     */
    public function getStaff()
    {
        return $this->staff;
    }
}
