<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContributionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('dateEnd', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('workDays')
            ->add('workedDays')
            ->add('daysInPeriod')
            ->add('hourCost')
            ->add('hoursDay')
            ->add('theoricalIncome')
            ->add('totalOvertime')
            ->add('allowance')
            ->add('declaredIncome')
            ->add('totalTheoricalIncome')
            ->add('voluntary')
            ->add('absences')
            ->add('debt')
            ->add('employeeTax')
            ->add('theoricalEmployeeContribution')
            ->add('employerTax')
            ->add('theoricalEmployerContribution')
            ->add('theoricalContribution')
            ->add('parEmployeeContribution')
            ->add('parEmployerContribution')
            ->add('parTotalContribution')
            ->add('theoricalTotalEmployeeContribution')
            ->add('theoricalTotalEmployerContribution')
            ->add('theoricalTotalContribution')
            ->add('paidEmployeeContribution')
            ->add('paidEmployerContribution')
            ->add('paidTotalContribution')
            ->add('person')
            ->add('employer')
            ->add('contractType')
            ->add('contractStatus');

        $builder->add(
            'licenses',
            CollectionType::class,
            array(
                'entry_type'   => EmployeeLicenseType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );

        /*$builder->add(
            'overtime',
            CollectionType::class,
            array(
                'entry_type'   => ContributionOvertimeType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );*/
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Contribution',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_contribution';
    }
}
