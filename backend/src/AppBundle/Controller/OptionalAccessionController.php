<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OptionalAccession;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class OptionalAccessionController extends BaseController
{
    protected $repository = 'AppBundle:OptionalAccession';

    protected $form = 'AppBundle\Form\OptionalAccessionType';

    protected $entity = OptionalAccession::class;

    /**
     * @param  int  $id
     * @param  Request $request
     * @return array
     */
    public function getCurrentByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $accession = $this->getRepository()->getCurrentByPerson($person);
        if (!$accession) {
            throw $this->createNotFoundException('There is no active optional accession for the provided person');
        }

        return $this->createApiResponse($accession, Response::HTTP_OK, array('details'));
    }

    /**
     * @param  int  $id
     * @param  Request $request
     * @return array
     */
    public function getLatestByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $accession = $this->getRepository()->getLatestByPerson($person);
        if (!$accession) {
            throw $this->createNotFoundException('There is no optional accession for the provided person');
        }

        return $this->createApiResponse($accession, Response::HTTP_OK, array('details'));
    }
}
