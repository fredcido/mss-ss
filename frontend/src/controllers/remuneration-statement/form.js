app.controller('RemunerationStatementController', 
    function(
        $scope,
        $state,
        $stateParams,
        $q,
        $timeout,
        $translate,
        AuthService,
        ViewHelper,  
        GenericService, 
        PersonService, 
        EmployerService, 
        CompulsoryAccessionService, 
        EnumService,
        RemunerationStatementService,
        RemunerationPersonService,
        RemunerationDayService,
        RemunerationRevisionService,
        SettingService
    ) {
        var endpoint = 'remuneration-statement';

        $scope.id = $stateParams.id || null;
        $scope.employerId = $stateParams.employer || null;
        $scope.user = AuthService.getUserLogged();

        $scope.employer = null;
        $scope.employeesSubscribed = [];
        $scope.model = RemunerationStatementService.initModel();
        
        $scope.maxYear = parseInt(moment().format('YYYY'));
        $scope.maxDays = parseInt(SettingService.get('worked_days_reference')); //parseInt(moment().format('D'));
        $scope.maxMonth = parseInt(moment().format('MM'));
        $scope.today = new Date();
        $scope.months = [];
        $scope.days = [];
      
        $scope.lowDayLimit = SettingService.get('lower_limit_mandatory_contribution');
        $scope.highDayLimit = SettingService.get('upper_limit_mandatory_contribution');

        $scope.assessmentOptions = [
            {id: 1, label: $translate.instant('correct')},
            {id: 0, label: $translate.instant('incorrect')},
        ];

        $scope.workers = {
            total: 0,
            national: 0,
            percent: 0
        };

        // FUNCTIONS
        $scope.canEdit = canEdit;
        $scope.allowLowLimit = allowLowLimit;
        $scope.allowHighLimit = allowHighLimit;
        $scope.setCompany = setCompany;
        $scope.calcWorkTotal = calcWorkTotal;
        $scope.save = save;
        $scope.addReview = addReview;
        $scope.removeRevision = removeRevision;
        $scope.removeTCO = removeTCO;
        $scope.addTCO = addTCO;

        if ($scope.user.isEmployer()) {
            $scope.employerId = $scope.user.employer.internalCode;
        }

        $scope.$watch(
            '[model.year, model.month]',
            function(newValues) {
                var monthYear = newValues.join('-');
                //$scope.maxDays = parseInt(moment(monthYear, "YYYY-MM").daysInMonth());

                //initDays();
                //initRemunerationDay();
                checkRemuneration();
            }
        );

         $scope.$watch(
            'model.year',
            function(year) {
                if (year == $scope.maxYear) {
                    $scope.maxMonth = parseInt(moment().format('MM'));
                } else {
                    $scope.maxMonth = 12;
                }

                initMonths();
            }
        );

        init();

        function init() {
            if ($scope.user.isEmployer()) {
                $scope.employerId = $scope.user.employer.internalCode;
            }

            if ($scope.employerId) {
                initEmployer();
            }

            if ($scope.id) {
                initRemuneration();
            }

            initMonths();
            initDays();
        }

        function checkRemuneration() {
            if (!$scope.employer || !$scope.model.month) return;

            var filters = {
                employer: $scope.employer.id,
                month: $scope.model.month,
                year: $scope.model.year
            };

            search(filters).then(function(response){
                var data = response.data;

                if (data.length) {
                    swal({ 
                        title: $translate.instant('remuneration_statement_already_inserted'),
                        text: $translate.instant('remuneration_statement_already_inserted'),
                        type: "error" 
                    }, function(){
                        $scope.$apply(function(){
                            $scope.model.month = null;
                        });
                    });
                }
            })
        }

        function initRemuneration() {
            ViewHelper.blockContainer('.container', $translate.instant('loading'));
            return GenericService.get(endpoint, $scope.id).then(function (response) {
                $scope.model = RemunerationStatementService.populateModel(response.data);

                // Make sure the Employer user can see only its own declaration
                if ($scope.user.isEmployer()) {
                    if ($scope.model.employer.internalCode != $scope.user.employer.internalCode) {
                        $timeout(function(){
                            $state.go(
                                'app.remuneration-statement-employer', 
                                {
                                    employer: $scope.employer.internalCode,
                                    ignoreLocalStorage: true
                                }
                            );
                        });
                    }
                }

                $scope.employer = $scope.model.employer;

                ViewHelper.unblockContainer('.container');
                return $q.when(response);
            });
        }

        function initMonths() {
            $scope.months = [];
            for (var m = 1; m <= $scope.maxMonth; m++) {
                $scope.months.push(m);
            }
        }

        function initDays() {
            $scope.days = [];
            for (var d = 1; d <= $scope.maxDays; d++) {
                $scope.days.push(d);
            }
        }

        function initRemunerationDay() {
            $scope.model.remunerationPeople.some(function(person){
                if (person.period) {
                    return false;
                }

                person.remunerationDays = [];
                for (var d = 1; d <= $scope.maxDays; d++) {
                    var day = RemunerationDayService.initModel();
                    day.day = d;
                    person.remunerationDays.push(day);
                }

                person.workedDays = 0;
            });
        }

        function initEmployer() {
            ViewHelper.blockContainer('.container', $translate.instant('loading'));
            var version = EnumService.getVersion('standard');

            return GenericService.getVersion('employer', $scope.employerId, version).then(function (response) {
                $scope.employer = EmployerService.populateModel(response.data);
                $scope.model.employer = $scope.employer;

                var invalidStatuses = [
                    EnumService.getSubscriptionStatus('pending'),
                    EnumService.getSubscriptionStatus('error')
                ];

                // The employer cannot be either pending or with error
                // it needs to be submitted first
                if ($scope.employer.isOfAnyStatus(invalidStatuses)) {
                    swal({ 
                        title: $translate.instant('company_not_submitted'),
                        text: $translate.instant('company_not_submitted_text'),
                        type: "error" 
                      },
                      function(){
                        $timeout(function(){
                            $state.go('app.company-form', {
                                id: $scope.employer.internalCode,
                                ignoreLocalStorage: true,
                                updateSubscription: '0'
                            });
                        });
                    });
                }

                ViewHelper.unblockContainer('.container');
                getEmployeesSubscribed();
                return $q.when(response);
            });
        }

        function canEdit() {
            return $scope.user.isEmployer() 
                && $scope.employer.internalCode == $scope.user.employer.internalCode
                && allowLowLimit()
                && allowHighLimit();
        }

        function setCompany(employer) {
            $timeout(function() {
                $state.go(
                    'app.remuneration-statement-employer', 
                    {
                        employer: employer.internalCode,
                        ignoreLocalStorage: true
                    }
                );
            }, 500);
        };

        function getEmployeesSubscribed() {
            ViewHelper.blockContainer('.workers-container', $translate.instant('loading'));

            var uri = 'compulsory-subscription/search';
            var dataIni = moment($scope.model.year + '-' + $scope.model.month + '-01', 'YYYY-MM-DD');
            var filters = {
                employer: $scope.employer.id,
                version: EnumService.getVersion('standard'),
                wasActiveBetween: {
                    dateStart: dataIni.format('YYYY-MM-DD'),
                    dateEnd: dataIni.endOf('month').format('YYYY-MM-DD')
                },
            };

            GenericService.httpGet(uri, filters).then(function(response){
                $scope.workers = [];
                response.data.map(function(subscription){
                    var remunerationPerson = RemunerationPersonService.initModel();
                    var person = subscription.compulsoryAccession.person;

                    remunerationPerson.compulsoryAccessionSubscription = subscription;
                    remunerationPerson.period = subscription.workSituation.regime.id == 1 ? 1 : 0;
                    remunerationPerson.selected = 1;

                    $scope.model.remunerationPeople.push(remunerationPerson);

                    if (person.isNational()) {
                        $scope.workers.national++;
                    }
                });

                $scope.employeesSubscribed = response.data;

                initRemunerationDay();

                $scope.workers.total = response.data.length;
                $scope.workers.percent = $scope.workers.national / $scope.workers.total * 100;
                
                ViewHelper.unblockContainer('.workers-container');
            });
        }

        function calcWorkTotal(row) {
            row.workedDays = row.remunerationDays.filter(function(day){
                return day.worked;
            }).length;
        }

        function callbackSave(data) {
            $state.go('app.remuneration-statement', {
                id: data.id,
                ignoreLocalStorage: true
            });

            /*ViewHelper.unblockContainer('.container');
            $scope.model = RemunerationStatementService.populateModel(data);
            ViewHelper.scrollTo('.inss-logo');*/
        }

        function callbackErrorSave(form) {
            return function(response) {
                ViewHelper.errorServer(form, response);
                ViewHelper.unblockContainer('.container');
            }
        }

        function save(form, model) {
            if (form.$valid) {
                ViewHelper.blockContainer('.container', $translate.instant('data_saving'));
                $timeout(function() {
                    ViewHelper.scrollTo('.container .blockMsg');
                }, 500);

                var data = RemunerationStatementService.preparateData(model);

                if (data.id) {
                    GenericService.edit(endpoint, data.id, data, callbackSave, callbackErrorSave(form));
                } else {
                    GenericService.add(endpoint, data, callbackSave, callbackErrorSave(form));
                }
            } else {
                ViewHelper.errorForm(form);
            }
        }

        function search(filters) {
            var url = 'remuneration-statement/search';

            ViewHelper.blockContainer('.container', $translate.instant('loading'));
            return GenericService.httpGet(url, filters).then(function(response){
                ViewHelper.unblockContainer('.container');
                return $q.when(response);
            });
        }

        function addReview() {
            var revision = RemunerationRevisionService.initModel();
            $scope.model.revisions.push(revision);
        }

        function removeRevision(revision) {
            $scope.model.revisions.some(function(i) {
                if (i == revision) {
                    $scope.model.revisions.splice($scope.model.revisions.indexOf(i), 1);
                }
            });
        }

        function allowLowLimit() {
            return parseInt(moment().format('D')) >= parseInt($scope.lowDayLimit);
        }

        function allowHighLimit() {
            return parseInt(moment().format('D')) <= parseInt($scope.highDayLimit);
        }

        function removeTCO(row) {
            swal({
                title: $translate.instant('confirm'),
                text: $translate.instant('confirm_statement_remove'),
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $timeout(function() {
                        row.selected = 0;
                        ViewHelper.scrollTo('.workers-container:first');
                    });
                }
            });
        }

        function addTCO(row) {
            row.selected = 1;
            ViewHelper.scrollTo('.workers-container:last');
        }
    }
);