<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Security\Actions;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SerializerVoterAclSubscriber implements EventSubscriberInterface
{
    use ContainerAwareTrait;

    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [[
            'event'  => 'serializer.post_serialize',
            'method' => 'onPostSerialize',
        ]];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onPostSerialize(ObjectEvent $event)
    {
        $entity = $event->getObject();

        if (!in_array(EntityPermissionTrait::class, class_uses($entity))) {
            return;
        }

        $attributes = $entity::getAttributes();
        $acl = array_fill_keys($attributes, false);

        foreach ($acl as $key => $allow) {
            $acl[$key] = $this->authorizationChecker->isGranted($key, $entity);
        }
        
        $event->getVisitor()->addData('acl', $acl);
    }
}
