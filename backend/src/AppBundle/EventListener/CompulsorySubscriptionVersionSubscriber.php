<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class CompulsorySubscriptionVersionSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!($entity instanceof CompulsoryAccessionSubscription)) {
                continue;
            }

            if ($entity->isStandard()) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!($entity instanceof CompulsoryAccessionSubscription)) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['version']) 
                || $changedValues['version'][0] == $changedValues['version'][1]
                || $changedValues['version'][0] != Version::STANDARD) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        $duplicator = $this->container->get('app.duplicate_entity');
        foreach ($injectEntities as $subscription) {

            // Get person and accession from the subscription
            $accession = clone $subscription->getCompulsoryAccession();
            $accession->setSource($subscription->getCompulsoryAccession());
            
            $person = $accession->getPerson();

            // Duplicate the person
            $duplicatePerson = $duplicator->duplicatePerson($person);
            $duplicatePerson->setSource($person);

            // Duplicate the saved subscription
            $newSubscription = clone $subscription;
            $newSubscription->setSource($subscription);

            // Duplicate the work Situation from the subscription
            $workSituation = clone $subscription->getWorkSituation();
            $workSituation->setPerson($duplicatePerson);
            $newSubscription->setWorkSituation($workSituation);

            // Remove all the subscriptions from the accession
            $accession->cleanCompulsorySubscriptions();
            $accession->addCompulsorySubscription($newSubscription);

            // Remove all the acesssions from the person, making sure the clone
            // version will only have the current accession and subscription
            $duplicatePerson->cleanCompulsoryAccessions();
            $duplicatePerson->cleanOptionalAccessions();
            $duplicatePerson->addCompulsoryAccession($accession);

            // Set the proper version for the cloned entity
            $duplicatePerson->setVersion($subscription->getVersion());
            $newSubscription->setVersion($subscription->getVersion());
            $accession->setVersion($subscription->getVersion());

            // Restore the subscription version for the STANDARD
            $subscription->setVersion(Version::STANDARD);
            $this->compute($em, $subscription);

            // Persist the new person version
            $uow->persist($duplicatePerson);
            $metadata = $em->getClassMetadata(get_class($duplicatePerson));
            $uow->computeChangeSets();
        }
    }
}
