<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Entity\OptionalAccessionSubscription;
use AppBundle\Enum\Status;
use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class OptionalSubscriptionStepSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $today = new \DateTime('now');
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!($entity instanceof OptionalAccessionSubscription)) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['optionalStep'])) {
                continue;
            }

            $optionalStep = $changedValues['optionalStep'];

            $historySubscription = clone $entity;

            $historySubscription->setStatus(Status::INACTIVE)
                    ->setDateEnd($today)
                    ->setOptionalStep($optionalStep[1]);

            $entity->setDateStart($today);
            $uow->persist($entity);

            $this->compute($em, $entity);
            $this->compute($em, $entity);
        }
    }
}
