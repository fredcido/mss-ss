<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompulsoryAccessionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('person')
                ->add('version');

        $builder->add(
            'compulsorySubscriptions',
            CollectionType::class,
            array(
                'entry_type'   => CompulsoryAccessionSubscriptionType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\CompulsoryAccession',
            'allow_extra_fields' => true,

        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_compulsoryaccession';
    }
}
