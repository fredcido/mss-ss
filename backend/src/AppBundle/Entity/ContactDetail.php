<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ContactDetail
 *
 * @ORM\Table(name="contact_detail")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactDetailRepository")
 */
class ContactDetail
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="array", length=255, nullable=true)
     * @JMS\Groups({"details"})
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Telephone", mappedBy="contactDetail", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     *
     * @var  ArrayCollection
     */
    private $telephones;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->telephones = new ArrayCollection();
        $this->email = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactDetail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add telephones
     *
     * @param Telephone $telephones
     * @return Race
     */
    public function addTelephone(Telephone $telephone)
    {
        $this->telephones[] = $telephone;
        $telephone->setContactDetail($this);
        return $this;
    }

    /**
     * Remove telephone
     *
     * @param Telephone $telephone
     */
    public function removeTelephone(Telephone $telephone)
    {
        $this->telephones->removeElement($telephone);
    }

    /**
     * @return self
     */
    public function cleanTelephones()
    {
        $this->telephones = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Get telephones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    public function __clone()
    {
        if ($this->id) {
            $telephones = $this->getTelephones();
            $this->cleanTelephones();
            foreach ($telephones as $telephone) {
                $this->addTelephone(clone $telephone);
            }
        }
    }
}
