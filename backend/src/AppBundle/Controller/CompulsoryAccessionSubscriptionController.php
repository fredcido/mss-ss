<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class CompulsoryAccessionSubscriptionController extends BaseController
{
    protected $repository = 'AppBundle:CompulsoryAccessionSubscription';

    protected $form = 'AppBundle\Form\CompulsoryAccessionSubscriptionType';

    protected $entity = CompulsoryAccessionSubscription::class;

    public function getCurrentByPersonEmployerAction($person, $employer, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->getCurrentByPersonEmployer($person, $employer);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }

    public function getLatestByPersonEmployerAction($person, $employer, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->getLatestByPersonEmployer($person, $employer);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }

    public function findByEmployerAction($employer, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findByEmployer($employer);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('list'));
    }

    public function findActiveByPersonAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findActiveEmployments($id);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('list'));
    }

    /**
     * @param  int  $id
     * @param  Request $request
     * @return array
     */
    public function getLatestByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $personRepository = $em->getRepository('AppBundle:Person');
        $person = $personRepository->find($id);

        if (!$person) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $subscription = $this->getRepository()->getLatestByPerson($person);
        if (!$subscription) {
            throw $this->createNotFoundException('There is no compulsory subscription for the provided person');
        }

        return $this->createApiResponse($subscription, Response::HTTP_OK, array('details'));
    }
}
