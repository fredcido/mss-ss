app.controller("TaxReductionFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'tax-reduction';
    var _stateList = 'app.tax-reduction-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { 
        status: 1, 
        reduction: 0, 
        employers: 0, 
        nationalPercent: 0 
    };

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
