app.service("UserService", 
    function(
        $translate, 
        $timeout, 
        AddressService, 
        PersonService, 
        EmployerService, 
        EnumService, 
        GenericService,
        OptionalAccessionService,
        CompulsoryAccessionService
    ) {

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.employer) {
            model.employer = EmployerService.populateModel(model.employer);
        }

        if (model.person) {
            model.person = PersonService.populateModel(model.person);
        }

        if (model.compulsorySubscription) {
            model.compulsorySubscription = CompulsoryAccessionService.populateSubscriptionModel(model.compulsorySubscription);
        }

        if (model.optionalSubscription) {
            model.optionalSubscription = OptionalAccessionService.populateSubscriptionModel(model.optionalSubscription);
        }

        _attachMethods(model);

        return model;
    };

    var _attachMethods = function(model) {

        model.isEmployer = function() {
            return model.employer;
        };

        model.isPerson = function() {
            return model.person;
        };

        model.isStaff = function() {
            return !this.isEmployer() && !this.isPerson();
        };
    };

    var _checkOptionalSubsmission = function(user)
    {
        if (!user.isPerson())
            return;

        var person = user.person;
        var compulsoryUrl = 'compulsory-subscription/' + person.id + '/active';
        var optionalUrl = 'optional-subscription/person/' + person.id + '/current';

        GenericService.httpGet(optionalUrl).then(function(response){
            if (!response.data || !response.data.length) return;
            GenericService.httpGet(compulsoryUrl).then(function(response) {
                    if (!response.data || !response.data.length) return;
                    swal({ 
                        title: $translate.instant('worker_compulsory_with_optional'),
                        text: $translate.instant('worker_compulsory_with_optional'),
                        type: "warning" 
                      });
                });
            }
        );
    };

    return {
        populateModel: _populateModel,
        checkOptionalSubsmission: _checkOptionalSubsmission
    };
});
