<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ActivitySector
 *
 * @ORM\Table(name="activity_sector")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActivitySectorRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 * @UniqueEntity(fields="code", message="code_already_use")
 */
class ActivitySector
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use EntityTreeTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @var ActivitySector
     *
     * @ORM\ManyToOne(targetEntity="ActivitySector", inversedBy="children")
     * @JMS\Groups({"list", "details"})
     *
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="ActivitySector", mappedBy="parent")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $children;


    public function __construct()
    {
        $this->children = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ActivitySector
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets the value of code.
     *
     * @param string $code the code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Gets the value of parent.
     *
     * @return ActivitySector
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Sets the value of parent.
     *
     * @param ActivitySector $parent the parent
     *
     * @return self
     */
    public function setParent(ActivitySector $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }
}
