<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 */
trait FileContainerTrait
{
     /**
     * @var array
     *
     * @ORM\Column(name="files", type="simple_array", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $files;

    /**
     * @param array $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
        return $this->files;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $file
     */
    public function addFile($file)
    {
        $this->files[] = $file;
        return $this;
    }
}
