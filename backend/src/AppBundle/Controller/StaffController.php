<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Staff;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class StaffController extends BaseController
{
    protected $repository = 'AppBundle:Staff';

    protected $form = 'AppBundle\Form\StaffType';

    protected $entity = Staff::class;
}
