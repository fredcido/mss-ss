app.component('contributionTotalOverviewGrid', {
    templateUrl: 'views/components/contributionTotalOverviewGrid.html',
    bindings: {
        personId: '='
    },
    controller: function($translate, $timeout, ViewHelper, $cookies, EnumService, AppConstant, $compile, GenericService) {
        var self = this;
        self.displayCollection = [];
        self.rowCollection = [];

        GenericService.httpGet('contribution/' + self.personId + '/total').then(function(response) {
            self.rowCollection = response.data;
        });
        
        self.enumService = EnumService;
    }
});
