app.controller("BankListController", function($scope, $translate, ViewHelper, GenericService) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    var _uri = 'bank/find?all=1';
    var _init = function() {
        ViewHelper.blockContainer('.table', $translate.instant('loading'));

        GenericService.httpGet(_uri).then(function(response) {
            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
