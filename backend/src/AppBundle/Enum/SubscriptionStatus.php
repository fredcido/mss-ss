<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class SubscriptionStatus implements Enum
{
    use EnumTrait;

    const PENDING   = 0;
    const SUBMITTED = 1;
    const ERROR     = 2;
    const APPROVED  = 3;
    const CEASED    = 4;
    const SUSPENDED = 5;

    /**
     * @var array
     */
    protected static $data = [
        self::PENDING   => 'Pending',
        self::SUBMITTED => 'Submitted',
        self::ERROR     => 'Error',
        self::APPROVED  => 'Approved',
        self::CEASED    => 'Ceased',
        self::SUSPENDED => 'Supended',
    ];
}
