app.service("RemunerationRevisionService", function($http, AppConstant, EnumService) {

    var _preparateData = function(model) {
        var data = angular.copy(model);

        var convertObjects = [
            'remunerationStatement',
            'assessment'
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        if (data.assessmentDate) {
            data.assessmentDate = moment(data.assessmentDate).format('YYYY-MM-DD');
        }

        if (data.notificationDate) {
            data.notificationDate = moment(data.assessmentDate).format('YYYY-MM-DD');
        }

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.assessmentDate) {
            model.assessmentDate = moment(model.assessmentDate, "YYYY/MM/DD").toDate();
        }

        if (model.notificationDate) {
            model.notificationDate = moment(model.notificationDate, "YYYY/MM/DD").toDate();
        }

        return model;
    };

    var _initModel = function() {
        var model = {
            assessmentDate: new Date(),
            notificationDate: new Date(),
            assessment: 0
        };

        return model;
    };


    return {
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel
    };
});
