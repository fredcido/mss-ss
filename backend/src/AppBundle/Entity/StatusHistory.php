<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * StatusHistory
 *
 * @ORM\Table(name="status_history")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatusHistoryRepository")
 */
class StatusHistory
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="previousStatus", type="smallint", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $previousStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="statusChanged", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $statusChanged;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusChanged
     *
     * @param integer $statusChanged
     *
     * @return statusChangedHistory
     */
    public function setStatusChanged($statusChanged)
    {
        $this->statusChanged = $statusChanged;

        return $this;
    }

    /**
     * Get statusChanged
     *
     * @return int
     */
    public function getStatusChanged()
    {
        return $this->statusChanged;
    }

    /**
     * Gets the value of previousStatus.
     *
     * @return int
     */
    public function getPreviousStatus()
    {
        return $this->previousStatus;
    }

    /**
     * Sets the value of previousStatus.
     *
     * @param int $previousStatus the previous status
     *
     * @return self
     */
    public function setPreviousStatus($previousStatus)
    {
        $this->previousStatus = $previousStatus;
        return $this;
    }
}
