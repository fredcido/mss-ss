<?php

namespace AppBundle\Entity;

use AppBundle\Security\Actions;

/**
 *
 */
trait EntityPermissionTrait
{
    /**
     * @var array
     */
    protected static $attributes = array();

    protected static $resource;

    /**
     * @return array
     */
    public static function getAttributes()
    {
        $attributes = Actions::getAll();
        if (!empty(self::$attributes)) {
            $attributes = self::$attributes;
        }

        return $attributes;
    }

    /**
     * @return string
     */
    public static function getResource()
    {
        if (empty(self::$resource)) {
            self::$resource = self::generateResourceName();
        }

        return self::$resource;
    }

    /**
     * @return string
     */
    protected static function generateResourceName()
    {
        $className = explode('\\', __CLASS__);
        $resource = array_pop($className);
        $resource = preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $resource);

        return strtolower($resource);
    }
}
