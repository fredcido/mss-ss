app.directive('minDate', [function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            scope.$watch(function() {
                return [scope.$eval(attrs.minDate), ctrl.$modelValue];
            }, function(values) {
                if (values[0] && values[1]) {
                    var precision = attrs.minDatePrecision || "day";

                    var dateStart = moment(values[0]);
                    var dateEnd = moment(values[1]);

                    ctrl.$setValidity('minDate', dateEnd.isSameOrAfter(dateStart, precision));
                    ctrl.$dirty = true;
                } else {
                    ctrl.$setValidity('minDate', true);
                }
            }, true);
        }
    };
}]);
