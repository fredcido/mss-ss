<?php

namespace AppBundle\Service;

use AppBundle\Entity\InternalCodeTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class InternalCode
{
    use ContainerAwareTrait;

    public function inject($entity)
    {
        if (!in_array(InternalCodeTrait::class, class_uses($entity))) {
            throw new \InvalidArgumentException('Entity must have "InternalCodeTrait" to inject internal code');
        }

        $nextInternalCode = $this->getNextInternalCodeCode($entity);
        $internalCode  = sprintf("%s-%s", date('Y'), str_pad($nextInternalCode, 6, '0', \STR_PAD_LEFT));
        $entity->setInternalCode($internalCode);
    }

    protected function getNextInternalCodeCode($entity)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(get_class($entity));

        $lastInternalCode = $repository->createQueryBuilder('m')
                                        ->select('MAX(m.internalCode)')
                                        ->getQuery()
                                        ->getSingleScalarResult();

        $internalCode = explode('-', $lastInternalCode);
        if ($internalCode[0] != date('Y')) {
            $internalCode[1] = 0;
        }

        return $internalCode[1] + 1;
    }

    public function hasInternalCode($entity)
    {
        if (!in_array(InternalCodeTrait::class, class_uses($entity))) {
            throw new \InvalidArgumentException('Entity must have "InternalCodeTrait" to inject internal code');
        }

        $internalCode = trim($entity->getInternalCode());
        return !empty($internalCode);
    }
}
