app.component('feedback', {
    templateUrl: 'views/components/feedback.html',
    bindings: {
        model: '=',
        reference: '@',
        disabled: '<',
        onNewFeedback: '&',
        onEditFeedback: '&',
    },
    controller: function($scope, $translate, $timeout, GenericService, ViewHelper, EnumService) {
        var self = this;
        var _endpoint = 'feedback';

        self.subscriptionStatuses = EnumService.geSubscriptionStatuses();
        self.feedbacks = [];
        self.feedback = {};

        var getFeedbacks = function() {
            var url = _endpoint + '/' + self.reference + '/' + self.model.id;
            GenericService.httpGet(url).then(function(response){
                self.feedbacks = response.data;
                setRowCollection(self.feedbacks);
            });
        };

        var setRowCollection = function(rows) {
            self.rowCollection = rows;
            self.displayCollection = self.rowCollection;
        };

        var prepareData = function(model) {
            var data = angular.copy(model);
            return data;
        };

        var callBackSave = function(response) {
            var hasAlready = self.feedbacks.some(function(item){
                if (item.id == response.id) {
                    item = response;
                    return true;
                }
            });

            if (!hasAlready) {
                self.feedbacks.unshift(response);
                if (typeof self.onNewFeedback == 'function') {
                    self.onNewFeedback({feedback: response});
                }
            } else {
                if (typeof self.onEditFeedback == 'function') {
                    self.onEditFeedback({feedback: response});
                }
            }

            $scope.$apply(function(){
                setRowCollection(self.feedbacks);
            });
            $scope.feedbackForm.$dirty = false;
            self.cancel();

            $timeout(function() {
                ViewHelper.scrollTo('.table-feedback');
            }, 500);
        };

        self.$onInit = function() {
            getFeedbacks();
            self.cancel();
        };

        self.getSubscriptionStatusDescription = function(status) {
            var description = EnumService.geSubscriptionStatusesDescription(status);
            return description;
        };

        self.cancel = function() {
            self.feedback = {
                reference: self.reference
            };

            self.feedback[self.reference] = self.model.id;

            if ($scope.feedbackForm)
                $scope.feedbackForm.$setPristine();
        };

        self.save = function(form, model) {
            if (form.$valid) {
                var data = prepareData(model);

                if (data.id) {
                    GenericService.edit(_endpoint, data.id, data, callBackSave)
                        .catch(function (response) {
                            ViewHelper.errorServer(form, response);
                        });
                } else {
                    GenericService.add(_endpoint, data, callBackSave)
                        .catch(function (response) {
                            ViewHelper.errorServer(form, response);
                        });
                }
            } else {
                ViewHelper.errorForm(form);
            }
        };

        self.edit = function(row) {
            self.feedback = row;
            self.feedback[self.reference] = self.model.id;
            self.feedback.subscriptionStatusDesc = self.getSubscriptionStatusDescription(row.subscriptionStatus);

            $timeout(function() {
                ViewHelper.scrollTo('.feedback-form');
            }, 500);
        };

        self.isCommentRequired = function() {
            return self.feedback.subscriptionStatus != EnumService.getSubscriptionStatus('approved');
        };
    }
});
