<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EmailQueue
 *
 * @ORM\Table(name="email_queue")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmailQueueRepository")
 */
class EmailQueue
{
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $subject;

    /**
     * @var array
     *
     * @ORM\Column(name="fromMail", type="array")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $fromMail;

    /**
     * @var array
     *
     * @ORM\Column(name="toMail", type="array")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $toMail;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     * @JMS\Groups({"details", "form"})
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="whenSend", type="datetime")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $whenSend;

    /**
     * @var int
     *
     * @ORM\Column(name="sent", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $sent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sentAt", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $sentAt;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="text", nullable=true)
     */
    private $error;


    public function __construct()
    {
        $this->whenSend = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailQueue
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set fromMail
     *
     * @param array $fromMail
     *
     * @return EmailQueue
     */
    public function setFromMail($fromMail)
    {
        $this->fromMail = $fromMail;

        return $this;
    }

    /**
     * Get fromMail
     *
     * @return array
     */
    public function getFromMail()
    {
        return $this->fromMail;
    }

    /**
     * Set toMail
     *
     * @param array $toMail
     *
     * @return EmailQueue
     */
    public function setToMail($toMail)
    {
        if (!is_array($toMail)) {
            $toMail = [$toMail];
        }

        $this->toMail = $toMail;
        return $this;
    }

    /**
     * Get toMail
     *
     * @return array
     */
    public function getToMail()
    {
        return $this->toMail;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return EmailQueue
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set whenSend
     *
     * @param \DateTime $whenSend
     *
     * @return EmailQueue
     */
    public function setWhenSend($whenSend)
    {
        $this->whenSend = $whenSend;

        return $this;
    }

    /**
     * Get whenSend
     *
     * @return \DateTime
     */
    public function getWhenSend()
    {
        return $this->whenSend;
    }

    /**
     * Set sent
     *
     * @param integer $sent
     *
     * @return EmailQueue
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return int
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return EmailQueue
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Gets the value of error.
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Sets the value of error.
     *
     * @param string $error the error
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }
}
