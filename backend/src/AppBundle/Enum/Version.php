<?php

namespace AppBundle\Enum;

/**
 *
 */
abstract class Version implements Enum
{
    use EnumTrait;
    
    const STANDARD     = 0;
    const SUBSCRIPTION = 1;
    const UPDATE       = 2;

    /**
     * @var array
     */
    protected static $data = [
        self::STANDARD     => 'Standard',
        self::SUBSCRIPTION => 'Subscription',
        self::UPDATE       => 'Update',
    ];
}
