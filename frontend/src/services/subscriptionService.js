app.service("SubscriptionService", function($http, AppConstant, EnumService) {

    var _attachMethods = function(model) {
        model.isSubmitted = function() {
            var status = EnumService.getSubscriptionStatus('submitted') == this.subscriptionStatus;
            return status;
        };

        model.isApproved = function() {
            var status = EnumService.getSubscriptionStatus('approved') == this.subscriptionStatus;
            return status;
        };

        model.isPending = function() { 
            var status = EnumService.getSubscriptionStatus('pending') == this.subscriptionStatus;
            return status;
        };

        model.isCeased = function() {
            var status = EnumService.getSubscriptionStatus('ceased') == this.subscriptionStatus;
            return status;
        };

        model.isErrored = function() {
            var status = EnumService.getSubscriptionStatus('error') == this.subscriptionStatus;
            return status;
        };

        model.isSuspended = function() {
            var status = EnumService.getSubscriptionStatus('suspended') == this.subscriptionStatus;
            return status;
        };

        model.isCeased = function() {
            var status = EnumService.getSubscriptionStatus('ceased') == this.subscriptionStatus;
            return status;
        };

        model.isOfAnyStatus = function(statuses) {
            var status =  statuses && statuses.indexOf(this.subscriptionStatus) > -1;
            return status;
        };

        model.isActive = function() {
            var statuses = [
                EnumService.getSubscriptionStatus('suspended'),
                EnumService.getSubscriptionStatus('ceased')
            ];
            var status = statuses.indexOf(this.subscriptionStatus) < 0;
            return status;
        };

        model.hasSubscribed = function() {
            var statuses = [
                EnumService.getSubscriptionStatus('pending'),
                EnumService.getSubscriptionStatus('error')
            ];
            var status = statuses.indexOf(this.subscriptionStatus) < 0;
            return status;
        };
    };


    return {
        attachMethods: _attachMethods
    };
});
