<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RemunerationRevision
 *
 * @ORM\Table(name="remuneration_revision")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RemunerationRevisionRepository")
 */
class RemunerationRevision
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="assessment", type="smallint", options={"default" : 1})
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $assessment = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assessmentDate", type="date")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $assessmentDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="notificationDate", type="date")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $notificationDate;

    /**
     * @var RemunerationStatement
     *
     * @ORM\ManyToOne(targetEntity="RemunerationStatement", inversedBy="revisions")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $remunerationStatement;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    protected $observation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set assessment
     *
     * @param integer $assessment
     *
     * @return RemunerationRevision
     */
    public function setAssessment($assessment)
    {
        $this->assessment = (int)$assessment;
        return $this;
    }

    /**
     * Get assessment
     *
     * @return int
     */
    public function getAssessment()
    {
        return $this->assessment;
    }

    /**
     * Set assessmentDate
     *
     * @param \DateTime $assessmentDate
     *
     * @return RemunerationRevision
     */
    public function setAssessmentDate($assessmentDate)
    {
        $this->assessmentDate = $assessmentDate;

        return $this;
    }

    /**
     * Get assessmentDate
     *
     * @return \DateTime
     */
    public function getAssessmentDate()
    {
        return $this->assessmentDate;
    }

    /**
     * Set notificationDate
     *
     * @param \DateTime $notificationDate
     *
     * @return RemunerationRevision
     */
    public function setNotificationDate($notificationDate)
    {
        $this->notificationDate = $notificationDate;

        return $this;
    }

    /**
     * Get notificationDate
     *
     * @return \DateTime
     */
    public function getNotificationDate()
    {
        return $this->notificationDate;
    }

    /**
     * Set remunerationStatement
     *
     * @param \AppBundle\Entity\RemunerationStatement $remunerationStatement
     *
     * @return RemunerationRevision
     */
    public function setRemunerationStatement(\AppBundle\Entity\RemunerationStatement $remunerationStatement = null)
    {
        $this->remunerationStatement = $remunerationStatement;

        return $this;
    }

    /**
     * Get remunerationStatement
     *
     * @return \AppBundle\Entity\RemunerationStatement
     */
    public function getRemunerationStatement()
    {
        return $this->remunerationStatement;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return RemunerationRevision
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }
}
