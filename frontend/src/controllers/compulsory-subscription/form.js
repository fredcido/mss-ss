app.controller("CompulsorySubscriptionFormController", function($scope, $state, $stateParams, GenericService) {
    $scope.person = {};

    $scope.selectPerson = function(person) {
        $scope.person = person;
    };
});
