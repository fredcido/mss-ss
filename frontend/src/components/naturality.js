app.component('naturality', {
    templateUrl: 'views/components/naturality.html',
    bindings: {
        model: '=',
        required: '=',
        hasHeadquarter: '=',
        disabled: '<'
    },
    controller: function($scope, GenericService, ViewHelper, $timeout) {
        var self = this;
        self.disableTL = false;
        self.disableCountries = false;
        self.municipios = [];
        self.postosAdministrativos = [];
        self.sukus = [];
        self.villages = [];

        self.$onInit = function() {
            _getMunicipios();
            _getCountries();
            checkNaturality();
            
            if (!self.model) {
                self.model = {
                    naturalityInTL: 1
                }
            }
        };

        self.$onChanges = function(changesObj) {
            if (changesObj.model) {
                self.model = changesObj.model.currentValue;
            }

            checkNaturality();
        };

        var checkNaturality = function() {
            if (self.model) {
                self.model.naturalityInTL = self.model.cityAbroad ? 0 : 1;
            }
        };

        self.changeMunicipio = function(municipioId) {
            self.model.postoAdministrativo = null;
            self.postosAdministrativos = [];
            self.model.suku = null;
            self.sukus = [];
            if (municipioId) {
                self.model.country = null;
                self.model.cityAbroad = null;
                _getPostoAdministrativos(municipioId);
            }
        };

        self.changeCountry = function() {
            if ((self.model.country) || (self.model.cityAbroad)) {
                self.model.municipio = null;
                self.model.postoAdministrativo = null;
                self.model.suku = null;
                self.model.village = null;
                self.postosAdministrativos = [];
                self.sukus = [];
                self.villages = [];
            }
        };

        self.checkInputDisabled = function() {
            if (self.model) {
                if (!self.model.country && !self.model.cityAbroad && !self.model.municipio) {
                    self.disableTL = false;
                    self.disableCountries = false;
                }

                if (self.model.country || self.model.cityAbroad) {
                    self.disableTL = true;
                    self.disableCountries = false;
                }

                if (self.model.municipio) {
                    self.disableTL = false;
                    self.disableCountries = true;
                }
            }
        };

        self.changePostoAdministrativo = function(postoAdministrativoId) {
            self.model.suku = null;
            self.model.village = null;
            self.sukus = [];
            self.villages = [];
            _getSukus(postoAdministrativoId);
        };

        self.changeSuku = function(sukuId) {
            self.model.village = null;
            self.villages = [];
            _getVillages(sukuId);
        };

        var _getCountries = function() {
            GenericService.find('country').then(function(response) {
                self.countries = response.data;
            });
        };

        var _getMunicipios = function() {
            GenericService.find('municipio').then(function(response) {
                self.municipios = response.data;

                if (self.model && self.model.municipio) {
                    _getPostoAdministrativos(self.model.municipio.id);
                }
            });
        };

        var _getPostoAdministrativos = function(municipioId) {
            if (municipioId) {
                GenericService.httpGet('posto-administrativo/municipio/' + municipioId).then(function(response) {
                    self.postosAdministrativos = response.data;

                    if (self.model && self.model.postoAdministrativo) {
                        _getSukus(self.model.postoAdministrativo.id);
                    }
                });
            } else {
                self.postosAdministrativos = [];
            }
        };

        var _getSukus = function(postoAdministrativoId) {
            if (postoAdministrativoId) {
                GenericService.httpGet('suku/posto-administrativo/' + postoAdministrativoId).then(function(response) {
                    self.sukus = response.data;

                    if (self.model && self.model.suku) {
                        _getVillages(self.model.suku.id);
                    }
                });
            } else {
                self.sukus = [];
            }
        };

        var _getVillages = function(sukuId) {
            if (sukuId) {
                GenericService.httpGet('village/suku/' + sukuId).then(function(response) {
                    self.villages = response.data;
                });
            } else {
                self.villages = [];
            }
        };
    }
});
