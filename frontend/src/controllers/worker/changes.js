app.controller("WorkerChangesController", 
    function(
        $scope, 
        $stateParams, 
        $uibModal, 
        $translate, 
        $state, 
        $timeout,
        GenericService,
        PersonService,
        AuthService, 
        EnumService, 
        ViewHelper
    ) {

    var _personEndpoint = "person";
    var checkVersion = new RegExp('_' + EnumService.getVersion('subscription') + '$');

    $scope.user = AuthService.getUserLogged();
    $scope.employerId = $stateParams.employer || null;
    $scope.id = $stateParams.id || null;

    if ($scope.user.employer) {
        $scope.employerId = $scope.user.employer.internalCode;
    }

    $scope.actionsGrid = [{
        title: $translate.instant('view'),
        icon: 'fa-eye',
        callback: function(subscription) {
            $state.go('app.worker-version', {
                id: subscription.person.internalCode,
                employer: subscription.workSituation.employer.internalCode,
                updateSubscription: checkVersion.exec(subscription.version) ? 0 : 1,
                version: subscription.person.id
            });
        }
    }];

    var versions = [
        $scope.employerId + '_' + EnumService.getVersion('subscription'),
        $scope.employerId + '_' + EnumService.getVersion('update'),
    ];

    var filters = {
        version: versions,
        all_status: 1
    };

    var search = function() {
        var uri = 'compulsory-subscription/search';
        GenericService.httpGet(uri, filters).then(function(response) {
            $scope.rows = response.data;
            ViewHelper.unblockContainer('.panel');
        });
    };

    var init = function() {
        var version = EnumService.getVersion('standard');
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));
        GenericService.getVersion(_personEndpoint, $scope.id, version).then(function (response) {
            $scope.person = PersonService.populateModel(response.data);

            filters.person = $scope.person.internalCode;
            search();
        });
    };

    init();
});
