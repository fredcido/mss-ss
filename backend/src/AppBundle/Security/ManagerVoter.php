<?php

namespace AppBundle\Security;

use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Entity\User;
use AppBundle\Security\Actions;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter as AbstractVoter;

class ManagerVoter extends AbstractVoter
{
    use RoleVoterTrait;

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$this->decisionManager->decide($token, array('ROLE_MANAGER'))) {
            return false;
        }

        if (Actions::VIEW != $attribute) {
            return !($subject instanceof User);
        } else {
            return true;
        }
    }
}
