app.service("AuthService", 
    function(
        $cookies, 
        $http, 
        $q,
        AppConstant, 
        EmployerService, 
        UserService,
        GenericService
    ) {

    var _hasPermission = function(state) {
        var hasPermission = false;
        var permissions = localStorage.getItem('token') || null;

        if (permissions !== null) {
            hasPermission = true;
        }
        var stateAllowed = [
            'login',
            'logout',
            '404',
            '500',
            'forbidden'
        ].some(function(s) {
            return state == s;
        });

        return (stateAllowed || hasPermission) ? true : false;
    };


    var _authenticate = function(username, password) {
        var data = {
            'grant_type': 'password',
            'client_id': AppConstant.clientId,
            'client_secret': AppConstant.clientSecret,
            'username': username,
            'password': password
        };

        return $http({
            method: 'POST',
            url: AppConstant.apiUrl + "oauth/v2/token",
            data: $.param(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        });
    };

    var _getProfile = function() {
        return $http({
            method: 'GET',
            url: AppConstant.apiUrl + "profile/get",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        });
    };

    var _getUserLogged = function() {
        var profileJson = localStorage.getItem('profile');

        if (profileJson) {
            var profile = JSON.parse(profileJson);
            return UserService.populateModel(profile);
        } else {
            return null;
        }
    };

    var _setUser = function(user) {
        if (user.person) {
            var person = user.person;
            var compulsoryUrl = 'compulsory-subscription/' + person.id + '/latest';
            var optionalUrl = 'optional-subscription/person/' + person.id + '/latest';

            // Search for option subscription
            userLoad = GenericService.httpGet(optionalUrl).then(
                            function(response){
                                user.optionalSubscription = response.data;
                                return $q.when();
                            },
                            function() {
                                delete user.optionalSubscription;
                            }
                        )
                        .finally(function(){
                            // Search for compulsory subscription
                            return GenericService.httpGet(compulsoryUrl).then(
                                function(response) {
                                    if (response.data) {
                                        user.compulsorySubscription = response.data;
                                    }
                                    return $q.when();
                                },
                                function() {
                                    delete user.compulsorySubscription;
                                }
                            );
                        });
        } else {
            userLoad = $q.when();
        }

        return userLoad.finally(function(){
            localStorage.setItem("profile", JSON.stringify(user));
            return $q.when(user);
        });
    };

    var _clearAuth = function() {
        localStorage.removeItem('profile');
        localStorage.removeItem('token');
        localStorage.removeItem('permissions');
    };

    return {
        hasPermission: _hasPermission,
        authenticate: _authenticate,
        getProfile: _getProfile,
        getUserLogged: _getUserLogged,
        clearAuth: _clearAuth,
        setUser: _setUser,
    };
});
