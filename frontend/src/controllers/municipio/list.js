app.controller("MunicipioListController", function($scope, GenericService, ViewHelper, $translate) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    var _uri = 'municipio/find?all=1';
    var _init = function() {
        ViewHelper.blockContainer('.table', $translate.instant('loading'));

        GenericService.httpGet(_uri).then(function(response) {
            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
