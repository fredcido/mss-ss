<?php

namespace AppBundle\Validator;

use AppBundle\Entity\PersonAwareInterface;
use AppBundle\Service\Settings;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SubscriptionAgeValidator extends ConstraintValidator
{
    use ContainerAwareTrait;

    protected $settings;

    public function setSettings(Settings $settings)
    {
        $this->settings = $settings;
        return $this;
    }

    public function validate($value, Constraint $constraint)
    {
        $person = $value->getPerson();

        if (!$person) return;

        $setting = 'minimal_contribution_age_men';
        if ('F' == $person->getGender()) {
            $setting = 'minimal_contribution_age_women';
        }

        if ((int)$person->getAge() < (int)$this->settings->get($setting)) {
            $this->context->buildViolation($constraint->message)
                            ->atPath('compulsoryAccession.person.dob')
                            ->addViolation();
        }
    }
}
