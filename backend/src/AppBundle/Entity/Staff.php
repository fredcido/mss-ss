<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Staff
 *
 * @ORM\Table(name="staff")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaffRepository")
 */
class Staff
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="StaffRole", mappedBy="staff", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $roles;

    /**
     * @ORM\ManyToOne(targetEntity="Person", cascade={"persist"})
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="Employer", inversedBy="staff")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $employer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return self
     */
    public function cleanRoles()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        return $this;
    }

    /**
     * Add role
     *
     * @param StaffRole $role
     *
     * @return Staff
     */
    public function addRole(StaffRole $role)
    {
        $this->roles[] = $role;
        $role->setStaff($this);
        return $this;
    }

    /**
     * Remove role
     *
     * @param StaffRole $role
     */
    public function removeRole(StaffRole $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set person
     *
     * @param Person $person
     *
     * @return Staff
     */
    public function setPerson(Person $person = null)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Get person
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set employer
     *
     * @param Employer $employer
     *
     * @return Staff
     */
    public function setEmployer(Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    public function __clone()
    {
        if ($this->id) {
            $roles = $this->getRoles();
            $this->cleanRoles();
            foreach ($roles as $role) {
                $this->addRole(clone $role);
            }
        }
    }
}
