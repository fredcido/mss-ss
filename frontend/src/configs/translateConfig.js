app.config(function($translateProvider, AppConstant) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.useStaticFilesLoader({
        'prefix': AppConstant.pathLang,
        'suffix': '.json'
    });
    $translateProvider.preferredLanguage('en');

    //console.log($translateProvider.translations(), 'translate');
});
