app.component('personSearch', {
    templateUrl: 'views/components/personSearch.html',
    bindings: {
        actions: "=",
        params: "<"
    },
    controller: function($translate, GenericService, ViewHelper) {
        var self = this;
        var _uri = 'person/search';
        self.searchForm = false;
        self.calendar = {
            opened: false
        };
        self.displayCollection = [];
        self.rowCollection = [];

        self.openCalendar = function() {
            self.calendar.opened = true;
        };

        var _search = function(params) {
            ViewHelper.blockContainer('.personContainer', $translate.instant('loading'));

            if(self.params){
                params = angular.extend({}, params, self.params);
            }

            GenericService.httpGet(_uri, params).then(function(response) {
                self.rows = response.data;
                ViewHelper.unblockContainer('.personContainer');
            });
        };

        self.$onInit = function() {
            _search();
        };

        self.reload = function(params) {
            var _params = angular.copy(params) || null;

            if (_params) {
                if (_params.address) {
                    if (_params.address.country) {
                        _params.address.country = _params.address.country.id;
                    }
                    if (_params.address.municipio) {
                        _params.address.municipio = _params.address.municipio.id;
                    }
                    if (_params.address.postoAdministrativo) {
                        _params.address.postoAdministrativo = _params.address.postoAdministrativo.id;
                    }
                    if (_params.address.suku) {
                        _params.address.suku = _params.address.suku.id;
                    }
                }

                if (_params.dob) {
                    _params.dob = moment(_params.dob).format('YYYY-MM-DD');
                }
            }

            _search(_params);
        };
    }
});
