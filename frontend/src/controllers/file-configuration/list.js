app.controller("FileConfigurationListController", function($scope, $translate, ViewHelper, GenericService, EnumService) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    $scope.references = EnumService.getFileReferences();
    $scope.enumService = EnumService;

    var _uri = 'file-configuration/find?all=1';
    var _init = function() {
        ViewHelper.blockContainer('.table', $translate.instant('loading'));

        GenericService.httpGet(_uri).then(function(response) {
            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
