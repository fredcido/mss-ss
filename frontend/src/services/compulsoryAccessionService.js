app.service("CompulsoryAccessionService", function($http, AppConstant, EnumService, WorkSituationService, SubscriptionService, PersonService) {

    var _populateSubscriptionModel = function(model) {
        var model = angular.copy(model);

        if (model.dateStart) {
            model.dateStart = moment(model.dateStart, "YYYY/MM/DD").toDate();
        }

        if (model.dateEnd) {
            model.dateEnd = moment(model.dateEnd, "YYYY/MM/DD").toDate();
        }

        model.workSituation = WorkSituationService.populateModel(model.workSituation);
        model.workSituation.dateStart = model.dateStart;

        SubscriptionService.attachMethods(model);

        if (model.compulsoryAccession) {
            model.compulsoryAccession = _populateModel(model.compulsoryAccession);
        }
        
        return model;
    };

    var _preparateSubscriptionData = function(model) {
        var data = angular.copy(model);

        if (data.dateEnd) {
            data.dateEnd = moment(data.dateEnd).format('YYYY-MM-DD');
        }

        data.workSituation = WorkSituationService.preparateData(model.workSituation);
        data.dateStart = data.workSituation.dateStart;

        if (data.compulsoryAccession) {
            data.compulsoryAccession = data.compulsoryAccession.id;
        }

        return data;
    };

    var _populateAbroadModel = function(model) {
        var model = angular.copy(model);

        return model;
    };

    var _preparateAbroadData = function(model) {
        var data = angular.copy(model);

        if (data.country) {
            data.country = data.country.id;
        }

        if (data.countryWork) {
            data.countryWork = data.countryWork.id;
        }

        if (data.person) {
            data.person = data.person.id;
        }

        return data;
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.compulsorySubscriptions) {
            data.compulsorySubscriptions = data.compulsorySubscriptions.map(function(s){
                return _preparateSubscriptionData(s);
            });
        }

        if (data.compulsoryAccessionsAbroad) {
            data.compulsoryAccessionsAbroad = data.compulsoryAccessionsAbroad.map(function(s){
                return _preparateAbroadData(s);
            });
        }

        if (data.person) {
            data.person = data.person.id;
        }

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.compulsorySubscriptions) {
            model.compulsorySubscriptions.some(function(s){
                if (s) {
                    s = _populateSubscriptionModel(s);
                }
            });
        }

        if (model.compulsoryAccessionsAbroad) {
            model.compulsoryAccessionsAbroad.some(function(s){
                if (s) {
                    s = _populateAbroadModel(s);
                }
            });
        }

        if (model.person) {
            model.person = PersonService.populateModel(model.person);
        }

        return model;
    };

    var _initModel = function() {
        var model = {
        };

        return model;
    };

    var _initAbroadModel = function() {
        var model = {
            hasSubsidie: '0'
        };

        return model;
    };

    var _initSubscriptionModel = function() {
        var model = {
            workSituation: WorkSituationService.initModel(),
            subscription: 1,
            subscriptionStatus: EnumService.getSubscriptionStatus('pending')
        };

        SubscriptionService.attachMethods(model);

        return model;
    };

    return {
        populateSubscriptionModel: _populateSubscriptionModel,
        preparateSubscriptionData: _preparateSubscriptionData,
        populateAbroadModel: _populateAbroadModel,
        preparateAbroadData: _preparateAbroadData,
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel,
        initAbroadModel: _initAbroadModel,
        initSubscriptionModel: _initSubscriptionModel
    };
});
