app.directive('serverValidate', function($rootScope, $translate, $timeout, ViewHelper) {
    return {
        restrict: 'A',
        require: 'form',
        link: function($scope, $elem, $attrs, form) {
            $rootScope.serverErrors = null;

            var nestedCollectionControl = {};

            var invalidateField = function (field, errors) {
                var changeListener = function () {
                    field.$setValidity('server', true);

                    var index = field.$viewChangeListeners.indexOf(changeListener);
                    if (index > -1) {
                        field.$viewChangeListeners.splice(index, 1);
                    }

                    $(field.$$element).closest('.form-group').find('.server-errors').remove();
                };

                field.$setDirty();
                field.$setValidity('server', false);
                field.$viewChangeListeners.push(changeListener);
                insertErrors(field, errors);
            };

            var insertErrors = function(field, errors) {
                var container = $('<div>');
                container.addClass('server-errors');

                if (!angular.isArray(errors))
                    errors = [errors];

                angular.forEach(
                    errors,
                    function(error) {
                        container.append($('<i>').addClass('help-block').html($translate.instant(error)));
                    }
                );

                var $formGroup = $(field.$$element).closest('.form-group');
                if($formGroup.find('.help-block').length == 1){
                    $formGroup.find('.help-block').html(container);
                } else {
                    $formGroup.append(container);
                }
            };

            var isNestedForm = function(value) {
                return value && typeof value === 'object' && value.hasOwnProperty('$$controls');
            };

            var isFormField = function(value) {
                return value && typeof value === 'object' && value.hasOwnProperty('$modelValue');
            };

            var makeInvalidField = function(formInvalid, field, errors) {
                var foundInNested = false;
                var foundField = formInvalid.$$controls.some( 
                    function(value, key) {
                        // Check if the current element is a nested form
                        if (isNestedForm(value)) {
                            var subForm = value;
                            // Maybe the error comes from a nested form
                            var maybeNestedName = field + 'Form';
                            if (subForm.$name == maybeNestedName) {
                                
                                // If we got an array of errors
                                if (angular.isArray(errors)) {
                                    // Try to match the position of the error returned with the position of the form in the DOM
                                    if (angular.isUndefined(nestedCollectionControl[subForm.$name])) {
                                        nestedCollectionControl[subForm.$name] = -1;
                                    }

                                    // Get the current subForm position
                                    var pos = ++nestedCollectionControl[subForm.$name];

                                    // If there are errors for the current position
                                    if (angular.isDefined(errors[pos])) {
                                        angular.forEach(
                                            errors[pos],
                                            function(subErrors, subField) {
                                                if (makeInvalidField(subForm, subField, subErrors)) {
                                                    foundInNested = true;
                                                }
                                            }
                                        );
                                    }
                                } else {
                                    angular.forEach(
                                        errors,
                                        function(subErrors, subField) {
                                            if (makeInvalidField(subForm, subField, subErrors)) {
                                                foundInNested = true;
                                            }
                                        }
                                    );
                                }

                            } else if (makeInvalidField(value, field, errors)) {
                                return true;
                            }
                            
                        } else if (isFormField(value) && value.$name == field) {
                            invalidateField(value, errors);
                            return true;
                        }
                    }
                );

                return foundField || foundInNested;
            };

            $rootScope.$watch('serverErrors', function (serverErrors) {
                if (serverErrors) {

                    $($elem).find('.errors-container').remove();
                    var messageErrors = [];
                    messageErrors.push($translate.instant('error_form'));

                    nestedCollectionControl = {};

                    angular.forEach(
                        serverErrors,
                        function (errors, field)
                        {
                            if (angular.isNumber(field) || !makeInvalidField(form, field, errors)) {
                                if (!angular.isArray(errors)) {
                                    errors = [errors];
                                }
                                
                                angular.forEach(
                                    errors,
                                    function(error) {
                                        messageErrors.push($translate.instant(error));
                                    }
                                );
                            }
                        }
                    );

                    var errorContainer = $('<div>').addClass('errors-container');
                    angular.forEach(
                        messageErrors,
                        function(error) {
                            var p = $('<p>').addClass('alert alert-dismissible alert-warning').html(error);
                            p.prepend('<button type="button" class="close" data-dismiss="alert">×</button>');

                            errorContainer.append(p);
                        }
                    );

                    $($elem).prepend(errorContainer);

                    $timeout(
                        function() {
                            $($elem).find('.errors-container').remove();
                        },
                        20 * 1000
                    );
                }
            });
        }
    };
});
