app.controller("SukuFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'suku';
    var _stateList = 'app.suku-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.municipios = [];
    $scope.postosAdministrativos = [];

    $scope.$watch('model.municipio', function() {
        if ($scope.model.municipio) {
            _getPostosAdmnistrativos($scope.model.municipio);
        } else {
            $scope.postosAdministrativos = [];            
        }
    });

    var _getMunicipios = function() {
        GenericService.find('municipio').then(function(response) {
            $scope.municipios = response.data;
        });
    };

    var _getPostosAdmnistrativos = function(municipioId) {
        GenericService.httpGet('posto-administrativo/municipio/' + municipioId).then(function(response) {
            $scope.postosAdministrativos = response.data;
        });
    };

    _getMunicipios();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;

            $scope.model.municipio = $scope.model.postoAdministrativo.municipio.id;
            $scope.model.postoAdministrativo = $scope.model.postoAdministrativo.id;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
