<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * OptionalContributionGroup
 *
 * @ORM\Table(name="optional_contribution_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OptionalContributionGroupRepository")
 * @UniqueEntity(fields="name", message="The name is already in use, choose another.")
 */
class OptionalContributionGroup
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="minAge", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $minAge;

    /**
     * @var int
     *
     * @ORM\Column(name="canHaveCompulsory", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $canHaveCompulsory = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="ableToWork", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $ableToWork = 1;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OptionalContributionGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set minAge
     *
     * @param integer $minAge
     *
     * @return OptionalContributionGroup
     */
    public function setMinAge($minAge)
    {
        $this->minAge = $minAge;

        return $this;
    }

    /**
     * Get minAge
     *
     * @return int
     */
    public function getMinAge()
    {
        return $this->minAge;
    }

    /**
     * Set canHaveCompulsory
     *
     * @param integer $canHaveCompulsory
     *
     * @return OptionalContributionGroup
     */
    public function setCanHaveCompulsory($canHaveCompulsory)
    {
        $this->canHaveCompulsory = (int)$canHaveCompulsory;

        return $this;
    }

    /**
     * Get canHaveCompulsory
     *
     * @return int
     */
    public function getCanHaveCompulsory()
    {
        return $this->canHaveCompulsory;
    }

    /**
     * Set ableToWork
     *
     * @param integer $ableToWork
     *
     * @return OptionalContributionGroup
     */
    public function setAbleToWork($ableToWork)
    {
        $this->ableToWork = (int)$ableToWork;

        return $this;
    }

    /**
     * Get ableToWork
     *
     * @return int
     */
    public function getAbleToWork()
    {
        return $this->ableToWork;
    }
}
