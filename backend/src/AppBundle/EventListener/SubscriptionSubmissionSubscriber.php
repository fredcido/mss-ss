<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\GetEntityClassTrait;
use AppBundle\Entity\SubscriptionTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Proxy;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SubscriptionSubmissionSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;
    use GetEntityClassTrait;

    protected $needsFlush;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!in_array(SubscriptionTrait::class, class_uses($this->getEntityClass($entity)))) {
                continue;
            }

            if (!$entity->isSubmitted()) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!in_array(SubscriptionTrait::class, class_uses($this->getEntityClass($entity)))) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['subscriptionStatus']) || !$entity->isSubmitted()) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        $today = new \DateTime('now');
        foreach ($injectEntities as $entity) {
            $entity->setSubmissionDate($today);

            $this->compute($em, $entity);

            // Send email about the company subscription submission
            if ($entity instanceof Employer) {
                $this->sendEmployerMailSubmission($em, $entity);
            }

            // Check whether the person has optional subscription
            // to notify the user
            if ($entity instanceof CompulsoryAccessionSubscription) {
                $this->checkPersonOptionSubscription($em, $entity);
            }
        }
    }

    protected function sendEmployerMailSubmission($em, Employer $entity)
    {
        // Check whether the employer is related to an user
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['employer' => $entity]);

        // If there is an user for that employer, send a email
        if (!empty($user)) {
            $emailQueue = $this->container->get('app.mail.user_company')->submissionMail($user);
            $this->compute($em, $emailQueue);
        }
    }

    protected function checkPersonOptionSubscription($em, CompulsoryAccessionSubscription $subscription)
    {
        $person = $subscription->getPerson();

        // Check whether the person is related to an user
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['person' => $person]);

        $repository = $em->getRepository('AppBundle:OptionalAccessionSubscription');
        $optionalSubscription = $repository->getCurrentByPerson($person);

        // warn the user to suspend its optional subscription due to the recently submitted compulsory subscription
        if ($user && $optionalSubscription) {
            $userPersonMailer = $this->container->get('app.mail.user_person');
            $emailQueue = $userPersonMailer->optionalSubscriptionWarningEmail($user, $subscription, $optionalSubscription);
            $this->compute($em, $emailQueue);
        }
    }
}
