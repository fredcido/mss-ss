app.component('workContract', {
    templateUrl: 'views/components/workContract.html',
    bindings: {
        contract: '=',
        active: '<',
        disabled: '<'
    },
    controller: function($scope, $translate, $timeout, GenericService) {
        var self = this;

        self.contractTypes = [];
        self.regimes = [];
        self.labourLaws = [];
        self.contractStatuses = [];

        var getContractTypes = function() {
            GenericService.find('contract-type').then(function(response) {
                self.contractTypes = response.data;
            });
        };

        var getRegimes = function() {
            GenericService.find('regime').then(function(response) {
                self.regimes = response.data;
            });
        };

        var getLabourLaws = function() {
            GenericService.find('labour-law').then(function(response) {
                self.labourLaws = response.data;
            });
        };

        var setDefaultContractStatus = function() {
            if (1 == self.contractStatuses.length && self.contract && !self.contract.contractStatus) {
                self.contract.contractStatus = self.contractStatuses[0];
            }
        };

        var getContractStatuses = function(activeOnly) {
            var promise;
            if (activeOnly) {
                promise = GenericService.httpGet('contract-status/active/1');
            } else {
                promise = GenericService.find('contract-status');
            }
            
            promise.then(function(response) {
                self.contractStatuses = response.data;
                setDefaultContractStatus();
            });
        };

        self.changeLabourLaw = function(labourLawId) {
            self.contract.contractType = null;
            self.searchContractType(labourLawId);
        };

        self.searchContractType = function(labourLawId) {
            self.contractTypes = [];
            GenericService.httpGet('contract-type/labour-law/' + labourLawId).then(function(response) {
                self.contractTypes = response.data;
            });
        };

        $scope.$watch(
            function(scope){ 
                if (self.contract && self.contract.labourLaw)
                    return self.contract.labourLaw; 
                else
                    return null;
            },
            function(labourLaw){
                if (labourLaw) {
                    self.searchContractType(self.contract.labourLaw.id);
                }
            }
        );

        self.$onInit = function() {
            self.active = angular.isUndefined(self.active) ? true : self.active;

            getContractTypes();
            getRegimes();
            getLabourLaws();
            getContractStatuses(self.active);
        };

        self.$onChanges = function() {
            setDefaultContractStatus();
        };
    }
});
