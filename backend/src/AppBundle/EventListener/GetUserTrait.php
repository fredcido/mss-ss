<?php

namespace AppBundle\EventListener;

trait GetUserTrait
{
    public function getUser()
    {
        if (null === ($token = $this->container->get('security.token_storage')->getToken())) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }
}
