<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class CleanTemporaryFilesListener
{
    use ContainerAwareTrait;

    public function cleanFiles(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $cleanFiles = $this->container->get('app.clean_files');
        $cleanFiles->clean();
    }
}
