app.controller("EmployerListController", function($scope, $translate, $state, EnumService) {
    $scope.actionsGrid = [{
        title: $translate.instant('edit'),
        callback: function(employer) {
            $state.go('app.employer-form', {
                id: employer.id
            });
        }
    }];

    $scope.params = {
        all_status: 1,
        version: EnumService.getVersion('standard')
    };
});
