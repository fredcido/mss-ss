<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LabourLaw
 *
 * @ORM\Table(name="labour_law")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LabourLawRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 */
class LabourLaw
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="LicenseLabourLaw", mappedBy="labourLaw", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $licenseLabourLaws;

    /**
     * @ORM\OneToMany(targetEntity="ContractType", mappedBy="labourLaw")
     */
    private $contractTypes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->licenseLabourLaws = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contractTypes = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LabourLaw
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add licenseLabourLaw
     *
     * @param LicenseLabourLaw $licenseLabourLaw
     *
     * @return License
     */
    public function addLicenseLabourLaw(LicenseLabourLaw $licenseLabourLaw)
    {
        $this->licenseLabourLaws[] = $licenseLabourLaw;
        $licenseLabourLaw->setLicense($this);
        return $this;
    }

    /**
     * Remove licenseLabourLaw
     *
     * @param LicenseLabourLaw $licenseLabourLaw
     */
    public function removeLicenseLabourLaw(LicenseLabourLaw $licenseLabourLaw)
    {
        $this->licenseLabourLaws->removeElement($licenseLabourLaw);
    }

    /**
     * Get licenseLabourLaws
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicenseLabourLaws()
    {
        return $this->licenseLabourLaws;
    }

    /**
     * Add contractTypes
     *
     * @param ContractType $contractType
     * @return Race
     */
    public function addContractType(ContractType $contractType)
    {
        $this->contractTypes[] = $contractType;
        return $this;
    }

    /**
     * Remove contractType
     *
     * @param ContractType $contractType
     */
    public function removeContractType(ContractType $contractType)
    {
        $this->contractTypes->removeElement($contractType);
    }

    /**
     * Get contractTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContractTypes()
    {
        return $this->contractTypes;
    }
}
