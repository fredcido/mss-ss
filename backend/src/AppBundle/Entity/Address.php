<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address
{
    use EntityAuditTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=200, nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="cityAbroad", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $cityAbroad;

    /**
     * @var Suku
     *
     * @ORM\ManyToOne(targetEntity="Suku")
     * @JMS\Groups({"list", "details"})
     */
    private $suku;

    /**
     * @var Village
     *
     * @ORM\ManyToOne(targetEntity="Village")
     * @JMS\Groups({"list", "details"})
     */
    private $village;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @JMS\Groups({"list", "details"})
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(name="main", type="smallint", options={"default" : 1})
     * @JMS\Groups({"list", "details"})
     */
    private $main = 1;

    public function __construct()
    {
        $this->main = 1;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get main
     *
     * @return string
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set main
     *
     * @param string $main
     *
     * @return Address
     */
    public function setMain($main)
    {
        $this->main = (int)$main;
        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Address
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set cityAbroad
     *
     * @param string $cityAbroad
     *
     * @return Address
     */
    public function setCityAbroad($cityAbroad)
    {
        $this->cityAbroad = $cityAbroad;

        return $this;
    }

    /**
     * Get cityAbroad
     *
     * @return string
     */
    public function getCityAbroad()
    {
        return $this->cityAbroad;
    }

    /**
     * Gets the value of country.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the value of country.
     *
     * @param Country $country the country
     *
     * @return self
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Gets the value of suku.
     *
     * @return Suku
     */
    public function getSuku()
    {
        return $this->suku;
    }

    /**
     * Sets the value of suku.
     *
     * @param Suku $suku the suku
     *
     * @return self
     */
    public function setSuku(Suku $suku = null)
    {
        $this->suku = $suku;
        return $this;
    }

    /**
     * Gets the value of village.
     *
     * @return Village
     */
    public function getVillage()
    {
        return $this->village;
    }

    /**
     * Sets the value of village.
     *
     * @param Suku $village the village
     *
     * @return self
     */
    public function setVillage(Village $village = null)
    {
        $this->village = $village;
        return $this;
    }
}
