app.directive('maxShared', [function() {
    var maxSharedValues = {};
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            if (maxSharedValues[attrs.maxShared] == undefined) {
                maxSharedValues[attrs.maxShared] = {};
            }

            var index = (new Date()).getTime();
            maxSharedValues[attrs.maxShared][index] = ctrl;

            var calcTotal = function(totalReference)
            {
                var totalShared = 0;
                angular.forEach(
                    maxSharedValues[attrs.maxShared],
                    function(item) {
                        var value = !(item.$viewValue) ? 0 : item.$viewValue;
                        totalShared += parseFloat(value);
                    }
                );

                angular.forEach(
                    maxSharedValues[attrs.maxShared],
                    function(item) {
                        item.$setValidity('maxShared', totalReference >= totalShared);
                    }
                );
            };

            scope.$on('$destroy', 
                function()
                {
                    delete maxSharedValues[attrs.maxShared][index];
                    calcTotal(parseFloat(scope.$eval(attrs.maxShared)));
                }
            );

            scope.$watch(function() {
                return [scope.$eval(attrs.maxShared), ctrl.$modelValue];
            }, function(values) {
                if (values.length >= 2) {
                    var totalReference = parseFloat(!(values[0]) ? 0 : values[0]);
                    calcTotal(totalReference);
                } else{
                    angular.forEach(
                        maxSharedValues[attrs.maxShared],
                        function(item) {
                            item.$setValidity('maxShared', true);
                        }
                    );
                }
            }, true);
        }
    };
}]);
