app.directive("checkEntity", function($timeout, $translate, GenericService, ViewHelper) {
    return {
        restrict: 'A',
        require: "ngModel",
        scope:{
            onFound: '&',
            onNotFound: '&',
            onDeclined: '&',
            onAccepted: '&',
            params: '<'
        },
        link: function(scope, elem, attr, ctrl) {
            var entity = attr.checkEntity ? attr.checkEntity : 'employer';
            var attribute = attr.checkAttr ? attr.checkAttr : elem.attr('name');

            var search = function(value) {
                var filters = {
                    all_status: 1,
                    full_check: true
                };
                
                filters[attribute] = value;

                filters = angular.extend(filters, scope.params);
                var container = $(elem).closest('div');

                ViewHelper.blockContainer(container, $translate.instant('loading'));
                GenericService.httpGet(entity + '/search', filters).then(function(response) {
                    $(elem).data('check-entity', null);
                    var data = response.data;

                    ViewHelper.unblockContainer(container);

                    if (data.length >= 1) {
                        var entityFound = data[0];

                        if (scope.onFound && false === scope.onFound({entity: entityFound})) {
                            return false;
                        }

                        var attributeDash = attribute.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
                        var replacements = {'code': $translate.instant(attributeDash), 'value': entityFound[attribute]};
                        var message = $translate.instant(entity + '_inserted_message_confirm', replacements);

                        swal({
                                title: $translate.instant(entity + '_inserted'),
                                text: message,
                                type: 'warning',
                                showCancelButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (!isConfirm) {
                                    scope.onDeclined();
                                } else {
                                    scope.onAccepted({entity: entityFound});
                                }
                            }
                        );
                    } else {
                        scope.onNotFound();
                    }
                });
            };

            $(elem).on(
                'change',
                function() {
                    var value = $(this).val();
                    var isDirty = ctrl.$dirty;

                    if (!value || !isDirty) {
                        return;
                    }

                    $timeout(function(){
                        if ($(elem).val() == value && $(elem).data('check-entity') !== value) {
                            $(elem).data('check-entity', value);
                            search($(elem).val());
                        }
                    }, 400);
                }
            );
        }
    };
});
