<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Contribution
 *
 * @ORM\Table(name="contribution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContributionRepository")
 */
class Contribution
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="contributions")
     * @JMS\Groups({"list", "details"})
     */
    private $person;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $dateEnd;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer")
     * @JMS\Groups({"list", "details"})
     */
    private $employer;

    /**
     * @var ContractType
     *
     * @ORM\ManyToOne(targetEntity="ContractType")
     * @JMS\Groups({"list", "details"})
     */
    private $contractType;

    /**
     * @var ContractStatus
     *
     * @ORM\ManyToOne(targetEntity="ContractStatus")
     * @JMS\Groups({"list", "details"})
     */
    private $contractStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="workDays", type="integer")
     * @JMS\Groups({"list", "details"})
     */
    private $workDays;

    /**
     * @var int
     *
     * @ORM\Column(name="workedDays", type="integer")
     * @JMS\Groups({"list", "details"})
     */
    private $workedDays;

    /**
     * @var int
     *
     * @ORM\Column(name="daysInPeriod", type="integer")
     * @JMS\Groups({"list", "details"})
     */
    private $daysInPeriod;

    /**
     * @var float
     *
     * @ORM\Column(name="hourCost", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $hourCost;

    /**
     * @var float
     *
     * @ORM\Column(name="hoursDay", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $hoursDay;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalIncome", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $theoricalIncome;

    /**
     * @var float
     *
     * @ORM\Column(name="totalOvertime", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $totalOvertime;

    /**
     * @var float
     *
     * @ORM\Column(name="allowance", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $allowance;

    /**
     * @var float
     *
     * @ORM\Column(name="declaredIncome", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $declaredIncome;

    /**
     * @var float
     *
     * @ORM\Column(name="totalTheoricalIncome", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $totalTheoricalIncome;

    /**
     * @var float
     *
     * @ORM\Column(name="voluntary", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $voluntary;

    /**
     * @var int
     *
     * @ORM\Column(name="absences", type="integer")
     * @JMS\Groups({"list", "details"})
     */
    private $absences;

    /**
     * @var float
     *
     * @ORM\Column(name="debt", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $debt;

    /**
     * @var float
     *
     * @ORM\Column(name="employeeTax", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $employeeTax;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalEmployeeContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalEmployeeContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="employerTax", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $employerTax;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalEmployerContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalEmployerContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="parEmployeeContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $parEmployeeContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="parEmployerContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $parEmployerContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="parTotalContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $parTotalContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalTotalEmployeeContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalTotalEmployeeContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalTotalEmployerContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalTotalEmployerContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="theoricalTotalContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $theoricalTotalContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="paidEmployeeContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $paidEmployeeContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="paidEmployerContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $paidEmployerContribution;

    /**
     * @var float
     *
     * @ORM\Column(name="paidTotalContribution", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    private $paidTotalContribution;

    /**
     * @ORM\OneToMany(targetEntity="EmployeeLicense", mappedBy="contribution", cascade={"persist"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details"})
     *
     * @var  ArrayCollection
     */
    private $licenses;

    /**
     * @ORM\OneToMany(targetEntity="ContributionOvertime", mappedBy="contribution", cascade={"persist"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details"})
     *
     * @var  ArrayCollection
     */
    private $overtime;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->licenses = new ArrayCollection();
        $this->overtime = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Contribution
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Contribution
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set declaredIncome
     *
     * @param string $declaredIncome
     *
     * @return Contribution
     */
    public function setDeclaredIncome($declaredIncome)
    {
        $this->declaredIncome = $declaredIncome;

        return $this;
    }

    /**
     * Get declaredIncome
     *
     * @return string
     */
    public function getDeclaredIncome()
    {
        return $this->declaredIncome;
    }

    /**
     * Set workedDays
     *
     * @param integer $workedDays
     *
     * @return Contribution
     */
    public function setWorkedDays($workedDays)
    {
        $this->workedDays = $workedDays;

        return $this;
    }

    /**
     * Get workedDays
     *
     * @return int
     */
    public function getWorkedDays()
    {
        return $this->workedDays;
    }

    /**
     * Set absences
     *
     * @param integer $absences
     *
     * @return Contribution
     */
    public function setAbsences($absences)
    {
        $this->absences = $absences;

        return $this;
    }

    /**
     * Get absences
     *
     * @return int
     */
    public function getAbsences()
    {
        return $this->absences;
    }

    /**
     * Set debt
     *
     * @param string $debt
     *
     * @return Contribution
     */
    public function setDebt($debt)
    {
        $this->debt = $debt;

        return $this;
    }

    /**
     * Get debt
     *
     * @return string
     */
    public function getDebt()
    {
        return $this->debt;
    }

    /**
     * Gets the value of contractType.
     *
     * @return ContractType
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Sets the value of contractType.
     *
     * @param ContractType $contractType the contract type
     *
     * @return self
     */
    public function setContractType(ContractType $contractType)
    {
        $this->contractType = $contractType;
        return $this;
    }

    /**
     * Add license
     *
     * @param EmployeeLicense $license
     */
    public function addLicense(EmployeeLicense $license)
    {
        $this->licenses[] = $license;
        $license->setContribution($this);
        return $this;
    }

    /**
     * Remove license
     *
     * @param EmployeeLicense $license
     */
    public function removeLicense(EmployeeLicense $license)
    {
        $this->licenses->removeElement($license);
    }

    /**
     * Get license
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicenses()
    {
        return $this->licenses;
    }

    /**
     * Gets the value of person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets the value of person.
     *
     * @param Person $person the person
     *
     * @return self
     */
    public function setPerson(Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Set employer
     *
     * @param \AppBundle\Entity\Employer $employer
     *
     * @return Contribution
     */
    public function setEmployer(\AppBundle\Entity\Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return \AppBundle\Entity\Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Set workDays
     *
     * @param integer $workDays
     *
     * @return Contribution
     */
    public function setWorkDays($workDays)
    {
        $this->workDays = $workDays;

        return $this;
    }

    /**
     * Get workDays
     *
     * @return integer
     */
    public function getWorkDays()
    {
        return $this->workDays;
    }

    /**
     * Set daysInPeriod
     *
     * @param integer $daysInPeriod
     *
     * @return Contribution
     */
    public function setDaysInPeriod($daysInPeriod)
    {
        $this->daysInPeriod = $daysInPeriod;

        return $this;
    }

    /**
     * Get daysInPeriod
     *
     * @return integer
     */
    public function getDaysInPeriod()
    {
        return $this->daysInPeriod;
    }

    /**
     * Set hourCost
     *
     * @param string $hourCost
     *
     * @return Contribution
     */
    public function setHourCost($hourCost)
    {
        $this->hourCost = $hourCost;

        return $this;
    }

    /**
     * Get hourCost
     *
     * @return string
     */
    public function getHourCost()
    {
        return $this->hourCost;
    }

    /**
     * Set hoursDay
     *
     * @param string $hoursDay
     *
     * @return Contribution
     */
    public function setHoursDay($hoursDay)
    {
        $this->hoursDay = $hoursDay;

        return $this;
    }

    /**
     * Get hoursDay
     *
     * @return string
     */
    public function getHoursDay()
    {
        return $this->hoursDay;
    }

    /**
     * Set theoricalIncome
     *
     * @param string $theoricalIncome
     *
     * @return Contribution
     */
    public function setTheoricalIncome($theoricalIncome)
    {
        $this->theoricalIncome = $theoricalIncome;

        return $this;
    }

    /**
     * Get theoricalIncome
     *
     * @return string
     */
    public function getTheoricalIncome()
    {
        return $this->theoricalIncome;
    }

    /**
     * Set totalOvertime
     *
     * @param string $totalOvertime
     *
     * @return Contribution
     */
    public function setTotalOvertime($totalOvertime)
    {
        $this->totalOvertime = $totalOvertime;

        return $this;
    }

    /**
     * Get totalOvertime
     *
     * @return string
     */
    public function getTotalOvertime()
    {
        return $this->totalOvertime;
    }

    /**
     * Set allowance
     *
     * @param string $allowance
     *
     * @return Contribution
     */
    public function setAllowance($allowance)
    {
        $this->allowance = $allowance;

        return $this;
    }

    /**
     * Get allowance
     *
     * @return string
     */
    public function getAllowance()
    {
        return $this->allowance;
    }

    /**
     * Set totalTheoricalIncome
     *
     * @param string $totalTheoricalIncome
     *
     * @return Contribution
     */
    public function setTotalTheoricalIncome($totalTheoricalIncome)
    {
        $this->totalTheoricalIncome = $totalTheoricalIncome;

        return $this;
    }

    /**
     * Get totalTheoricalIncome
     *
     * @return string
     */
    public function getTotalTheoricalIncome()
    {
        return $this->totalTheoricalIncome;
    }

    /**
     * Set voluntary
     *
     * @param string $voluntary
     *
     * @return Contribution
     */
    public function setVoluntary($voluntary)
    {
        $this->voluntary = $voluntary;

        return $this;
    }

    /**
     * Get voluntary
     *
     * @return string
     */
    public function getVoluntary()
    {
        return $this->voluntary;
    }

    /**
     * Set employeeTax
     *
     * @param string $employeeTax
     *
     * @return Contribution
     */
    public function setEmployeeTax($employeeTax)
    {
        $this->employeeTax = $employeeTax;

        return $this;
    }

    /**
     * Get employeeTax
     *
     * @return string
     */
    public function getEmployeeTax()
    {
        return $this->employeeTax;
    }

    /**
     * Set theoricalEmployeeContribution
     *
     * @param string $theoricalEmployeeContribution
     *
     * @return Contribution
     */
    public function setTheoricalEmployeeContribution($theoricalEmployeeContribution)
    {
        $this->theoricalEmployeeContribution = $theoricalEmployeeContribution;

        return $this;
    }

    /**
     * Get theoricalEmployeeContribution
     *
     * @return string
     */
    public function getTheoricalEmployeeContribution()
    {
        return $this->theoricalEmployeeContribution;
    }

    /**
     * Set employerTax
     *
     * @param string $employerTax
     *
     * @return Contribution
     */
    public function setEmployerTax($employerTax)
    {
        $this->employerTax = $employerTax;

        return $this;
    }

    /**
     * Get employerTax
     *
     * @return string
     */
    public function getEmployerTax()
    {
        return $this->employerTax;
    }

    /**
     * Set theoricalEmployerContribution
     *
     * @param string $theoricalEmployerContribution
     *
     * @return Contribution
     */
    public function setTheoricalEmployerContribution($theoricalEmployerContribution)
    {
        $this->theoricalEmployerContribution = $theoricalEmployerContribution;

        return $this;
    }

    /**
     * Get theoricalEmployerContribution
     *
     * @return string
     */
    public function getTheoricalEmployerContribution()
    {
        return $this->theoricalEmployerContribution;
    }

    /**
     * Set theoricalContribution
     *
     * @param string $theoricalContribution
     *
     * @return Contribution
     */
    public function setTheoricalContribution($theoricalContribution)
    {
        $this->theoricalContribution = $theoricalContribution;

        return $this;
    }

    /**
     * Get theoricalContribution
     *
     * @return string
     */
    public function getTheoricalContribution()
    {
        return $this->theoricalContribution;
    }

    /**
     * Set parEmployeeContribution
     *
     * @param string $parEmployeeContribution
     *
     * @return Contribution
     */
    public function setParEmployeeContribution($parEmployeeContribution)
    {
        $this->parEmployeeContribution = $parEmployeeContribution;

        return $this;
    }

    /**
     * Get parEmployeeContribution
     *
     * @return string
     */
    public function getParEmployeeContribution()
    {
        return $this->parEmployeeContribution;
    }

    /**
     * Set parEmployerContribution
     *
     * @param string $parEmployerContribution
     *
     * @return Contribution
     */
    public function setParEmployerContribution($parEmployerContribution)
    {
        $this->parEmployerContribution = $parEmployerContribution;

        return $this;
    }

    /**
     * Get parEmployerContribution
     *
     * @return string
     */
    public function getParEmployerContribution()
    {
        return $this->parEmployerContribution;
    }

    /**
     * Set parTotalContribution
     *
     * @param string $parTotalContribution
     *
     * @return Contribution
     */
    public function setParTotalContribution($parTotalContribution)
    {
        $this->parTotalContribution = $parTotalContribution;

        return $this;
    }

    /**
     * Get parTotalContribution
     *
     * @return string
     */
    public function getParTotalContribution()
    {
        return $this->parTotalContribution;
    }

    /**
     * Set theoricalTotalEmployeeContribution
     *
     * @param string $theoricalTotalEmployeeContribution
     *
     * @return Contribution
     */
    public function setTheoricalTotalEmployeeContribution($theoricalTotalEmployeeContribution)
    {
        $this->theoricalTotalEmployeeContribution = $theoricalTotalEmployeeContribution;

        return $this;
    }

    /**
     * Get theoricalTotalEmployeeContribution
     *
     * @return string
     */
    public function getTheoricalTotalEmployeeContribution()
    {
        return $this->theoricalTotalEmployeeContribution;
    }

    /**
     * Set theoricalTotalEmployerContribution
     *
     * @param string $theoricalTotalEmployerContribution
     *
     * @return Contribution
     */
    public function setTheoricalTotalEmployerContribution($theoricalTotalEmployerContribution)
    {
        $this->theoricalTotalEmployerContribution = $theoricalTotalEmployerContribution;

        return $this;
    }

    /**
     * Get theoricalTotalEmployerContribution
     *
     * @return string
     */
    public function getTheoricalTotalEmployerContribution()
    {
        return $this->theoricalTotalEmployerContribution;
    }

    /**
     * Set theoricalTotalContribution
     *
     * @param string $theoricalTotalContribution
     *
     * @return Contribution
     */
    public function setTheoricalTotalContribution($theoricalTotalContribution)
    {
        $this->theoricalTotalContribution = $theoricalTotalContribution;

        return $this;
    }

    /**
     * Get theoricalTotalContribution
     *
     * @return string
     */
    public function getTheoricalTotalContribution()
    {
        return $this->theoricalTotalContribution;
    }

    /**
     * Set paidEmployeeContribution
     *
     * @param string $paidEmployeeContribution
     *
     * @return Contribution
     */
    public function setPaidEmployeeContribution($paidEmployeeContribution)
    {
        $this->paidEmployeeContribution = $paidEmployeeContribution;

        return $this;
    }

    /**
     * Get paidEmployeeContribution
     *
     * @return string
     */
    public function getPaidEmployeeContribution()
    {
        return $this->paidEmployeeContribution;
    }

    /**
     * Set paidEmployerContribution
     *
     * @param string $paidEmployerContribution
     *
     * @return Contribution
     */
    public function setPaidEmployerContribution($paidEmployerContribution)
    {
        $this->paidEmployerContribution = $paidEmployerContribution;

        return $this;
    }

    /**
     * Get paidEmployerContribution
     *
     * @return string
     */
    public function getPaidEmployerContribution()
    {
        return $this->paidEmployerContribution;
    }

    /**
     * Set paidTotalContribution
     *
     * @param string $paidTotalContribution
     *
     * @return Contribution
     */
    public function setPaidTotalContribution($paidTotalContribution)
    {
        $this->paidTotalContribution = $paidTotalContribution;

        return $this;
    }

    /**
     * Get paidTotalContribution
     *
     * @return string
     */
    public function getPaidTotalContribution()
    {
        return $this->paidTotalContribution;
    }

    /**
     * Add overtime
     *
     * @param \AppBundle\Entity\ContributionOvertime $overtime
     *
     * @return Contribution
     */
    public function addOvertime(\AppBundle\Entity\ContributionOvertime $overtime)
    {
        $this->overtime[] = $overtime;
        $overtime->setContribution($this);
        return $this;
    }

    /**
     * Remove overtime
     *
     * @param \AppBundle\Entity\ContributionOvertime $overtime
     */
    public function removeOvertime(\AppBundle\Entity\ContributionOvertime $overtime)
    {
        $this->overtime->removeElement($overtime);
    }

    /**
     * Get overtime
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOvertime()
    {
        return $this->overtime;
    }

    /**
     * Gets the value of contractStatus.
     *
     * @return ContractStatus
     */
    public function getContractStatus()
    {
        return $this->contractStatus;
    }

    /**
     * Sets the value of contractStatus.
     *
     * @param ContractStatus $contractStatus the contract status
     *
     * @return self
     */
    public function setContractStatus(ContractStatus $contractStatus)
    {
        $this->contractStatus = $contractStatus;

        return $this;
    }
}
