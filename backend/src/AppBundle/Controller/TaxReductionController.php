<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TaxReduction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class TaxReductionController extends BaseController
{
    protected $repository = 'AppBundle:TaxReduction';

    protected $form = 'AppBundle\Form\TaxReductionType';

    protected $routeGet = 'app.tax_reduction.get';

    protected $entity = TaxReduction::class;

    protected function getOrderBy()
    {
        return array('year' => 'asc');
    }
}
