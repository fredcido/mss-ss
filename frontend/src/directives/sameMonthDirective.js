app.directive('sameMonth', [function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            scope.$watch(function() {
                return [scope.$eval(attrs.sameMonth), ctrl.$viewValue];
            }, function(values) {
                if (values[0] && values[1]) {
                    var dateRerence = moment(values[0]);
                    var currentDate = moment(values[1], "DD/MM/YYYY");
                    var isSame = dateRerence.isSame(currentDate, 'month') && dateRerence.isSame(currentDate, 'year');
                    ctrl.$setValidity('sameMonth', isSame);
                }
                else{
                    ctrl.$setValidity('sameMonth', true);
                }
            }, true);
        }
    };
}]);
