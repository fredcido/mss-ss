app.service("ViewHelper", function($translate, $rootScope, $timeout) {
    var _renderOption = function(options, value) {
        var res = '';
        if (options instanceof Array) {
            options.some(function(i) {
                if (value == i.value) {
                    res = i.label;
                }
            });
        }
        return res;
    };

    var _renderStatus = function(value) {
        var options = [{
            value: 0,
            label: "<b class='text-capitalize'><i class='fa fa-circle' style='color:#FF0039'></i>&nbsp;" + $translate.instant('inative') + '</b>'
        }, {
            value: 1,
            label: "<b class='text-capitalize'><i class='fa fa-circle' style='color:#3FB618'></i>&nbsp;" + $translate.instant('active') + '</b>'
        }];

        return _renderOption(options, value);
    };

    var _renderLink = function(state, title, id) {
        var $div = $('<div>');
        var $btn = $('<a href="javascript:;" class="btn btn-default btn-sm text-capitalize">')
            .attr('ui-sref', state + "({id:" + id + "})")
            .append(title)
            .appendTo($div);

        return $div.html();
    };


    var _scrollTo = function(seletor, timer) {
        if (!$(seletor).length)
            return false;

        el = $(seletor);
        timer = timer || 900;

        if (el.closest('.model-modal').length) {

            scroller = el.closest('.model-modal');
            scroll = el.position().top;

        } else {

            scroller = 'html,body';
            scroll = el.offset().top - 120;
        }

        $(scroller).animate({
            scrollTop: scroll
        }, timer);

        return true;
    };

    var _showErrorApi = function(response) {
        var $container = $('<div>');
        var $ul = $('<ul class="nav text-left error-api">');

        swal({
            title: response.title,
            text: $container.append($ul).html(),
            type: 'error',
            html: true
        });
    };

    var _getErrorApi = function(errors, $ul) {
        try {
            if (errors) {
                for (var i in errors) {
                    var $li = $('<li>').appendTo($ul);
                    var label = angular.element("input[name='" + i + "']").parent().find('.control-label').html() || '';
                    $li.append('<b>' + label + '</b>');

                    if (typeof errors[i] === 'object') {
                        var $ulChild = $('<ul>').appendTo($li);
                        $li.append(_getErrorApi(errors[i], $ulChild));
                    } else {
                        $li.append(errors);
                    }
                }
            }

            return $ul;
        } catch (e) {}
    };

    var _getErrorApiForm = function(errors) {
        try {
            if (errors) {
                for (var i in errors) {
                    if ($("input[name='" + i + "']").length >= 1) {
                        var $input =  angular.element("input[name='" + i + "']").addClass('ng-dirty ng-invalid');
                        var $formGroup = $input.parent().addClass('has-error');

                        if ($formGroup.find('.help-block').length >= 1) {
                            $formGroup.find('.help-block').html(errors[i]);
                        } else {
                            $formGroup.append('<span class="help-block">' + errors[i] + '</span>');
                        }
                    }
                }
            }
        } catch (e) {}
    };

    var _blockContainer = function(container, message) {
        $(container).block({
            message: message,
            css: {
                border: 'none',
                padding: '10px',
                backgroundColor: '#8F8F8F',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: 0.5,
                color: '#fff'
            },
            overlayCSS: {
                backgroundColor: '#ccc',
                opacity: 0.5,
                cursor: 'wait'
            },
        });
    };

    var _unblockContainer = function(container) {
        $(container).unblock();
    };

    var _blockPage = function(message) {
        $.blockUI({
            message: message,
            css: {
                border: 'none',
                padding: '10px',
                backgroundColor: '#8F8F8F',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                color: '#fff',
                'font-size': '18px'
            },
            overlayCSS: {
                backgroundColor: '#ccc',
                opacity: 0.9,
                cursor: 'wait'
            },
        });
    };

    var _unblockPage = function() {
        $.unblockUI();
    };

    var _makeFormDirty = function(form) {
        try {
            form.$dirty = true;
            angular.forEach(form.$error, function(field) {
                angular.forEach(field, function(errorField) {
                    errorField.$dirty = true;
                    _makeFormDirty(errorField);
                });
            });
        } catch(e) {

        }
    };

    var _errorForm = function(form) {
        _makeFormDirty(form);

        swal({
            title: $translate.instant('invalid_form_title'), 
            text: $translate.instant('invalid_form_message'), 
            type: 'warning'
        }, function(){
            $timeout(
                function(){
                    var selector = '.has-error:visible:first :input:visible';
                    _scrollTo(selector);
                    $(selector).focus();
                },
                500
            );
        });
    };

    var _setErrorServer = function(form, errorServer) {
        var formControls = {};
        angular.forEach(form, function(value, key) {
            if (value && typeof value === 'object' && value.hasOwnProperty('$modelValue')) {
                formControls[value.$name] = value; 
            }
        });

        form.$invalid = true;
        form.$dirty = true;
        angular.forEach(formControls, function(value, key) {
            _setErrorServer(value, errorServer);
            value.$invalid = true;
            value.$dirty = true;
        });
    };

    var _errorServer = function(form, responseServer) {
        var title, message;
        if (responseServer.status == 400) {
            $rootScope.errorServer = responseServer.errors;
            title = $translate.instant('invalid_form_title');
            message = $translate.instant('invalid_form_message');
        } else{
            title = $translate.instant('error_title');
            message = $translate.instant('error_text');
        }

        swal({
            title: title, 
            text: message, 
            type: 'error'
        }, function(){
            $timeout(
                function(){
                    var selector = '.has-error:visible:first :input';
                    if ($(selector).length > 0) {
                        $(selector).focus();
                    } else {
                        selector = '.errors-container';
                    }

                    _scrollTo(selector);
                },
                500
            );
        });
    };

    return {
        renderOption: _renderOption,
        renderStatus: _renderStatus,
        scrollTo: _scrollTo,
        renderLink: _renderLink,
        showErrorApi: _showErrorApi,
        blockContainer: _blockContainer,
        unblockContainer: _unblockContainer,
        blockPage: _blockPage,
        unblockPage: _unblockPage,
        errorForm: _errorForm,
        errorServer: _errorServer
    };
});
