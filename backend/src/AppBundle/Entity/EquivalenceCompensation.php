<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * EquivalenceCompensation
 *
 * @ORM\Table(name="equivalence_compensation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EquivalenceCompensationRepository")
 */
class EquivalenceCompensation
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    const COMPULSORY = 'C';
    const OPTIONAL = 'O';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="accession", type="string", length=1)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $accession;

    /**
     * @ORM\ManyToOne(targetEntity="Supplement", inversedBy="incidenceBasis")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="supplement_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $supplement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accession
     *
     * @param string $accession
     *
     * @return EquivalenceCompensation
     */
    public function setAccession($accession)
    {
        $validAccessions = array(self::COMPULSORY, self::OPTIONAL);
        if (!in_array($accession, $validAccessions)) {
            throw new \InvalidArgumentException(sprintf("Invalid accession: %s provided for %s", $accession, __CLASS__));
        }

        $this->accession = $accession;

        return $this;
    }

    /**
     * Get accession
     *
     * @return string
     */
    public function getAccession()
    {
        return $this->accession;
    }

    /**
     * Gets the value of supplement.
     *
     * @return mixed
     */
    public function getSupplement()
    {
        return $this->supplement;
    }

    /**
     * Sets the value of supplement.
     *
     * @param mixed $supplement the supplement
     *
     * @return self
     */
    public function setSupplement($supplement)
    {
        $this->supplement = $supplement;
        return $this;
    }
}
