<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Suku;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class SukuController extends BaseController
{
    protected $repository = 'AppBundle:Suku';

    protected $form = 'AppBundle\Form\SukuType';

    protected $routeGet = 'app.suku.get';

    protected $entity = Suku::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function getByPostoAdministrativoAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(['postoAdministrativo' => $id]);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('list');
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
