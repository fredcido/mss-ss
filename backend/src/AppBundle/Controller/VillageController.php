<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Village;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class VillageController extends BaseController
{
    protected $repository = 'AppBundle:Village';

    protected $form = 'AppBundle\Form\VillageType';

    protected $routeGet = 'app.village.get';

    protected $entity = Village::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function getBySukuAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(['suku' => $id]);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('list');
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
