<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Person;
use AppBundle\Form\PersonType;
use AppBundle\Security\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class PersonController extends BaseController
{
    protected $repository = 'AppBundle:Person';

    protected $form = PersonType::class;

    protected $entity = Person::class;

    public function getByVersionAction($id, $version, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->findByVersion($id, $version);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $this->denyAccessUnlessGranted(Actions::VIEW, $entity);

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }

    public function duplicateAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find person');
        }

        $duplicator = $this->get('app.duplicate_entity');
        $data = $request->request->all();

        $newPerson = $duplicator->duplicatePerson($entity, $data);
        $newPerson->setVersion($data['version']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newPerson);
        $em->flush();

        return $this->createApiResponse($newPerson, Response::HTTP_OK, array('details'));
    }
}
