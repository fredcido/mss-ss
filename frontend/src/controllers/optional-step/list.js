app.controller("OptionalStepListController", function($scope, $translate, ViewHelper, GenericService, SettingService) {
    $scope.displayCollection = [];
    $scope.rowCollection = [];

    var _uri = 'optional-step/find?all=1';
    var getSaii = function() {
        $scope.saii = SettingService.get('saii');
    };

    var _init = function() {
        getSaii();

        ViewHelper.blockContainer('.table', $translate.instant('loading'));
        GenericService.httpGet(_uri).then(function(response) {
            response.data.map(function(o){
                o.cost = o.indexer * $scope.saii;
            });

            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;

            ViewHelper.unblockContainer('.table');
        });
    };

    _init();
});
