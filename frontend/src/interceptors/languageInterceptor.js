app.factory("languageInterceptor", function($rootScope, $q, $location, $window, $translate, ViewHelper, $cookies) {
    return {
        request: function(config) {
            if (config.beforeSend)
                config.beforeSend();

            config.headers = config.headers || {};

            var langCurrent = localStorage.getItem('lang');
            if (langCurrent) {
                config.headers['Accept-Language'] = langCurrent;
            }
            return config;
        }
    };
});
