<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use AppBundle\Entity\User;
use AppBundle\Enum\Status;
use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserPersonSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof User || !$entity->hasRole('ROLE_PERSON')) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        /*foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof User || !$entity->hasRole('ROLE_PERSON')) {
                continue;
            }

            $injectEntities[] = $entity;
        }*/

        foreach ($injectEntities as $entity) {
            $entity->setRoles(['ROLE_PERSON']);

            if ($entity->getPerson()) {
                $person = $entity->getPerson();
                $person->setStatus(Status::INACTIVE)
                        ->setVersion(Version::STANDARD);

                $this->compute($em, $person);
                
                $emailQueue = $this->container->get('app.mail.user_person')->newUserMail($entity);
                $this->compute($em, $emailQueue);
            }

            $this->compute($em, $entity);
        }
    }
}
