<?php

namespace AppBundle\Entity;

use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
trait VersionTrait
{
    /**
     * @var smallint
     *
     * @ORM\Column(name="version", type="string", length=255, options={"default" : 0})
     * @JMS\Groups({"list", "details", "audit"})
     */
    private $version = 0;


    /**
     * Gets the value of version.
     *
     * @return smallint
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets the value of version.
     *
     * @param smallint $version the version
     *
     * @return self
     */
    public function setVersion($version)
    {
        if (is_null($version)) {
            $version = Version::STANDARD;
        }
        
        $this->version = $version;
        return $this;
    }

    /**
     * @param  string  $version
     * @return boolean
     */
    public function isOfVersion($version)
    {
        $currentVersion = $this->getVersion();
        $versionPart = explode('_', $currentVersion);
        return (int)array_pop($versionPart) === $version;
    }

    /**
     * @return boolean
     */
    public function isStandard()
    {
        return $this->isOfVersion(Version::STANDARD);
    }

    /**
     * @return boolean
     */
    public function isSubscription()
    {
        return $this->isOfVersion(Version::SUBSCRIPTION);
    }

    /**
     * @return boolean
     */
    public function isUpdate()
    {
        return $this->isOfVersion(Version::UPDATE);
    }
}
