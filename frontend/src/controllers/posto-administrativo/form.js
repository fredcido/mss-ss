app.controller("PostoAdministrativoFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'posto-administrativo';
    var _stateList = 'app.posto-administrativo-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.municipios = [];

    var _getMunicipios = function() {
        GenericService.find('municipio').then(function(response) {
            $scope.municipios = response.data;
        });
    };

    _getMunicipios();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;

            if ($scope.model.municipio) {
                $scope.model.municipio = $scope.model.municipio.id;
            }
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
