app.controller("ContributionListController", function($scope, $translate, $state) {
    $scope.gridAction = [{
        title: $translate.instant('edit'),
        callback: function(contribution) {
            $state.go('app.contribution-form', {
                id: contribution.id
            });
        }
    },{
        title: $translate.instant('new'),
        callback: function(contribution) {
            $state.go('app.contribution-person', {
                    person: contribution.person.id
                }
            );
        }
    }];
});
