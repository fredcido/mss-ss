<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Feedback
 *
 * @ORM\Table(name="feedback")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeedbackRepository")
 */
class Feedback
{
    const EMPLOYER = 'employer';

    use EntityAuditTrait;
    use EntityPermissionTrait;
    use SubscriptionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $reference;


    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $comment;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer")
     * @JMS\Groups({"list", "details"})
     */
    private $employer;

    public function __construct()
    {
        $this->setSubscriptionChangedAt(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Feedback
     */
    public function setReference($reference)
    {
        $valid = [self::EMPLOYER];
        if (!in_array($reference, $valid)) {
            throw new \InvalidArgumentException(sprintf("'%s' is not a valid reference type for %s", $reference, __CLASS__));
        }

        $this->reference = $reference;
        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Feedback
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Gets the value of employer.
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Sets the value of employer.
     *
     * @param Employer $employer the employer
     *
     * @return self
     */
    public function setEmployer(Employer $employer)
    {
        $this->employer = $employer;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function isCommentValid(ExecutionContextInterface $context)
    {
        $comment = trim($this->getComment());
        if (!$this->isApproved() && empty($comment)) {
            $context->buildViolation('not_blank')
                                ->atPath('comment')
                                ->addViolation();
        }
    }
}
