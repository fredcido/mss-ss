<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Supplement
 *
 * @ORM\Table(name="supplement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SupplementRepository")
 * @UniqueEntity(fields="name", message="The name is already in use, choose another.")
 */
class Supplement
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    const SUPPLEMEMNT = 'S';
    const SUBSIDIE = 'D';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="IncidenceBase", mappedBy="supplement")
     */
    private $incidenceBasis;


    public function __construct()
    {
        $this->incidenceBasis = new ArrayCollection();
    }

    /**
     * Add incidenceBases
     *
     * @param IncidenceBase $incidenceBase
     * @return Race
     */
    public function addIncidenceBase(IncidenceBase $incidenceBase)
    {
        $this->incidenceBasis[] = $incidenceBase;
        return $this;
    }

    /**
     * Remove incidenceBase
     *
     * @param IncidenceBase $incidenceBase
     */
    public function removeIncidenceBase(IncidenceBase $incidenceBase)
    {
        $this->incidenceBasis->removeElement($incidenceBase);
    }

    /**
     * Get incidenceBases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIncidenceBasis()
    {
        return $this->incidenceBasis;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Supplement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Supplement
     */
    public function setType($type)
    {
        $validTypes = array(self::SUPPLEMEMNT, self::SUBSIDIE);
        if (!in_array($type, $validTypes)) {
            throw new \InvalidArgumentException(sprintf("Invalid type: %s provided for %s", $type, __CLASS__));
        }

        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add incidenceBasi
     *
     * @param \AppBundle\Entity\IncidenceBase $incidenceBasi
     *
     * @return Supplement
     */
    public function addIncidenceBasi(\AppBundle\Entity\IncidenceBase $incidenceBasi)
    {
        $this->incidenceBasis[] = $incidenceBasi;

        return $this;
    }

    /**
     * Remove incidenceBasi
     *
     * @param \AppBundle\Entity\IncidenceBase $incidenceBasi
     */
    public function removeIncidenceBasi(\AppBundle\Entity\IncidenceBase $incidenceBasi)
    {
        $this->incidenceBasis->removeElement($incidenceBasi);
    }
}
