app.service("NaturalityService", function($http, AppConstant, $translate) {

    var _populateModel = function(model) {
        var _model = angular.copy(model);

        if (_model.suku) {
            _model.municipio = _model.suku.postoAdministrativo.municipio;
            _model.postoAdministrativo = _model.suku.postoAdministrativo;
            _model.suku = _model.suku;
        }

        return _model;
    };

    var _populateModels = function(models) {
        models = models.map(function(m){
            return _populateModel(m);
        });

        return models;
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.suku) {
            data.suku = data.suku.id;
        }

        if (data.village) {
            data.village = data.village.id;
        }

        if (data.postoAdministrativo) {
            data.postoAdministrativo = data.postoAdministrativo.id;
        }

        if (data.municipio) {
            data.municipio = data.municipio.id;
        }

        if (data.country) {
            data.country = data.country.id;
        }

        return data;
    };

    var _preparateDatas = function(data) {
        data = data.map(function(d){
            return _preparateData(d);
        });

        return data;
    };

    return {
        populateModel: _populateModel,
        populateModels: _populateModels,
        preparateData: _preparateData,
        preparateDatas: _preparateDatas,
    };
});
