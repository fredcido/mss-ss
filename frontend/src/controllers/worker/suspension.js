app.controller(
    "WorkerSuspensionController", 
    function(
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $uibModal, 
        $translate,
        $q, 
        ViewHelper, 
        GenericService,
        AuthService,
        EnumService,
        EmployerService,
        PersonService,
        CompulsoryAccessionService
    ) {
        var _employerEndpoint = "employer";
        var _personEndpoint = "person";
        var _subscriptionEndpoint = 'compulsory-subscription';

        $scope.employerId = $stateParams.employer || null;
        $scope.id = $stateParams.id || null;
        
        $scope.employer = null;
        $scope.person = null;
        $scope.subsription = null;

        $scope.user = AuthService.getUserLogged();
        $scope.today = moment().toDate();
        $scope.calendarOptions = {
            maxDate: $scope.today
        };

        $scope.suspendedStatus = EnumService.getSubscriptionStatus('suspended');
        $scope.ceasedStatus = EnumService.getSubscriptionStatus('ceased');
        $scope.action = {status: $scope.suspendedStatus};

        $scope.actions = [
            {value: $scope.suspendedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.suspendedStatus)},
            {value: $scope.ceasedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.ceasedStatus)}
        ];

        var init = function() {
            // Make sure the user can only see its own employer
            
            if ($scope.user.isEmployer()) {
                $scope.employerId = $scope.user.employer.internalCode;
            }

            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            var version = EnumService.getVersion('standard');
            GenericService.getVersion(_employerEndpoint, $scope.employerId, version).then(function (response) {
                $scope.employer = EmployerService.populateModel(response.data);

                // the entity cannot be pending
                // it needs to be submitted first
                if ($scope.employer.isPending()) {
                    swal({ 
                        title: $translate.instant('company_not_submitted'),
                        text: $translate.instant('company_not_submitted_text'),
                        type: "error" 
                      },
                      function(){
                        $timeout(function(){
                            if (!$scope.user.isPerson()) {
                                $state.go('app.company-form', {
                                    id: $scope.employerId,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });
                            }
                        });
                    });
                }

                // if the company was not approved yet, does not allow 
                // to submit changes
                if ($scope.employer.isSubmitted()) {
                    swal({ 
                        title: $translate.instant('company_not_approved'),
                        text: $translate.instant('company_not_approved_text'),
                        type: "error" 
                      },
                      function(){
                        $timeout(function(){
                            /*if (!$scope.user.isPerson()) {
                                $state.go('app.company-form', {
                                    id: $scope.id,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });
                            }*/
                        });
                    });
                }

                var version = EnumService.getVersion('standard');
                GenericService.getVersion(_personEndpoint, $scope.id, version).then(function (response) {
                    $scope.person = PersonService.populateModel(response.data);

                    var url = _subscriptionEndpoint + '/person/' + $scope.person.id + '/employer/' + $scope.employer.id  + '/current';
                    GenericService.httpGet(url).then(function(response) {
                        $scope.subscription = CompulsoryAccessionService.populateSubscriptionModel(response.data);
                        $scope.subscription.dateEnd = moment().toDate();

                    }).finally(function(){
                        ViewHelper.unblockContainer('.container');
                    });
                });
            });
        };

        $scope.isSuspended = function() {
            return $scope.subscription 
                    && EnumService.getSubscriptionStatus('suspended') == $scope.subscription.subscriptionStatus;
        };

        $scope.isCeased = function() {
            return $scope.subscription && $scope.subscription.isCeased();
        };
        
        $scope.canSuspend = function() {
            return $scope.user.isEmployer() && $scope.employer.isApproved() && !$scope.isSuspended();
        };

        $scope.canActive = function() {
            return $scope.user.isEmployer() && $scope.employer.isApproved();
        };

        saveSuspension = function() {
            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            var subscriptionData = CompulsoryAccessionService.preparateSubscriptionData($scope.subscription);
            var url = _subscriptionEndpoint + '/' + subscriptionData.id + '/edit';

            GenericService.httpPost(url, subscriptionData).then(function(response){
                $scope.subscription = CompulsoryAccessionService.populateSubscriptionModel(response.data);
                ViewHelper.scrollTo('.inss-logo');

                swal({
                    title: $translate.instant('success_title'),
                    text: $translate.instant('success_text'),
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false
                });
            }).catch(function(response){
                $scope.subscription.subscriptionStatus = $scope.oldSubscriptionStatus;
                ViewHelper.errorServer(form, response);
            }).finally(function(){
                ViewHelper.unblockContainer('.container');
            });
        };

        $scope.suspend = function(form) {
            if (form.$valid) {

                if ($scope.subscription.isSuspended()) {
                    $scope.action.status = $scope.ceasedStatus;
                }

                $scope.oldSubscriptionStatus = $scope.subscription.subscriptionStatus;
                $scope.subscription.subscriptionStatus = $scope.action.status;

                saveSuspension();
                
            } else {
                ViewHelper.errorForm(form);
            }
        };

        $scope.activate = function() {
            $scope.oldSubscriptionStatus = $scope.subscription.subscriptionStatus;
            $scope.subscription.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');

            saveSuspension();
        };

        init();
    }
);