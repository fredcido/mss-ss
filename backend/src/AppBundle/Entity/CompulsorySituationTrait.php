<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * CompulsorySituationTrait
 */
trait CompulsorySituationTrait
{
    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer")
     * @JMS\Groups({"list", "details"})
     */
    private $employer;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="Sector")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $sector;

    /**
     * @var ContractType
     *
     * @ORM\ManyToOne(targetEntity="ContractType")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $contractType;

    /**
     * @var Regime
     *
     * @ORM\ManyToOne(targetEntity="Regime")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $regime;

    /**
     * @var LabourLaw
     *
     * @ORM\ManyToOne(targetEntity="LabourLaw")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $labourLaw;

    /**
     * @var ContractStatus
     *
     * @ORM\ManyToOne(targetEntity="ContractStatus")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $contractStatus;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Post")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $post;

    /**
     * @var Occupation
     *
     * @ORM\ManyToOne(targetEntity="Occupation")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $occupation;

    /**
     * @ORM\ManyToMany(targetEntity="ActivitySector")
     * @JMS\Groups({"list", "details"})
     * )
     */
    private $activitySectors;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employer
     *
     * @param Employer $employer
     *
     * @return CompulsorySituationTrait
     */
    public function setEmployer(Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Set sector
     *
     * @param Sector $sector
     *
     * @return CompulsorySituationTrait
     */
    public function setSector(Sector $sector = null)
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return Sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set contractType
     *
     * @param ContractType $contractType
     *
     * @return CompulsorySituationTrait
     */
    public function setContractType(ContractType $contractType = null)
    {
        $this->contractType = $contractType;

        return $this;
    }

    /**
     * Get contractType
     *
     * @return ContractType
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Set regime
     *
     * @param Regime $regime
     *
     * @return CompulsorySituationTrait
     */
    public function setRegime(Regime $regime = null)
    {
        $this->regime = $regime;

        return $this;
    }

    /**
     * Get regime
     *
     * @return Regime
     */
    public function getRegime()
    {
        return $this->regime;
    }

    /**
     * Set labourLaw
     *
     * @param LabourLaw $labourLaw
     *
     * @return CompulsorySituationTrait
     */
    public function setLabourLaw(LabourLaw $labourLaw = null)
    {
        $this->labourLaw = $labourLaw;

        return $this;
    }

    /**
     * Get labourLaw
     *
     * @return LabourLaw
     */
    public function getLabourLaw()
    {
        return $this->labourLaw;
    }

    /**
     * Set contractStatus
     *
     * @param ContractStatus $contractStatus
     *
     * @return CompulsorySituationTrait
     */
    public function setContractStatus(ContractStatus $contractStatus = null)
    {
        $this->contractStatus = $contractStatus;

        return $this;
    }

    /**
     * Get contractStatus
     *
     * @return ContractStatus
     */
    public function getContractStatus()
    {
        return $this->contractStatus;
    }

    /**
     * Gets the value of post.
     *
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Sets the value of post.
     *
     * @param Post $post the post
     *
     * @return self
     */
    public function setPost(Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Gets the value of occupation.
     *
     * @return Occupation
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Sets the value of occupation.
     *
     * @param Occupation $occupation the occupation
     *
     * @return self
     */
    public function setOccupation(Occupation $occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Add activitySector
     *
     * @param ActivitySector $activitySector
     *
     * @return Employer
     */
    public function addActivitySector(ActivitySector $activitySector)
    {
        $this->activitySectors[] = $activitySector;

        return $this;
    }

    /**
     * Remove activitySector
     *
     * @param ActivitySector $activitySector
     */
    public function removeActivitySector(ActivitySector $activitySector)
    {
        $this->activitySectors->removeElement($activitySector);
    }

    /**
     * Get activitySectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivitySectors()
    {
        return $this->activitySectors;
    }
}
