<?php

namespace AppBundle\Controller;

use AppBundle\Api\ApiProblem;
use AppBundle\Api\ApiProblemException;
use AppBundle\Enum;
use AppBundle\Security\Actions;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends Controller
{
    protected function createApiResponse($data, $statusCode = Response::HTTP_OK, $groups = array('list'))
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true)->setGroups($groups);

        $serializer  = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($data, 'json', $context);

        return new Response(
            $jsonContent,
            $statusCode,
            array(
                'Content-Type' => 'application/json',
            )
        );
    }

    protected function createValidationErrorResponse(FormInterface $form)
    {
        $formJsonErrors = $this->get('app.forms.errors_json');
        $errors         = $formJsonErrors->getErrors($form);
        $data           = [
            'type'   => 'validation_error',
            'title'  => 'There was a validation error',
            'errors' => $errors,
        ];
        return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
    }

    protected function throwApiProblemValidationException(FormInterface $form)
    {
        $formJsonErrors = $this->get('app.forms.errors_json');
        $errors         = $formJsonErrors->getErrors($form);

        $apiProblem = new ApiProblem(
            Response::HTTP_BAD_REQUEST,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        $apiProblem->set('errors', $errors);

        throw new ApiProblemException($apiProblem);
    }

    protected function getOrderBy()
    {
        return array('name' => 'asc');
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function findAction(Request $request)
    {
        $repository = $this->getRepository();

        $this->denyAccessUnlessGranted(Actions::VIEW, $repository->getClassName());

        if ($request->query->get('all')) {
             $entities = $repository->findBy(array(), $this->getOrderBy());
             $groups = array('list', 'audit');
        } else {
            
            if (!$this->getParameter('use_cache') || !$this->has('cache')) {
                $entities = $repository->findByStatus(Enum\Status::ACTIVE, $this->getOrderBy());
            } else {
                $cache = $this->get('cache');
                if ($cache->contains($this->repository)) {
                    $entities = $cache->fetch($this->repository);
                } else {
                    $entities = $repository->findByStatus(Enum\Status::ACTIVE, $this->getOrderBy());
                    $cache->save($this->repository, $entities);
                }
            }

            $groups = array('form');
        }

        $entities = $this->handleFindEntities($entities);
        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }

    protected function handleFindEntities($entities)
    {
        return $entities;
    }

    public function getAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entity = $repository->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        $this->denyAccessUnlessGranted(Actions::VIEW, $entity);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('details');
        }

        return $this->createApiResponse($entity, Response::HTTP_OK, array('details'));
    }

    protected function handleForm($entity, Request $request, $statusCode = Response::HTTP_OK)
    {
        $form = $this->createForm(
            $this->form,
            $entity
        );

        //var_dump($request->request->all());exit;
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        //dump($entity);exit;

        $entity = $this->persistEntity($entity);
        $response = $this->createApiResponse($entity, $statusCode, array('details'));
        
        // Remove the cached data
        if ($this->getParameter('use_cache') && $this->has('cache')) {
            $cache = $this->get('cache');
            $cache->delete($this->repository);
        }

        return $response;
    }

    protected function persistEntity($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $entity;
    }

    public function addAction(Request $request)
    {
        $entity = new $this->entity();
        $this->denyAccessUnlessGranted(Actions::CREATE, $entity);

        return $this->handleForm($entity, $request, Response::HTTP_CREATED);
    }

    public function editAction($id, Request $request)
    {
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        $this->denyAccessUnlessGranted(Actions::EDIT, $entity);

        return $this->handleForm($entity, $request);
    }

    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $repository = $this->getRepository();

        $em->getConnection()->beginTransaction();
        try {
            foreach ($data['items'] as $id) {
                $entity = $this->getRepository()->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find entity');
                }

                $this->denyAccessUnlessGranted(Actions::DELETE, $entity);

                $entity->setStatus(0)->setJustification($data['justification']);
                $em->persist($entity);
            }

            // Remove the cached data
            $cache = $this->get('cache');
            $cache->delete($this->repository);

            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $em->clear();
            throw $e;
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    public function activeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $repository = $this->getRepository();
        $this->denyAccessUnlessGranted(Actions::EDIT, $repository->getClassName());

        $em->getConnection()->beginTransaction();
        try {
            foreach ($data['items'] as $id) {
                $entity = $this->getRepository()->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find entity');
                }

                $entity->setStatus(1);
                $em->persist($entity);
            }

            // Remove the cached data
            $cache = $this->get('cache');
            $cache->delete($this->repository);

            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();
            $em->clear();
            throw $e;
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    public function searchAction(Request $request)
    {
        $repository = $this->getRepository();
        $this->denyAccessUnlessGranted(Actions::VIEW, $repository->getClassName());

        $filters = $request->query->all();
        $entities = $repository->findByFilters($filters);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('list'));
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function treeAction(Request $request)
    {
        $repository = $this->getRepository();
        $this->denyAccessUnlessGranted(Actions::VIEW, $repository->getClassName());

        $where = [];
        if ($request->query->get('all')) {
             $groups = array('list', 'audit');
        } else {
            $where['status'] = Enum\Status::ACTIVE;
            $groups = array('form');
        }

        $orderBy = array_merge(['parent' => 'DESC'], $this->getOrderBy());
        $entities = $repository->findBy($where, $orderBy);
        $tree = $this->get('app.tree')->nestify($entities);

        return $this->createApiResponse($tree, Response::HTTP_OK, $groups);
    }

    protected function getRepository()
    {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository($this->repository);
    }
}
