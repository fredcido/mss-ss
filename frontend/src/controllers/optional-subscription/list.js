app.controller("OptionalSubscriptionListController", 
    function(
        $scope, 
        $translate, 
        $state, 
        $stateParams, 
        EnumService,
        AuthService,
        GenericService,
        ViewHelper
    ) {

    $scope.suspend = parseInt($stateParams.suspend);
    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.user = AuthService.getUserLogged();

    $scope.actionsGrid = [{
        title: $scope.user.isPerson() ? $translate.instant('edit') : $translate.instant('view'),
        icon: $scope.user.isPerson() ? 'fa-pencil' : 'fa-eye',
        callback: function(subscription) {
            if ($scope.suspend) {
                $state.go(
                    'app.optional-subscription-suspension', 
                    {
                        id: subscription.person.internalCode,
                    }
                );
            } else {
                $state.go(
                    'app.optional-subscription-form', 
                    {
                        id: subscription.person.internalCode,
                        updateSubscription: $scope.updateSubscription
                    }
                );
            }
        }
    }];

    if (!$scope.suspend) {
        var viewChangesButton = {
            title: $translate.instant('history'),
            icon: 'fa-history',
            callback: function(subscription) {
                $state.go(
                    'app.optional-subscription-list-changes', 
                    {
                        id: subscription.person.internalCode
                    }
                );
            }
        };
        $scope.actionsGrid.push(viewChangesButton);
    }

    var params = {
        version: EnumService.getVersion('standard'),
        all_status: 1
    };

    if ($scope.suspend) {
        params.hadSubscriptionStatus = [
            EnumService.getSubscriptionStatus('suspended'),
            EnumService.getSubscriptionStatus('ceased')
        ];
    } else if ($scope.updateSubscription) {
        params.notSubscriptionStatus = [
            EnumService.getSubscriptionStatus('pending')
        ];
    }

    if ($scope.user.isPerson()) {
        params.internalCode = $scope.user.person.internalCode;
    }

    search = function() {
        var url = 'optional-subscription/search';
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));
        GenericService.httpGet(url, params).then(function(response){
            $scope.rows = response.data;
            ViewHelper.unblockContainer('.panel');
        });
    };

    search();
});
