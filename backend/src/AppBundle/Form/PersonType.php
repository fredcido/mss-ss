<?php

namespace AppBundle\Form;

use AppBundle\Form\AddressType;
use AppBundle\Form\BankInformationType;
use AppBundle\Form\ContactDetailType;
use AppBundle\Form\NaturalityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('dob', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('dod', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('gender')
            ->add('files')
            ->add('taxNumber')
            ->add('identityCode')
            ->add('electoralCard')
            ->add('birthCertificate')
            ->add('numRef')
            ->add('description')
            ->add('contactDetail')
            ->add('disability')
            ->add('version')
            ->add('maritalStatus');

        $builder->add(
            'address',
            AddressType::class,
            array(
                'required'       => false,
                'error_bubbling' => false
            )
        );

        $builder->add(
            'naturality',
            NaturalityType::class,
            array(
                'required'       => false,
                'error_bubbling' => false
            )
        );

        $builder->add(
            'nationalities',
            EntityType::class,
            array(
                'class'          => 'AppBundle:Country',
                'multiple'       => true,
                'error_bubbling' => false,
            )
        );

        $builder->add(
            'bankInformation',
            CollectionType::class,
            array(
                'entry_type'   => BankInformationType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );

        /*$builder->add(
            'relatives',
            CollectionType::class,
            array(
                'entry_type'   => RelativeType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
            )
        );
        $builder->add(
            'publicWorker',
            PublicWorkerType::class,
            array('error_bubbling' => false)
        );*/

        $builder->add(
            'contactDetail',
            ContactDetailType::class,
            array('error_bubbling' => false)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Person',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_person';
    }
}
