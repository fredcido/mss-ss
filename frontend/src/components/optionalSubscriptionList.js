app.component('optionalSubscriptionList', {
    templateUrl: 'views/components/optionalSubscriptionList.html',
    bindings: {
        rows: '<',
        actions: '<'
    },
    controller: function($translate, $timeout, $uibModal, GenericService, ViewHelper, EnumService) {
        var self = this;
        var checkVersion = new RegExp('_' + EnumService.getVersion('subscription') + '$');

        this.$onChanges = function (changesObj) {
            if (changesObj.rows && changesObj.rows.currentValue) {
                
                ViewHelper.blockContainer('.optional-subscription-list', $translate.instant('loading'));

                self.rows = changesObj.rows.currentValue;

                self.rows.map(function(subscription){
                    subscription.person = subscription.optionalAccession.person;

                    subscription.isVersionStandard = self.isVersionStandard(subscription);
                    subscription.isVersionChange = self.isVersionChange(subscription);
                    subscription.isSubscription = self.isSubscription(subscription);

                    return subscription;
                });

                self.rowCollection = self.rows;
                self.displayCollection = self.rows;
                
                ViewHelper.unblockContainer('.optional-subscription-list');
            }
        };

        self.isVersionStandard = function(row) {
            return EnumService.getVersion('standard') == row.version;
        };

        self.isSubscription = function(row) {
            return !self.isVersionStandard(row) && checkVersion.exec(row.version);
        };

        self.isVersionChange = function(row) {
            return !self.isVersionStandard(row) && !checkVersion.exec(row.version);
        };
    }
});
