<?php

namespace AppBundle\Entity;

interface PersonAwareInterface
{
    public function getPerson();
}
