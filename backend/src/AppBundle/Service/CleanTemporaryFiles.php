<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class CleanTemporaryFiles
{
    use ContainerAwareTrait;

    public function clean()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $uploader = $this->container->get('app.uploader');
        $repository = $em->getRepository('AppBundle:File');

        $qb = $repository->createQueryBuilder('f');

        $now = new \DateTime('now');
        $threeDaysAgo = $now->sub(new \DateInterval("P3D"));

        $qb->where($qb->expr()->lte('f.created_at', ':three_days'))
                    ->andWhere('f.temporary = :temporary')
                    ->andWhere('f.entity IS NULL')
                    ->setParameter('temporary', 1)
                    ->setParameter('three_days', $threeDaysAgo);

        $temporaryFiles = $qb->getQuery()->getResult();

        foreach ($temporaryFiles as $result) {
            $uploader->delete($result);
            $em->remove($result);
        }

        $em->flush();

        return $temporaryFiles;
    }
}
