<?php

namespace AppBundle\Form;

use AppBundle\Form\DataTransformer\EntityEmbeddedTransformer;
use AppBundle\Form\EmployerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    protected $em;

    protected $employer;

    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email')
            ->add('username')
            ->add('enabled')
            ->add('locale')
            //->add('employer')
            ->add('person')
            ->add('roles')
            ->add(
                'plainPassword',
                RepeatedType::class,
                array(
                    'type' => PasswordType::class,
                )
            );

        $employerEntity = null;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use (&$employerEntity) {
            $user = $event->getData();
            $form = $event->getForm();

            if (!$user || empty($user['employer'])) {
                return;
            }

            $employer = $user['employer'];
            if (is_array($employer)) {
                $form->add(
                    'employer',
                    EmployerType::class,
                    array(
                        'required'          => false,
                        'error_bubbling'    => false,
                        'validation_groups' => ['User']
                    )
                );
            } else {
                $form->add('employer');
            }

            //$employerEntity = $this->em->getRepository('AppBundle:Employer')->find($employer['id']);
            //$this->em->persist($employerEntity);
        });

        /*$builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use (&$employerEntity) {
            $user = $event->getData();

            if (empty($employerEntity)) {
                return;
            }

            $user->setEmployer($employerEntity);
            $event->setData($user);
        });*/
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\User',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_telephone';
    }
}
