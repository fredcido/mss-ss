app.controller("ConventionFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'convention';
    var _stateList = 'app.convention-list';

    $scope.id = $stateParams.id || null;
    $scope.model = {
        status: 1
    };

    var _preparateData = function(data){
        var _data = angular.copy(data);
        _data.country = _data.country.id;

        return _data;
    };

    var _getCountries = function() {
        GenericService.find('country').then(function(response) {
            $scope.countries = response.data;
        });
    };

    _getCountries();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;

            if (response.country) {
                $scope.model.country = response.country.id;
            }
        });
    }

    // Add
    $scope.add = function(data) {
        var dataP = _preparateData(data);
        GenericService.add(_endpoint, dataP, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        var dataP = _preparateData(data);
        GenericService.edit(_endpoint, id, dataP, _stateList);
    };
});
