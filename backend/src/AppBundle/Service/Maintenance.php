<?php

namespace AppBundle\Service;

class Maintenance
{
    protected $file = 'mss_siss';

    protected function getFileName()
    {
        $tmpFolder = sys_get_temp_dir();
        $file = $tmpFolder . DIRECTORY_SEPARATOR . $this->file;

        return $file;
    }

    public function activate()
    {
        $file = $this->getFileName();
        return !!file_put_contents($file, 'mss_iss');
    }

    public function deactivate()
    {
        $file = $this->getFileName();
        if ($this->isOn()) {
            return !unlink($file);
        } else {
            return false;
        }
    }

    public function isOn()
    {
        $file = $this->getFileName();
        return file_exists($file);
    }
}
