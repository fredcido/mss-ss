<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Calendar;
use AppBundle\Enum;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class CalendarController extends BaseController
{
    protected $repository = 'AppBundle:Calendar';

    protected $form = 'AppBundle\Form\CalendarType';

    protected $entity = Calendar::class;

    protected function getOrderBy()
    {
        return array('day' => 'asc');
    }

    public function removeAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity');
        }

        $entity->setStatus(Enum\Status::INACTIVE);
        $em->persist($entity);
        $em->flush();

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function workDaysAction($dateStart, $dateEnd, Request $request)
    {
        $workDays = $this->get('app.calendar')->workDays($dateStart, $dateEnd);
        return $this->createApiResponse(['days' => $workDays]);
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function addWorkDaysAction($dateStart, $days, Request $request)
    {
        $dateEnd = $this->get('app.calendar')->addWorkDays($dateStart, $days);
        return $this->createApiResponse(['day' => $dateEnd->format('Y-m-d')]);
    }
}
