app.component('relatives', {
    templateUrl: 'views/components/relatives.html',
    bindings: {
        person: '=',
        disabled: '<'
    },
    controller: function($uibModal, $translate, $timeout, GenericService, ViewHelper) {
        var self = this;
        var listedRelatives = true;

        var setRelative = function(relative) {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'relative',
                resolve: {
                    person: self.person,
                    relative: relative
                }
            });

            modalInstance.result.then(function (relative) {
                if (relative) {
                    addRelative(relative);
                }
            }, function () {

            });
        };

        var hasRelativeAlready = function(person) {
            return self.relatives.some(function(relative) {
                if (relative.relative.id == person.id) {
                    return true;
                }
            });
        };

        var checkHasRelative = function(person) {
            if (hasRelativeAlready(person)) {
                swal($translate.instant('relative_already_added'), $translate.instant('relative_already_added'), 'error');
                return false;
            } else {
                return true;
            }
        };

        var addRelative = function(relative) {
            if (checkHasRelative(relative.person)) {
                $timeout(
                    function() {
                        self.relatives.push(reverseRelative(relative));
                    },
                    500
                )
            }
        };

        var replaceRelative = function(relative) {
            $timeout(
                function() {
                    self.relatives.some(function(r){
                        if (r.id == relative.id) {
                            r = relative;
                            return true;
                        }
                    });
                },
                500
            )
        };

        var reverseRelative = function(relative) {
            relative.reverse = relative.person.internalCode != self.person.internalCode;

            if (relative.reverse) {
                relative.display = relative.person;
            } else {
                relative.display = relative.relative;
            }

            return relative;
        };

        var listRelatives = function() {
            if (!self.person) {
                return;
            }

            self.listedRelatives = true;
            ViewHelper.blockContainer('.btn-relative, .table-relatives', $translate.instant('loading'));
            var url = 'relative/' + self.person.id + '/find?version=' + self.person.version;
            GenericService.httpGet(url).then(function(response) {
                self.relatives = response.data;
                self.relatives.map(function(relative){
                    return reverseRelative(relative);
                });

                ViewHelper.unblockContainer('.btn-relative, .table-relatives');
            });
        };

        self.searchExisting = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'personSearchDialog',
                resolve: {
                    notId: function(){
                        var relativeId = self.relatives.map(function(r) {
                            return r.relative.id;
                        });

                        var personId = self.relatives.map(function(r) {
                            return r.person.id;
                        });

                        return relativeId.concat(personId).concat([self.person.id]);
                    }
                }
            });

            modalInstance.result.then(function (person) {
                if (person && checkHasRelative(person)) {
                    setRelative(person);
                }
            }, function () {

            });
        };

        self.registerNew = function() {
            setRelative(null);
        };

        self.editRelative = function(relative) {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'relative',
                resolve: {
                    model: relative
                }
            });

            modalInstance.result.then(function (relative) {
                if (relative) {
                    replaceRelative(relative);
                }
            }, function () {

            });
        };

        self.removeRelative = function(relative) {
            swal({
                    title: $translate.instant('confirm'),
                    text: $translate.instant('confirm'),
                    type: 'warning',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (!isConfirm) return false;

                    GenericService.delete('relative', relative.id).then(function(response) {
                        swal({
                            title: $translate.instant('success_title'),
                            text: $translate.instant('success_text'),
                            type: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });

                        listRelatives();
                    });
                }
            );
        };

        self.$onInit = function() {
            self.relatives = [];
            //listRelatives();
        };

        self.$onChanges = function() {
            if (!self.listedRelatives) {
                listRelatives();
            }
        };
    }
});
