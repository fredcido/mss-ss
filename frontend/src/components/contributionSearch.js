app.component('contributionSearch', {
    templateUrl: 'views/components/contributionSearch.html',
    bindings: {
        actions: "=",
        deleteContributions: "="
    },
    controller: function($translate, GenericService, ViewHelper) {
        var self = this;
        var _uri = 'contribution/search';

        self.searchForm = false;
        self.calendar = {
            month: false,
            year: false,
            dateStart: false,
            dateEnd: false
        };

        self.yearOptions = {
            datepickerMode: "year",
            minMode: "year",
            maxDate: new Date(),
            showWeeks: "false",
        };

        self.monthOptions = {
            datepickerMode: "month",
            minMode: "month",
            maxDate: new Date(),
            showWeeks: "false",
        };

        self.displayCollection = [];
        self.rowCollection = [];
        self.contractsStatus = [];

        self.openCalendar = function(calendar) {
            self.calendar[calendar] = true;
        };

        var _search = function(params) {
            ViewHelper.blockContainer('.tableContribution', $translate.instant('loading'));

            GenericService.httpGet(_uri, params).then(function(response) {
                self.rowCollection = response.data.map(function(i) {
                    i.month = moment(i.dateStart).format('MMMM');
                    i.year = moment(i.dateStart).format('YYYY');
                    return i;
                });
                self.displayCollection = self.rowCollection;

                ViewHelper.unblockContainer('.tableContribution');
            });
        };

        self.reload = function(params) {
            var _params = angular.copy(params) || null;
            if (_params) {
                if (_params.month) {
                    _params.month = moment(_params.month).format('MM');
                }

                if (_params.year) {
                    _params.year = moment(_params.year).format('YYYY');
                }

                if (_params.dateStart) {
                    _params.dateStart = moment(_params.dateStart).format('YYYY-MM-DD');
                }

                if (_params.dateEnd) {
                    _params.dateEnd = moment(_params.dateEnd).format('YYYY-MM-DD');
                }


                if (_params.contractStatus) {
                    _params.contractStatus = _params.contractStatus.id;
                }
            }

            _search(_params);
        };

        self.checkAll = function() {
            if (self.selectedAll) {
                self.selectedAll = true;
            } else {
                self.selectedAll = false;
            }

            angular.forEach(self.displayCollection, function(item) {
                item.selected = self.selectedAll;
            });
        };

        var _getContractStatus = function() {
            GenericService.find('contract-status').then(function(response) {
                self.contractsStatus = response.data;
            });
        };

        self.activateContributions = function() {
            var activeItems = [];
            angular.forEach(self.displayCollection, function(item) {
                if (item.selected && !item.status) {
                    activeItems.push(item.id);
                }
            });

            if (!activeItems.length) {
                swal($translate.instant('no_items_selected'), $translate.instant('no_items_selected'));
                return false;
            }

            swal({
                    title: $translate.instant('active_contributions'),
                    text: $translate.instant('confirm_active_contributions'),
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (!isConfirm) return false;

                    GenericService.activateMany('contribution', activeItems).then(
                        function(response) {
                            angular.forEach(
                                self.displayCollection,
                                function(item) {
                                    if (activeItems.indexOf(item.id) > -1 && item.selected && !item.status) {
                                        item.status = 1;
                                        item.selected = false;
                                    }
                                }
                            );

                            swal({
                                title: $translate.instant('success_title'),
                                text: $translate.instant('success_text'),
                                type: 'success',
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    );
                });
        };

        self.removeContributions = function() {
            var deleteItems = [];
            angular.forEach(self.displayCollection, function(item) {
                if (item.selected && item.status) {
                    deleteItems.push(item.id);
                }
            });

            if (!deleteItems.length) {
                swal($translate.instant('no_items_selected'), $translate.instant('no_items_selected'));
                return false;
            }

            swal({
                    title: $translate.instant('remove_contributions'),
                    text: $translate.instant('confirm_remove_contributions'),
                    type: "input",
                    animation: "slide-from-top",
                    inputPlaceholder: $translate.instant('justification'),
                    showCancelButton: true,
                    closeOnConfirm: false
                },
                function(inputValue) {
                    if (inputValue === false) return false;

                    if (inputValue === "") {
                        swal.showInputError($translate.instant('required_message'));
                        return false;
                    }

                    GenericService.deleteMany('contribution', deleteItems, inputValue).then(
                        function(response) {
                            angular.forEach(
                                self.displayCollection,
                                function(item) {
                                    if (deleteItems.indexOf(item.id) > -1 && item.selected && item.status) {
                                        item.status = 0;
                                        item.selected = false;
                                    }
                                }
                            );

                            swal({
                                title: $translate.instant('success_title'),
                                text: $translate.instant('success_text'),
                                type: 'success',
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    );
                });
        };

        _search();
        _getContractStatus();
    }
});
