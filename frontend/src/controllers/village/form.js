app.controller("VillageFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'village';
    var _stateList = 'app.village-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.municipios = [];
    $scope.postosAdministrativos = [];
    $scope.sukus = [];

    $scope.$watch('model.municipio', function() {
        if ($scope.model.municipio) {
            _getPostosAdmnistrativos($scope.model.municipio);
        } else {
            $scope.postosAdministrativos = [];            
        }
    });

    $scope.$watch('model.postoAdministrativo', function() {
        if ($scope.model.postoAdministrativo) {
            _getSukus($scope.model.postoAdministrativo);
        } else {
            $scope.sukus = [];            
        }
    });

    var _getMunicipios = function() {
        GenericService.find('municipio').then(function(response) {
            $scope.municipios = response.data;
        });
    };

    var _getPostosAdmnistrativos = function(municipioId) {
        GenericService.httpGet('posto-administrativo/municipio/' + municipioId).then(function(response) {
            $scope.postosAdministrativos = response.data;
        });
    };

    var _getSukus = function(postoAdministrativoId) {
        GenericService.httpGet('suku/posto-administrativo/' + postoAdministrativoId).then(function(response) {
            $scope.sukus = response.data;
        });
    };

    _getMunicipios();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;

            $scope.model.municipio = $scope.model.suku.postoAdministrativo.municipio.id;
            $scope.model.postoAdministrativo = $scope.model.suku.postoAdministrativo.id;
            $scope.model.suku = $scope.model.suku.id;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
