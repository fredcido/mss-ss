<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Overtime
 *
 * @ORM\Table(name="overtime")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OvertimeRepository")
 * @UniqueEntity(fields="ratio", message="This value is already in use, choose other.")
 */
class Overtime
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ratio", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $ratio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ratio
     *
     * @param string $ratio
     *
     * @return Overtime
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return string
     */
    public function getRatio()
    {
        return $this->ratio;
    }
}
