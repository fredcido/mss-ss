app.controller("WorkerFormController", 
    function(
        $scope, 
        $timeout, 
        $state, 
        $q, 
        $stateParams, 
        $translate,
        AuthService,
        ViewHelper,  
        GenericService, 
        PersonService, 
        EmployerService, 
        CompulsoryAccessionService, 
        EnumService,
        PublicWorkerService,
        SettingService
    ) {

    $scope.id = $stateParams.id || null;
    $scope.employerId = $stateParams.employer || null;
    $scope.user = AuthService.getUserLogged();
    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.fromCompany = parseInt($stateParams.fromCompany) ? 1 : 0;
    $scope.version = $stateParams.version;
    $scope.personSaved = false;

    $scope.kinships = [];
    $scope.disabilities = [];
    $scope.nationalities = [];
    $scope.maritalStatuses = [];
    $scope.employeesSubscribed = [];
    $scope.employeesSubscribedAction = [{
        title: $translate.instant('view'),
        icon: 'fa-eye',
        callback: function(subscription) {
            $state.go('app.worker-edit', {
                employer: $scope.employer.internalCode,
                id: subscription.person.internalCode,
                updateSubscription: $scope.updateSubscription
            });
        }
    }];
    
    $scope.employer = null;
    $scope.person = null;
    $scope.accession = null;
    $scope.subscription = null;
    $scope.accessionAbroad = null;
    $scope.publicWorker = null;
    $scope.updateDeadline = null;
    $scope.optionalSubscription = null;

    $scope.searchPerson = false;
    $scope.loadingData = false;

    _personEndpoint = 'person';
    _acessionEndpoint = 'compulsory-accession';
    _subscriptionEndpoint = 'compulsory-subscription';
    _publicWorkerEndpoint = 'public-worker';
    _accessionAbroadEndpoint = 'accession-abroad';

    $scope.currentSteps = ['info'];

    var releaseSteps = function() {
        $timeout(function(){
            $scope.currentSteps = ['info'];

            if ($scope.person.id) {
                $scope.currentSteps.push('relative');
                $scope.currentSteps.push('employer');

                if ($scope.employer) {
                    $scope.currentSteps.push('contract');
                }

                if ($scope.subscription && $scope.subscription.id) {
                    $scope.currentSteps.push('abroad');
                    $scope.currentSteps.push('public_work');
                    $scope.currentSteps.push('files');
                }
            }
        });
    };

    var mixPersonModel = function(source, target) {
        var model = PersonService.populateModel(source);
        return angular.extend(target, model);
    };

    var afterSaveInformation = function(data) {
        $scope.id = data.internalCode;
        $scope.personSaved = true;

        initPerson($scope.id).then(function(){
            $timeout(function() {
                ViewHelper.scrollTo('.relative-container');
            }, 1000);
        });
    };

    var getDisabilities = function() {
        GenericService.find('disability').then(function(response) {
            $scope.disabilities = response.data;
        });
    };

    var getMaritalStatuses = function() {
        GenericService.find('marital-status').then(function(response) {
            $scope.maritalStatuses = response.data;
        });
    };

    var getNationalities = function() {
        GenericService.find('country').then(function(response) {
            $scope.nationalities = response.data;
        });
    };

    var callBackErrorSubmit = function(form) {
        return function(response) {
            ViewHelper.errorServer(form, response);
            ViewHelper.unblockContainer('.container');
        }
    };

    var saveAccession = function() {
        var data = angular.copy($scope.accession);
        data.person = $scope.person;
        var accession = CompulsoryAccessionService.preparateData(data);

        return GenericService.add(_acessionEndpoint, accession, undefined, undefined, true);
    };

    var saveSubscription = function(form) {
        var data = angular.copy($scope.subscription);
        data.compulsoryAccession = $scope.accession;
        var subscription = CompulsoryAccessionService.preparateSubscriptionData(data);

        var promise;
        if (subscription.id) {
            promise = GenericService.edit(_subscriptionEndpoint, subscription.id, subscription, undefined, undefined);
        } else {
            promise = GenericService.add(_subscriptionEndpoint, subscription, undefined, undefined);
        }

        promise.then(function(model){
            $scope.subscription = CompulsoryAccessionService.populateSubscriptionModel(model);
            $scope.person = PersonService.populateModel(model.compulsoryAccession.person);
            $scope.accession.person = $scope.person;

            releaseSteps();

            if (!subscription.id) {
                $timeout(function() {
                    ViewHelper.scrollTo('.abroad-container');
                }, 1000);    
            }

            ViewHelper.unblockContainer('.container');
            
        }, callBackErrorSubmit(form));
    };

    var initEmployer = function(id) {
        ViewHelper.blockContainer('.container', $translate.instant('loading'));

        var version = EnumService.getVersion('standard');
        return GenericService.getVersion('employer', id, version).then(function (response) {
            $scope.employer = EmployerService.populateModel(response.data);

            if (!$scope.user.isPerson()) {
                var invalidStatuses = [
                    EnumService.getSubscriptionStatus('pending'),
                    EnumService.getSubscriptionStatus('error')
                ];

                // The employer cannot be either pending or with error
                // it needs to be submitted first
                if ($scope.employer.isOfAnyStatus(invalidStatuses)) {
                    swal({ 
                        title: $translate.instant('company_not_submitted'),
                        text: $translate.instant('company_not_submitted_text'),
                        type: "error" 
                      },
                      function(){
                        $timeout(function(){
                            if (!$scope.user.isPerson()) {
                                $state.go('app.company-form', {
                                    id: $scope.employer.internalCode,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });
                            }
                        });
                    });
                }

                // if the company was not approved yet, does not allow 
                // to submit changes
                if ($scope.updateSubscription && $scope.employer.isSubmitted()) {
                    swal({ 
                        title: $translate.instant('company_not_approved'),
                        text: $translate.instant('company_not_approved_text'),
                        type: "error" 
                      },
                      function(){
                        $timeout(function(){
                            if (!$scope.user.isPerson()) {
                                $state.go('app.company-form', {
                                    id: $scope.id,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });
                            }
                        });
                    });
                }
            }

            ViewHelper.unblockContainer('.container');
            getEmployeesSubscribed(id);

            return $q.when(response);
        });
    };

    var getEmployeesSubscribed = function(id) {
        ViewHelper.blockContainer('.panel-employees-subscribed', $translate.instant('loading'));

        var uri = 'compulsory-subscription/search';
        var filters = {
            employer: $scope.employer.id,
            version: EnumService.getVersion('standard'),
            all_status: 1
        };

        GenericService.httpGet(uri, filters).then(function(response){
            $scope.employeesSubscribed = response.data;
            ViewHelper.unblockContainer('.panel-employees-subscribed');

            if (!$scope.id) {
                $timeout(function(){
                    ViewHelper.scrollTo('.identification-container');
                }, 500);
            }
        });
    };

    var getVersionByEmployer = function(version) {
        var version = version || EnumService.getVersion('subscription');
        return $scope.employer.internalCode + '_' + version;
    };

    var checkOptionalSybscription = function() {
        var optionalUrl = 'optional-subscription/person/' + $scope.person.id + '/current';

        // Search for option subscription
        GenericService.httpGet(optionalUrl).then(
            function(response){
                $scope.optionalSubscription = response.data;
                
                swal({ 
                    title: $translate.instant('alert'),
                    text: $translate.instant('worker_optional_subscribed'),
                    type: "warning" 
                  });
            }
        );   
    };

    var setPersonCallback = function(response) {
        $scope.person = PersonService.populateModel(response.data);

        if (!$scope.updateSubscription) {
            checkOptionalSybscription();
        }

        releaseSteps();
                
        getAccession();
        getAccessionAbroad();
        getPublicWorker();

        $timeout(function(){
            ViewHelper.scrollTo('.identification-container');
        }, 500);

        ViewHelper.unblockContainer('.container');
    };

    var initPerson = function(id) {
        ViewHelper.blockContainer('.container', $translate.instant('loading'));

        if ($scope.version) {
            return GenericService.get(_personEndpoint, $scope.version).then(setPersonCallback)
        } else {
            var version = EnumService.getVersion('standard');
            if (!$scope.updateSubscription) {
                version = getVersionByEmployer();
            }

            return GenericService.getVersion(_personEndpoint, id, version)
                .then(setPersonCallback)
                .catch(function(error){
                    if (404 != error.status) return;
                    return GenericService.getVersion(_personEndpoint, id, EnumService.getVersion('standard'))
                                    .then(setPersonCallback);
                });
        }
    };

    var initSubscription = function() {
        $scope.subscription = CompulsoryAccessionService.initSubscriptionModel();
        $scope.subscription.workSituation.person = $scope.person;
        $scope.subscription.workSituation.employer = $scope.employer;
    };

    var getAccession = function() {
        ViewHelper.blockContainer('form', $translate.instant('loading'));
        var url = _acessionEndpoint + '/person/' + $scope.person.id + '/current';

        GenericService.httpGet(url).then(function(response) {
            $scope.accession = CompulsoryAccessionService.populateModel(response.data);
            getSubscription();
        }, function(){
            $scope.accession = CompulsoryAccessionService.initModel();
            initSubscription();
            ViewHelper.unblockContainer('form');
        });
    };

    var getSubscription = function() {
        if ($scope.version) {
            var url = _subscriptionEndpoint + '/person/' + $scope.person.id + '/employer/' + $scope.employer.id  + '/latest';
        } else {
            var url = _subscriptionEndpoint + '/person/' + $scope.person.id + '/employer/' + $scope.employer.id  + '/current';
        }

        GenericService.httpGet(url).then(function(response) {
            $scope.subscription = CompulsoryAccessionService.populateSubscriptionModel(response.data);
            
            releaseSteps();
            ViewHelper.unblockContainer('form');

            if (!$scope.version && $scope.updateSubscription && $scope.user.isEmployer() && $scope.canEdit()) {
                ViewHelper.scrollTo('.contract-container');
            }
        }, function(){
            initSubscription();
            ViewHelper.unblockContainer('form');

            if ($scope.updateSubscription) {
                ViewHelper.scrollTo('.contract-container');
            }
        });
    };

    var getAccessionAbroad = function() {
        ViewHelper.blockContainer('.abroad-container', $translate.instant('loading'));
        var url = _accessionAbroadEndpoint + '/person/' + $scope.person.id;

        GenericService.httpGet(url).then(function(response) {
            $scope.accessionAbroad = CompulsoryAccessionService.populateAbroadModel(response.data);
            ViewHelper.unblockContainer('.abroad-container');
        }, function(){
            $scope.accessionAbroad = CompulsoryAccessionService.initAbroadModel();
            ViewHelper.unblockContainer('.abroad-container');
        });
    };

    var getPublicWorker = function() {
        ViewHelper.blockContainer('.public-worker-container', $translate.instant('loading'));
        var url = _publicWorkerEndpoint + '/person/' + $scope.person.id;

        GenericService.httpGet(url).then(function(response) {
            $scope.publicWorker = PublicWorkerService.populateModel(response.data);
            ViewHelper.unblockContainer('.public-worker-container');
        }, function(){
            $scope.publicWorker = PublicWorkerService.initModel();
            ViewHelper.unblockContainer('.public-worker-container');
        });
    };

    var saveAbroad = function(form) {
        if (form.$valid) {
            $scope.accessionAbroad.person = $scope.person;
            var accessionAbroad = CompulsoryAccessionService.preparateAbroadData($scope.accessionAbroad);
            var urlAccessionAbroad = _accessionAbroadEndpoint;

            if (accessionAbroad.id) {
                urlAccessionAbroad += '/' + accessionAbroad.id + '/edit';
            } else {
                urlAccessionAbroad += '/add';
            }

            return GenericService.httpPost(urlAccessionAbroad, accessionAbroad);
        }
    };

    var savePublicWorker = function(form) {
        if (form.$valid) {
            $scope.publicWorker.person = $scope.person;
            var publicWorker = PublicWorkerService.preparateData($scope.publicWorker);

            var urlPublicWork = _publicWorkerEndpoint;
            if (publicWorker.id) {
                urlPublicWork += '/' + publicWorker.id + '/edit';
            } else {
                urlPublicWork += '/add';
            }

            return GenericService.httpPost(urlPublicWork, publicWorker);
        }
    };

    $scope.isCurrentStep = function(step) {
        return $scope.currentSteps.indexOf(step) > -1;
    };

    $scope.saveInformation = function(form, model) {
        if (form.$valid) {
            var data = PersonService.preparateData(model);

            if (data.id) {
                GenericService.edit(_personEndpoint, data.id, data, afterSaveInformation, callBackErrorSubmit(form));
            } else {
                GenericService.add(_personEndpoint, data, afterSaveInformation, callBackErrorSubmit(form));
            }
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.isSubmitted = function() {
        return $scope.subscription && 
            (
                EnumService.getSubscriptionStatus('submitted') == $scope.subscription.subscriptionStatus 
                || 
                EnumService.getSubscriptionStatus('approved') == $scope.subscription.subscriptionStatus
            );
    };

    $scope.isErrored = function() {
        return $scope.subscription && EnumService.getSubscriptionStatus('error') == $scope.subscription.subscriptionStatus;
    };

    $scope.isExpiredUpdate = function() {
        return $scope.updateDeadline && moment().format('D') > $scope.updateDeadline;
    };

    $scope.canEdit = function() {
        var invalidStatuses = [
            EnumService.getSubscriptionStatus('suspended'),
            EnumService.getSubscriptionStatus('ceased')
        ];

        // The employer cannot be either suspended or ceased
        return !$scope.user.isStaff()
                && !$scope.optionalSubscription
                && !$scope.version
                && !$scope.employer.isOfAnyStatus(invalidStatuses)
                && (!$scope.subscription || !$scope.subscription.isOfAnyStatus(invalidStatuses))
                && ($scope.updateSubscription || !$scope.isSubmitted());
    };

    $scope.canEditPerson = function() {
        return !$scope.updateSubscription || $scope.personSaved || !$scope.id || $scope.user.isPerson();
    };

    $scope.canEditEmployment = function() {
        return !$scope.isSubmitted() || $scope.user.isEmployer();
    };

    var duplicateWorker = function() {
        var url = _personEndpoint + '/' + $scope.person.id + '/duplicate';
        var version = $scope.employer.internalCode + '_' + EnumService.getVersion('subscription');
        var data = {
            version: version,
            accession: $scope.accession.id,
            subscription: $scope.subscription.id,
        };

        return GenericService.httpPost(url, data);
    };

    var savePerson = function()
    {
        var person = PersonService.preparateData($scope.person);
        return GenericService.httpPost(_personEndpoint + '/' + person.id + '/edit', person);
    };

    var saveAll = function(form) {
        var callbackError = function(response) {
            ViewHelper.errorServer(form, response);
            ViewHelper.unblockContainer('.container');
            return $q.reject(response);
        };

        ViewHelper.blockContainer('.container', $translate.instant('data_saving'));
        $timeout(function() {
            ViewHelper.scrollTo('.container .blockMsg');
        }, 500);

        // Save the person data
        return savePerson().then(function(){
            // Save Abroad
            return saveAbroad(form);
        }).then(function(abroad){
            $timeout(function(){
                $scope.accessionAbroad = CompulsoryAccessionService.populateAbroadModel(abroad.data);
            });

            // Save Public Worker
            return savePublicWorker(form);
        }).then(function(publicWorker){
            $timeout(function(){
                $scope.publicWorker = PublicWorkerService.populateModel(publicWorker.data);
            });

            // Save the subscription data
            var subscription = CompulsoryAccessionService.preparateSubscriptionData($scope.subscription);

            // If it's updating the standard version, save a update version of it
            if ($scope.updateSubscription && subscription.version == EnumService.getVersion('standard')) {
                subscription.version = getVersionByEmployer(EnumService.getVersion('update'));
            }
            
            var urlSubscription = _subscriptionEndpoint + '/' + subscription.id + '/edit';
            return GenericService.httpPost(urlSubscription, subscription);
        }).then(function(subscription){
            $timeout(function(){
                $scope.subscription = CompulsoryAccessionService.populateSubscriptionModel(subscription.data);
            });

            return $q.when();
        }).catch(callbackError);
    };

    var saveCallback = function() {
        ViewHelper.unblockContainer('.container');

        var title, message, callback, cancelCallback;

        cancelCallback = function() {
            $timeout(function() {
                getEmployeesSubscribed($scope.employer.id);
                ViewHelper.scrollTo('.inss-logo');
            }, 500);
        };

        // if the subscription is submitted
        if ($scope.isSubmitted()) {
            title = $translate.instant('suscription_submitted');

            // If the current user is a person
            if ($scope.user.isPerson()) {
                if ($scope.updateSubscription) {
                    callback = function() {
                        $state.go('app.worker-list-update', {
                            updateSubscription: '1'
                        }, {reload: true, inherit: false, notify: true});
                    };
                } else {
                    callback = function() {
                        $state.go('app.subscription-thankyou');
                    };    
                }

                message = $translate.instant('person_suscription_submitted_text');
                
            } else {

                // If it's updating the subscription
                if ($scope.updateSubscription) {
                    title = $translate.instant('suscription_submitted');
                    message = $translate.instant('suscription_submitted_text');

                    callback = function() {
                        $state.go('app.worker-list-update', {
                            updateSubscription: '1'
                        }, {reload: true, inherit: false, notify: true});
                    };

                } else {
                    title = $translate.instant('suscription_submitted');
                    message = $translate.instant('worker_suscription_submitted_text');

                    // if the company is subscribing workers and it does not 
                    // want to subscribe more, redirect to thank you page
                    cancelCallback = function() {
                        $state.go('app.subscription-thankyou', {fromCompany: $scope.fromCompany});
                    };

                    callback = function() {
                        $state.go('app.employer-worker-form', {
                            employer: $scope.employer.internalCode,
                            ignoreLocalStorage: true,
                            updateSubscription: '0',
                            fromCompany: $scope.fromCompany
                        }, {reload: true, inherit: false, notify: true});
                    };
                }
            }
        } else {
            title = $translate.instant('worker_save_title');
            message = $translate.instant('worker_save_text');
            callback = function() {
                $scope.submit($scope.form);
            };
        }

        swal({
                title: title,
                text: message,
                type: 'success',
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                $timeout(function() {
                    if (isConfirm) {
                        callback();
                    } else {
                        cancelCallback();
                    }
                }, 300);
            }
        );

        $timeout(function(){
            $scope.person = mixPersonModel($scope.subscription.compulsoryAccession.person, $scope.person);
            $scope.accession.person = $scope.person;
            ViewHelper.scrollTo('.inss-logo');
        });
    };

    $scope.save = function(form) {
        if (form.$valid) {
            if ($scope.updateSubscription && form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_worker'),
                    type: 'warning'
                });
                return false;
            }

            saveAll(form).then(saveCallback);
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.submit = function(form) {
        if (form.$valid) {
            if ($scope.updateSubscription && form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_worker'),
                    type: 'warning'
                });
                return false;
            }

            form.$submitted = true;

            // Submit the subscription
            var oldSubscriptionStatus = $scope.subscription.subscriptionStatus;
            var oldVersion  = $scope.subscription.version;

            if ($scope.updateSubscription && $scope.subscription.version == EnumService.getVersion('standard')) {
                $scope.subscription.version = getVersionByEmployer(EnumService.getVersion('update'));
            } else {
                $scope.subscription.version = getVersionByEmployer();
            }

            $scope.subscription.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');
            var saveAllPromise = saveAll(form);

            saveAllPromise
                .then(saveCallback)
                .catch(function(){
                    $scope.subscription.subscriptionStatus = oldSubscriptionStatus;
                    $scope.subscription.version = oldVersion;
                });
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.newWorker = function() {
        $state.go('app.employer-worker-form', {
            employer: $scope.employer.internalCode,
            ignoreLocalStorage: true,
            updateSubscription: '0',
            fromCompany: $scope.fromCompany
        }, {reload: true, inherit: false, notify: true});
    };

    $scope.saveWorkContract = function(form) {
        if (form.$valid) {

            ViewHelper.blockContainer('.container', $translate.instant('loading'));
            savePerson().then(
                function(response) {
                    $scope.person = PersonService.populateModel(response.data);

                    var deferred = $q.defer();
                    if (!$scope.accession.id) {
                        accessionPromise = saveAccession();
                    } else {
                        deferred.resolve($scope.accession);
                        accessionPromise = deferred.promise;
                    }

                    accessionPromise.then(function(accession) {
                        $scope.accession = CompulsoryAccessionService.populateModel(accession);
                        saveSubscription();
                    }, callBackErrorSubmit(form));
                }
            ).catch(callBackErrorSubmit(form));
            
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.setWorker = function(person) {
        if ($scope.user.isPerson()) {
            $timeout(function(){
                swal({ 
                    title: $translate.instant('forbidden_title'),
                    text: $translate.instant('forbidden_text'),
                    type: "error" 
                });
            }, 500);
        } else {
            $timeout(function(){
                $state.go('app.worker-edit', {
                    employer: $scope.employer.internalCode,
                    id: person.internalCode,
                    ignoreLocalStorage: true,
                    updateSubscription: '0'
                });
            });
        }
    };

    $scope.setEmployer = function(employer) {
        $scope.employer = employer;
        return initEmployer(employer.internalCode);
    };

    var init = function() {
        getDisabilities();
        getMaritalStatuses();
        getNationalities();

        if ($scope.user.isEmployer()) {
            $scope.employerId = $scope.user.employer.internalCode;
        }

        if ($scope.user.isPerson()) {
            $scope.id = $scope.user.person.internalCode;
        }

        var deferred = $q.defer();
        if ($scope.employerId) {
            employerPromise = initEmployer($scope.employerId);
        } else {
            employerPromise = deferred.resolve(null);
        }

        employerPromise.then(function(){
            if ($scope.id) {
                initPerson($scope.id);
            } else {
                $scope.person = PersonService.initModel();
            }

            $timeout(function(){
                ViewHelper.scrollTo('.identification-container');
            }, 500);
        });

        if ($scope.updateSubscription) {
            $scope.updateDeadline = SettingService.get('day_update_subscription');
        }
    };

    init();
});
