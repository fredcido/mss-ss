app.controller("IncidenceBaseFormController", function($scope, $stateParams, $translate, GenericService) {
    $scope.supplements = [];
    $scope.basis = {'C':[], 'O':[]};
    $scope.compulsoryBase = {};
    $scope.optionalBase = {};

    var _endpoint = 'incidence-base';

    var searchSupplements = function() {
        GenericService.find('supplement').then(function(response) {
            response.data.map(function(o){
                if ('S' == o.type)
                    o.typeName = $translate.instant('supplement');
                else
                    o.typeName = $translate.instant('subsidie');
            });

            $scope.supplements = response.data;
        });
    };

    var searchBasis = function(accession) {
        GenericService.httpGet('incidence-base/find/' + accession).then(function(response) {
            response.data.map(function(incidence){
                $scope.basis[accession][incidence.supplement.id] = 1;
            });
        });
    };

    $scope.changedBase = function(supplement, accession) {
        if ($scope.basis[accession][supplement]) {
            addBase(supplement, accession);
        } else {
            removeBase(supplement, accession);
        }
    };

    var addBase = function(supplement, accession) {
        var data = {
            'accession': accession,
            'supplement': supplement
        };

        GenericService.httpPost(_endpoint + '/add', data).then(function(){

        }).catch(function(){
            $scope.basis[accession][supplement] = 0;
        });
    };

    var removeBase = function(supplement, accession) {
        var data = {
            'accession': accession,
            'supplement': supplement
        };

        GenericService.deleteMany(_endpoint, data).then(function(){

        }).catch(function(){
            $scope.basis[accession][supplement] = 1;
        });
    };

    var init = function() {
        searchSupplements();
        searchBasis('C');
        searchBasis('O');
    };

    init();
});
