<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Person;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class EmployeeUserSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        $gatherEntities = function ($entity) use (&$injectEntities) {
            if ($entity instanceof CompulsoryAccessionSubscription && $entity->isSubmitted()) {
                $injectEntities[] = $entity;
            }
        };

        $entities = array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates());
        array_walk($entities, $gatherEntities);

        $userManager = $this->container->get('fos_user.user_manager');
        $defaultPassword = $this->container->getParameter('user_tco_password');
        $userManipulator = $this->container->get('fos_user.util.user_manipulator');

        foreach ($injectEntities as $entity) {
            $person = $entity->getPerson();

            // we only care about the standard version
            if (!$person->isStandard()) {
                continue;
            }

            // Check whether the person is related to an user
            $userPerson = $userManager->findUserBy(['person' => $person]);
            if (!empty($userPerson)) {
                continue;
            }

            // Create the user with username using the NISS
            $user = $userManager->createUser();

            // Check whether the email is related to an user already
            $validEmail = $this->getValidEmail($person);
            $userEmail = $userManager->findUserBy(['email' => $validEmail]);
            if (!empty($userEmail)) {
                continue;
            }

            $user->setUsername($person->getSecurityCode());
            $user->setEmail($validEmail);
            $user->setPlainPassword($defaultPassword);
            $user->setEnabled(true);
            $user->setSuperAdmin(false);

            // Set the relation with the person
            $user->setPerson($person)->setRoles(['ROLE_PERSON']);

            $userManager->updateUser($user, false);

            //$uow->persist($user);
            $this->compute($em, $user);

            $emailQueue = $this->container->get('app.mail.user_person')->newUserMail($user);
            $this->compute($em, $emailQueue);
        }
    }

    protected function generateNewEmail(Person $person)
    {
        $mailDomain = $this->container->getParameter('mail_domain');
        $niss = $person->getSecurityCode();
        return sprintf('%s@%s', $niss, $mailDomain);
    }

    protected function getValidEmail(Person $person)
    {
        $contact = $person->getContactDetail();
        $emails = $contact->getEmail();

        $validEmail = null;
        foreach ($emails as $email) {
            if (!!filter_var($email, \FILTER_VALIDATE_EMAIL)) {
                $validEmail = $email;
            }
        }

        if (empty($validEmail)) {
            $validEmail = $this->generateNewEmail($person);
        }

        return $validEmail;
    }
}
