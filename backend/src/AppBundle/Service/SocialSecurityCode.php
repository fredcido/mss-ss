<?php

namespace AppBundle\Service;

use AppBundle\Entity\SocialSecurityInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SocialSecurityCode
{
    use ContainerAwareTrait;

    /**
     * @param  SocialSecurityInterface $entity
     */
    public function generate(SocialSecurityInterface $entity)
    {
        $securityCodeLabel = "%s%s";
        $nextSecurityCode = $this->getNextSecurityCode($entity);
        $formatSecurityCode = str_pad($nextSecurityCode, 8, '0', \STR_PAD_LEFT);
        $securityCode = sprintf($securityCodeLabel, $entity->getSecurityCodePrefix(), $formatSecurityCode);

        $entity->setSecurityCode($securityCode);
    }

    protected function getNextSecurityCode(SocialSecurityInterface $entity)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(get_class($entity));

        $lastSecurityCode = $repository->createQueryBuilder('m')
                                        ->select('MAX(m.securityCode)')
                                        ->getQuery()
                                        ->getSingleScalarResult();

        $sizePrefix = strlen($entity->getSecurityCodePrefix());
        $maxSecurityCode = substr($lastSecurityCode, $sizePrefix);
        return $maxSecurityCode + 1;
    }

    /**
     * @param  SocialSecurityInterface $entity
     * @return boolean
     */
    public function hasSecurityCode(SocialSecurityInterface $entity)
    {
        $securityCode = trim($entity->getSecurityCode());
        return !empty($securityCode);
    }
}
