app.controller("SettingController", function($scope, $stateParams, $translate, $cookies, $timeout, GenericService, SettingService, AppConstant, ViewHelper) {
    var _endpoint = "setting";
    var _stateList = "app.setting";
    var _find = function() {
        ViewHelper.blockContainer('.table-settings', $translate.instant('loading'));
        GenericService.find(_endpoint, true).then(function(response) {
            $scope.rowCollection = response.data;
            $scope.displayCollection = self.rowCollection;
            ViewHelper.unblockContainer('.table-settings');
        });
    };

    var init = function() {
        $scope.settings = [];
        $scope.model = {};
        $scope.editable = null;
        $scope.settingSelected = null;

        _find();
    };

    init();

    $scope.edit = function(setting) {
        $scope.model = angular.copy(setting);
    };

    $scope.cancel = function(model) {
        $scope.model = {};
    };

    $scope.history = function(model) {
        $scope.settingSelected = model;
        $scope.historyRowCollection = [];
        $scope.historyDisplayCollection = [];
        ViewHelper.blockContainer('.table-history', $translate.instant('loading'));

        GenericService.httpGet('setting/' + model.name + '/history?all').then(function(response) {
            $scope.historyRowCollection = response.data;
            $scope.historyDisplayCollection = $scope.rowCollection;
            ViewHelper.unblockContainer('.table-history');
        });
    };

    $scope.save = function(form, data, event) {
        if (form.$valid) {
            var inputGroup = $(event.target).closest('.input-group');
            ViewHelper.blockContainer(inputGroup, $translate.instant('loading'));

            var httpPromise = SettingService.save(_endpoint, data);
            httpPromise.then(function(json) {
                SettingService.load();
                $timeout(function(){
                    var data = json.data;
                    angular.forEach($scope.displayCollection, function(setting, key) {
                        if (setting.name == data.name) {
                            $scope.displayCollection[key] = data;
                        }
                    });
                });

                $scope.cancel();
            }).finally(function(){
                ViewHelper.unblockContainer(inputGroup);
            });
        } else {
            ViewHelper.errorForm(form);
        }
    };
});
