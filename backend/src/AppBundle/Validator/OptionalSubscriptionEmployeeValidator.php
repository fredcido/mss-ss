<?php

namespace AppBundle\Validator;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class OptionalSubscriptionEmployeeValidator extends ConstraintValidator
{
    use ContainerAwareTrait;

    public function validate($value, Constraint $constraint)
    {
        if ($value->isCeased() || $value->isSuspended()) {
            return;
        }

        $person = $value->getPerson();
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('AppBundle:CompulsoryAccessionSubscription');

        $activeEmployment = $repository->findActiveEmployments($person);

        if (!empty($activeEmployment)) {
            $this->context->buildViolation($constraint->message)
                        ->addViolation();
        }
    }
}
