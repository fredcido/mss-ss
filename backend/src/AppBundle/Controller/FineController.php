<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fine;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class FineController extends BaseController
{
    protected $repository = 'AppBundle:Fine';

    protected $form = 'AppBundle\Form\FineType';

    protected $routeGet = 'app.fine.get';

    protected $entity = Fine::class;

    protected function getOrderBy()
    {
        return array('days' => 'asc');
    }
}
