<?php

namespace AppBundle\Security;

use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Security\Actions;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AclBuilder
{
    use ContainerAwareTrait;

    const EMPLOYER_SUBSCRIPTION = 'employer-subscription';
    const COMPULSORY_SUBSCRIPTION = 'compulsory-subscription';
    const OPTIONAL_SUBSCRIPTION = 'optional-subscription';

    public function build()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $authorizationChecker = $this->container->get('security.authorization_checker');

        $entities = $em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $entities = array_merge($entities, self::fixedPermissions());

        $acls = [];
        foreach ($entities as $entity) {
            if (!class_exists($entity)) {
                $attributes = Actions::getAll();
                $resource = $entity;
            } else {
                if (!in_array(EntityPermissionTrait::class, class_uses($entity))) {
                    continue;
                }

                $attributes = $entity::getAttributes();
                $resource = $entity::getResource();
            }
            
            $acl = array_fill_keys($attributes, false);

            foreach ($acl as $key => $allow) {
                $acl[$key] = $authorizationChecker->isGranted($key, $entity);
            }

            $acls[$resource] = $acl;
        }

        return $acls;
    }

    public static function fixedPermissions()
    {
        return [
            self::EMPLOYER_SUBSCRIPTION,
            self::COMPULSORY_SUBSCRIPTION
        ];
    }
}
