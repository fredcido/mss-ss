app.factory("loadingInterceptor", function($q, $rootScope, ViewHelper, $translate) {
   'use strict';

    var xhrCreations = 0;
    var xhrResolutions = 0;

    function isLoading() {
        return xhrResolutions < xhrCreations;
    }

    function updateStatus() {
        $rootScope.loading = isLoading();
        if(isLoading()){
            ViewHelper.blockPage($translate.instant('loading'));
        }
        else{
            ViewHelper.unblockPage();
        }
    }

    return {
        request: function (config) {
            xhrCreations++;
            updateStatus();
            return config;
        },
        requestError: function (rejection) {
            xhrResolutions++;
            updateStatus();
            return $q.reject(rejection);
        },
        response: function (response) {
            xhrResolutions++;
            updateStatus();
            return response;
        },
        responseError: function (rejection) {
            xhrResolutions++;
            updateStatus();
            return $q.reject(rejection);
        }
    };
});
