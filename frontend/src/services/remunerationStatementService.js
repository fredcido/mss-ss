app.service(
    "RemunerationStatementService", 
    function(
        $http, 
        AppConstant,
        RemunerationPersonService,
        RemunerationRevisionService,
        EmployerService,
        EnumService
    ) {

    var _preparateData = function(model) {
        var data = angular.copy(model);

        var convertObjects = [
            'employer',
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        if (model.remunerationPeople) {
            data.remunerationPeople = model.remunerationPeople.map(function(item){
                return RemunerationPersonService.preparateData(item);
            });
        }

        if (model.revisions) {
            data.revisions = model.revisions.map(function(item){
                return RemunerationRevisionService.preparateData(item);
            });
        }

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.employer) {
            model.employer = EmployerService.populateModel(model.employer);
        }

        if (model.remunerationPeople) {
            model.remunerationPeople = model.remunerationPeople.map(function(item){
                return RemunerationPersonService.populateModel(item);
            });
        }

        if (model.revisions) {
            model.revisions = model.revisions.map(function(item){
                return RemunerationRevisionService.populateModel(item);
            });
        }

        return model;
    };

    var _initModel = function() {
        var model = {
            month: parseInt(moment().format('M')),
            year: parseInt(moment().format('YYYY')),
            remunerationPeople: [],
            revisions: []
        };

        return model;
    };


    return {
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel
    };
});
