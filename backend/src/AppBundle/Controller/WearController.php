<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Wear;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class WearController extends BaseController
{
    protected $repository = 'AppBundle:Wear';

    protected $form = 'AppBundle\Form\WearType';

    protected $routeGet = 'app.wear.get';

    protected $entity = Wear::class;
}
