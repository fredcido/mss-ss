<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EmployerContact
 *
 * @ORM\Table(name="employer_contact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmployerContactRepository")
 */
class EmployerContact
{
    use EntityAuditTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @var ContactDetail
     *
     * @ORM\ManyToOne(targetEntity="ContactDetail", cascade={"persist"})
     * @JMS\Groups({"details"})
     */
    private $contactDetail;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="Post")
     * @JMS\Groups({"details"})
     */
    private $post;

    /**
     * @var Employer
     *
     * @ORM\ManyToOne(targetEntity="Employer", inversedBy="contacts")
     */
    private $employer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EmployerContact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contactDetail
     *
     * @param \AppBundle\Entity\ContactDetail $contactDetail
     *
     * @return EmployerContact
     */
    public function setContactDetail(\AppBundle\Entity\ContactDetail $contactDetail = null)
    {
        $this->contactDetail = $contactDetail;

        return $this;
    }

    /**
     * Get contactDetail
     *
     * @return \AppBundle\Entity\ContactDetail
     */
    public function getContactDetail()
    {
        return $this->contactDetail;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return EmployerContact
     */
    public function setPost(\AppBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set employer
     *
     * @param \AppBundle\Entity\Employer $employer
     *
     * @return EmployerContact
     */
    public function setEmployer(\AppBundle\Entity\Employer $employer = null)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return \AppBundle\Entity\Employer
     */
    public function getEmployer()
    {
        return $this->employer;
    }
}
