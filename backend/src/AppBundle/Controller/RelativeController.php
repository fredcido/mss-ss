<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Relative;
use AppBundle\Form\RelativeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RelativeController extends BaseController
{
    protected $repository = 'AppBundle:Relative';

    protected $form = RelativeType::class;

    protected $routeGet = 'app.person.get';

    protected $entity = Relative::class;

    public function findByPersonAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository($this->repository)->findAllByPerson($id);

        return $this->createApiResponse($entities);
    }

    public function removeAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getRepository()->find($id);
        $em->remove($entity);
        $em->flush();

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    public function removeSchoolAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getRepository()->find($id);
        $entity->setInSchool(false)->setSchool(null)->setSchoolYear(null);
        $em->persist($entity);
        $em->flush();

        return $this->createApiResponse($entity);
    }
}
