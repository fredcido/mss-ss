<?php

namespace AppBundle\EventListener;

use AppBundle\Service\Maintenance;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class MaintenanceListener
{
    protected $maintenance;

    public function setMaintenance(Maintenance $maintenance)
    {
        $this->maintenance = $maintenance;
        return $this;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $currentRoute = $event->getRequest()->get('_route');
        if ('app.maintenance.deactivate' == $currentRoute) {
            return;
        }

        if ($this->maintenance->isOn()) {
            $data = json_encode(['maintenance' => 'On']);

            $response = new Response(
                $data,
                Response::HTTP_SERVICE_UNAVAILABLE,
                array(
                    'Content-Type' => 'application/json',
                )
            );
            $event->setResponse($response);
        }
    }
}