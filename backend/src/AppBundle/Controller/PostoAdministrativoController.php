<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PostoAdministrativo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class PostoAdministrativoController extends BaseController
{
    protected $repository = 'AppBundle:PostoAdministrativo';

    protected $form = 'AppBundle\Form\PostoAdministrativoType';

    protected $routeGet = 'app.posto_administrativo.get';

    protected $entity = PostoAdministrativo::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function getByMunicipioAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(['municipio' => $id]);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('list');
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
