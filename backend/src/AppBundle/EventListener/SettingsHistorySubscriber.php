<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Setting;
use AppBundle\Entity\SettingValue;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 *
 */
class SettingsHistorySubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $settingsValues = array();

    /**
     * (non-PHPdoc)
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::onFlush];
    }

    /**
     *
     * @param  LifecycleEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $em  = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Setting) {
                continue;
            }

            $changedValues = $uow->getEntityChangeSet($entity);
            if (!isset($changedValues['value'])) {
                return;
            }

            $settingValue = new SettingValue();
            $settingValue->setNewValue($changedValues['value'][1])
                ->setOldValue($changedValues['value'][0])
                ->setSetting($entity);

            $entity->addHistory($settingValue);
            $metadata = $em->getClassMetadata(get_class($entity));
            $uow->computeChangeSet($metadata, $entity);
            //$this->compute($em, $entity);
        }
    }
}
