<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * License
 *
 * @ORM\Table(name="license")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LicenseRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 * @UniqueEntity(fields="acronym", message="code_already_use")
 */
class License
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $acronym;

    /**
     * @ORM\OneToMany(targetEntity="LicenseLabourLaw", mappedBy="license", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $licenseLabourLaws;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return License
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return License
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of acronym.
     *
     * @return string
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Sets the value of acronym.
     *
     * @param string $acronym the acronym
     *
     * @return self
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->licenseLabourLaws = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add licenseLabourLaw
     *
     * @param LicenseLabourLaw $licenseLabourLaw
     *
     * @return License
     */
    public function addLicenseLabourLaw(LicenseLabourLaw $licenseLabourLaw)
    {
        $this->licenseLabourLaws[] = $licenseLabourLaw;
        $licenseLabourLaw->setLicense($this);
        return $this;
    }

    /**
     * Remove licenseLabourLaw
     *
     * @param LicenseLabourLaw $licenseLabourLaw
     */
    public function removeLicenseLabourLaw(LicenseLabourLaw $licenseLabourLaw)
    {
        $this->licenseLabourLaws->removeElement($licenseLabourLaw);
    }

    /**
     * Get licenseLabourLaws
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicenseLabourLaws()
    {
        return $this->licenseLabourLaws;
    }
}
