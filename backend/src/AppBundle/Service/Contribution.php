<?php

namespace AppBundle\Service;

use AppBundle\Entity\Person;
use AppBundle\Enum;
use AppBundle\Enum\Status;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Contribution
{
    use ContainerAwareTrait;

    protected $settings;

    /**
     * Gets the value of settings.
     *
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Sets the value of settings.
     *
     * @param mixed $settings the settings
     *
     * @return self
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    public function summaryByPerson($person)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repositoryContribution = $em->getRepository('AppBundle:Contribution');
        $repositoryEmployeeLicense = $em->getRepository('AppBundle:EmployeeLicense');

        $summary = $repositoryContribution->findSummaryByPerson($person);

        foreach ($summary as &$row) {
            $row['licenses'] = $repositoryEmployeeLicense->findGroupedPyPersonEmployer($person, $row['employer']);
        }

        return $summary;
    }

    public function summaryTotalByPerson($person)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repositoryContribution = $em->getRepository('AppBundle:Contribution');
        $repositoryEmployeeLicense = $em->getRepository('AppBundle:EmployeeLicense');

        $summary = $repositoryContribution->findSummaryTotalByPerson($person);
        $summary['licenses'] = $repositoryEmployeeLicense->findGroupedPyPersonEmployer($person);

        return $summary;
    }

    public function summaryByEmployer($employer)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repositoryContribution = $em->getRepository('AppBundle:Contribution');
        $repositoryEmployeeLicense = $em->getRepository('AppBundle:EmployeeLicense');
        
        $repositoryFine = $em->getRepository('AppBundle:Fine');
        $fines = $repositoryFine->findByStatus(Enum\Status::ACTIVE, ['days' => 'DESC']);

        $fineValues = array();
        foreach ($fines as $fine) {
            $fineValues[$fine->getDays()] = $fine->getValue();
        }

        // Tax of interest
        $interest = $this->settings->get(Settings::INTEREST);

        $summary = $repositoryContribution->findSummaryByEmployer($employer);
        $today = new \DateTime();

        foreach ($summary as &$row) {
            $fineValue = 0;
            $interestTax = 0;

            if (!empty($row['debt'])) {
                $dateEnd = new \DateTime($row['dateEnd']);
                $daysFromDateEnd = $today->diff($dateEnd)->format("%a");

                if ($daysFromDateEnd > 0) {
                    foreach ($fineValues as $days => $value) {
                        if ($daysFromDateEnd >= $days) {
                            $fineValue = $value;
                            break;
                        }
                    }
                }

                $interestTax = $interest;
            }

            $row['interest'] = $row['debt'] * ($interestTax / 100 + 1);
            $row['fine'] = $fineValue;
            $row['finalDebt'] = $row['debt'] + $row['interest'] + $fineValue;

            $row['licenses'] = $repositoryEmployeeLicense->findGroupedPyPersonEmployer($row['person'], $employer);
        }

        return $summary;
    }

    public function personCurrentState($person)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $repositoryPerson = $em->getRepository('AppBundle:Person');
        $repositoryContribution = $em->getRepository('AppBundle:Contribution');
        $repositoryRelative = $em->getRepository('AppBundle:Relative');

        if (!($person instanceof Person)) {
            $person = $repositoryPerson->find($person);
        }

        $hasState = false;

        // If there is a death date
        if ($person->getDod()) {
            $person->setCurrentState(Person::DEATH);
            $hasState = true;
        }

        if (!$hasState) {
            // If there is an active employment
            $latestContribution = $repositoryContribution->findCurrentByPerson($person);
            if (empty($latestContribution)) {
                $person->setCurrentState(Person::NO_REGISTER);
                $hasState = true;
            } elseif ($latestContribution->getContractStatus()->getId() == Status::ACTIVE) {
                $person->setCurrentState(Person::EMPLOYED)->setCurrentContract($latestContribution);
                $hasState = true;
            }
        }

        if (!$hasState) {
            $age = $person->getDob()->diff(new \DateTime('now'))->y;
            if ($age < 18) {
                if ($repositoryRelative->isStudent($person)) {
                    $person->setCurrentState(Person::STUDENT);
                    $hasState = true;
                } else {
                    $person->setCurrentState(Person::UNDERAGE);
                    $hasState = true;
                }
            } else {
                $person->setCurrentState(Person::UNEMPLOYED);
                $hasState = true;
            }
        }

        return $person->getCurrentState();
    }
}
