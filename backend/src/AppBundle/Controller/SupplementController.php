<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Supplement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class SupplementController extends BaseController
{
    protected $repository = 'AppBundle:Supplement';

    protected $form = 'AppBundle\Form\SupplementType';

    protected $routeGet = 'app.supplement.get';

    protected $entity = Supplement::class;
}
