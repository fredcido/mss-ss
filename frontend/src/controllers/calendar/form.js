app.controller("CalendarFormController", function($scope, $state, $stateParams, calendarPromise, CalendarService, GenericService) {
    var _endpoint = 'calendar';
    var _stateList = 'app.calendar-list';

    $scope.options = {
        customClass: getDayClass,
        dateDisabled: getDaysDisabled,
        showWeeks: false
    };

    $scope.today = function() {
        $scope.dt = new Date();
    };

    $scope.dt = null;
    $scope.update = 1;
    $scope.events = [];
    $scope.calendar = calendarPromise.data;
    $scope.holidays = {};

    $scope.$watch('update', function(newValue, oldValue) {
        $scope.holidaysTotal = Object.keys($scope.holidays).length;
    });

    var _initHolidays = function()
    {
        angular.forEach(
            calendarPromise.data,
            function(row)
            {
                $scope.holidays[row.day] = row;
            }
        );
    };

    $scope.setDate = function(newValue) {
        _setDate(newValue);
    };

    var _addHoliday = function(date) {
        var data = {
            day: date
        };

        var httpPromise = CalendarService.save(_endpoint, data);
        httpPromise.then(function(json) {
            $scope.holidays[date] = json.data;
            $scope.update++;
        });
    };

    var _removeHoliday = function(date) {
        var httpPromise = GenericService.delete(_endpoint, date.id);
        httpPromise.then(function(json) {
            delete $scope.holidays[date.day];
            $scope.update++;
        });
    };

    var _setDate = function(date) {
        var dateFormated = moment(date).format('YYYY-MM-DD');

        if ($scope.holidays[dateFormated] === undefined) {
            _addHoliday(dateFormated);
        } else {
            _removeHoliday($scope.holidays[dateFormated]);
        }
    };

    var _setClassHoliday = function(date) {
        var css = '';
        var dayToCheck = moment(date).format('YYYY-MM-DD');
        if ($scope.holidays[dayToCheck] !== undefined) {
            css = 'holiday';
        }

        return css;
    };

    function getDayClass(calendar) {
        if (calendar.mode == 'day') {
            if (calendar.date) {
                return _setClassHoliday(calendar.date);
            }
        }

        return '';
    }

    function getDaysDisabled(calendar) {
        if (calendar.mode == 'day') {
            var dayToCheck = moment(calendar.date);
            if (dayToCheck.isoWeekday() == 6 || dayToCheck.isoWeekday() == 7) {
                return true;
            }
        }

        return '';
    }

    _initHolidays();
});
