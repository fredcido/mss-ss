app.component('personSearchDialog', {
    templateUrl: 'views/components/personSearchDialog.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function ($translate, GenericService, ViewHelper) {
        var self = this;
        self.person = {};

        self.gridAction = [{
            title: $translate.instant('select'),
            callback: function (person) {
                self.close({
                    $value: person
                });
            }
        }];

        self.closeModal = function () {
            self.close({
                $value: null
            });
        };


        self.$onInit = function() {
            self.gridParams = self.resolve;
        };
    }
});