<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EmailQueue;
use AppBundle\Security\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class EmailQueueController extends BaseController
{
    protected $repository = 'AppBundle:EmailQueue';

    protected $form = 'AppBundle\Form\EmailQueueType';

    protected $entity = EmailQueue::class;

    public function findAction(Request $request)
    {
        $repository = $this->getRepository();

        $this->denyAccessUnlessGranted(Actions::VIEW, $repository->getClassName());

        $entities = $repository->findBy([], ['id' => 'DESC']);
        $groups = array('list', 'audit');

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
