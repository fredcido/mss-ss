<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\InternalCodeTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class InternalCodeSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $internalCodeGenerator;

    protected $needsFlush = false;

    public function setInternalCodeGenerator($service)
    {
        $this->internalCodeGenerator = $service;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::postPersist, Events::postUpdate, Events::postFlush];
    }

    protected function performInjection(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em     = $args->getEntityManager();

        if (!in_array(InternalCodeTrait::class, class_uses($entity))) {
            return;
        }

        if ($this->internalCodeGenerator->hasInternalCode($entity)) {
            return;
        }

        $this->internalCodeGenerator->inject($entity);
        $em->persist($entity);
        $this->needsFlush = true;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->performInjection($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->performInjection($args);
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }
}
