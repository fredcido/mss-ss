app.directive('employerName', function($translate) {
    return {
        restrict: 'E',
        scope: {
            employer: "<"
        },
        template: '<span translate="employer"></span>: <span ng-if="employer.internalCode"><b>{{ employer.internalCode }} - </b></span><b>{{ employer.name }}</b><span ng-if="employer.securityCode"> - <b>[{{ employer.securityCode }}]</b></span><p><small><span translate="created_at"></span> : {{ ::employer.created_at | date:"dd/MM/yyyy HH:mm" }}</small></p>',
        template: '<span translate="employer"></span>: <b>{{ employer.displayName }}</b><p><small><span translate="created_at"></span> : {{ ::employer.created_at | date:"dd/MM/yyyy HH:mm" }}</small></p>',
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                setEmployer(e.employer);
            });

            var setEmployer = function(value) {
                scope.employer = value;
            };
        }
    };
});
