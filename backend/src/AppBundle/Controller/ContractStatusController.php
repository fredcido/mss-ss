<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContractStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class ContractStatusController extends BaseController
{
    protected $repository = 'AppBundle:ContractStatus';

    protected $form = 'AppBundle\Form\ContractStatusType';

    protected $entity = ContractStatus::class;

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function findByActiveAction($active, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findBy(['active' => $active]);

        $groups = array('form');
        if ($request->query->get('all')) {
            $groups = array('list');
        }

        return $this->createApiResponse($entities, Response::HTTP_OK, $groups);
    }
}
