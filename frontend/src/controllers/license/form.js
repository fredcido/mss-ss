app.controller("LicenseFormController", function($scope, $stateParams, $translate, GenericService) {
    var _endpoint = 'license';
    var _stateList = 'app.license-list';
    $scope.labourLaws = [];

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1, licenseLabourLaws: [] };
    $scope.types = [{
        value: 0,
        label: $translate.instant('not_affect')
    }, {
        value: 1,
        label: $translate.instant('affect')
    }];

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            var model = response.data;
            if (model.licenseLabourLaws) {
                model.licenseLabourLaws.map(function(item){
                    item.labourLaw = item.labourLaw.id;
                });
            }
            $scope.model = model;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };

    var _getLabourLaws = function () {
        GenericService.find('labour-law').then(function (response) {
            $scope.labourLaws = response.data;
        });
    };

    $scope.addLabourLaw = function () {
        $scope.model.licenseLabourLaws.push({
            labourLaw: null,
            daysInMonth: 0,
            daysInYear: 0
        });
    };

    $scope.removeLicenseLabourLaw = function (license) {
        $scope.model.licenseLabourLaws.some(function (i) {
            if (i == license) {
                $scope.model.licenseLabourLaws.splice($scope.model.licenseLabourLaws.indexOf(i), 1);
            }
        });
    };

    _getLabourLaws();
});
