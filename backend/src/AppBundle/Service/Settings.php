<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

class Settings
{
    const CONTRIBUTION_EMPLOYER = 'contribution_employer';
    const CONTRIBUTION_EMPLOYEE = 'contribution_employee';
    const MINIMAL_AGE_WOMEN = 'minimal_age_women';
    const MINIMAL_AGE_MEN = 'minimal_age_men';
    const INTEREST = 'interest';

    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /*
     * Returns the value for the $settingName
     *
     */
    public function get($settingName)
    {
        $setting = $this->em->getRepository('AppBundle:Setting')->findOneBy(
            ['name' => $settingName]
        );

        if (!$setting) {
            throw new \Exception(sprintf("Attempt to get the value for the configuration setting '%s' which does not exist!", $settingName));
        }

        return $setting->getValue();
    }

}
