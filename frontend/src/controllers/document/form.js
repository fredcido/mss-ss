app.controller("DocumentFormController", function($scope, $state, $stateParams, $translate, GenericService, Upload, AppConstant, ViewHelper) {
    var _endpoint = 'document';
    var _stateList = 'app.document-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.types = [];

    var _getTypes = function() {
        GenericService.data('document-type').then(function(response) {
            $scope.types = response.data;
        });
    };

    _getTypes();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            response.data.type = {id: response.data.type};
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        upload(data, 'add');
    };

    // Edit
    $scope.edit = function(id, data) {
        if (!data.file) {
            delete data.file;
        }

        upload(data, id + '/edit');
    };

    function upload(data, action) {
        ViewHelper.blockContainer('.container', $translate.instant('data_saving'));

        console.log(data);
        data.type = data.type.id;
        console.log(data);

        Upload.upload({
            url: AppConstant.apiUrl + _endpoint + '/' + action,
            data: data
        }).then(function (response) {
            ViewHelper.unblockContainer('.container');

            swal({
                title: $translate.instant('success_title'),
                text: $translate.instant('success_text'),
                type: 'success',
                timer: 2000,
                showConfirmButton: false
            },
            function() {
                $state.go(_stateList);
                swal.close();
            });

        }, function (response) {
            ViewHelper.unblockContainer('.container');
            ViewHelper.errorServer($scope.form, response);
        });
    }
});
