<?php

namespace AppBundle\Entity;

use AppBundle\Enum\Document as DocumentEnum;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 */
class Document
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $type;

    /**
     * @var string
     *
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\File(maxSize="100M", mimeTypes={"text/plain", "application/msword", "application/pdf", "image/*", "application/vnd.ms-excel", "text/csv", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/pdf,application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"} )
     */
    private $file;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Document
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Document
     */
    public function setType($type)
    {
        $type = (int)$type;
        $valid = DocumentEnum::getOptions();
        if (!isset($valid[$type])) {
            throw new \InvalidArgumentException(sprintf("'%s' is not a valid document type: %s", $subscriptionStatus, __CLASS__));
        }

        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getTypeName()
    {
        return DocumentEnum::get($this->type);
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
}

