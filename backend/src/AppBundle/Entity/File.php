<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileRepository")
 */
class File
{
    use EntityAuditTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $path;

    /**
     * @var string
     *
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\File(maxSize="10M", mimeTypes={"text/plain", "application/msword", "application/pdf", "image/*", "application/vnd.ms-excel", "text/csv", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/pdf,application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"} )
     */
    private $file;

    /**
     * @var int
     *
     * @ORM\Column(name="temporary", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $temporary = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $reference;

    /**
     * @var int
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $entity;


    /**
     * @var FileConfiguration
     *
     * @ORM\ManyToOne(targetEntity="FileConfiguration")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $configuration;


    public function __construct()
    {
        $this->temporary = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set temporary
     *
     * @param integer $temporary
     *
     * @return File
     */
    public function setTemporary($temporary)
    {
        $this->temporary = (int)$temporary;
        return $this;
    }

    /**
     * Get temporary
     *
     * @return int
     */
    public function getTemporary()
    {
        return $this->temporary;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return File
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set entity
     *
     * @param integer $entity
     *
     * @return File
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return int
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set configuration
     *
     * @param \AppBundle\Entity\FileConfiguration $configuration
     *
     * @return File
     */
    public function setConfiguration(\AppBundle\Entity\FileConfiguration $configuration = null)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return \AppBundle\Entity\FileConfiguration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
