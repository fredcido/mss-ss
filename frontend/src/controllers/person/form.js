app.controller("PersonFormController", function($scope, $timeout, $state, $stateParams, ViewHelper, $translate, GenericService, PersonService, EnumService, $uibModal) {

    var _endpoint = "person";
    var _stateList = "app.person-form";

    $scope.id = $stateParams.id || null;
    $scope.model = {
        bankInformation: [],
        address: {
            main: 1
        }
    };
    $scope.rowRelativeCollection = [];
    $scope.displayRelativeCollection = [];
    $scope.disabilities = [];
    $scope.nationalities = [];
    $scope.maritalStatuses = [];
    $scope.calendar = {
        dob: false,
        dod: false
    };

    $scope.calendarOptions = {
        maxDate: new Date(),
    };

    $scope.openCalendar = function(param) {
        $scope.calendar[param] = true;
    };

    $scope.focus = function(element) {
        $timeout(function() {
            angular.element('input[name="' + element + '"]').focus();
        });
    };

    $scope.personAlreadyThere = function(person) {
        $timeout(function() {
            $state.go('app.person-form', {
                id: person.id,
                ignoreLocalStorage: true
            });
        }, 500);
    };

    $scope.submit = function(form, model) {
        if (form.$valid) {
            var dataPreparate = PersonService.preparateData(model);

            if ($scope.id) {
                GenericService.edit(_endpoint, $scope.id, dataPreparate, _callBackSubmit);
            } else {
                GenericService.add(_endpoint, dataPreparate, _callBackSubmit);
            }
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.canEdit = function() {
        return angular.isUndefined($scope.model.id);
    };

    var getDisabilities = function() {
        GenericService.find('disability').then(function(response) {
            $scope.disabilities = response.data;
        });
    };

    var getMaritalStatuses = function() {
        GenericService.find('marital-status').then(function(response) {
            $scope.maritalStatuses = response.data;
        });
    };

    var getNationalities = function() {
        GenericService.find('country').then(function(response) {
            $scope.nationalities = response.data;
        });
    };

    var _callBackSubmit = function(data) {
        $timeout(function() {
            ViewHelper.unblockContainer('.panel');
        }, 200);
        $state.go(_stateList, {
            id: data.id
        });
    };

    var _callBackErrorSubmit = function(responseServer) {
        $scope.form.$invalid = true;
        $scope.errorServer = responseServer.errors;
        angular.forEach($scope.errorServer, function(msg, field) {
            $scope.errorServer[field] = msg.join('; ');
            $scope.form[field].$invalid = true;
        });

        ViewHelper.showErrorApi(responseServer);
        ViewHelper.unblockContainer('.panel');
    };

    var _init = function() {
        getDisabilities();
        getNationalities();
        getMaritalStatuses();

        if ($scope.id) {
            ViewHelper.blockContainer('.panel', $translate.instant('loading'));

            GenericService.get(_endpoint, $scope.id).then(function(response) {
                var data = response.data;

                $scope.model = PersonService.populateModel(data);
                $scope.model.id = $scope.id;

                /*PersonService.fetchCurrentContribution($scope.model.id).then(
                    function(response) {
                        var current = response.data;
                        if (current.currentContract) {
                            var currentContract = current.currentContract;
                            currentContract.capitalDescription = EnumService.getCapitalDescription(currentContract.employer.capital);
                            $scope.currentContract = currentContract;
                        }

                        $scope.currentState = $translate.instant(current.state);
                    }
                );*/
                ViewHelper.unblockContainer('.panel');
            });
        }
    };

    _init();
});
