<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MaritalStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class MaritalStatusController extends BaseController
{
    protected $repository = 'AppBundle:MaritalStatus';

    protected $form = 'AppBundle\Form\MaritalStatusType';

    protected $routeGet = 'app.marital_status.get';

    protected $entity = MaritalStatus::class;
}
