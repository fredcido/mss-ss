app.controller(
    "OptionalSubscriptionSuspensionController", 
    function(
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $uibModal, 
        $translate,
        $q, 
        ViewHelper, 
        GenericService,
        AuthService,
        EnumService,
        EmployerService,
        PersonService,
        OptionalAccessionService
    ) {
        var _personEndpoint = "person";
        var _accessionEndpoint = 'optional-accession';
        var _subscriptionEndpoint = 'optional-subscription';

        $scope.id = $stateParams.id || null;
        
        $scope.person = null;
        $scope.accession = null;
        $scope.subscription = null;

        $scope.user = AuthService.getUserLogged();
        $scope.today = moment().toDate();
        $scope.calendarOptions = {
            maxDate: $scope.today
        };

        $scope.suspendedStatus = EnumService.getSubscriptionStatus('suspended');
        $scope.ceasedStatus = EnumService.getSubscriptionStatus('ceased');
        $scope.action = {status: $scope.suspendedStatus};

        $scope.actions = [
            {value: $scope.suspendedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.suspendedStatus)},
            {value: $scope.ceasedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.ceasedStatus)}
        ];

        var init = function() {

            // Make sure the user can only see its own profile
            if ($scope.user.isPerson()) {
                $scope.id = $scope.user.person.internalCode;
            }

            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            // Get the person
            var version = EnumService.getVersion('standard');
            GenericService.getVersion(_personEndpoint, $scope.id, version).then(function (response) {
                $scope.person = PersonService.populateModel(response.data);
                return $q.when(response);
            }).then(function() {

                // Get the current accession
                var url = _accessionEndpoint + '/person/' + $scope.person.id + '/latest';
                return GenericService.httpGet(url).then(function(response) {
                    $scope.accession = OptionalAccessionService.populateModel(response.data);
                    $scope.accession.dateEnd = moment().toDate();

                    return $q.when(response);
                });
            }).then(function(){

                // Get the current subscription
                var url = _subscriptionEndpoint + '/person/' + $scope.person.id + '/latest';
                return GenericService.httpGet(url).then(function(response) {
                    $scope.subscription = OptionalAccessionService.populateSubscriptionModel(response.data);
                    $scope.calendarOptions.minDate = $scope.subscription.dateStart;
                    return $q.when(response);
                });
            }).finally(function(){
                ViewHelper.unblockContainer('.container');
            });
        };

        $scope.isSuspended = function() {
            return $scope.accession && $scope.accession.isSuspended();
        };

        $scope.isCeased = function() {
            return $scope.accession && $scope.accession.isCeased();
        };
        
        $scope.canSuspend = function() {
            return $scope.user.isPerson() && !$scope.isSuspended() && !$scope.isCeased();
        };

        $scope.canActive = function() {
            return $scope.user.isPerson() && $scope.person && !$scope.accession.isCeased();
        };

        var saveSuspension = function() {
            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            var accessionData = OptionalAccessionService.preparateData($scope.accession);
            var url = _accessionEndpoint + '/' + accessionData.id + '/edit';

            GenericService.httpPost(url, accessionData).then(function(response){
                $scope.accession = OptionalAccessionService.populateModel(response.data);
                ViewHelper.scrollTo('.inss-logo');

                // Update the user to release couple of things
                AuthService.setUser($scope.user).then(
                    function(){
                        $rootScope.$broadcast('userChanged');
                        swal({
                            title: $translate.instant('success_title'),
                            text: $translate.instant('success_text'),
                            type: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });                        
                    }
                );

            }).catch(function(response){
                $scope.accession.subscriptionStatus = $scope.oldSubscriptionStatus;
                ViewHelper.errorServer(form, response);
            }).finally(function(){
                ViewHelper.unblockContainer('.container');
            });
        };

        $scope.suspend = function(form) {
            if (form.$valid) {

                if ($scope.accession.isSuspended()) {
                    $scope.action.status = $scope.ceasedStatus;
                }

                $scope.oldSubscriptionStatus = $scope.accession.subscriptionStatus;
                $scope.accession.subscriptionStatus = $scope.action.status;

                saveSuspension();
                
            } else {
                ViewHelper.errorForm(form);
            }
        };

        $scope.activate = function() {
            $scope.oldSubscriptionStatus = $scope.accession.subscriptionStatus;
            $scope.accession.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');

            saveSuspension();
        };

        init();
    }
);