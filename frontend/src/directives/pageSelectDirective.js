app.directive('pageSelect', function() {
    return {
        restrict: 'E',
        template: '<input type="text" style="width: 80px !important" class="form-control text-center select-page" ng-model="inputPage" ng-change="selectPage(inputPage)">',
        link: function(scope, element, attrs) {
            scope.$watch('currentPage', function(c) {
                scope.inputPage = c;
            });
        }
    };
});
