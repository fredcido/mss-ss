app.component('currentWorkSituation', {
    templateUrl: 'views/components/currentWorkSituation.html',
    bindings: {
        model: '=',
        disabled: '<',
        active: '<',
    },
    controller: function($scope, $translate, $timeout, GenericService) {
        var self = this;

        self.occupations = [];
        self.posts = [];
        self.activitiesSectors = [];
        self.activitiesSectorsTree = [];
        self.sectorsTree = [];

        var getOccupations = function() {
            GenericService.find('occupation').then(function(response) {
                self.occupations = response.data;
            });
        };

        var getPosts = function() {
            GenericService.find('post').then(function(response) {
                self.posts = response.data;
            });
        };

        var getActivitiesSectors = function () {
            GenericService.httpGet('activity-sector/tree').then(function (response) {
                self.activitiesSectorsTree = GenericService.addTreeLevel(response.data);
                filterDuplicatedActivities();
            });
        };

        var filterDuplicatedActivities = function() {
            $timeout(function(){
                if (!self.model || !self.model.activitySectors || !self.model.activitySectors.length || !self.activitiesSectorsTree) return;

                self.activitiesSectorsTree = self.activitiesSectorsTree.filter(function(item){
                    return !self.model.activitySectors.some(function(row){
                        return row.id == item.id;
                    });
                });
            }, 1000);
        };

        $scope.$watch(
            function(){ return self.model; }, 
            function(){
                filterDuplicatedActivities();
            }
        );

        self.setEmployer = function(employer) {
            self.model.employer = employer;
        };

        self.$onInit = function() {
            self.active = angular.isUndefined(self.active) ? true : self.active;
            
            getOccupations();
            getPosts();
            getActivitiesSectors();

            self.today = moment().toDate();
            self.dateEndOptions = {
                maxDate: self.today
            };

            filterDuplicatedActivities();
        };
    }
});
