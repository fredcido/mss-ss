<?php

namespace AppBundle\Mailer;

use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\OptionalAccessionSubscription;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UserPerson
{
    use ContainerAwareTrait;
    use MailerTrait;

    protected $newUserTemplatePath              = 'emails/person_registered.html.twig';
    protected $activateUserTemplatePath         = 'emails/person_activate.html.twig';
    protected $optionalSubscriptionTemplatePath = 'emails/person_optional_warning.html.twig';

    public function getNewUserTemplatePath()
    {
        return $this->newUserTemplatePath;
    }

    public function getActivateUserTemplatePath()
    {
        return $this->activateUserTemplatePath;
    }

    public function getOptionalSubscriptionTemplatePath()
    {
        return $this->optionalSubscriptionTemplatePath;
    }

    /**
     * Send a mail to the user when a new person user is created
     * @param  User   $user
     * @return EmailQueue
     */
    public function newUserMail(User $user)
    {
        $context = ['user' => $user];
        $subject = $this->buildTitle('user_person_registration');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template = $this->getNewUserTemplatePath();
        $body     = $this->renderBody($template, $context);

        $emailQueue->setBody($body)->setToMail($user->getEmail());

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send a mail to the admin when a new person user is activated
     * @param  User   $user
     * @return EmailQueue
     */
    public function activateEmail(User $user)
    {
        $context = ['user' => $user];
        $subject = $this->buildTitle('user_person_activate');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template = $this->getActivateUserTemplatePath();
        $body     = $this->renderBody($template, $context);

        $mailAdmin = $this->container->getParameter('mail_admin');
        $emailQueue->setBody($body)->setToMail($mailAdmin);

        return $this->saveEmail($emailQueue);
    }

    /**
     * Send a mail to the user when a new compulsory subscription is submitted and it has a optional subscription active
     * @param  User   $user
     * @return EmailQueue
     */
    public function optionalSubscriptionWarningEmail(
        User $user,
        CompulsoryAccessionSubscription $subscription,
        OptionalAccessionSubscription $optionalSubscription
    ) {
        $context = [
            'user'                 => $user,
            'subscription'         => $subscription,
            'optionalSubscription' => $optionalSubscription,
        ];

        $subject = $this->buildTitle('user_person_subscription_warning');

        $emailQueue = $this->newEmailQueue();
        $emailQueue->setSubject($subject);

        $template = $this->getOptionalSubscriptionTemplatePath();
        $body     = $this->renderBody($template, $context);

        $emailQueue->setBody($body)->setToMail($user->getEmail());

        return $this->saveEmail($emailQueue);
    }
}
