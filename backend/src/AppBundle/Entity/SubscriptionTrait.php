<?php

namespace AppBundle\Entity;

use AppBundle\Enum\SubscriptionStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
trait SubscriptionTrait
{

    /**
     * @var smallint
     *
     * @ORM\Column(name="subscriptionStatus", type="smallint", options={"default" : 0})
     * @JMS\Groups({"list", "details", "audit"})
     */
    private $subscriptionStatus = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscriptionChangedAt", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "audit"})
     */
    protected $subscriptionChangedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submissionDate", type="datetime", nullable=true)
     * @JMS\Groups({"list", "details", "audit"})
     */
    protected $submissionDate;


    /**
     * Gets the value of subscriptionStatus.
     *
     * @return smallint
     */
    public function getSubscriptionStatus()
    {
        return $this->subscriptionStatus;
    }

    public function getSubscriptionStatusDesc()
    {
        return SubscriptionStatus::get($this->subscriptionStatus);
    }

    /**
     * Sets the value of subscriptionStatus.
     *
     * @param smallint $subscriptionStatus the subscription status
     *
     * @return self
     */
    public function setSubscriptionStatus($subscriptionStatus)
    {
        $subscriptionStatus = (int)$subscriptionStatus;
        $valid = SubscriptionStatus::getOptions();
        if (!isset($valid[$subscriptionStatus])) {
            throw new \InvalidArgumentException(sprintf("'%s' is not a valid subscription status: %s", $subscriptionStatus, __CLASS__));
        }

        $this->subscriptionStatus = $subscriptionStatus;
        return $this;
    }

    /**
     * Gets the value of subscriptionChangedAt.
     *
     * @return \DateTime
     */
    public function getSubscriptionChangedAt()
    {
        return $this->subscriptionChangedAt;
    }

    /**
     * Sets the value of subscriptionChangedAt.
     *
     * @param \DateTime $subscriptionChangedAt the subscription changed at
     *
     * @return self
     */
    public function setSubscriptionChangedAt($subscriptionChangedAt)
    {
        $this->subscriptionChangedAt = $subscriptionChangedAt;
        return $this;
    }

    /**
     * @param  string  $status
     * @return boolean
     */
    public function isOfSubscriptionStatus($status)
    {
        return $this->getSubscriptionStatus() === $status;
    }

    /**
     * @return boolean
     */
    public function isSubmitted()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::SUBMITTED);
    }

    /**
     * @return boolean
     */
    public function isPending()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::PENDING);
    }

    /**
     * @return boolean
     */
    public function isErrored()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::ERROR);
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::APPROVED);
    }

    /**
     * @return boolean
     */
    public function isCeased()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::CEASED);
    }

    /**
     * @return boolean
     */
    public function isSuspended()
    {
        return $this->isOfSubscriptionStatus(SubscriptionStatus::SUSPENDED);
    }

    /**
     * Gets the value of submissionDate.
     *
     * @return \DateTime
     */
    public function getSubmissionDate()
    {
        return $this->submissionDate;
    }

    /**
     * Sets the value of submissionDate.
     *
     * @param \DateTime $submissionDate the submission date
     *
     * @return self
     */
    public function setSubmissionDate(\DateTime $submissionDate)
    {
        $this->submissionDate = $submissionDate;
        return $this;
    }
}
