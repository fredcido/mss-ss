<?php

namespace AppBundle\Controller;

use AppBundle\Entity\IncidenceBase;
use AppBundle\Enum\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class IncidenceBaseController extends BaseController
{
    protected $repository = 'AppBundle:IncidenceBase';

    protected $form = 'AppBundle\Form\IncidenceBaseType';

    protected $routeGet = 'app.incidence_base.get';

    protected $entity = IncidenceBase::class;

    /**
     *
     * @param  string  $access
     * @param  Request $request
     * @return Response
     */
    public function findByAccessionAction($accession, Request $request)
    {
        $entities = $this->getRepository()->findByAccession($accession);
        return $this->createApiResponse($entities, Response::HTTP_OK, ['list']);
    }

    protected function persistEntity($entity)
    {
        $repository = $this->getRepository();

        $currentEntity = $repository->findOneBy(
            ['accession' => $entity->getAccession(),'supplement' => $entity->getSupplement()]
        );

        if (!empty($currentEntity)) {
            $entity = $currentEntity;
        }

        $entity->setStatus(Status::ACTIVE);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $entity;
    }

    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item = $data['items'];

        $repository = $this->getRepository();

        $entity = $repository->findOneBy(
            ['accession' => $item['accession'],'supplement' => $item['supplement']]
        );

        $entity->setStatus(Status::INACTIVE);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        return $this->createApiResponse($entity);
    }
}
