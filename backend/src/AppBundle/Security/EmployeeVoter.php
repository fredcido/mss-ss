<?php

namespace AppBundle\Security;

use AppBundle\Entity\ActivitySector;
use AppBundle\Entity\Calendar;
use AppBundle\Entity\CompulsoryAccession;
use AppBundle\Entity\CompulsoryAccessionAbroad;
use AppBundle\Entity\CompulsoryAccessionSubscription;
use AppBundle\Entity\ContractStatus;
use AppBundle\Entity\ContractType;
use AppBundle\Entity\Country;
use AppBundle\Entity\Disability;
use AppBundle\Entity\Document;
use AppBundle\Entity\Employer;
use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Entity\Kinship;
use AppBundle\Entity\LabourLaw;
use AppBundle\Entity\MaritalStatus;
use AppBundle\Entity\Municipio;
use AppBundle\Entity\Occupation;
use AppBundle\Entity\OptionalAccession;
use AppBundle\Entity\OptionalAccessionSubscription;
use AppBundle\Entity\OptionalContributionGroup;
use AppBundle\Entity\OptionalStep;
use AppBundle\Entity\Person;
use AppBundle\Entity\Post;
use AppBundle\Entity\PostoAdministrativo;
use AppBundle\Entity\PublicWorker;
use AppBundle\Entity\Regime;
use AppBundle\Entity\Relative;
use AppBundle\Entity\SchoolYear;
use AppBundle\Entity\Sector;
use AppBundle\Entity\Setting;
use AppBundle\Entity\Suku;
use AppBundle\Entity\User;
use AppBundle\Security\AclBuilder;
use AppBundle\Security\Actions;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter as AbstractVoter;

class EmployeeVoter extends AbstractVoter
{
    use RoleVoterTrait;

    protected $allowedViews = array(
        Setting::class,
        Employer::class,
        Person::class,
        Relative::class,
        Sector::class,
        Post::class,
        ActivitySector::class,
        Municipio::class,
        PostoAdministrativo::class,
        Suku::class,
        Country::class,
        Kinship::class,
        Disability::class,
        MaritalStatus::class,
        CompulsoryAccession::class,
        CompulsoryAccessionSubscription::class,
        OptionalAccession::class,
        OptionalAccessionSubscription::class,
        OptionalContributionGroup::class,
        OptionalStep::class,
        ContractType::class,
        Regime::class,
        LabourLaw::class,
        ContractStatus::class,
        Occupation::class,
        PublicWorker::class,
        SchoolYear::class,
        Calendar::class,
        Document::class,
        AclBuilder::COMPULSORY_SUBSCRIPTION,
        AclBuilder::OPTIONAL_SUBSCRIPTION
    );

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$this->decisionManager->decide($token, array('ROLE_PERSON'))) {
            return false;
        }

        $subjectClass = $this->getEntityClass($subject);

        if (Actions::VIEW == $attribute && in_array($subjectClass, $this->allowedViews)) {
            return true;
        } else {
            if ($subject instanceof Person && $subject->getId()) {
                return $subject->getInternalCode() == $user->getPerson()->getInternalCode();
            } else {
                $permissions = AclBuilder::fixedPermissions();
                if (in_array($subject, $permissions)) {
                    return true;
                }

                switch ($subjectClass) {
                    case AclBuilder::COMPULSORY_SUBSCRIPTION:
                    case AclBuilder::OPTIONAL_SUBSCRIPTION:
                    case CompulsoryAccession::class:
                    case CompulsoryAccessionSubscription::class:
                    case CompulsoryAccessionAbroad::class:
                    case PublicWorker::class:
                    case Person::class:
                    case Relative::class:
                    case OptionalAccession::class:
                    case OptionalAccessionSubscription::class:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
