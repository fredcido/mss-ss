app.service("RemunerationDayService", function($http, AppConstant, EnumService) {

    var _preparateData = function(model) {
        var data = angular.copy(model);

        var convertObjects = [
            'remunerationPerson',
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        return data;
    };

    var _populateModel = function(model) {
        var model = angular.copy(model);

        return model;
    };

    var _initModel = function() {
        var model = {
            worked: 0,
        };

        return model;
    };


    return {
        preparateData: _preparateData,
        populateModel: _populateModel,
        initModel: _initModel
    };
});
