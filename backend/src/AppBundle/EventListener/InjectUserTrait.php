<?php

namespace AppBundle\EventListener;

trait InjectUserTrait
{
    public function injectCreate($entity)
    {
        $reflection = new \ReflectionClass($entity);

        if ($reflection->hasProperty('created_at')) {
            $now = new \DateTime('now');
            $entity->setCreatedAt($now);
            $entity->setUpdatedAt($now);
        }

        if ($reflection->hasProperty('created_by')) {
            $user = $this->getUser();
            $entity->setCreatedBy($user);
            $entity->setUpdatedBy($user);
        }
    }

    public function injectUpdate($entity)
    {
        $reflection = new \ReflectionClass($entity);

        if ($reflection->hasProperty('updated_at')) {
            $now = new \DateTime('now');
            $entity->setUpdatedAt($now);
        }

        if ($reflection->hasProperty('updated_by')) {
            $user = $this->getUser();
            $entity->setUpdatedBy($user);
        }
    }
}
