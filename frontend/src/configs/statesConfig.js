app.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow JSONP calls that match this pattern
    'http://127.0.0.1:8000/**'
  ]);
});

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider.state('app', {
            url: '',
            templateUrl: 'views/main.html',
            controller: 'MainController',
            abstract: true
        })
        .state('404', {
            url: '/erro/404',
            templateUrl: 'views/404.html',
            resource: '404'
        })
        .state('500', {
            url: '/erro/500',
            templateUrl: 'views/500.html',
            resource: '500'
        })
        .state('app.forbidden', {
            url: '/error/forbidden',
            templateUrl: 'views/forbidden.html',
            resource: 'forbidden'
        })
        .state('app.profile', {
            url: '/profile',
            templateUrl: 'views/profile/form.html',
            controller: 'ProfileController'
        })
        .state('app.home', {
            url: '/home',
            templateUrl: 'views/home/dashboard.html',
            controller: 'HomeDashboardController',
            resource: 'home',
            action: 'view'
        })
        .state('app.home-company', {
            url: '/home-company',
            templateUrl: 'views/home/company.html',
            controller: 'HomeCompanyController'
        })
        .state('app.setting', {
            url: '/setting',
            templateUrl: 'views/setting/form.html',
            controller: 'SettingController',
            resource: 'setting',
            action: 'view'
        })
        .state('app.person-list', {
            url: '/person/list',
            templateUrl: 'views/person/list.html',
            controller: 'PersonListController',
            resource: 'person',
            action: 'view'
        })
        .state('app.person-form', {
            url: '/person/form/:id',
            templateUrl: 'views/person/form.html',
            params: {
                ignoreLocalStorage: false
            },
            controller: 'PersonFormController',
            resource: 'person',
            action: 'create'
        })
        .state('app.employer-list', {
            url: '/employer/list',
            templateUrl: 'views/employer/list.html',
            controller: 'EmployerListController',
            resource: 'employer',
            action: 'view'
        })
        .state('app.employer-form', {
            url: '/employer/form/:id',
            params: {
                ignoreLocalStorage: false
            },
            templateUrl: 'views/employer/form.html',
            controller: 'EmployerFormController',
            resource: 'employer',
            action: 'create'
        })
        .state('app.activity-sector-list', {
            url: '/activity-sector/list',
            templateUrl: 'views/activity-sector/list.html',
            controller: 'ActivitySectorListController',
            resource: 'activity-sector',
            action: 'view'
        })
        .state('app.activity-sector-form', {
            url: '/activity-sector/form/:id',
            templateUrl: 'views/activity-sector/form.html',
            controller: 'ActivitySectorFormController',
            resource: 'activity-sector',
            action: 'create'
        })
        .state('app.contract-type-list', {
            url: '/contract-type/list',
            templateUrl: 'views/contract-type/list.html',
            controller: 'ContractTypeListController',
            resource: 'contract-type',
            action: 'view'
        })
        .state('app.contract-type-form', {
            url: '/contract-type/form/:id',
            templateUrl: 'views/contract-type/form.html',
            controller: 'ContractTypeFormController',
            resource: 'contract-type',
            action: 'create'
        })
        .state('app.kinship-list', {
            url: '/kinship/list',
            templateUrl: 'views/kinship/list.html',
            controller: 'KinshipListController',
            resource: 'kinship',
            action: 'view'
        })
        .state('app.kinship-form', {
            url: '/kinship/form/:id',
            templateUrl: 'views/kinship/form.html',
            controller: 'KinshipFormController',
            resource: 'kinship',
            action: 'create'
        })
        .state('app.disability-list', {
            url: '/disability/list',
            templateUrl: 'views/disability/list.html',
            controller: 'DisabilityListController',
            resource: 'disability',
            action: 'view'
        })
        .state('app.disability-form', {
            url: '/disability/form/:id',
            templateUrl: 'views/disability/form.html',
            controller: 'DisabilityFormController',
            resource: 'disability',
            action: 'create'
        })
        .state('app.country-list', {
            url: '/country/list',
            templateUrl: 'views/country/list.html',
            controller: 'CountryListController',
            resource: 'country',
            action: 'view'
        })
        .state('app.country-form', {
            url: '/country/form/:id',
            templateUrl: 'views/country/form.html',
            controller: 'CountryFormController',
            resource: 'country',
            action: 'create'
        })
        .state('app.document-list', {
            url: '/document/list',
            templateUrl: 'views/document/list.html',
            controller: 'DocumentListController',
            resource: 'document',
            action: 'view'
        })
        .state('app.document-view', {
            url: '/document/view',
            templateUrl: 'views/document/view.html',
            controller: 'DocumentViewController'
        })
        .state('app.document-form', {
            url: '/document/form/:id',
            templateUrl: 'views/document/form.html',
            controller: 'DocumentFormController',
            resource: 'document',
            action: 'create'
        })
        .state('app.email-queue-list', {
            url: '/email-queue/list',
            templateUrl: 'views/email-queue/list.html',
            controller: 'EmailQueueListController',
            resource: 'email-queue',
            action: 'view'
        })
        .state('app.email-queue-form', {
            url: '/email-queue/form/:id',
            templateUrl: 'views/email-queue/form.html',
            controller: 'EmailQueueFormController',
            resource: 'email-queue',
            action: 'create'
        })
        .state('app.post-list', {
            url: '/post/list',
            templateUrl: 'views/post/list.html',
            controller: 'PostListController',
            resource: 'post',
            action: 'view'
        })
        .state('app.post-form', {
            url: '/post/form/:id',
            templateUrl: 'views/post/form.html',
            controller: 'PostFormController',
            resource: 'post',
            action: 'create'
        })
        /*.state('app.overtime-list', {
            url: '/overtime/list',
            templateUrl: 'views/overtime/list.html',
            controller: 'OvertimeListController'
        })
        .state('app.overtime-form', {
            url: '/overtime/form/:id',
            templateUrl: 'views/overtime/form.html',
            controller: 'OvertimeFormController'
        })*/
        .state('app.occupation-list', {
            url: '/occupation/list',
            templateUrl: 'views/occupation/list.html',
            controller: 'OccupationListController',
            resource: 'occupation',
            action: 'view'
        })
        .state('app.occupation-form', {
            url: '/occupation/form/:id',
            templateUrl: 'views/occupation/form.html',
            controller: 'OccupationFormController',
            resource: 'occupation',
            action: 'create'
        })
        .state('app.fine-list', {
            url: '/fine/list',
            templateUrl: 'views/fine/list.html',
            controller: 'FineListController',
            resource: 'fine',
            action: 'view'
        })
        .state('app.fine-form', {
            url: '/fine/form/:id',
            templateUrl: 'views/fine/form.html',
            controller: 'FineFormController',
            resource: 'fine',
            action: 'create'
        })
        .state('app.municipio-list', {
            url: '/municipio/list',
            templateUrl: 'views/municipio/list.html',
            controller: 'MunicipioListController',
            resource: 'municipio',
            action: 'view'
        })
        .state('app.municipio-form', {
            url: '/municipio/form/:id',
            templateUrl: 'views/municipio/form.html',
            controller: 'MunicipioFormController',
            resource: 'municipio',
            action: 'create'
        })
        .state('app.posto-administrativo-list', {
            url: '/posto-administrativo/list',
            templateUrl: 'views/posto-administrativo/list.html',
            controller: 'PostoAdministrativoListController',
            resource: 'posto-administrativo',
            action: 'view'
        })
        .state('app.posto-administrativo-form', {
            url: '/posto-administrativo/form/:id',
            templateUrl: 'views/posto-administrativo/form.html',
            controller: 'PostoAdministrativoFormController',
            resource: 'posto-administrativo',
            action: 'create'
        })
        .state('app.suku-list', {
            url: '/suku/list',
            templateUrl: 'views/suku/list.html',
            controller: 'SukuListController',
            resource: 'suku',
            action: 'view'
        })
        .state('app.suku-form', {
            url: '/suku/form/:id',
            templateUrl: 'views/suku/form.html',
            controller: 'SukuFormController',
            resource: 'suku',
            action: 'create'
        })
        .state('app.village-list', {
            url: '/village/list',
            templateUrl: 'views/village/list.html',
            controller: 'VillageListController',
            resource: 'village',
            action: 'view'
        })
        .state('app.village-form', {
            url: '/village/form/:id',
            templateUrl: 'views/village/form.html',
            controller: 'VillageFormController',
            resource: 'village',
            action: 'create'
        })
        .state('app.license-list', {
            url: '/license/list',
            templateUrl: 'views/license/list.html',
            controller: 'LicenseListController'
        })
        .state('app.license-form', {
            url: '/license/form/:id',
            templateUrl: 'views/license/form.html',
            controller: 'LicenseFormController',
            resource: 'license',
            action: 'create'
        })
        .state('app.convention-form', {
            url: '/convention/form/:id',
            templateUrl: 'views/convention/form.html',
            controller: 'ConventionFormController',
            resource: 'convention',
            action: 'create'
        })
        .state('app.convention-list', {
            url: '/convention/list',
            templateUrl: 'views/convention/list.html',
            controller: 'ConventionListController',
            resource: 'convention',
            action: 'view'
        })
        .state('app.school-year-form', {
            url: '/school-year/form/:id',
            templateUrl: 'views/school-year/form.html',
            controller: 'SchoolYearFormController',
            resource: 'school-year',
            action: 'create'
        })
        .state('app.school-year-list', {
            url: '/school-year/list',
            templateUrl: 'views/school-year/list.html',
            controller: 'SchoolYearListController',
            resource: 'school-year',
            action: 'view'
        })
        .state('app.user-list', {
            url: '/user/list',
            templateUrl: 'views/user/list.html',
            controller: 'UserListController',
            resource: 'user',
            action: 'view'
        })
        .state('app.user-form', {
            url: '/user/form/:id',
            templateUrl: 'views/user/form.html',
            controller: 'UserFormController',
            resource: 'user',
            action: 'create'
        })
        .state('app.contract-status-form', {
            url: '/contract-status/form/:id',
            templateUrl: 'views/contract-status/form.html',
            controller: 'ContractStatusFormController',
            resource: 'contract-status',
            action: 'create'
        })
        .state('app.contract-status-list', {
            url: '/contract-status/list',
            templateUrl: 'views/contract-status/list.html',
            controller: 'ContractStatusListController',
            resource: 'contract-status',
            action: 'view'
        })
        .state('app.marital-status-list', {
            url: '/marital-status/list',
            templateUrl: 'views/marital-status/list.html',
            controller: 'MaritalStatusListController',
            resource: 'marital-status',
            action: 'view'
        })
        .state('app.marital-status-form', {
            url: '/marital-status/form/:id',
            templateUrl: 'views/marital-status/form.html',
            controller: 'MaritalStatusFormController',
            resource: 'marital-status',
            action: 'create'
        })
        .state('app.labour-law-form', {
            url: '/labour-law/form/:id',
            templateUrl: 'views/labour-law/form.html',
            controller: 'LabourLawFormController',
            resource: 'labour-law',
            action: 'create'
        })
        .state('app.labour-law-list', {
            url: '/labour-law/list',
            templateUrl: 'views/labour-law/list.html',
            controller: 'LabourLawListController',
            resource: 'labour-law',
            action: 'view'
        })
        .state('login', {
            url: '/login',
            templateUrl: "views/login.html",
            controller: 'LoginController',
            resource: 'login'
        })
        .state('logout', {
            url: '/logout',
            controller: 'LogoutController',
            resource: 'logout'
        })
        .state('app.calendar-form', {
            url: '/calendar/form/',
            templateUrl: 'views/calendar/form.html',
            controller: 'CalendarFormController',
            resolve: {
                calendarPromise: function(GenericService) {
                    return GenericService.find('calendar');
                }
            },
            resource: 'calendar',
            action: 'create'
        })
        .state('app.sector-list', {
            url: '/sector/list',
            templateUrl: 'views/sector/list.html',
            controller: 'SectorListController',
            resource: 'sector',
            action: 'view'
        })
        .state('app.sector-form', {
            url: '/sector/form/:id',
            templateUrl: 'views/sector/form.html',
            controller: 'SectorFormController',
            resource: 'sector',
            action: 'create'
        })
        .state('app.supplement-list', {
            url: '/supplement/list',
            templateUrl: 'views/supplement/list.html',
            controller: 'SupplementListController',
            resource: 'supplement',
            action: 'view'
        })
        .state('app.supplement-form', {
            url: '/supplement/form/:id',
            templateUrl: 'views/supplement/form.html',
            controller: 'SupplementFormController',
            resource: 'supplement',
            action: 'create'
        })
        .state('app.bank-list', {
            url: '/bank/list',
            templateUrl: 'views/bank/list.html',
            controller: 'BankListController',
            resource: 'bank',
            action: 'view'
        })
        .state('app.bank-form', {
            url: '/bank/form/:id',
            templateUrl: 'views/bank/form.html',
            controller: 'BankFormController',
            resource: 'bank',
            action: 'create'
        })
        .state('app.wear-list', {
            url: '/wear/list',
            templateUrl: 'views/wear/list.html',
            controller: 'WearListController',
            resource: 'wear',
            action: 'view'
        })
        .state('app.wear-form', {
            url: '/wear/form/:id',
            templateUrl: 'views/wear/form.html',
            controller: 'WearFormController',
            resource: 'wear',
            action: 'create',
        })
        .state('app.contribution-form', {
            url: '/contribution/form/:id',
            templateUrl: 'views/contribution/form.html',
            controller: 'ContributionFormController',
            resource: 'contribution',
            action: 'create'
        })
        .state('app.regime-list', {
            url: '/regime/list',
            templateUrl: 'views/regime/list.html',
            controller: 'RegimeListController',
            resource: 'regime',
            action: 'view'
        })
        .state('app.regime-form', {
            url: '/regime/form/:id',
            templateUrl: 'views/regime/form.html',
            controller: 'RegimeFormController',
            resource: 'regime',
            action: 'create'
        })
        .state('app.contribution-list', {
            url: '/contribution/list/:id',
            templateUrl: 'views/contribution/list.html',
            controller: 'ContributionListController',
            resource: 'contribution',
            action: 'view'
        })
        .state('app.contribution-person', {
            url: '/contribution/person/:person',
            templateUrl: 'views/contribution/form.html',
            controller: 'ContributionFormController',
            resource: 'contribution-person',
            action: 'create'
        })
        .state('app.tax-reduction-list', {
            url: '/tax-reduction/list',
            templateUrl: 'views/tax-reduction/list.html',
            controller: 'TaxReductionListController',
            resource: 'tax-reduction',
            action: 'view'
        })
        .state('app.tax-reduction-form', {
            url: '/tax-reduction/form/:id',
            templateUrl: 'views/tax-reduction/form.html',
            controller: 'TaxReductionFormController',
            resource: 'tax-reduction',
            action: 'create'
        })
        .state('app.additional-career-form', {
            url: '/additional-career/form/:id',
            templateUrl: 'views/additional-career/form.html',
            controller: 'AdditionalCareerFormController',
            resource: 'additional-career',
            action: 'create'
        })
        .state('app.additional-career-list', {
            url: '/additional-career/list',
            templateUrl: 'views/additional-career/list.html',
            controller: 'AdditionalCareerListController',
            resource: 'additional-career',
            action: 'view'
        })
        .state('app.optional-contribution-group-list', {
            url: '/optional-contribution-group/list',
            templateUrl: 'views/optional-contribution-group/list.html',
            controller: 'OptionalContributionGroupListController',
            resource: 'optional-contribution-group',
            action: 'create'
        })
        .state('app.optional-contribution-group-form', {
            url: '/optional-contribution-group/form/:id',
            templateUrl: 'views/optional-contribution-group/form.html',
            controller: 'OptionalContributionGroupFormController',
            resource: 'optional-contribution-group',
            action: 'create'
        })
        .state('app.incidence-base', {
            url: '/incidence-base',
            templateUrl: 'views/incidence-base/form.html',
            controller: 'IncidenceBaseFormController',
            resource: 'incidence-base',
            action: 'create'
        })
        .state('app.equivalence-compensation', {
            url: '/equivalence-compensation',
            templateUrl: 'views/equivalence-compensation/form.html',
            controller: 'EquivalenceCompensationFormController',
            resource: 'equivalence-compensation',
            action: 'create'
        })
        .state('app.optional-step-list', {
            url: '/optional-step/list',
            templateUrl: 'views/optional-step/list.html',
            controller: 'OptionalStepListController',
            resource: 'optional-step',
            action: 'view'
        })
        .state('app.optional-step-form', {
            url: '/optional-step/form/:id',
            templateUrl: 'views/optional-step/form.html',
            controller: 'OptionalStepFormController',
            resource: 'optional-step',
            action: 'create'
        })
        .state('app.compulsory-subscription', {
            url: '/compulsory-subscription',
            templateUrl: 'views/compulsory-subscription/form.html',
            controller: 'CompulsorySubscriptionFormController'
        })
        .state('app.file-configuration-form', {
            url: '/file-configuration/form/:id',
            templateUrl: 'views/file-configuration/form.html',
            controller: 'FileConfigurationFormController',
            resource: 'file-configuration',
            action: 'create'
        })
        .state('app.file-configuration-list', {
            url: '/file-configuration/list',
            templateUrl: 'views/file-configuration/list.html',
            controller: 'FileConfigurationListController',
            resource: 'file-configuration',
            action: 'view'
        })
        .state('app.optional-subscription-form', {
            url: '/optional-subscription/form/:updateSubscription/:id/:version',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0',
                version: null
            },
            templateUrl: 'views/optional-subscription/form.html',
            controller: 'OptionalSubscriptionFormController'
        })
        .state('app.optional-subscription-list', {
            url: '/optional-subscription/list/:updateSubscription/:suspend',
            templateUrl: 'views/optional-subscription/list.html',
            controller: 'OptionalSubscriptionListController'
        })
        .state('app.optional-subscription-list-changes', {
            url: '/optional-subscription/changes/:id',
            templateUrl: 'views/optional-subscription/changes.html',
            controller: 'OptionalSubscriptionChangesController'
        })
        .state('app.optional-subscription-version', {
            url: '/optional-subscription/:updateSubscription/:id/version/:version',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0',
                version: null
            },
            templateUrl: 'views/optional-subscription/form.html',
            controller: 'OptionalSubscriptionFormController'
        })
        .state('app.optional-subscription-suspension', {
            url: '/optional-subscription-suspension/:id',
            templateUrl: 'views/optional-subscription/suspension.html',
            controller: 'OptionalSubscriptionSuspensionController'
        })
        .state('app.optional-subscription-compulsory', {
            url: '/optional-subscription-compulsory',
            templateUrl: 'views/optional-subscription/compulsory.html',
            controller: 'OptionalSubscriptionCompulsoryController'
        })
        .state('app.optional-subscription-thankyou', {
            url: '/optional-subscription-thankyou',
            templateUrl: 'views/optional-subscription/thankyou.html',
            controller: 'OptionalSubscriptionThankyouController'
        })
        .state('app.company-list-subscription', {
            url: '/company/subscription',
            params: {
                updateSubscription: '0',
                suspend: '0',
            },
            templateUrl: 'views/company/list.html',
            controller: 'CompanyListController'
        })
        .state('app.company-list-update', {
            url: '/company/update',
            params: {
                updateSubscription: '1',
                suspend: '0',
            },
            templateUrl: 'views/company/list.html',
            controller: 'CompanyListController'
        })
        .state('app.company-list-suspend', {
            url: '/company/suspend',
            params: {
                updateSubscription: '0',
                suspend: '1',
            },
            templateUrl: 'views/company/list.html',
            controller: 'CompanyListController'
        })
        .state('app.company-form', {
            url: '/company/form/:updateSubscription/:id',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0'
            },
            templateUrl: 'views/company/form.html',
            controller: 'CompanyFormController'
        })
        .state('app.company-version', {
            url: '/company/:updateSubscription/:id/version/:version',
            params: {
                updateSubscription: '0',
                version: null
            },
            templateUrl: 'views/company/form.html',
            controller: 'CompanyFormController'
        })
        .state('app.company-suspension', {
            url: '/company/suspension/:id',
            params: {
                ignoreLocalStorage: false,
            },
            templateUrl: 'views/company/suspension.html',
            controller: 'CompanySuspensionController'
        })
        .state('app.company-list-changes', {
            url: '/company/changes/:id',
            templateUrl: 'views/company/changes.html',
            controller: 'CompanyChangesController'
        })
        .state('app.company-thankyou', {
            url: '/company-thankyou',
            templateUrl: 'views/company/thankyou.html',
            controller: 'CompanyThankyouController'
        })
        .state('app.worker-list-subscription', {
            url: '/worker/subscription',
            templateUrl: 'views/worker/list.html',
            controller: 'WorkerListController',
            params: {
                suspend: '0',
                updateSubscription: '0'
            }
        })
        .state('app.worker-list-update', {
            url: '/worker/update',
            templateUrl: 'views/worker/list.html',
            controller: 'WorkerListController',
            params: {
                suspend: '0',
                updateSubscription: 1
            }
        })
        .state('app.worker-list-suspend', {
            url: '/worker/suspend',
            templateUrl: 'views/worker/list.html',
            controller: 'WorkerListController',
            params: {
                suspend: '1',
                updateSubscription: '0'
            }
        })
        .state('app.worker-list-changes', {
            url: '/worker/changes/:id/employer/:employer',
            templateUrl: 'views/worker/changes.html',
            controller: 'WorkerChangesController'
        })
        .state('app.worker-form', {
            url: '/worker/:updateSubscription',
            templateUrl: 'views/worker/form.html',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0'
            },
            controller: 'WorkerFormController'
        })
        .state('app.worker-suspension', {
            url: '/worker-suspension/:id/employer/:employer',
            templateUrl: 'views/worker/suspension.html',
            controller: 'WorkerSuspensionController'
        })
        .state('app.employer-worker-form', {
            url: '/worker/employer/:employer/:fromCompany',
            templateUrl: 'views/worker/form.html',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0',
                fromCompany: '0'
            },
            controller: 'WorkerFormController'
        })
        .state('app.worker-edit', {
            url: '/worker/:updateSubscription/:id/employer/:employer',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0'
            },
            templateUrl: 'views/worker/form.html',
            controller: 'WorkerFormController'
        })
        .state('app.worker-version', {
            url: '/worker/:updateSubscription/:id/employer/:employer/version/:version',
            params: {
                ignoreLocalStorage: false,
                updateSubscription: '0',
                version: null
            },
            templateUrl: 'views/worker/form.html',
            controller: 'WorkerFormController'
        })
        .state('app.remuneration-statement-employer', {
            url: '/remuneration-statement/employer/:employer',
            params: {
                ignoreLocalStorage: false,
            },
            templateUrl: 'views/remuneration-statement/form.html',
            controller: 'RemunerationStatementController'
        })
        .state('app.remuneration-statement', {
            url: '/remuneration-statement/id/:id',
            params: {
                ignoreLocalStorage: false,
            },
            templateUrl: 'views/remuneration-statement/form.html',
            controller: 'RemunerationStatementController'
        })
        .state('app.remuneration-statement-list', {
            url: '/remuneration-statement/list',
            templateUrl: 'views/remuneration-statement/list.html',
            controller: 'RemunerationStatementListController'
        })
        .state('app.contribution-overview', {
            url: '/contribution-overview',
            templateUrl: 'views/contribution-overview/overview.html',
            controller: 'ContributionOverviewController'
        })
        .state('app.subscription-thankyou', {
            url: '/subscription-thankyou/:fromCompany',
            params: {
                fromCompany: '0',
            },
            templateUrl: 'views/worker/thankyou.html',
            controller: 'SubscriptionThankyouController'
        })
        .state('app.payment-form', {
            url: '/payment/form',
            templateUrl: 'views/payment/form.html',
            controller: 'PaymentFormController'
        });

    $urlRouterProvider.otherwise('/home');
});
