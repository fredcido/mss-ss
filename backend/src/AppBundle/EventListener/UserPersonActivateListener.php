<?php

namespace AppBundle\EventListener;

use AppBundle\Enum\Status;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UserPersonActivateListener
{
    use ContainerAwareTrait;
    use GetUserTrait;

    public function activatePerson(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $person = $user->getPerson();
        if ($person && $person->getStatus() == Status::INACTIVE) {
            $person->setStatus(Status::ACTIVE);

            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->persist($person);

            $this->container->get('app.mail.user_person')->activateEmail($user);

            $em->flush();
        }
    }
}
