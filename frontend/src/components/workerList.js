app.component('workerList', {
    templateUrl: 'views/components/workerList.html',
    bindings: {
        rows: '<',
        actions: '<',
        rowAction: '&',
        employer: '<',
    },
    controller: function($translate, $timeout, $uibModal, GenericService, ViewHelper, EnumService) {
        var self = this;
        var checkVersion = new RegExp('_' + EnumService.getVersion('subscription') + '$');

        this.$onChanges = function (changesObj) {
            if (changesObj.actions && changesObj.actions.currentValue) {
                self.actions.map(function(item){
                    if (!angular.isFunction(item.title)) {
                        var title = item.title;
                        item.title = function(row){
                            return title; 
                        };
                    }

                    if (!angular.isFunction(item.icon)) {
                        var icon = item.icon;
                        item.icon = function(row){
                            return icon; 
                        };
                    }

                    return item;
                });
            }


            if (changesObj.rows && changesObj.rows.currentValue) {
                ViewHelper.blockContainer('.worker-list', $translate.instant('loading'));
                self.rows = changesObj.rows.currentValue;

                self.rows.map(function(worker){
                    worker.subdsidiary = worker.workSituation.subdsidiary;
                    worker.person = worker.compulsoryAccession.person;

                    if (!worker.subdsidiary) {
                        worker.subdsidiary = worker.workSituation.employer;
                    }

                    worker.isVersionStandard = self.isVersionStandard(worker);
                    worker.isVersionChange = self.isVersionChange(worker);
                    worker.isSubscription = self.isSubscription(worker);

                    return worker;
                });

                self.rowCollection = self.rows;
                self.displayCollection = self.rows;
                
                ViewHelper.unblockContainer('.worker-list');
            }
        };

        self.clickRow = function(row) {
            if (self.rowAction) {
                self.rowAction(row);
            }
        };

        self.isVersionStandard = function(row) {
            return EnumService.getVersion('standard') == row.version;
        };

        self.isSubscription = function(row) {
            return !self.isVersionStandard(row) && checkVersion.exec(row.version);
        };

        self.isVersionChange = function(row) {
            return !self.isVersionStandard(row) && !checkVersion.exec(row.version);
        };
    }
});
