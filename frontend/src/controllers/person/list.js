app.controller("PersonListController", function($scope, $translate, $state, EnumService) {
    $scope.personSearchAction = [{
        title: $translate.instant('edit'),
        callback: function(person) {
            $state.go('app.person-form', {
                id: person.id
            });
        }
    }];

    $scope.selectSinglePerson = function(person) {
       $state.go('app.person-form', {
            id: person.id
        });
    };

    $scope.params = {
        all_status: 1,
        version: EnumService.getVersion('standard')
    };
});
