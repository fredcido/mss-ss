app.controller("UserFormController", 
    function(
        $scope, 
        $stateParams, 
        $translate, 
        $uibModal, 
        $timeout,
        $translate,
        GenericService, 
        PasswordHelper, 
        EmployerService,
        PersonService,
        ViewHelper
    ) {

    var _endpoint = 'user';
    var _stateList = 'app.user-list';

    $scope.id = $stateParams.id || null;
    $scope.roles = [];
    $scope.model = {
        enabled: true, 
        roles: null
    };

    $scope.locales = [
        {id: 'en', 'name': $translate.instant('en')},
        {id: 'pt', 'name': $translate.instant('pt')},
        {id: 'tl', 'name': $translate.instant('tl')},
    ];

    var _prepareData = function(data) {
        var _data = angular.copy(data);
        _data.roles = [data.roles.id];

        if (!data.enabled) {
            delete _data.enabled;
        }

        if (data.employer) {
            if (data.employer.id) {
                _data.employer = data.employer.id
            } else {
                _data.employer = EmployerService.preparateData(data.employer);
            }
            delete _data.employer.addresses;
        }

        if (data.person) {
            _data.person = data.person.id;
        }

        return _data;
    };

    var _populate = function(data) {
        var _data = {
            roles: null,
            plainPassword: {
                first: null,
                second: null
            }
        };

        _data.id = data.id;
        _data.email = data.email;
        _data.username = data.username;
        _data.enabled = data.enabled;
        _data.roles = {id: data.roles[0], name: handleRoleName(data.roles[0])};
        _data.locale = data.locale;

        if (data.employer) {
            _data.employer = EmployerService.populateModel(data.employer)
        }

        if (data.person) {
            _data.person = PersonService.populateModel(data.person);
        }

        return _data;
    };

    var handleRoleName = function(name) {
        name = name.toLowerCase();
        return $translate.instant(name);
    };

    var _getRoles = function() {
        GenericService.data('roles').then(function(response) {
            var data = response.data;
            data.map(function(item) {
                item.name = handleRoleName(item.name);
                return item;
            })

            $scope.roles = data;
        });
    };

    _getRoles();

    // Populate
    if ($scope.id) {
        ViewHelper.blockContainer('form', $translate.instant('loading'));
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            var data = response.data;
            $scope.model = _populate(data);
            ViewHelper.unblockContainer('form');
        });
    }

    // Add
    $scope.add = function(data) {
        var dataPrepared = _prepareData(data);
        GenericService.add(_endpoint, dataPrepared, _stateList);
    };

    $scope.$watch('model.roles', function(){
        if ($scope.model.roles && 'ROLE_COMPANY' == $scope.model.roles.id) {
            if (!$scope.model.employer || !$scope.model.employer.id)
                $scope.model.employer = EmployerService.initModel();

            delete $scope.model.person;
        } else if ($scope.model.roles && 'ROLE_PERSON' == $scope.model.roles.id) {
            if (!$scope.model.person || !$scope.model.person.id)
                $scope.model.person = PersonService.initModel();

            delete $scope.model.employer;
        } else {
            delete $scope.model.employer;
            delete $scope.model.person;
        }
    });

    $scope.generatePassword = function() {
        var newPassword = PasswordHelper.generatePassword();

        $scope.model.generatePassword = newPassword;
        $scope.model.plainPassword = {
            first: newPassword,
            second: newPassword,
        };
    };

    // Edit
    $scope.edit = function(id, data) {
        var dataPrepared = _prepareData(data);
        GenericService.edit(_endpoint, id, dataPrepared, _stateList);
    };

    $scope.searchEmployer = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            component: 'employerSearchDialog'
        });

        modalInstance.result.then(function (employer) {
            if (employer) {
                $scope.setEmployer(employer);
            }
        }, function () {

        });
    };

    $scope.setEmployer = function(employer) {
        $timeout(function(){
            $scope.model.employer = employer;
        });
    };

    $scope.cleanEmployer = function() {
        $timeout(function(){
            $scope.model.employer = EmployerService.initModel();
        });
    };

    $scope.searchPerson = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            component: 'personSearchDialog'
        });

        modalInstance.result.then(function (person) {
            if (person) {
                $scope.setPerson(person);
            }
        }, function () {

        });
    };

    $scope.setPerson = function(person) {
        $timeout(function(){
            $scope.model.person = person;
        });
    };

    $scope.cleanPerson = function() {
        $timeout(function(){
            $scope.model.person = PersonService.initModel();
        });
    };
});
