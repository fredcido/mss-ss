app.directive('personName', function($translate) {
    return {
        restrict: 'E',
        scope: {
            person: "<"
        },
        template: '<span translate="person"></span>: <span ng-if="person.internalCode"><b>{{ person.internalCode }} - </b></span><b>{{ person.name }}</b><span ng-if="person.securityCode"> - <b>[{{ person.securityCode }}]</b></span><p><small><span translate="created_at"></span> : {{ ::person.created_at | date:"dd/MM/yyyy HH:mm" }}</small></p>',
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                setPerson(e.person);
            });

            var setPerson = function(value) {
                scope.person = value;
            };
        }
    };
});
