app.component('employerSearchDialog', {
    templateUrl: 'views/components/employerSearchDialog.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function($translate, GenericService, ViewHelper) {
        var self = this;
        self.employer = {};
 
        self.gridAction = [{
            title: $translate.instant('select'),
            callback: function(employer){
                self.close({$value: employer});
            }
        }];

        self.closeModal = function() {
            self.close({$value: null});
        };

        self.$onInit = function() {
            self.gridParams = self.resolve;
        };
    }
});
