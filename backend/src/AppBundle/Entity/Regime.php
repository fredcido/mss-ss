<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Regime
 *
 * @ORM\Table(name="regime")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegimeRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 */
class Regime
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="fullTime", type="smallint", options={"default": 0})
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $fullTime = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Regime
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getFullTime()
    {
        return $this->fullTime;
    }

    /**
     * @param int $fullTime
     *
     * @return self
     */
    public function setFullTime($fullTime)
    {
        $this->fullTime = (int)$fullTime;
        return $this;
    }
}
