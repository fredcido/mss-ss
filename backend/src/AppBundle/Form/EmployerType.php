<?php

namespace AppBundle\Form;

use AppBundle\Form\AddressType;
use AppBundle\Form\BankInformationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('shortName')
            ->add('commercialName')
            ->add('taxNumber')
            ->add('activitySectors')
            ->add('numWorkers')
            ->add('numWorkersNational')
            ->add('files')
            ->add('internalCode')
            ->add('version')
            ->add('status')
            ->add('dateStartActivity', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('dateEndActivity', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('dateEmployees', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('sector')
            ->add('subscriptionStatus');

        $builder->add(
            'contactDetail',
            ContactDetailType::class,
            array('required' => false, 'error_bubbling' => false)
        );

        $builder->add(
            'staff',
            CollectionType::class,
            array(
                'entry_type'   => StaffType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false
            )
        );

        $builder->add(
            'subsidiaries',
            CollectionType::class,
            array(
                'entry_type'     => SubsidiaryType::class,
                'allow_add'      => true,
                'by_reference'   => false,
                'allow_delete'   => true,
                'error_bubbling' => false
            )
        );

        $builder->add(
            'bankInformation',
            CollectionType::class,
            array(
                'entry_type'   => BankInformationType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false
            )
        );

        $builder->add(
            'addresses',
            CollectionType::class,
            array(
                'entry_type'   => AddressType::class,
                'allow_add'    => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Employer',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_employer';
    }

}
