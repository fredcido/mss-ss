<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RemunerationDay
 *
 * @ORM\Table(name="remuneration_day")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RemunerationDayRepository")
 */
class RemunerationDay
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="day", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $day;

    /**
     * @var int
     *
     * @ORM\Column(name="worked", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $worked;

    /**
     * @var RemunerationPerson
     *
     * @ORM\ManyToOne(targetEntity="RemunerationPerson", inversedBy="remunerationDays")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $remunerationPerson;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return RemunerationDay
     */
    public function setDay($day)
    {
        $this->day = (int)$day;
        return $this;
    }

    /**
     * Get day
     *
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set worked
     *
     * @param integer $worked
     *
     * @return RemunerationDay
     */
    public function setWorked($worked)
    {
        $this->worked = (int)$worked;
        return $this;
    }

    /**
     * Get worked
     *
     * @return int
     */
    public function getWorked()
    {
        return $this->worked;
    }

    /**
     * Set remunerationPerson
     *
     * @param RemunerationPerson $remunerationPerson
     *
     * @return RemunerationDay
     */
    public function setRemunerationPerson(RemunerationPerson $remunerationPerson = null)
    {
        $this->remunerationPerson = $remunerationPerson;

        return $this;
    }

    /**
     * Get remunerationPerson
     *
     * @return RemunerationPerson
     */
    public function getRemunerationPerson()
    {
        return $this->remunerationPerson;
    }
}
