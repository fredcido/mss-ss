<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 *
 */
trait EntityTreeTrait
{
    /**
     * Add child
     *
     * @param ctivitySector $child
     *
     * @return Sector
     */
    public function addChild($child)
    {
        $this->children[] = $child;
        return $this;
    }

    /**
     * Remove child
     *
     * @param ctivitySector $child
     */
    public function removeChild($child)
    {
        $this->children->removeElement($child);
    }

    public function cleanChildren()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
