app.controller("HomeDashboardController", 
    function (
        $scope, 
        $translate, 
        $q,
        $state,
        GenericService, 
        AuthService,
        UserService,
        ViewHelper
    ) {

    var user = AuthService.getUserLogged();
    if (user.isStaff()) {
        $state.go('app.home');
    } else {
        $state.go('app.home-company');
    }

    var stats = {
        employers: 0,
        employees: 0,
        statements: 0,
        optional: 0
    };

    var charts = {

    };

    var dashboardStatsUrl = 'dashboard/stats/';
    var dashboardChartsUrl = 'dashboard/charts/';

    $scope.stats = stats;
    $scope.charts = charts;
    $scope.hasCharts = function() {
        return Object.keys($scope.charts).length;
    }

    var statsFilters = {};
    var chartsFilter = {};

    function fetchStats(stats) {
        var container = '.mini-stat.' + stats;
        var url = dashboardStatsUrl + stats;

        ViewHelper.blockContainer(container, $translate.instant('loading'));

        GenericService.httpGet(url, statsFilters).then(function(response){
            $scope.stats[stats] = response.data.stat;
        }, function(){
            $scope.stats[stats] = 0;
        }).finally(function(){
            ViewHelper.unblockContainer(container);
        });
    }

    function fetchChartData(chart) {
        var container = '.chart.' + chart;
        var url = dashboardChartsUrl + chart;

        ViewHelper.blockContainer(container, $translate.instant('loading'));

        return GenericService.httpGet(url, chartsFilter).then(function(response){
            return $q.when(response.data);
        }, function(){
            return $q.reject(null);
        }).finally(function(){
            ViewHelper.unblockContainer(container);
        });
    }

    function initChartDistrictEmployer() {
        var chartName = 'district-employer';
        fetchChartData(chartName).then(
            function(data) {
                //if (data.length < 1) return;

                data = _.map(data, function(item, key){
                    return {
                        name: key,
                        y: item
                    }
                });

                var options = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: $translate.instant('chart_title_district_employer')
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    series: [{
                        name: $translate.instant('municipio'),
                        data: data
                    }]
                };

                $scope.charts[chartName] = options;
            }
        );
    }

    function initChartDistrictEmployee() {
        var chartName = 'district-employee';
        fetchChartData(chartName).then(
            function(data) {
                //if (data.length < 1) return;

                data = _.map(data, function(item, key){
                    return {
                        name: key,
                        y: item
                    }
                });

                var options = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: $translate.instant('chart_title_district_employee')
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    series: [{
                        name: $translate.instant('municipio'),
                        colorByPoint: true,
                        data: data
                    }]
                };

                $scope.charts[chartName] = options;
            }
        );
    }

    function initChartRemunerationMonth() {
        var chartName = 'remuneration-month';
        fetchChartData(chartName).then(
            function(data) {
                //if (data.length < 1) return;

                var chartData = {
                    categories: [],
                    data: [],
                };

                _.map(data, function(item, key){
                    chartData.categories.push(key);
                    chartData.data.push(item);
                });

                var options = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: $translate.instant('chart_title_remuneration_month')
                    },
                    xAxis: {
                        categories: chartData.categories
                    },
                    series: [{
                        name: $translate.instant('month'),
                        data: chartData.data
                    }]
                }

                $scope.charts[chartName] = options;
            }
        );
    }

    function initChartOptionalGender() {
        var chartName = 'optional-gender';
        fetchChartData(chartName).then(
            function(data) {
                //if (data.length < 1) return;

                var chartData = {
                    male: [],
                    female: [],
                    categories: []
                };

                _.map(data, function(item, key){
                    chartData.categories.push(key);
                    chartData.male.push(item.male);
                    chartData.female.push(item.female);
                });

                var options = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: $translate.instant('chart_title_optional_gender')
                    },
                    xAxis: {
                        categories: chartData.categories
                    },
                    series: [
                        {
                            name: $translate.instant('male'),
                            data: chartData.male
                        },
                        {
                            name: $translate.instant('female'),
                            data: chartData.female
                        }
                    ]
                }

                $scope.charts[chartName] = options;
            }
        );
    }

    function init() {
        _.forEach(stats, function(value, key) {
            fetchStats(key);
        });

        initChartDistrictEmployer();
        initChartDistrictEmployee();
        initChartRemunerationMonth();
        initChartOptionalGender();
    }

    init();

    /*GenericService.httpGet('dashboard/chart-contribution-by-gender-by-localization').then(function(response) {
        var data = response.data;
        $scope.chartContributionByGenderByLocalization = {
            options: {
                credits: false,
                chart: {
                    type: "column"
                },
                title: {
                    text: $translate.instant('chart_contribution_by_gender_by_localization')
                },
                xAxis: {
                    type: 'category'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                tooltip: {
                    headerFormat: "<span style=\"font-size:11px\">{series.name}</span><br>",
                    pointFormat: "<span style=\"color:{point.color}\">{point.name}</span>: <b>{point.y}</b><br/>"
                },
                drilldown: data.drilldown,
            },
            series: data.series
        };
    });*/
});
