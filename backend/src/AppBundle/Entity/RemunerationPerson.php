<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RemunerationPerson
 *
 * @ORM\Table(name="remuneration_person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RemunerationPersonRepository")
 */
class RemunerationPerson
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="workedDays", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $workedDays;

    /**
     * @var string
     *
     * @ORM\Column(name="remuneration", type="decimal", precision=10, scale=2)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $remuneration;

    /**
     * @var int
     *
     * @ORM\Column(name="period", type="smallint", options={"default" : 1})
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $period = 1;

    /**
     * @var RemunerationStatement
     *
     * @ORM\ManyToOne(targetEntity="RemunerationStatement", inversedBy="remunerationPeople")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $remunerationStatement;

    /**
     * @var CompulsoryAccessionSubscription
     *
     * @ORM\ManyToOne(targetEntity="CompulsoryAccessionSubscription")
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $compulsoryAccessionSubscription;

    /**
     * @ORM\OneToMany(targetEntity="RemunerationDay", mappedBy="remunerationPerson", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JMS\Groups({"details"})
     *
     * @var RemunerationDay
     */
    private $remunerationDays;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->remunerationDays = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set workedDays
     *
     * @param integer $workedDays
     *
     * @return RemunerationPerson
     */
    public function setWorkedDays($workedDays)
    {
        $this->workedDays = $workedDays;

        return $this;
    }

    /**
     * Get workedDays
     *
     * @return int
     */
    public function getWorkedDays()
    {
        return $this->workedDays;
    }

    /**
     * Set remuneration
     *
     * @param string $remuneration
     *
     * @return RemunerationPerson
     */
    public function setRemuneration($remuneration)
    {
        $this->remuneration = $remuneration;

        return $this;
    }

    /**
     * Get remuneration
     *
     * @return string
     */
    public function getRemuneration()
    {
        return $this->remuneration;
    }

    /**
     * Set period
     *
     * @param integer $period
     *
     * @return RemunerationPerson
     */
    public function setPeriod($period)
    {
        $this->period = (int)$period;
        return $this;
    }

    /**
     * Get period
     *
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set compulsoryAccessionSubscription
     *
     * @param CompulsoryAccessionSubscription $compulsoryAccessionSubscription
     *
     * @return RemunerationPerson
     */
    public function setCompulsoryAccessionSubscription(CompulsoryAccessionSubscription $compulsoryAccessionSubscription = null)
    {
        $this->compulsoryAccessionSubscription = $compulsoryAccessionSubscription;

        return $this;
    }

    /**
     * Get compulsoryAccessionSubscription
     *
     * @return CompulsoryAccessionSubscription
     */
    public function getCompulsoryAccessionSubscription()
    {
        return $this->compulsoryAccessionSubscription;
    }

    /**
     * Set remunerationStatement
     *
     * @param RemunerationStatement $remunerationStatement
     *
     * @return RemunerationPerson
     */
    public function setRemunerationStatement(RemunerationStatement $remunerationStatement = null)
    {
        $this->remunerationStatement = $remunerationStatement;

        return $this;
    }

    /**
     * Get remunerationStatement
     *
     * @return RemunerationStatement
     */
    public function getRemunerationStatement()
    {
        return $this->remunerationStatement;
    }

    /**
     * Add remunerationDay
     *
     * @param RemunerationDay $remunerationDay
     *
     * @return RemunerationPerson
     */
    public function addRemunerationDay(RemunerationDay $remunerationDay)
    {
        $this->remunerationDays[] = $remunerationDay;
        $remunerationDay->setRemunerationPerson($this);
        return $this;
    }

    /**
     * Remove remunerationDay
     *
     * @param RemunerationDay $remunerationDay
     */
    public function removeRemunerationDay(RemunerationDay $remunerationDay)
    {
        $this->remunerationDays->removeElement($remunerationDay);
    }

    /**
     * Get remunerationDays
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRemunerationDays()
    {
        return $this->remunerationDays;
    }
}
