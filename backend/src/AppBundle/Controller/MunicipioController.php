<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Municipio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class MunicipioController extends BaseController
{
    protected $repository = 'AppBundle:Municipio';

    protected $form = 'AppBundle\Form\MunicipioType';

    protected $routeGet = 'app.municipio.get';

    protected $entity = Municipio::class;
}
