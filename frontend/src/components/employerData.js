app.component('employerData', {
    templateUrl: 'views/components/employerData.html',
    bindings: {
        employer: '=',
        addAddress: '<',
        endActivity: '<',
        disabled: '<',
        hideNumWorker: '<'
    },
    controller: function($translate, $timeout, GenericService) {
        var self = this;

        self.calendarOptions = {
            minMode: 'month'
        };

        var getActivitiesSectors = function () {
            GenericService.httpGet('activity-sector/tree').then(function (response) {
                self.activitiesSectorsTree = GenericService.addTreeLevel(response.data);
                filterDuplicatedActivities();
            });
        };

        var filterDuplicatedActivities = function() {
            if (!self.employer || !self.employer.activitySectors 
                || !self.employer.activitySectors.length || !self.activitiesSectorsTree) return;

            self.activitiesSectorsTree = self.activitiesSectorsTree.filter(function(item){
                return !self.employer.activitySectors.some(function(row){
                    return row.id == item.id;
                });
            });
        };

        self.$onChanges = function() {
            filterDuplicatedActivities();
        };

        self.$onInit = function() {
            if (angular.isUndefined(self.addAddress)) {
                self.addAddress = true;
            }

            if (angular.isUndefined(self.endActivity)) {
                self.endActivity = false;
            }

            self.activitiesSectorsTree = [];
            self.hideNumWorker = !_.isUndefined(self.hideNumWorker) || self.hideNumWorker;
            self.curDate = moment().toDate();

            getActivitiesSectors();
        };
    }
});
