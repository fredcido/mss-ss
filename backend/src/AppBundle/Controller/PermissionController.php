<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class PermissionController extends BaseController
{
    protected $repository = 'AppBundle:Post';

    protected $form = 'AppBundle\Form\PostType';

    protected $routeGet = 'app.post.get';

    protected $entity = Post::class;
}
