<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Village
 *
 * @ORM\Table(name="village")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VillageRepository")
 */
class Village
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use ReferenceCodeTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Suku", inversedBy="villages")
     * @Assert\NotBlank(message = "not_blank")
     * @ORM\JoinColumn(name="suku_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $suku;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Village
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of suku.
     *
     * @return mixed
     */
    public function getSuku()
    {
        return $this->suku;
    }

    /**
     * Sets the value of suku.
     *
     * @param mixed $suku the suku
     *
     * @return self
     */
    public function setSuku(Suku $suku)
    {
        $this->suku = $suku;
        return $this;
    }
}
