<?php

namespace AppBundle\Form;

use AppBundle\Form\DataTransformer\OptionalStepTransformer;
use AppBundle\Form\WorkSituationType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionalAccessionType extends AbstractType
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('person')
            ->add('optionalContributionGroup')
            ->add('dateStart', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('dateEnd', DateType::class, array(
                'widget' => 'single_text',
                'html5' => true
            ))
            ->add('version')
            ->add('subscriptionStatus');

        $builder->add(
            'workSituation',
            WorkSituationType::class,
            array(
                'required'       => true,
                'error_bubbling' => false
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\OptionalAccession',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_optionalaccession';
    }
}
