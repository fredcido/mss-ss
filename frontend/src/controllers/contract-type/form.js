app.controller("ContractTypeFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'contract-type';
    var _stateList = 'app.contract-type-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };
    $scope.labourLaws = [];

    var _getLabourLaws = function() {
        GenericService.find('labour-law').then(function(response) {
            $scope.labourLaws = response.data;
        });
    };

    _getLabourLaws();

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;

            if ($scope.model.labourLaw) {
                $scope.model.labourLaw = $scope.model.labourLaw.id;
            }
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
