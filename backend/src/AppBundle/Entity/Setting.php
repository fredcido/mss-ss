<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityAuditTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Setting
 *
 * @ORM\Table(name="setting")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SettingRepository")
 */
class Setting
{
    use EntityPermissionTrait;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     * @Assert\NotBlank(message = "not_blank")
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "max_chars"
     * )
     * @JMS\Groups({"list", "details", "form"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     * @JMS\Groups({"list", "details"})
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="measure", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $measure;

    /**
     * @var string
     *
     * @ORM\Column(name="mask", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    private $mask;

    /**
     * @var string
     *
     * @ORM\Column(name="explanation", type="string", length=1000)
     * @Assert\Length(
     *      max = 1000,
     *      maxMessage = "max_chars"
     * )
     * @JMS\Groups({"list", "details"})
     */
    private $explanation;

    /**
     * @ORM\OneToMany(targetEntity="SettingValue", mappedBy="setting", cascade={"persist"})
     * @JMS\Exclude()
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $history;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->history = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Setting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Setting
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set explanation
     *
     * @param string $value
     *
     * @return Setting
     */
    public function setExplanation($explanation)
    {
        $this->explanation = $explanation;

        return $this;
    }

    /**
     * Get explanation
     *
     * @return string
     */
    public function getExplanation()
    {
        return $this->explanation;
    }

    /**
     * Gets the value of measure.
     *
     * @return string
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * Sets the value of measure.
     *
     * @param string $measure the measure
     *
     * @return self
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;
        return $this;
    }

    /**
     * Add history
     *
     * @param SettingValue $history
     *
     * @return Setting
     */
    public function addHistory(SettingValue $history)
    {
        $this->history[] = $history;

        return $this;
    }

    /**
     * Remove history
     *
     * @param SettingValue $history
     */
    public function removeHistory(SettingValue $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Gets the value of mask.
     *
     * @return string
     */
    public function getMask()
    {
        return $this->mask;
    }

    /**
     * Sets the value of mask.
     *
     * @param string $mask the mask
     *
     * @return self
     */
    public function setMask($mask)
    {
        $this->mask = $mask;
        return $this;
    }
}
