app.component('inlineSearchEmployer', {
    templateUrl: 'views/components/inlineSearchEmployer.html',
    bindings: {
        callback: '&',
        employer: '=',
        hideSearchButton: '<',
        clearAfterFound: '<',
        employerRequired: '<'
    },
    controller: function($translate, $timeout, $uibModal, $element, GenericService, ViewHelper) {
        var self = this;
        var _uri = 'employer/search';
        var modal = '#modal-inline-search-employer';

        self.$onInit = function() {
            self.search = {filter: 'securityCode'};
            self.peopleList = [];
            self.actionsEmployersList = [{
                title: $translate.instant('select'),
                callback: function(employer) {
                    $(modal).modal('toggle');
                    callbackEmployer(employer);
                }
            }];
            self.employerNotFound = false;

            self.filters = [
                {id: 'securityCode', name: $translate.instant('security_code')},
                {id: 'internalCode', name: $translate.instant('internal_code')},
                {id: 'taxNumber', name: $translate.instant('tax_number')},
            ];
        };

        self.$onChanges = function(changesObj) {
            var configItems = ['hideSearchButton', 'clearAfterFound', 'employerRequired'];
            configItems.map(function(item){
                if (changesObj[item]) {
                    self[item] = !angular.isUndefined(changesObj[item].currentValue);
                }
            });
        };

        self.employerModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                component: 'employerSearchDialog'
            });

            modalInstance.result.then(function (employer) {
                if (employer) {
                    callbackEmployer(employer);
                }
            }, function () {

            });
        };

        self.searchEmployer = function() {
            if (!self.search.term) return;

            var termTyped = angular.copy(self.search.term);
            $timeout(function(){
                if (termTyped === self.search.term) {
                    self.validateEmployer();
                }
            },1000);
        };

        self.validateEmployer = function() {
            self.employer = {};
            self.employerNotFound = false;
            self.displayEmployer = false;

            $timeout(
                function() {
                    ViewHelper.blockContainer($element.find('.inline-search-employer'), $translate.instant('loading'));
                    GenericService.httpGet(_uri, self.search).then(function(response) {
                        var data = response.data;

                        if (data.length > 1) {
                            self.employersList = data;
                            $(modal).modal('toggle');
                        } else if (data.length == 1) {
                            callbackEmployer(data[0]);
                        } else {
                            self.employerNotFound = true;
                            $timeout(
                                function() {
                                    self.employerNotFound = false;
                                },
                                5000
                            );
                        }

                        ViewHelper.unblockContainer($element.find('.inline-search-employer'));
                    });
                },
                500
            );
        };

        var callbackEmployer = function(employer) {
            self.employer = employer;
            self.employer.employerDisplay = employer.internalCode + ' - ' + employer.name;
            
            self.filters.some(function(filter){
                if (employer[filter.id]) {
                    self.search.filter = filter.id;
                    self.search.term = employer[filter.id];
                    return true;
                }
            });

            if (typeof self.callback == 'function') {
                self.callback({employer: employer});
            }

            if (self.clearAfterFound) {
                self.employer = {};
                self.search.term = null;
            } else {
                self.displayEmployer = true;
            }

            self.employerNotFound = false;
        };
    }
});
