<?php

namespace AppBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EntityEmbeddedTransformer implements DataTransformerInterface
{
    private $manager;

    private $repository;

    public function __construct(EntityManagerInterface $manager, $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * Transforms an object, keep as is
     *
     * @param  $entity
     * @return string
     */
    public function transform($entity)
    {
        return $entity;
    }

    /**
     * Transforms an array to entity
     *
     * @param  string $data
     * @return $entity
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($data)
    {
        dump($data);exit;
        if (!is_array($data) || empty($data['id'])) {
            return $data;
        }

        $entity = $this->manager
            ->getRepository($this->repository)
            ->find($data['id']);

        if (null === $entity) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An entity with ID "%s" does not exist!',
                $id
            ));
        }

        return $entity;
    }
}
