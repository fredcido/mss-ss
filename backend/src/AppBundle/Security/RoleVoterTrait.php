<?php

namespace AppBundle\Security;

use AppBundle\Entity\Employer;
use AppBundle\Entity\EntityPermissionTrait;
use AppBundle\Security\AclBuilder;
use AppBundle\Security\Actions;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Doctrine\Common\Persistence\Proxy;

trait RoleVoterTrait
{
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, Actions::getAll())) {
            return false;
        }

        $subject = $this->getEntityClass($subject);

        if (!in_array($subject, AclBuilder::fixedPermissions()) && !in_array(EntityPermissionTrait::class, class_uses($subject))) {
            return false;
        }

        return true;
    }


    protected function getEntityClass($subject)
    {
        if (is_object($subject)) {
            $subject = ($subject instanceof Proxy)
                ? get_parent_class($subject)
                : get_class($subject);
        }

        return $subject;
    }
}
