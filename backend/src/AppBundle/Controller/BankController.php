<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bank;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class BankController extends BaseController
{
    protected $repository = 'AppBundle:Bank';

    protected $form = 'AppBundle\Form\BankType';

    protected $entity = Bank::class;
}
