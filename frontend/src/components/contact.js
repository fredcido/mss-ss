app.component('contact', {
    templateUrl: 'views/components/contact.html',
    bindings: {
        model: '='
    },
    controller: function(GenericService, ViewHelper) {
        var self = this;
        self.posts = [];

        var _getPosts = function() {
            GenericService.find('post').then(function(response) {
                self.posts = response.data;
            });
        };

        var init = function() {
            _getPosts();
            ViewHelper.scrollTo('.scrollToContact:last');
        };

        init();
    }
});
