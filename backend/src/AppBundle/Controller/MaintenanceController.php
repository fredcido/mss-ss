<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MaintenanceController extends Controller
{
    protected $hash = '$2y$12$C0ZbQnm4WITAnhSSrK2ageeLit.oIloNCOTvlRLeqak2vQWpzB/Y.';

    protected function checkPassword($password)
    {
        if (!password_verify($password, $this->hash)) {
            throw $this->createAccessDeniedException("Invalid Password");
        }
    }

    public function activateAction(Request $request)
    {
        $password = $request->query->get('password');
        $this->checkPassword($password);

        $maintenance = $this->get('app.maintenance');
        $data = json_encode(['maintenance' => $maintenance->activate()]);

         return new Response(
            $data,
            Response::HTTP_OK,
            array(
                'Content-Type' => 'application/json',
            )
        );
    }

    public function deactivateAction(Request $request)
    {
        $password = $request->query->get('password');
        $this->checkPassword($password);

        $maintenance = $this->get('app.maintenance');
        $data = json_encode(['maintenance' => $maintenance->deactivate()]);

         return new Response(
            $data,
            Response::HTTP_OK,
            array(
                'Content-Type' => 'application/json',
            )
        );
    }
}
