<?php

namespace AppBundle\Controller;

use AppBundle\Entity\RemunerationStatement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Security("has_role('ROLE_USER')")
 */
class RemunerationStatementController extends BaseController
{
    protected $repository = 'AppBundle:RemunerationStatement';

    protected $form = 'AppBundle\Form\RemunerationStatementType';

    protected $entity = RemunerationStatement::class;
}
