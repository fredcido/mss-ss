<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\Employer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class EmployerSecurityCodeSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    protected $securityCodeGenerator;
    protected $needsFlush;

    public function setSecurityCodeGenerator($service)
    {
        $this->securityCodeGenerator = $service;
    }

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [Events::postPersist, Events::postUpdate, Events::postFlush];
    }

    protected function performInjection(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em     = $args->getEntityManager();

        if (!$entity instanceof Employer) {
            return;
        }

        if ($this->securityCodeGenerator->hasSecurityCode($entity)) {
            return;
        }

        $this->securityCodeGenerator->generate($entity);
        $em->persist($entity);
        $this->needsFlush = true;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->performInjection($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->performInjection($args);
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        if ($this->needsFlush) {
            $this->needsFlush = false;
            $eventArgs->getEntityManager()->flush();
        }
    }
}
