app.controller("OccupationFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'occupation';
    var _stateList = 'app.occupation-list';
    $scope.wears = [];

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1, occupationWears: [] };

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            var model = response.data;
            if (model.occupationWears) {
                model.occupationWears.map(function(item){
                    item.wear = item.wear.id;
                });
            }
            $scope.model = model;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };

    var _getWears = function () {
        GenericService.find('wear').then(function (response) {
            $scope.wears = response.data;
        });
    };

    $scope.addOccupationWear = function () {
        $scope.model.occupationWears.push({
            wear: null,
            years: 0
        });
    };

    $scope.removeOccupationWear = function (occupation) {
        $scope.model.occupationWears.some(function (i) {
            if (i == occupation) {
                $scope.model.occupationWears.splice($scope.model.occupationWears.indexOf(i), 1);
            }
        });
    };

    _getWears();
});
