<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feedback;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 */
class FeedbackController extends BaseController
{
    protected $repository = 'AppBundle:Feedback';

    protected $form = 'AppBundle\Form\FeedbackType';

    protected $entity = Feedback::class;

    /**
     * @param  int  $id
     * @param  Request $request
     * @return Response
     */
    public function findByEmployerAction($id, Request $request)
    {
        $repository = $this->getRepository();
        $entities = $repository->findByEmployer($id);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('list', 'audit'));
    }
}
