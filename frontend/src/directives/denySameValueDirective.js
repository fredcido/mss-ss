app.directive('denySameValue', [function() {
    var denySameValueItems = {};
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var index = (new Date()).getTime();
            denySameValueItems[index] = ctrl;

            var checkReference = function()
            {
                var valuesToCheck = {};
                angular.forEach(
                    denySameValueItems,
                    function(item) {
                        var itemValue = angular.isObject(item.$viewValue) ? item.$viewValue.id : item.$viewValue;
                        if (valuesToCheck[itemValue] === undefined) {
                            valuesToCheck[itemValue] = [];
                        }
                        valuesToCheck[itemValue].push(item);
                    }
                );
                
                angular.forEach(
                    valuesToCheck,
                    function(items) {
                        var valid = items.length <= 1;
                        items.map(function (item) {
                            item.$setValidity('denySameValue', valid);
                        });
                    }
                );
            };

            scope.$on('$destroy', 
                function()
                {
                    delete denySameValueItems[index];
                    checkReference();
                }
            );

            scope.$watch(function() {
                return ctrl.$viewValue;
            }, function(values) {
                checkReference()
            }, true);
        }
    };
}]);
