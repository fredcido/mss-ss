<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ComputeChangeTrait;
use AppBundle\Entity\RemunerationStatement;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class RemunerationStatementSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;
    use ComputeChangeTrait;

    /**
     * (non-PHPdoc).
     *
     * @see \Doctrine\Common\EventSubscriber::getSubscribedEvents()
     */
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }

    public function onFlush(OnFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();
        $em     = $args->getEntityManager();

        $injectEntities = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof RemunerationStatement) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof RemunerationStatement) {
                continue;
            }

            $injectEntities[] = $entity;
        }

        $emailRemuneration = $this->container->get('app.mail.remuneration_statement');
        foreach ($injectEntities as $entity) {
            $emailQueue = $emailRemuneration->newStatement($entity);
            $this->compute($em, $emailQueue);
        }
    }
}
