app.controller("WorkerListController", 
    function(
        $scope, 
        $stateParams, 
        $uibModal, 
        $translate, 
        $state, 
        $timeout,
        GenericService,
        AuthService, 
        EnumService, 
        ViewHelper,
        CompulsoryAccessionService
    ) {

    $scope.suspend = parseInt($stateParams.suspend);
    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.user = AuthService.getUserLogged();

    $scope.editWorker = function(person, employer) {
        $state.go('app.worker-edit', {
            id: person.internalCode,
            employer: employer.internalCode,
            updateSubscription: $scope.updateSubscription
        });
    };

    $scope.suspendWorker = function(person, employer) {
        $state.go('app.worker-suspension', {
            id: person.internalCode,
            employer: employer.internalCode,
        });
    };

    var btnLabel = $translate.instant('edit');
    var btnIcon = 'fa-pencil';
    if ($scope.suspend) {
        btnLabel = $translate.instant('suspend_cease');
        btnIcon = 'fa-minus-circle';
    }

    if ($scope.user.isStaff()) {
        btnLabel = $translate.instant('view');
        btnIcon = 'fa-eye';   
    }

    $scope.actionsGrid = [{
        show: function(row) {
            if ($scope.user.isEmployer()) {
                return true;
            }
            
            subscription = CompulsoryAccessionService.populateSubscriptionModel(row);
            return subscription.isActive();
        },
        title: function(row)
        { 
            return btnLabel;
        },
        icon: function(row){
            return btnIcon;
        },
        callback: function(subscription) {
            if ($scope.suspend) {
                $scope.suspendWorker(subscription.person, subscription.workSituation.employer);
            } else {
                $scope.editWorker(subscription.person, subscription.workSituation.employer);
            }
        }
    }];

    if (!$scope.suspend) {
        var viewChangesButton = {
            title: $translate.instant('history'),
            icon: 'fa-history',
            callback: function(subscription) {
                $state.go(
                    'app.worker-list-changes', 
                    {
                        id: subscription.person.internalCode,
                        employer: subscription.workSituation.employer.internalCode,
                    }
                );
            }
        };
        $scope.actionsGrid.push(viewChangesButton);
    }

    var filters = {
        version: EnumService.getVersion('standard'),
        all_status: 1
    };

    if ($scope.suspend) {
        if (!$scope.user.isStaff()) {
            filters.notSubscriptionStatus = [
                EnumService.getSubscriptionStatus('pending')
            ];
        } else {
            filters.hadSubscriptionStatus = [
                EnumService.getSubscriptionStatus('suspended'),
                EnumService.getSubscriptionStatus('ceased')
            ];
        }
    } else if($scope.updateSubscription) {
        filters.notSubscriptionStatus = [
            EnumService.getSubscriptionStatus('pending')
        ];
    }

    if ($scope.user.isEmployer()) {
        // If the employer is still pending, request the submission
        if ($scope.user.employer.isPending()) {
            swal({ 
                title: $translate.instant('company_not_submitted'),
                text: $translate.instant('company_not_submitted_text'),
                type: "error" 
              },
              function(){
                $timeout(function(){
                    $state.go('app.company-form', {
                        id: $scope.user.employer.id,
                        ignoreLocalStorage: true,
                        updateSubscription: '0'
                    });
                });
            });
        }

        // if the company was not approved yet, does not allow 
        // to submit changes
        if ($scope.user.employer.isSubmitted()) {
            swal({ 
                title: $translate.instant('company_not_approved'),
                text: $translate.instant('company_not_approved_text'),
                type: "error" 
              },
              function(){
                $timeout(function(){
                    $state.go('app.company-form', {
                        id: $scope.id,
                        ignoreLocalStorage: true,
                        updateSubscription: '0'
                    });
                });
            });
        }

        $scope.employer = $scope.user.employer;
        filters.employer = $scope.employer.id;
    }

    if ($scope.user.isPerson()) {
        $scope.person = $scope.user.person;
        filters.person = $scope.person.internalCode;
    }

    var search = function() {
        ViewHelper.blockContainer('.panel', $translate.instant('loading'));
        var uri = 'compulsory-subscription/search';
        GenericService.httpGet(uri, filters).then(function(response) {
            $scope.rows = response.data;
            ViewHelper.unblockContainer('.panel');
        });
    };

    search();
});
