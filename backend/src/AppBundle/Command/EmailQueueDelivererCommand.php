<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DBLS\Bundle\JerseyColoursBundle\Adapters\FileAdapter;

class EmailQueueDelivererCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mss:email:deliver')
             ->addOption(
                'limit',
                null,
                InputOption::VALUE_OPTIONAL,
                'The number of e-mails to be delivered.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');

        $limit = (int)$input->getOption('limit');

        $today = new \DateTime('now');
        $emailQueueRepository = $em->getRepository('AppBundle:EmailQueue');
        $pool = $emailQueueRepository->findEmailToSend($today, $limit);

        // Mail parameters
        $mailFrom = $container->getParameter('mail_from');
        $mailAdmin = $container->getParameter('mail_admin');
        $mailName = $container->getParameter('mail_name');

        $sent = [];
        $mailer = $container->get('mailer');
        foreach ($pool as $mail) {
            try {
                $message = \Swift_Message::newInstance()
                            ->setSubject($mail->getSubject())
                            ->setFrom($mail->getFromMail())
                            ->setReplyTo($mailFrom)
                            ->setTo($mail->getToMail())
                            ->setBcc($mailAdmin)
                            ->setBody($mail->getBody(), 'text/html');

                $mailer->send($message);

                // Set the message to be sent
                $mail->setSent(1)->setSentAt($today);

                $sent[] = $mail;
            } catch (\Exception $e) {
                $mail->setError($e->getTraceAsString());
            }

            $em->persist($mail);
        }

        $poolOldMessages = $emailQueueRepository->findOldSentMessages($limit);
        foreach ($poolOldMessages as $message) {
            $em->remove($message);
        }

        $em->flush();
        
        $output->writeln(sprintf('%s messages delivered', count($sent)));
        $output->writeln(sprintf('%s old messages removed', count($poolOldMessages)));
    }
}
