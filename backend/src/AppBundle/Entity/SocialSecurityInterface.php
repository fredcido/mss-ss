<?php

namespace AppBundle\Entity;

interface SocialSecurityInterface
{
    public function setSecurityCode($securityCode);

    public function getSecurityCode();

    public function getSecurityCodePrefix();
    
    public function getId();
}
