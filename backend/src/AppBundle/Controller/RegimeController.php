<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Regime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class RegimeController extends BaseController
{
    protected $repository = 'AppBundle:Regime';

    protected $form = 'AppBundle\Form\RegimeType';

    protected $routeGet = 'app.regime.get';

    protected $entity = Regime::class;
}
