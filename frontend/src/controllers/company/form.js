app.controller(
    "CompanyFormController", 
    function(
        $rootScope,
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $uibModal, 
        $translate, 
        ViewHelper, 
        EnumService,
        GenericService, 
        PersonService, 
        EmployerService, 
        AuthService,
        SettingService
    ) {

    var _endpoint = "employer";

    $scope.sectorsTree = [];
    $scope.user = AuthService.getUserLogged();
    $scope.id = $stateParams.id || null;
    $scope.updateSubscription = parseInt($stateParams.updateSubscription);
    $scope.version = $stateParams.version;

    $scope.calendarOptions = {};
    $scope.calendar = {};

    $scope.errorDeadline = {};
    $scope.updateDeadline = null;

    $scope.standard = null;
    $scope.model = null;

    var getSectors = function() {
        GenericService.httpGet('sector/tree').then(function(response) {
            $scope.sectorsTree = GenericService.addTreeLevel(response.data);
            filterDuplicatedSectors();
        });
    };

    var filterDuplicatedSectors = function() {
        if (!$scope.model || !$scope.model.sector || !$scope.sectorsTree.length) return;

        $scope.sectorsTree = $scope.sectorsTree.filter(function(item){
            return $scope.model.sector.id != item.id;
        });
    };

    var callBackSave = function(response) {
        var data = response.data;
        ViewHelper.unblockContainer('.container');

        $scope.model = EmployerService.populateModel(data);
        if ($scope.user.isEmployer()) {
            $scope.user.employer = $scope.model;
            AuthService.setUser($scope.user).then(
                function(){
                    $rootScope.$broadcast('userChanged');
                }
            );
        }

        ViewHelper.scrollTo('.inss-logo');

        if (!$scope.updateSubscription) {
            if ($scope.isSubmitted()) {
                // If previously we had an errored subscription, we just thank the user
                if ($scope.oldSubscriptionStatus == EnumService.getSubscriptionStatus('error')) {
                    $state.go('app.company-thankyou');
                } else {
                    askToCreateTCO();
                }
            } else {
                askToSubmit();
            }

            $scope.updateSubscription = $scope.isSubmitted() ? 1 : 0;
        } else {
            swal({
                title: $translate.instant('success_title'),
                text: $translate.instant('success_text'),
                type: 'success'
            }, function(){
                $state.go('app.company-list-changes', {
                    id: $scope.id,
                });
            });
        }
    };
 
    var askToSubmit = function() {
        swal({
                title: $translate.instant('company_ask_submit_title'),
                text: $translate.instant('company_ask_submit_message'),
                type: 'success',
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $timeout(function() {
                        $scope.submit($scope.form, $scope.model);
                    }, 500);
                } else {
                    $timeout(function() {
                        ViewHelper.scrollTo('.inss-logo');
                    }, 500);
                }
            }
        );
    };

    var askToCreateTCO = function() {
        swal({
                title: $translate.instant('company_add_employee_title'),
                text: $translate.instant('company_add_employee_message'),
                type: 'success',
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (!isConfirm) {
                     $timeout(function() {
                        $state.go('app.company-thankyou');
                    }, 500);
                } else {
                    $state.go('app.employer-worker-form', {
                        employer: $scope.id,
                        ignoreLocalStorage: true,
                        fromCompany: 1
                    });
                }
            }
        );
    };

    var callBackErrorSubmit = function(form) {
        return function(response) {
            ViewHelper.errorServer(form, response);
            ViewHelper.unblockContainer('.container');
        }
    };

    var initErrored = function() {
        var days = SettingService.get('days_open_subscription'); 
        $scope.errorDeadline = {
            days: days
        };

        var dateStart = moment($scope.standard.subscriptionChangedAt).format('YYYY-MM-DD');
        var url = 'calendar/add-work-days/' + dateStart + '/' + days;
        GenericService.httpGet(url).then(function(response){
            $scope.errorDeadline.moment = moment(response.data.day);
            $scope.errorDeadline.date = $scope.errorDeadline.moment.format('DD/MM/YYYY');    
        });
    };

    var callbackSetEmployer = function(response) {
        $scope.model = EmployerService.populateModel(response.data);
        filterDuplicatedSectors();

        // If we are seeing a versioned, ignore everything else
        if ($scope.version) {
            ViewHelper.unblockContainer('.container');
            ViewHelper.scrollTo('.inss-logo');
            return;
        }

        // If it is updating the subscription
        if ($scope.updateSubscription) {

            // the entity cannot be pending
            // it needs to be submitted first
            if ($scope.model.isPending()) {
                swal({ 
                    title: $translate.instant('company_not_submitted'),
                    text: $translate.instant('company_not_submitted_text'),
                    type: "error" 
                  },
                  function(){
                    $timeout(function(){
                        $state.go('app.company-form', {
                            id: $scope.id,
                            ignoreLocalStorage: true,
                            updateSubscription: '0'
                        });
                    });
                });
            }

            // if the company was not approved yet, does not allow 
            // to submit changes
            if ($scope.model.isSubmitted()) {
                swal({ 
                    title: $translate.instant('company_not_approved'),
                    text: $translate.instant('company_not_approved_text'),
                    type: "error" 
                  },
                  function(){
                    $timeout(function(){
                        $state.go('app.company-form', {
                            id: $scope.id,
                            ignoreLocalStorage: true,
                            updateSubscription: '0'
                        });
                    });
                });
            }
        }

        // If there is a error on the subscription and we are at the subscription form
        if ($scope.standard.isErrored()) {
            // Use the entity from the standard version
            $scope.model = $scope.standard;
            initErrored();
        }

        ViewHelper.unblockContainer('.container');
        ViewHelper.scrollTo('.inss-logo');
    };

    var init = function() {
        getSectors();

        // Make sure the user can only see its own employer
        if ($scope.user.employer && $scope.user.employer.internalCode != $scope.id) {
            $scope.id = $scope.user.employer.internalCode;
        }

        if ($scope.id) {
        
            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            // Get the standard version first
            GenericService.getVersion(_endpoint, $scope.id, EnumService.getVersion('standard'))
                .then(function(response){
                    $scope.standard = EmployerService.populateModel(response.data);

                    if ($scope.version) {
                        GenericService.get(_endpoint, $scope.version).then(callbackSetEmployer)
                    } else {
                        // If it's not updating the subscription
                        if (!$scope.updateSubscription) {

                            // Gets the requested version, if not found, gets the standard version of it
                            var version = EnumService.getVersion('subscription');
                            GenericService.getVersion(_endpoint, $scope.id, version)
                                .then(callbackSetEmployer)
                                .catch(function(error){
                                    if (404 != error.status) return;
                                    
                                    // If there is no subscription version, use the standard one
                                    callbackSetEmployer(response);
                                });
                        } else {
                            callbackSetEmployer(response);
                        }
                    }
                });
        } else {
            $scope.model = EmployerService.initModel();
            $scope.model.subscriptionStatus = EnumService.getSubscriptionStatus('pending');
        }

        if ($scope.updateSubscription) {
            $scope.updateDeadline = SettingService.get('day_update_subscription');
        }
    };

    $scope.setCompany = function(employer) {
        if ($scope.user.isEmployer() && employer.internalCode != $scope.user.employer.internalCode) {
            return;
        }

        ViewHelper.blockContainer('.container', $translate.instant('loading'));
        $timeout(function() {
            $state.go(
                'app.company-form', 
                {
                    id: employer.internalCode,
                    ignoreLocalStorage: true,
                    updateSubscription: $scope.updateSubscription,
                }
            );
        }, 300);
    };

    $scope.addSubsidiary = function() {
        $scope.model.subsidiaries.push({
            addresses: [{main: 1}],
            employer: $scope.model
        });
    };

    $scope.removeSubsidiary = function(subsidiary) {
        $scope.model.subsidiaries.some(function(i) {
            if (i == subsidiary) {
                $scope.model.subsidiaries.splice($scope.model.subsidiaries.indexOf(i), 1);
            }
        });
    };

    $scope.canEdit = function() {
        canEdit = false;
        
        if ($scope.version) {
            canEdit = false;
        } else if (!$scope.model || !$scope.user.employer || $scope.model.isCeased() || $scope.model.isSuspended()) {
            canEdit = false;
        } else if ($scope.updateSubscription) {
            canEdit = (!$scope.isErrored() || $scope.isErrorExpired()) && !$scope.isExpiredUpdate();
        } else {
            canEdit = !$scope.isSubmitted() && !$scope.isErrorExpired();
        }

        return canEdit;
    };

    $scope.isSubmitted = function() {
        return $scope.model && $scope.model.isSubmitted();
    };

    $scope.isCeased = function() {
        return $scope.model && $scope.model.isCeased();
    };

    $scope.isErrored = function() {
        return $scope.model && $scope.model.isErrored();
    };

    $scope.isErrorExpired = function() {
        return $scope.isErrored() && !angular.isUndefined($scope.errorDeadline.date) && moment().isAfter($scope.errorDeadline.moment);
    };

    $scope.isExpiredUpdate = function() {
        return false; //$scope.updateDeadline && moment().format('D') > $scope.updateDeadline;
    };

    $scope.openCalendar = function(param) {
        $scope.calendar[param] = true;
    };

    var saveCompany = function(model) {
        var data = EmployerService.preparateData(model);
        var url = _endpoint + '/add';

        if (data.id) {
            url = _endpoint + '/' + data.id + '/edit';

            if ($scope.updateSubscription && data.version == EnumService.getVersion('standard')) {
                data.version = EnumService.getVersion('update');
            }
        } else {
            data.subscriptionStatus = EnumService.getSubscriptionStatus('pending');
        }

        return GenericService.httpPost(url, data);
    };

    $scope.save = function(form, model) {
        if (form.$valid) {
            if (form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_company'),
                    type: 'warning'
                });
                return false;
            }

            ViewHelper.blockContainer('.container', $translate.instant('data_saving'));
            $timeout(function() {
                ViewHelper.scrollTo('.container .blockMsg');
            }, 500);

            saveCompany(model)
                .then(callBackSave)
                .catch(callBackErrorSubmit(form));
        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.submit = function(form, model) {
        if (form.$valid) {
            if ($scope.updateSubscription && form.$pristine) {
                swal({
                    title: $translate.instant('no_data_changed'),
                    text: $translate.instant('no_data_changed_company'),
                    type: 'warning'
                });
                return false;
            }

            form.$submitted = true;

            ViewHelper.blockContainer('.container', $translate.instant('data_saving'));
            $timeout(function() {
                ViewHelper.scrollTo('.container .blockMsg');
            }, 500);

            // If we are submitting for the first time or the subscription was errored
            if (model.isPending() || (model.isErrored() && !$scope.isErrorExpired())) {
                var oldVersion = model.version;
                model.version = EnumService.getVersion('subscription');
                
                // Submit the subscription
                $scope.oldSubscriptionStatus = model.subscriptionStatus;
                model.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');
            }

            saveCompany(model)
                .then(callBackSave)
                .catch(function(response){
                    if (angular.isDefined($scope.oldSubscriptionStatus)) {
                        model.subscriptionStatus = $scope.oldSubscriptionStatus;
                    }

                    if (angular.isDefined(oldVersion)) {
                        model.version = oldVersion;
                    }

                    callBackErrorSubmit(form)(response);
                });

        } else {
            ViewHelper.errorForm(form);
        }
    };

    $scope.setNewFile = function(file, model) {
        if (!model.files) {
            model.files = [];
        }

        model.files.push(file.id);
    };

    init();
});
