app.controller(
    "CompanySuspensionController", 
    function(
        $rootScope,
        $scope, 
        $timeout, 
        $state, 
        $stateParams, 
        $uibModal, 
        $translate, 
        ViewHelper, 
        GenericService,
        AuthService,
        EnumService,
        EmployerService
    ) {
        var _endpoint = "employer";

        $scope.id = $stateParams.id || null;
        $scope.employer = null;
        $scope.suspensionAction = EnumService.getSubscriptionStatus('suspended');
        $scope.user = AuthService.getUserLogged();
        $scope.today = moment().toDate();
        $scope.calendarOptions = {
            maxDate: $scope.today
        };

        $scope.suspendedStatus = EnumService.getSubscriptionStatus('suspended');
        $scope.ceasedStatus = EnumService.getSubscriptionStatus('ceased');
        $scope.action = {status: $scope.suspendedStatus};

        $scope.actions = [
            {value: $scope.suspendedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.suspendedStatus)},
            {value: $scope.ceasedStatus, label: EnumService.geSubscriptionStatusesDescription($scope.ceasedStatus)}
        ];

        var init = function() {

            // Make sure the user can only see its own employer
            if ($scope.user.isEmployer()) {
                $scope.id = $scope.user.employer.internalCode;
            }

            if ($scope.id) {
                
                ViewHelper.blockContainer('.container', $translate.instant('loading'));
                var version = EnumService.getVersion('standard');
                GenericService.getVersion(_endpoint, $scope.id, version).then(function (response) {
                    $scope.employer = EmployerService.populateModel(response.data);

                    // the entity cannot be pending
                    // it needs to be submitted first
                    if ($scope.employer.isPending()) {
                        swal({ 
                            title: $translate.instant('company_not_submitted'),
                            text: $translate.instant('company_not_submitted_text'),
                            type: "error" 
                          },
                          function(){
                            $timeout(function(){
                                $state.go('app.company-form', {
                                    id: $scope.id,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });
                            });
                        });
                    }

                    // if the company was not approved yet, does not allow 
                    // to submit changes
                    if ($scope.employer.isSubmitted()) {
                        swal({ 
                            title: $translate.instant('company_not_approved'),
                            text: $translate.instant('company_not_approved_text'),
                            type: "error" 
                          },
                          function(){
                            $timeout(function(){
                                /*$state.go('app.company-form', {
                                    id: $scope.id,
                                    ignoreLocalStorage: true,
                                    updateSubscription: '0'
                                });*/
                            });
                        });
                    }
                    
                    $scope.employer.dateEndActivity = moment().toDate();
                    ViewHelper.unblockContainer('.container');
                });
            }
        };

        $scope.isSuspended = function() {
            return $scope.employer && $scope.employer.isSuspended();
        };

        $scope.isCeased = function() {
            return $scope.employer && $scope.employer.isCeased();
        };

        $scope.isApproved = function() {
            return $scope.employer && $scope.employer.isApproved();  
        };

        $scope.canSuspend = function() {
            return $scope.user.isEmployer() && $scope.isApproved();// !$scope.isSuspended() && !$scope.isCeased();
        };

        $scope.canActive = function() {
            return $scope.user.isEmployer() && $scope.employer && $scope.employer.isSuspended();
        };

        $scope.setCompany = function(employer) {
            $timeout(function() {
                $state.go(
                    'app.company-suspension', 
                    {
                        id: employer.internalCode,
                        ignoreLocalStorage: true
                    }
                );
            }, 500);
        };

        saveEmployer = function() {
            ViewHelper.blockContainer('.container', $translate.instant('loading'));

            var employerData = EmployerService.preparateData($scope.employer);
            var url = _endpoint + '/' + employerData.id + '/edit';

            GenericService.httpPost(url, employerData).then(function(response){
                $scope.employer = EmployerService.populateModel(response.data);
                if ($scope.user.employer) {
                    $scope.user.employer = $scope.employer;
                }

                swal({
                    title: $translate.instant('success_title'),
                    text: $translate.instant('success_text'),
                    type: 'success',
                }, function(){
                    AuthService.setUser($scope.user).then(
                        function(){
                            $rootScope.$broadcast('userChanged');
                        }
                    );
                });

                ViewHelper.scrollTo('.inss-logo');
            }).catch(function(response){
                $scope.employer.subscriptionStatus = $scope.oldSubscriptionStatus;
                ViewHelper.errorServer(form, response);
            }).finally(function(){
                ViewHelper.unblockContainer('.container');
            });
        };

        $scope.suspend = function(form) {
            if (form.$valid) {

                if ($scope.employer.isSuspended()) {
                    $scope.action.status = $scope.ceasedStatus;
                }

                $scope.oldSubscriptionStatus = $scope.employer.subscriptionStatus;
                $scope.employer.subscriptionStatus = $scope.action.status;

                saveEmployer();
                
            } else {
                ViewHelper.errorForm(form);
            }
        };

        $scope.activate = function() {
            $scope.oldSubscriptionStatus = $scope.employer.subscriptionStatus;
            $scope.employer.subscriptionStatus = EnumService.getSubscriptionStatus('submitted');

            saveEmployer();
        };

        init();
    }
);