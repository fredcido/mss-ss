<?php

namespace AppBundle\Service;

use AppBundle\Entity\Employer;
use AppBundle\Entity\Person;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class DuplicateEntity
{
    use ContainerAwareTrait;

    public function duplicate($entity)
    {
        $duplicateEntity = clone $entity;

        return $duplicateEntity;
    }

    public function duplicateEmployer(Employer $entity)
    {
        $employer = clone $entity;

        $staffs        = $employer->getStaff();
        $subsidiaries  = $employer->getSubsidiaries();
        $addresses     = $employer->getAddresses();

        if ($entity->getContactDetail()) {
            $employer->setContactDetail(clone $entity->getContactDetail());
        }

        $employer->cleanStaff();
        $employer->cleanSubsidiaries();
        $employer->cleanAddresses();

        foreach ($staffs as $staff) {
            $employer->addStaff(clone $staff);
        }

        foreach ($subsidiaries as $subsidiary) {
            $employer->addSubsidiary(clone $subsidiary);
        }

        foreach ($addresses as $address) {
            $employer->addAddress(clone $address);
        }

        return $employer;
    }

    public function duplicatePerson(Person $entity, $accessions = array())
    {
        $person          = clone $entity;

        if ($entity->getNaturality()) {
            $person->setNaturality(clone $entity->getNaturality());
        }

        if ($entity->getAddress()) {
            $person->setAddress(clone $entity->getAddress());
        }

        if ($entity->getPublicWorker()) {
            $person->setPublicWorker(clone $entity->getPublicWorker());
        }

        if ($entity->getAccessionAbroad()) {
            $person->setAccessionAbroad(clone $entity->getAccessionAbroad());
        }

        if ($entity->getContactDetail()) {
            $person->setContactDetail(clone $entity->getContactDetail());
        }

        $bankInformation = $person->getBankInformation();
        $person->cleanBankInformation();
        foreach ($bankInformation as $bank) {
            $person->addBankInformation(clone $bank);
        }

        $relatives = $person->getRelatives();
        $person->cleanRelatives();
        foreach ($relatives as $relative) {
            $person->addRelative(clone $relative);
        }

        return $person;
    }
}
