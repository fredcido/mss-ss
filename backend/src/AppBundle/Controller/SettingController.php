<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Setting;
use AppBundle\Enum\Status;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class SettingController extends BaseController
{
    protected $repository = 'AppBundle:Setting';

    protected $form = 'AppBundle\Form\SettingType';

    protected $routeGet = 'app.setting.get';

    protected $entity = Setting::class;

    public function historyAction($name, Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->repository)->findByName($name);

        if (empty($entity)) {
            throw $this->createNotFoundException('Unable to find setting');
        }

        $setting = $entity[0];
        return $this->createApiResponse($setting->getHistory(), Response::HTTP_OK, array('details', 'audit'));
    }
}
