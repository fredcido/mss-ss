app.component('activitySectorData', {
    templateUrl: 'views/components/activitySectorData.html',
    bindings: {
        model: '=',
        name: '@',
        required: '<'
    },
    controller: function($scope, GenericService, ViewHelper) {
        var self = this;
        self.sectors = [];
        self.query = '';
        self.inputSector = null;

        self.$onInit = function() {
            GenericService.httpGet('activity-sector/tree?all=1').then(function(response) {
                self.sectors = response.data;
            });

            if (!self.model) {
                self.model = [];
            }
        };

        self.isChecked = function(node) {
            return self.model.some(function(e) {
                if (node.id == e.id) {
                    return e;
                }
            });
        };

        self.toggle = function(scope) {
            $scope.toggle();
        };

        self.toggleActivitySector = function(sector) {
            if (self.isChecked(sector)) {
                self.removeActivitySector(sector);
            } else {
                self.model.push(sector);
            }
        };

        self.removeActivitySector = function(sector) {
            self.model.some(function(p) {
                if (sector.id == p.id) {
                    self.model.splice(self.model.indexOf(p), 1);
                }
            });
        };
    }
});
