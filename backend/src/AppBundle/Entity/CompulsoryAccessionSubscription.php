<?php

namespace AppBundle\Entity;

use AppBundle\Enum\Status;
use AppBundle\Validator\Constraints as InssAssert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * CompulsoryAccessionSubscription
 *
 * @ORM\Table(name="compulsory_accession_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompulsoryAccessionSubscriptionRepository")
 * @InssAssert\SubscriptionAge
 * InssAssert\SubscriptionPeriod
 */
class CompulsoryAccessionSubscription implements PersonAwareInterface
{
    use EntityAuditTrait;
    use EntityPermissionTrait;
    use SubscriptionTrait;
    use VersionTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date")
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     * @Assert\LessThanOrEqual(value="today", message="not_later_today")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $dateEnd;

    /**
     * @var CompulsoryAccession
     *
     * @ORM\ManyToOne(targetEntity="CompulsoryAccession", inversedBy="compulsorySubscriptions", cascade={"persist"})
     * @JMS\Groups({"list", "details"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $compulsoryAccession;

    /**
     * @var float
     *
     * @ORM\Column(name="incomeBase", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $incomeBase;

    /**
     * @var float
     *
     * @ORM\Column(name="monthlyHours", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $monthlyHours;

    /**
     * @var float
     *
     * @ORM\Column(name="weeklyHours", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $weeklyHours;

    /**
     * @var float
     *
     * @ORM\Column(name="weeklyDays", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\LessThanOrEqual(value=7)
     */
    private $weeklyDays;

    /**
     * @var float
     *
     * @ORM\Column(name="timePercentage", type="decimal", precision=10, scale=2, nullable=true)
     * @JMS\Groups({"list", "details", "form"})
     */
    private $timePercentage;

    /**
     * @var WorkSituation
     *
     * @ORM\ManyToOne(targetEntity="WorkSituation", cascade={"persist"})
     * @JMS\Groups({"list", "details"})
     */
    private $workSituation;

    /**
     * @ORM\ManyToMany(targetEntity="StatusHistory")
     * @JMS\Groups({"list", "details"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $statusHistory;

    /**
     * @var CompulsoryAccessionSubscription
     *
     * @ORM\ManyToOne(targetEntity="CompulsoryAccessionSubscription", inversedBy="versions")
     *
     */
    private $source;

    /**
     * @ORM\OneToMany(targetEntity="CompulsoryAccessionSubscription", mappedBy="source")
     */
    private $versions;


    public function __construct()
    {
        $this->status = Status::ACTIVE;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return CompulsoryAccessionSubscription
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return CompulsoryAccessionSubscription
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set compulsoryAccession
     *
     * @param CompulsoryAccession $compulsoryAccession
     *
     * @return CompulsoryAccessionSubscription
     */
    public function setCompulsoryAccession(CompulsoryAccession $compulsoryAccession = null)
    {
        $this->compulsoryAccession = $compulsoryAccession;

        return $this;
    }

    /**
     * Get compulsoryAccession
     *
     * @return CompulsoryAccession
     */
    public function getCompulsoryAccession()
    {
        return $this->compulsoryAccession;
    }

    /**
     * Gets the value of incomeBase.
     *
     * @return float
     */
    public function getIncomeBase()
    {
        return $this->incomeBase;
    }

    /**
     * Sets the value of incomeBase.
     *
     * @param float $incomeBase the income base
     *
     * @return self
     */
    public function setIncomeBase($incomeBase)
    {
        $this->incomeBase = $incomeBase;

        return $this;
    }

    /**
     * Gets the value of monthlyHours.
     *
     * @return float
     */
    public function getMonthlyHours()
    {
        return $this->monthlyHours;
    }

    /**
     * Sets the value of monthlyHours.
     *
     * @param float $monthlyHours the monthly hours
     *
     * @return self
     */
    public function setMonthlyHours($monthlyHours)
    {
        $this->monthlyHours = $monthlyHours;

        return $this;
    }

    /**
     * Gets the ", type="decimal", precision=10, scale=2).
     *
     * @return float
     */
    public function getTimePercentage()
    {
        return $this->timePercentage;
    }

    /**
     * Sets the ", type="decimal", precision=10, scale=2).
     *
     * @param float $timePercentage the time percentage
     *
     * @return self
     */
    public function setTimePercentage($timePercentage)
    {
        $this->timePercentage = $timePercentage;

        return $this;
    }

    /**
     * Set workSituation
     *
     * @param WorkSituation $workSituation
     *
     * @return CompulsoryAccessionSubscription
     */
    public function setWorkSituation(WorkSituation $workSituation = null)
    {
        $this->workSituation = $workSituation;
        return $this;
    }

    /**
     * Get workSituation
     *
     * @return WorkSituation
     */
    public function getWorkSituation()
    {
        return $this->workSituation;
    }

    /**
     * Gets the value of weeklyDays.
     *
     * @return float
     */
    public function getWeeklyDays()
    {
        return $this->weeklyDays;
    }

    /**
     * Sets the value of weeklyDays.
     *
     * @param float $weeklyDays the weekly days
     *
     * @return self
     */
    public function setWeeklyDays($weeklyDays)
    {
        $this->weeklyDays = $weeklyDays;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        $person = null;
        if ($this->getCompulsoryAccession()) {
            $person = $this->getCompulsoryAccession()->getPerson();
        }

        return $person;
    }

    /**
     * Gets the value of weeklyHours.
     *
     * @return float
     */
    public function getWeeklyHours()
    {
        return $this->weeklyHours;
    }

    /**
     * Sets the value of weeklyHours.
     *
     * @param float $weeklyHours the weekly hours
     *
     * @return self
     */
    public function setWeeklyHours($weeklyHours)
    {
        $this->weeklyHours = $weeklyHours;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function isStartDateBeforeEEStartDate(ExecutionContextInterface $context)
    {
        $workSituation = $this->getWorkSituation();
        $employer = $workSituation->getEmployer();

        if ($this->getDateStart() < $employer->getDateStartActivity()) {
            $context->buildViolation('subscription_date_employer')
                                ->atPath('dateStart')
                                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function isStartDateBeforePersonDod(ExecutionContextInterface $context)
    {
        $person = $this->getPerson();
        if ($person && $person->getDod() && $this->getDateStart() > $person->getDod()) {
            $context->buildViolation('subscription_date_start_before_dod')
                                ->atPath('dateStart')
                                ->addViolation();
        }
    }

    /**
     * Add statusHistory
     *
     * @param StatusHistory $statusHistory
     *
     * @return Setting
     */
    public function addStatusHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory[] = $statusHistory;
        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function isDateValid(ExecutionContextInterface $context)
    {
        if ($this->getDateStart() && $this->getDateEnd() && $this->getDateStart() > $this->getDateEnd()) {
            $context->buildViolation('error_min_date')
                                ->atPath('dateEnd')
                                ->addViolation();
        }
    }

    /**
     * Remove statusHistory
     *
     * @param StatusHistory $statusHistory
     */
    public function removeHistory(StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Get statusHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatusHistory()
    {
        return $this->statusHistory;
    }

    /**
     * @return CompulsoryAccessionSubscription
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param CompulsoryAccessionSubscription $source
     *
     * @return self
     */
    public function setSource(CompulsoryAccessionSubscription $source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Remove statusHistory
     *
     * @param \AppBundle\Entity\StatusHistory $statusHistory
     */
    public function removeStatusHistory(\AppBundle\Entity\StatusHistory $statusHistory)
    {
        $this->statusHistory->removeElement($statusHistory);
    }

    /**
     * Add version
     *
     * @param \AppBundle\Entity\CompulsoryAccessionSubscription $version
     *
     * @return CompulsoryAccessionSubscription
     */
    public function addVersion(\AppBundle\Entity\CompulsoryAccessionSubscription $version)
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param \AppBundle\Entity\CompulsoryAccessionSubscription $version
     */
    public function removeVersion(\AppBundle\Entity\CompulsoryAccessionSubscription $version)
    {
        $this->versions->removeElement($version);
    }

    /**
     * Get versions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions()
    {
        return $this->versions;
    }
}
