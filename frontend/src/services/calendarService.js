app.service("CalendarService", function($http, AppConstant, $translate, $state) {

    var _save = function(endpoint, data) {
        return $http({
            method: 'POST',
            url: AppConstant.apiUrl + endpoint + "/add",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: $.param(data),
        });
    };

    return {
        save: _save
    };
});
