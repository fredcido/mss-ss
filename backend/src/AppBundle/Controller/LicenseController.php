<?php

namespace AppBundle\Controller;

use AppBundle\Entity\License;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class LicenseController extends BaseController
{
    protected $repository = 'AppBundle:License';

    protected $form = 'AppBundle\Form\LicenseType';

    protected $routeGet = 'app.license.get';

    protected $entity = License::class;
}
