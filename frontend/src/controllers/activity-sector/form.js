app.controller("ActivitySectorFormController", function($scope, $stateParams, GenericService) {
    $scope.endpoint = 'activity-sector';
    var _stateList = 'app.activity-sector-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get($scope.endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add($scope.endpoint, data, _stateList).success(function(response){
            localStorage.removeItem($scope.endpoint);
        });
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit($scope.endpoint, id, data, _stateList);
    };
});
