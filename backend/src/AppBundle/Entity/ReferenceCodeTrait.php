<?php

namespace AppBundle\Entity;

use AppBundle\Enum\SubscriptionStatus;
use AppBundle\Enum\Version;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 */
trait ReferenceCodeTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="referenceCode", type="string", length=255, nullable=true)
     * @JMS\Groups({"list", "details", "audit"})
     */
    private $referenceCode;


    /**
     * @return string
     */
    public function getReferenceCode()
    {
        return $this->referenceCode;
    }

    /**
     * @param string $referenceCode
     *
     * @return self
     */
    public function setReferenceCode($referenceCode)
    {
        $this->referenceCode = $referenceCode;
        return $this;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"list", "details", "form"})
     */
    public function getDisplayName()
    {
        $name = $this->getName();
        if ($this->getReferenceCode()) {
            $name = sprintf('%s - %s', $this->getReferenceCode(), $name);
        }

        return $name;
    }
}
