app.controller("PostFormController", function($scope, $stateParams, GenericService) {
    var _endpoint = 'post';
    var _stateList = 'app.post-list';

    $scope.id = $stateParams.id || null;
    $scope.model = { status: 1 };

    // Populate
    if ($scope.id !== null && $scope.id !== '') {
        GenericService.get(_endpoint, $scope.id).then(function(response) {
            delete response.id;
            $scope.model = response.data;
        });
    }

    // Add
    $scope.add = function(data) {
        GenericService.add(_endpoint, data, _stateList);
    };

    // Edit
    $scope.edit = function(id, data) {
        GenericService.edit(_endpoint, id, data, _stateList);
    };
});
