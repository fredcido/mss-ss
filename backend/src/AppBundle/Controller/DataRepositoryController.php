<?php

namespace AppBundle\Controller;

use AppBundle\Enum\Document;
use AppBundle\Enum\Status;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DataRepositoryController extends BaseController
{
    protected function responseData($data)
    {
        return $this->createApiResponse($data, Response::HTTP_OK, array('form'));
    }

    public function statusAction()
    {
        $data = Status::getOptions();
        $rows = array();
        foreach ($data as $id => $label) {
            $rows[] = ['id' => $id, 'name' => $label];
        }

        return $this->responseData($rows);
    }

    public function documentTypeAction()
    {
        $data = Document::getOptions();
        $rows = array();
        foreach ($data as $id => $label) {
            $rows[] = ['id' => $id, 'name' => $label];
        }

        return $this->responseData($rows);
    }

    public function banksAction()
    {
        $em        = $this->getDoctrine()->getManager();
        $employers = $em->getRepository('AppBundle:Employer');
        $sectors   = $em->getRepository('AppBundle:ActivitySector');

        $bankSectors = $sectors->findBySimilarName('Bank');
        $entities    = $employers->findBySector($bankSectors);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('details'));
    }

    public function schoolsAction()
    {
        $em        = $this->getDoctrine()->getManager();
        $employers = $em->getRepository('AppBundle:Employer');
        $sectors   = $em->getRepository('AppBundle:ActivitySector');

        $educationSectors = $sectors->findBySimilarName('EDUKASAUN');
        $entities    = $employers->findBySector($educationSectors);

        return $this->createApiResponse($entities, Response::HTTP_OK, array('details'));
    }

    public function rolesAction()
    {
        $originalRoles = $this->getParameter('security.role_hierarchy.roles');

        $roles = array();
        array_walk_recursive($originalRoles, function ($val) use (&$roles) {
            $roles[$val] = ['id' => $val, 'name' => $val];
        });

        unset($roles['ROLE_USER']);
        $roles = array_values($roles);
        sort($roles);

        return $this->responseData($roles);
    }
}
