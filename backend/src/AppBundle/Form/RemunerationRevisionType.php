<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RemunerationRevisionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('assessment')
            ->add('assessmentDate', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('notificationDate', DateType::class, array(
                'widget' => 'single_text',
                'html5'  => true,
            ))
            ->add('observation')
            ->add('remunerationStatement');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\RemunerationRevision',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_remunerationrevision';
    }

}
