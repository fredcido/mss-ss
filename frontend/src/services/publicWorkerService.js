app.service("PublicWorkerService", function($http, AppConstant, EnumService, AddressService) {

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.sinceWhen) {
            model.sinceWhen = moment(model.sinceWhen, "YYYY/MM/DD").toDate();
        }

        return model;
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.sinceWhen) {
            data.sinceWhen = moment(data.sinceWhen).format('YYYY-MM-DD');
        }

        if (data.person) {
            data.person = data.person.id;
        }

        return data;
    };

    var _initModel = function() {
        var model = {
            hasSubsidie: '0'
        };

        return model;
    };

    return {
        populateModel: _populateModel,
        preparateData: _preparateData,
        initModel: _initModel
    };
});
