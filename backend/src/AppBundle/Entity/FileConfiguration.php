<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FileConfiguration
 *
 * @ORM\Table(name="file_configuration")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FileConfigurationRepository")
 * @UniqueEntity(fields="name", message="name_already_use")
 */
class FileConfiguration
{
    use EntityAuditTrait;
    use EntityPermissionTrait;

    const EMPLOYER = 'E';
    const PERSON = 'P';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="required", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $required = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="multiple", type="smallint")
     * @JMS\Groups({"list", "details", "form"})
     */
    private $multiple = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     * @JMS\Groups({"list", "details", "form"})
     * @Assert\NotBlank(message="not_blank")
     */
    private $reference;

    public function __construct()
    {
        $this->required = 0;
        $this->multiple = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FileConfiguration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set required
     *
     * @param integer $required
     *
     * @return FileConfiguration
     */
    public function setRequired($required)
    {
        $this->required = (int)$required;

        return $this;
    }

    /**
     * Get required
     *
     * @return int
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set multiple
     *
     * @param integer $multiple
     *
     * @return FileConfiguration
     */
    public function setMultiple($multiple)
    {
        $this->multiple = (int)$multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return int
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return FileConfiguration
     */
    public function setReference($reference)
    {
        $valid = array(self::EMPLOYER, self::PERSON);
        if (!in_array($reference, $valid)) {
            throw new \InvalidArgumentException(sprintf("'%s' is not a valid reference in %s", $reference, __CLASS__));
        }

        $this->reference = $reference;
        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }
}
