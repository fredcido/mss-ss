app.directive('subscriptionStatus', function($translate, EnumService) {
    return {
        restrict: 'E',
        scope: {
            model: "="
        },
        template: "<b class='text-capitalize label {{color}}'>{{description}}</b>",
        link: function(scope, element, attrs) {
            scope.$watch(function(e) {
                if (e.model) {
                    setStatus(e.model);
                }
            });

            var setStatus = function(model) {
                switch (model.subscriptionStatus) {
                    case EnumService.getSubscriptionStatus('pending'):
                        scope.color = 'label-warning';
                        scope.description = $translate.instant('pending');
                        break;
                    case EnumService.getSubscriptionStatus('submitted'):
                        scope.color = 'label-info';
                        scope.description = $translate.instant('submitted');
                        break;
                    case EnumService.getSubscriptionStatus('approved'):
                        scope.color = 'label-success';
                        scope.description = $translate.instant('approved');
                        break;
                    case EnumService.getSubscriptionStatus('error'):
                        scope.color = 'label-danger';
                        scope.description = $translate.instant('error');
                        break;
                    case EnumService.getSubscriptionStatus('suspended'):
                        scope.color = 'label-warning';
                        scope.description = $translate.instant('suspended');
                        break;
                    case EnumService.getSubscriptionStatus('ceased'):
                        scope.color = 'label-danger';
                        scope.description = $translate.instant('ceased');
                        break;
                }
            };
        }
    };
});
