<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DBLS\Bundle\JerseyColoursBundle\Adapters\FileAdapter;

class ActivateMaintenanceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mss:maintenance:activate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $now = new \DateTime('now');
        $when = new \DateTime('2017-08-20');

        if ($now < $when ) {
            $output->writeln('Not time to activate');            
        } else {
            $maintenance = $container->get('app.maintenance');
            $maintenance->activate();

            $output->writeln('Maintenance is activate');
        }
    }
}
