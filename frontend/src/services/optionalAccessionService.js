app.service("OptionalAccessionService", function($http, AppConstant, EnumService, WorkSituationService, SubscriptionService) {

    var _populateModel = function(model) {
        var model = angular.copy(model);

        if (model.dateStart) {
            model.dateStart = moment(model.dateStart, "YYYY/MM/DD").toDate();
        }

        if (model.dateEnd) {
            model.dateEnd = moment(model.dateEnd, "YYYY/MM/DD").toDate();
        }

        if (model.optionalAccessionSubscriptions) {
            model.optionalAccessionSubscriptions.map(function(item){
                return _populateSubscriptionModel(item);
            });
        }

        SubscriptionService.attachMethods(model);

        return model;
    };

    var _populateSubscriptionModel = function(model) {
        var model = angular.copy(model);

        if (model.dateStart) {
            model.dateStart = moment(model.dateStart, "YYYY/MM/DD").toDate();
        }

        if (model.dateEnd) {
            model.dateEnd = moment(model.dateEnd, "YYYY/MM/DD").toDate();
        }

        if (model.optionalStep) {
            model.optionalStepName = _formatOptionalStep(model.optionalStep);
        }

        return model;
    };

    var _preparateData = function(model) {
        var data = angular.copy(model);

        if (data.dateStart) {
            data.dateStart = moment(data.dateStart).format('YYYY-MM-DD');
        }

        if (data.dateEnd) {
            data.dateEnd = moment(data.dateEnd).format('YYYY-MM-DD');
        }

        var convertObjects = [
            'person', 
            'optionalContributionGroup'
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        data.workSituation.person = model.person;
        data.workSituation = WorkSituationService.preparateData(data.workSituation);

        if (data.optionalAccessionSubscriptions) {
            data.optionalAccessionSubscriptions.map(function(item){
                item = _preparateSubscriptionData(item);

                if (!item.dateStart) {
                    item.dateStart = data.dateStart;
                }

                if (!item.dateEnd) {
                    item.dateEnd = data.dateEnd;
                }

                return item;
            });
        }

        return data;
    };

    var _preparateSubscriptionData = function(model) {
        var data = angular.copy(model);

        if (data.dateStart) {
            data.dateStart = moment(data.dateStart).format('YYYY-MM-DD');
        } else if(data.optionalAccession && data.optionalAccession.dateStart) {
            data.dateStart = moment(data.optionalAccession.dateStart).format('YYYY-MM-DD');
        }

        if (data.dateEnd) {
            data.dateEnd = moment(data.dateEnd).format('YYYY-MM-DD');
        } else if(data.optionalAccession && data.optionalAccession.dateEnd) {
            data.dateEnd = moment(data.optionalAccession.dateEnd).format('YYYY-MM-DD');
        }

        var convertObjects = [
            'optionalAccession', 
            'optionalStep',  
            'post'
        ];

        convertObjects.map(function(item){
            if (data[item] && angular.isObject(data[item])) {
                data[item] = data[item].id;
            }
        });

        return data;
    };

    var _initModel = function() {
        var model = {
            workSituation: WorkSituationService.initModel(),
            subscription: 1,
            subscriptionStatus: EnumService.getSubscriptionStatus('pending'),
            version: EnumService.getVersion('standard')
        };

        SubscriptionService.attachMethods(model);

        return model;
    };

    var _initSubscriptionModel = function() {
        var model = {
            version: EnumService.getVersion('standard')
        };

        return model;
    };

    var _formatOptionalStep = function(item) {
        return item.id + ' - ' + item.indexer + ' x SAII = $' + item.cost;
    };

    return {
        initModel: _initModel,
        initSubscriptionModel: _initSubscriptionModel,
        populateModel: _populateModel,
        populateSubscriptionModel: _populateSubscriptionModel,
        preparateData: _preparateData,
        preparateSubscriptionData: _preparateSubscriptionData,
        formatOptionalStep: _formatOptionalStep
    };
});
