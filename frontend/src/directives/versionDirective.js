app.directive('version', function($translate, EnumService) {
    return {
        restrict: 'E',
        scope: {
            entity: "="
        },
        template: "<b class='text-capitalize label {{color}}'>{{description}}</b>",
        link: function(scope, element, attrs) {
            var checkVersion = new RegExp('_' + EnumService.getVersion('subscription') + '$');

            scope.$watch(function(e) {
                if (e.entity) {
                    setVersion(e.entity);
                }
            });

            var isVersionStandard = function(entity) {
                return EnumService.getVersion('standard') == entity.version;
            };

            var isSubscription = function(entity) {
                return !isVersionStandard(entity) 
                        && 
                        (EnumService.getVersion('subscription') == entity.version || !!checkVersion.exec(entity.version));
            };

            var isVersionChange = function(entity) {
                return !isVersionStandard(entity) 
                        && 
                        (EnumService.getVersion('update') == entity.version || !checkVersion.exec(entity.version));
            };

            var setVersion = function(model) {
                if (!model.version) return;
                switch (true) {
                    case isSubscription(model):
                        scope.color = 'label-warning';
                        scope.description = $translate.instant('subscription');
                        break;
                    case isVersionChange(model):
                        scope.color = 'label-info';
                        scope.description = $translate.instant('subscription_change');
                        break;
                    default:
                        scope.color = 'label-primary';
                        scope.description = $translate.instant('standard');
                }
            };
        }
    };
});
